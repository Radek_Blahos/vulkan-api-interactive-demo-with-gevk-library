/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\SyncPrimitive.h>
#include <geAx\Exception.h>

using namespace ge::Vk;

/* ------ SyncPrimitive Util Class ------ */
void FencePrimitive::create(ge::Vk::Device& device) {
	CreateInfo::Fence info;
	info.pDevice = &device;
	_lockPtr = (void*) new Fence(info);
}

void FencePrimitive::destroy() {
	Fence* ptr = (Fence*)_lockPtr;
	delete ptr;
}

void EventPrimitive::create(ge::Vk::Device& device) {
	CreateInfo::Event info;
	info.pDevice = &device;
	_lockPtr = (void*) new Event(info);
}

void EventPrimitive::destroy() {
	Event* ptr = (Event*)_lockPtr;
	delete ptr;
}
