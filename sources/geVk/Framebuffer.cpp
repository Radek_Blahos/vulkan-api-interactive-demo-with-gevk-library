/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\Framebuffer.h>
#include <geVk\RenderPass.h>
#include <geVk\ImageView.h>
#include <geVk\Device.h>
#include <geAx\Exception.h>

using namespace ge::Vk;

/* ------- CreateInfoClass ------- */
CreateInfo::Framebuffer::operator VkFramebufferCreateInfo() {
	_createInfoAttachmentRefs.clear();
	// covnert intern imagesview into VkImagesView
	for (auto entry : attachmentRefs)
		_createInfoAttachmentRefs.push_back(*entry); // operator ImageView::VkImageView()

	return {
		VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
		nullptr,
		0,
		activeRenderPass->getVkRenderPass(),
		(uint32_t) _createInfoAttachmentRefs.size(),
		_createInfoAttachmentRefs.data(),
		width,
		height,
		layersCount
	};
}

/* ------- Core Class ------- */
void Framebuffer::init(CreateInfo::Framebuffer& createInfo) {
	if (_framebuffer != VK_NULL_HANDLE)
		return;

	VkFramebufferCreateInfo framebuffCI = createInfo;
	VkResult res = vkCreateFramebuffer(*createInfo.pDevice, &framebuffCI, nullptr, &_framebuffer);
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Creation("geVk Framebuffer");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	// decision to alloc futher info struct or just info
	if (createInfo.pDevice->abundantInfo())
		_description = new FurtherInfo::Framebuffer(createInfo);
	else
		_description = new Info::Framebuffer(createInfo);

}

void Framebuffer::deinit() {
	// destroy vk buffer object
	if (_framebuffer != VK_NULL_HANDLE)
		vkDestroyFramebuffer(*_description->pDevice, _framebuffer, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _framebuffer = VK_NULL_HANDLE;
}

void Framebuffer::move(Framebuffer& withdraw) {
	if (_framebuffer != VK_NULL_HANDLE || _description != nullptr)
		deinit();

	// move ptr on resources
	_framebuffer = withdraw._framebuffer;
	_description = withdraw._description;

	// disvalidate withdraw's ptrs, when all set like this dtor of withdraw object will not affect moved resources
	withdraw._framebuffer = VK_NULL_HANDLE;
	withdraw._description = nullptr;

}