/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\CommandProcessor.h>
#include <geVk\CommandBuffer.inl>
#include <geAx\Exception.h>
#include <unordered_map>

using namespace std;
using namespace ge::Vk;

/* Info Classes */
Info::Methods::PostSingleBuffer::operator VkSubmitInfo() {
	return {
		VK_STRUCTURE_TYPE_SUBMIT_INFO,
		nullptr,
		(uint32_t)waitOn.size(),
		(!waitOn.size()) ? nullptr : waitOn.data(),
		insideStages.data(),
		1,
		pBuffer->getVkBufferPtr(),
		(uint32_t)signalUponFinish.size(),
		(!signalUponFinish.size()) ? nullptr : signalUponFinish.data()
	};
}

/* Core */
void CommandProcessor::init(CreateInfo::CommandProcessor& createInfo) {
	if (_description != nullptr)
		delete _description;

	_description = new Info::CommandProcessor(createInfo);

}

void CommandProcessor::deinit() {
	if (_description != nullptr)
		delete _description;
}

VkQueue CommandProcessor::findQueueToSubmit(uint32_t queueFamilyIdx) {
	auto queues = _description->pDevice->getQueueHandles(queueFamilyIdx);
	uint32_t idx = (queues.lastUsedQueue+1) % queues.availableQueues.size();
	return *queues.availableQueues[idx]; // converts object to vk object
}

// All buffers all posted into single queue
bool CommandProcessor::singleQueueCommandsPost(Info::Methods::PostCommandBuffers& buffers, Fence* signalUponFinish) {
	vector<VkSubmitInfo> submitInfos;
	// create submit info for every command buffer
	for (auto execInfo : buffers.bulk) 
		submitInfos.push_back(execInfo);

	// all buffers are allocated from same pool -> have same QfamilyIdx
	VkQueue submitTo = findQueueToSubmit(buffers.bulk.front().pBuffer->getFamilyIndex());

	return vkQueueSubmit(submitTo, (uint32_t)submitInfos.size(), submitInfos.data(), *signalUponFinish) == VK_SUCCESS;
}

bool ge::Vk::CommandProcessor::multiQueueCommandsPost(Info::Methods::PostCommandBuffers& buffers) {
	vector<VkSubmitInfo> submitInfos(buffers.bulk.size());
	// create submit info for every command buffer
	for (unsigned i = 0; i < buffers.bulk.size(); i++)
		submitInfos[i] = buffers.bulk[i];

	// Here obtain handles to all available queues and distribute execution of cmd buffers among them
	uint32_t submitStatus = VK_SUCCESS; // if stays zero all submits were succesfull else -> some fault
	Info::QueueFamily queues = _description->pDevice->getQueueHandles(buffers.bulk.front().pBuffer->getFamilyIndex());

	// find proper balance between available queues and cmd buffers to be posted -> currently optimaly one cmd buff per queue
	uint32_t submitsPerQueue = submitInfos.size() / queues.availableQueues.size();
	// if submitInfos.size() > queues.availableQueues.size() -> use all queues, else use as much queues as submitInfos
	uint32_t useQueues = (submitsPerQueue > 0) ? queues.availableQueues.size() : submitInfos.size();
	submitsPerQueue = (submitsPerQueue > 0) ? submitsPerQueue : 1;

	for (unsigned i = 0; i < useQueues; i++) {
		uint32_t alreadySubmited = i * submitsPerQueue;
		// if is to submit less cmd buffer then it is submitsPerQeues
		uint32_t submitsNow = ((submitsPerQueue + alreadySubmited) > submitInfos.size()) ? submitInfos.size() - alreadySubmited : submitsPerQueue;
		submitStatus |= vkQueueSubmit(*queues.availableQueues[i], submitsNow, submitInfos.data() + alreadySubmited, VK_NULL_HANDLE);
	}

	return submitStatus == VK_SUCCESS;
}

bool CommandProcessor::multiQueueCommandsPost(Info::Methods::PostCommandBuffers& buffers, std::vector<Fence*>& pFenceVec) {
	vector<VkSubmitInfo> submitInfos(buffers.bulk.size());
	// create submit info for every command buffer
	for (unsigned i = 0; i < buffers.bulk.size(); i++)
		submitInfos[i] = buffers.bulk[i];
	
	// Here obtain handles to all available queues and distribute execution of cmd buffers among them
	uint32_t submitStatus = VK_SUCCESS; // if stays zero all submits were succesfull else -> some fault
	Info::QueueFamily queues = _description->pDevice->getQueueHandles(buffers.bulk.front().pBuffer->getFamilyIndex());
	if (queues.availableQueues.size() > pFenceVec.size())
		throw ge::Ax::Exception::Bad::Execution("CommandProcessor::multiQueueCommandsPost -> smaller FenceVector than actual Queues Count");

	// find proper balance between available queues and cmd buffers to be posted -> currently optimaly one cmd buff per queue
	uint32_t submitsPerQueue = submitInfos.size() / queues.availableQueues.size();
	// if submitInfos.size() > queues.availableQueues.size() -> use all queues, else use as much queues as submitInfos
	uint32_t useQueues = (submitsPerQueue > 0) ? queues.availableQueues.size() : submitInfos.size();
	submitsPerQueue = (submitsPerQueue > 0) ? submitsPerQueue : 1;

	for (unsigned i = 0; i < useQueues; i++) {
		uint32_t alreadySubmited = i * submitsPerQueue;
		// if is to submit less cmd buffer then it is submitsPerQeues
		uint32_t submitsNow = ((submitsPerQueue + alreadySubmited) > submitInfos.size()) ? submitInfos.size() - alreadySubmited : submitsPerQueue;
		submitStatus |= vkQueueSubmit(*queues.availableQueues[i], submitsNow, submitInfos.data() + alreadySubmited, pFenceVec[i]->getVkFence());
		// set fences ptr to cmd buffers to they can wait for...
		for (int j = 0; j < submitsNow; j++) {
			if (buffers.bulk[alreadySubmited + j].pBuffer->getLockPtr() != nullptr && !buffers.bulk[alreadySubmited + j].pBuffer->getDescription().useEvent)
				buffers.bulk[alreadySubmited + j].pBuffer->getLock().getPrimitivePtr() = pFenceVec[i];
		}
			
	}

	return submitStatus == VK_SUCCESS;
}

bool CommandProcessor::postCommands(Info::Methods::PostSingleBuffer& buffer, Fence* signalUponFinish) {
	VkSubmitInfo submitInfo {
		VK_STRUCTURE_TYPE_SUBMIT_INFO,
		nullptr,
		(uint32_t)buffer.waitOn.size(),
		buffer.waitOn.data(),
		buffer.insideStages.data(),
		1,
		buffer.pBuffer->getVkBufferPtr(),
		(uint32_t)buffer.signalUponFinish.size(),
		buffer.signalUponFinish.data()
	};
	
	VkQueue submitTo = findQueueToSubmit(buffer.pBuffer->getFamilyIndex());

	return vkQueueSubmit(submitTo, 1, &submitInfo, (signalUponFinish != nullptr) ? signalUponFinish->passFenceToSignal() : VK_NULL_HANDLE) == VK_SUCCESS;
}