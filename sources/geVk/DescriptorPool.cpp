/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\DesciptorsPool.h>
#include <geAx\Exception.h>

using namespace ge::Vk;

/* ----------- Create Info Class ----------- */
CreateInfo::DescriptorPool::operator VkDescriptorPoolCreateInfo() {
	return {
		VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
		nullptr,
		flags,
		maxSets,
		(uint32_t) allocateObjectsInfo.size(),
		allocateObjectsInfo.data()
	};
}

/* ------- Core Class ------- */
void DescriptorPool::init(CreateInfo::DescriptorPool& createInfo) {
	if (_pool != VK_NULL_HANDLE)
		return;

	VkDescriptorPoolCreateInfo poolCI = createInfo;
	VkResult res = vkCreateDescriptorPool(*createInfo.pDevice, &poolCI, nullptr, &_pool);
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Creation("geVk DescriptorPool");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	_description = new Info::DescriptorPool(createInfo);

	// creating alloc map
	for (auto& entry : createInfo.allocateObjectsInfo) {
		auto x = _allocationAbleObjectsInfo.find(entry.type);
		if (x == _allocationAbleObjectsInfo.end())
			_allocationAbleObjectsInfo.emplace(entry.type, entry.descriptorCount);
		else
			_allocationAbleObjectsInfo[entry.type] += entry.descriptorCount;
	}

}

void ge::Vk::DescriptorPool::deinit() {
	// destroy vk buffer object
	if (_pool != VK_NULL_HANDLE)
		vkDestroyDescriptorPool(*_description->pDevice, _pool, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _pool = VK_NULL_HANDLE; _allocationAbleObjectsInfo.clear();
}