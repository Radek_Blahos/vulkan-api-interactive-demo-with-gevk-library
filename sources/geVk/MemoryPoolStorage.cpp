/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\MemoryManager.h>
#include <geVk\Device.h>
#include <geAx\Exception.h>

using namespace ge::Vk;
using namespace std;

MemoryPool* MemoryManager::insertPool(CreateInfo::MemoryPool const& createInfo) {
	auto fres = _poolStorage.find(createInfo.memoryTypeIndex);
	if (fres == _poolStorage.end()) {
		// create empty memory vec<pool> inside storage 
		auto resPair = _poolStorage.emplace(createInfo.memoryTypeIndex, vector<MemoryPool*>());

		// if inserting was successfull
		if (resPair.second) {
			// insert free pool inside vec
			resPair.first->second.push_back(new MemoryPool());
			// init it here
			allocatePool(createInfo, *resPair.first->second.back()); // resPair.first->second.back() means ptr to pool object
		}

		return resPair.first->second.back(); // returns allocated pool
	}
	else {
		// insert free pool inside vec
		fres->second.push_back(new MemoryPool());

		// init it here
		allocatePool(createInfo, *fres->second.back()); // resPair.first->second.back() means pool object
		
		return fres->second.back();
	}
}

void MemoryManager::cleanUpStorage() {
	for (auto& entry : _poolStorage) {
		for (auto& pool : entry.second) {
			releasePool(*pool); // release pool memory
			delete pool;
		}
	}
	
	_poolStorage.clear();

}

MemoryPool* MemoryManager::findSuitableMemoryPool(Info::MemoryRequierements& reqs) {
	for (auto& poolVecIter : _poolStorage) {
		// poolVecIter.first -> memTypeIdx spolecny pro vsechny pooly v tomhle zaznamu
		// Test jestli memIdx zaznamu spada do pozadovanych reqs.memTypeBits
		if ((poolVecIter.first & reqs.memReqs.memoryTypeBits) != reqs.memReqs.memoryTypeBits) {
			continue; // kdyz ne tak se pokracuje na dalsi memoryTypeIdx zaznam
		}

		// pokud ano, prohleda se vector poolu jestli v nekterem s tech poolu je volne misto pro alokaci
		for (auto& poolIter : poolVecIter.second) {
			// musim projit veskerou volnou pamet bloku a podivat se jestli se tam data jeste vlezou
			for (auto freeSpaceIter : poolIter->_allocationAbleMap) {
				if ((freeSpaceIter.offset - freeSpaceIter.start) >= reqs.memReqs.size && freeSpaceIter.start%reqs.memReqs.alignment == 0) {
					return poolIter; // nasel se vhodny memory pool
				}
			}
		}
	}
	
	return nullptr;
}

void MemoryManager::truncatePools() {
	vector<unordered_map<uint32_t, vector<MemoryPool*>>::iterator> memIndexFamilyToRemove;
	for (auto iter = _poolStorage.begin(); iter != _poolStorage.end(); iter++) {
		if (iter->second.size() == 0) {
			memIndexFamilyToRemove.push_back(iter);
			continue;
		}
			
		vector<vector<MemoryPool*>::iterator> poolsToRemove;
		// neni naalokovane zadne misto poolu
		for (auto viter = iter->second.begin(); viter != iter->second.end(); viter++) {
			if ((*viter)->getAllocationCount() == 0) {
				releasePool(**viter);  // vymazu pool objekt
				poolsToRemove.push_back(viter); // oznacim ho k odstraneni ze storage
			}
		}

		// erase empty pools inside vector
		for (auto& entry : poolsToRemove)
			iter->second.erase(entry);
	}
	
	// delete empty vectors from storage
	for (auto& entry : memIndexFamilyToRemove)
		_poolStorage.erase(entry); 
	
}