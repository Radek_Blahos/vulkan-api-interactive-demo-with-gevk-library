/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\DescriptorSetLayout.h>
#include <geVk\Device.h>
#include <geAx\Exception.h>

using namespace ge::Vk;
using namespace std;

/* ----------- Create Info Class ----------- */
CreateInfo::DescriptorSetLayout::operator VkDescriptorSetLayoutCreateInfo() {
	return {
		VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
		nullptr,
		0,
		(uint32_t)layoutBindings.size(),
		layoutBindings.data()
	};
}

/* ------- Core Class ------- */
void DescriptorSetLayout::init(CreateInfo::DescriptorSetLayout& createInfo) {
	if (_layout != VK_NULL_HANDLE)
		return;

	VkDescriptorSetLayoutCreateInfo layoutCI = createInfo;
	VkResult res = vkCreateDescriptorSetLayout(*createInfo.pDevice, &layoutCI, nullptr, &_layout);
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Creation("geVk DescriptorSetLayout");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	// decision to alloc futher info struct or just info
	if (createInfo.pDevice->abundantInfo())
		_description = new FurtherInfo::DescriptorSetLayout(createInfo);
	else
		_description = new Info::DescriptorSetLayout(createInfo);
}

void DescriptorSetLayout::deinit() {
	// destroy vk object
	if (_layout != VK_NULL_HANDLE)
		vkDestroyDescriptorSetLayout(*_description->pDevice, _layout, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _layout = VK_NULL_HANDLE;

}

void DescriptorSetLayout::move(DescriptorSetLayout & withdraw) {
	if (_layout != VK_NULL_HANDLE || _description != nullptr)
		deinit();

	// move ptr on resources
	_description = withdraw._description;
	_layout = withdraw._layout;

	// disvalidate withdraw's ptrs, when all set like this dtor of withdraw object will not affect moved resources
	withdraw._layout = VK_NULL_HANDLE;
	withdraw._description = nullptr;

}
