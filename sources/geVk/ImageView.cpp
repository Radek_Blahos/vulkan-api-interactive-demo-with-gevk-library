/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\ImageView.h>
#include <geVk\Image.h>
#include <geAx\Exception.h>

using namespace ge::Vk;

/* ------- CreateInfoClass ------- */
CreateInfo::ImageView::operator VkImageViewCreateInfo() {
	return {
		VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
		nullptr,
		0,
		image->getVkImage(),
		viewType,
		format,
		componentMaping,
		subresourceRange
	};
}

/* ------- Core Class ------- */
void ImageView::init(CreateInfo::ImageView& createInfo) {
	if (_view != VK_NULL_HANDLE)
		return;

	VkImageViewCreateInfo viewCI = createInfo;
	VkResult res = vkCreateImageView(createInfo.image->getVkDevice(), &viewCI, nullptr, &_view);
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Creation("geVk ImageView");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	_description = new Info::ImageView(createInfo);

}

void ImageView::deinit() {
	// destroy vk buffer object
	if (_view != VK_NULL_HANDLE)
		vkDestroyImageView(_description->image->getVkDevice(), _view, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _view = VK_NULL_HANDLE;

}

void ImageView::move(ImageView& withdraw) {
	if (_view != VK_NULL_HANDLE || _description != nullptr)
		deinit();

	// move ptr on resources
	_view = withdraw._view;
	_description = withdraw._description;

	// disvalidate withdraw's ptrs, when all set like this dtor of withdraw object will not affect moved resources
	withdraw._view = VK_NULL_HANDLE;
	withdraw._description = nullptr;

}