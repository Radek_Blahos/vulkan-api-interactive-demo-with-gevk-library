/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\ShaderModule.h>
#include <geVk\Device.h>
#include <geAx\FileOps.inl>
#include <geAx\Exception.h>

using namespace ge::Vk;
using namespace ge::Ax;
using namespace std;

/* ----------- Create Info Class ----------- */
CreateInfo::ShaderModule::operator VkShaderModuleCreateInfo() {
	// load shader data in spir-V format
	if (File::read(path, data, true) < 0) // 0 ... Sucess
		throw Exception::Bad::Load("ShaderModuleCI::Shader file");

	return {
		VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
		nullptr,
		0,
		(uint32_t) data.size() * sizeof(uint32_t),
		data.data()
	};
}

void ShaderModule::init(CreateInfo::ShaderModule& createInfo) {
	if (_shader != VK_NULL_HANDLE)
		return;

	VkShaderModuleCreateInfo shaderCI = createInfo;
	VkResult res = vkCreateShaderModule(*createInfo.pDevice, &shaderCI, nullptr, &_shader);
	if (res != VK_SUCCESS)
		throw Exception::Bad::Creation("geVk ShaderModule");

	// uvolnim shader data, kod je ulozen ve vk driver's objektu
	createInfo.data.clear();

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	_description = new Info::ShaderModule(createInfo);

}

void ShaderModule::deinit() {
	// destroy vk buffer object
	if (_shader != VK_NULL_HANDLE)
		vkDestroyShaderModule(*_description->pDevice, _shader, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _shader = VK_NULL_HANDLE;
}

void ShaderModule::move(ShaderModule& withdraw) {
	if (_shader != VK_NULL_HANDLE || _description != nullptr)
		deinit();

	// move ptr on resources
	_shader = withdraw._shader;
	_description = withdraw._description;

	// disvalidate withdraw's ptrs, when all set like this dtor of withdraw object will not affect moved resources
	withdraw._shader = VK_NULL_HANDLE;
	withdraw._description = nullptr;

}