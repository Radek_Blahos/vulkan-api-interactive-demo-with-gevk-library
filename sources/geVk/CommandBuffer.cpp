/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\CommandBuffer.inl>
#include <geVk\Buffer.h>
#include <geVk\Device.h>
#include <geAx\Exception.h>

using namespace ge::Vk;

/* ------- CreateInfoClass ------- */
CreateInfo::CommandBuffer::operator VkCommandBufferAllocateInfo() {
	return {
		VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
		nullptr,
		*pPool,
		level,
		1
	};
}

/* ------- Core Class ------- */
void CommandBuffer::init(CreateInfo::CommandBuffer& createInfo) {
	if (_buffer != VK_NULL_HANDLE)
		return;

	VkCommandBufferAllocateInfo cmdBuffCI = createInfo;
	VkResult res = vkAllocateCommandBuffers(*createInfo.pPool->getDescription().pDevice, &cmdBuffCI, &_buffer);
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Creation("geVk CommandBuffer");

	// if secondary buffer, allocate aux structure
	if (createInfo.level == VK_COMMAND_BUFFER_LEVEL_SECONDARY) {
		if (inheritanceInfo != nullptr)
			delete inheritanceInfo;

		inheritanceInfo = new VkCommandBufferInheritanceInfo();
	}

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	_description = new Info::CommandBuffer(createInfo);

	// create lock
	if (_lock != nullptr)
		delete _lock;

	if (createInfo.useEvent) {
		_lock = new EventPrimitive(*_description->pPool->getDescription().pDevice);
	}
	else {
		if (createInfo.createFence) 
			_lock = new FencePrimitive(*_description->pPool->getDescription().pDevice);
		
		else
			_lock = new FencePrimitive();
	}

}

void CommandBuffer::deinit() {
	// destroy vk buffer object
	if (_buffer != VK_NULL_HANDLE)
		vkFreeCommandBuffers(*_description->pPool->getDescription().pDevice, *_description->pPool, 1, &_buffer);

	if (_description->level == VK_COMMAND_BUFFER_LEVEL_SECONDARY)
		delete inheritanceInfo;

	// destroys info.
	if (_description != nullptr)
		delete _description;

	if (_lock != nullptr)
		delete _lock;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _buffer = VK_NULL_HANDLE; _lock = nullptr;
}

void CommandBuffer::move(CommandBuffer& withdraw) {
	// if object has allocated some resources, they has to be cleaned
	if (_buffer != VK_NULL_HANDLE || _description != nullptr)
		deinit();

	// move ptr on resources
	_buffer = withdraw._buffer;
	_description = withdraw._description;

	// disvalidate withdraw's ptrs, when all set like this dtor of withdraw object will not affect moved resources
	withdraw._buffer = VK_NULL_HANDLE;
	withdraw._description = nullptr;
}

bool CommandBuffer::create(VkCommandBuffer vkbuffer, CreateInfo::CommandBuffer& createInfo) {
	if (_buffer != VK_NULL_HANDLE || _description != nullptr)
		deinit();

	if (createInfo.createFence)
		_description = new Info::CommandBuffer(std::move(createInfo));
	
	_buffer = vkbuffer;

	return true;
}

/* Core Methods */
bool CommandBuffer::beginRecording(VkCommandBufferUsageFlags usage) {
	VkCommandBufferBeginInfo beginInfo{
		VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
		nullptr,
		usage,
		inheritanceInfo
	};

	return vkBeginCommandBuffer(_buffer, &beginInfo) == VK_SUCCESS;
}

void CommandBuffer::adopt(VkCommandBuffer buffer, CreateInfo::CommandBuffer info) {
	if (_buffer != VK_NULL_HANDLE)
		return;

	// crate new description
	if (_description != nullptr)
		delete _description;
	
	_description = new Info::CommandBuffer(info);

	// if created not a problem... , check if fence should be created
	if (_lock == nullptr) {
		if (info.useEvent) 
			_lock = new EventPrimitive(*_description->pPool->getDescription().pDevice);
		
		else {
			if (info.createFence)
				_lock = new FencePrimitive(*_description->pPool->getDescription().pDevice);
			else
				_lock = new FencePrimitive();
		}
	}

	_buffer = buffer;

}

void CommandBuffer::disinherit() {
	if (_description != nullptr) 
		delete _description;

	if (_lock != nullptr)
		delete _lock;

	_buffer = VK_NULL_HANDLE;
	_lock = nullptr;
	_description = nullptr;
}

void CommandBuffer::waitUntilExecutionFinished() {
	if (_lock == nullptr || _lock->getPrimitivePtr() == nullptr)
		return;

	_lock->waitUntilSignaled();
}

bool ge::Vk::CommandBuffer::isSubmited() {
	if (_lock == nullptr || _lock->getPrimitivePtr() == nullptr)
		return false;

	return _lock->isUsed();
}

/* Commands */
void CommandBuffer::bindDescriptorSets(Info::Commands::BindDescriptorSets& info) {
	std::vector<VkDescriptorSet> vksets;
	for (auto& set : info.pDescriptorSets)
		vksets.push_back(*set);

	vkCmdBindDescriptorSets(_buffer, info.bindPoint, *info.pPipelineLayout, info.firstSet, vksets.size(), vksets.data(), info.dynamicOffsets.size(), (info.dynamicOffsets.size()) ? info.dynamicOffsets.data() : nullptr);
}