/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\DeviceQueues.h>
#include <geVk\Device.h>
#include <geAx\Exception.h>

using namespace ge::Vk;

void Queue::init(CreateInfo::Queue& createInfo) {
	// obtain queue
	vkGetDeviceQueue(*createInfo.pDevice, createInfo.queueFamilyIndex, createInfo.allocationIndex, &_queue);
	
	if (_description != nullptr)
		delete _description;

	_description = new Info::Queue(createInfo);

}

void ge::Vk::Queue::deinit() {
	// destroys info.
	if (_description != nullptr)
		delete _description;
}