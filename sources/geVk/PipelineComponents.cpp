/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\PipelineComponents.h>

using namespace ge::Vk;

Info::Pipe::ColorBlendState::operator VkPipelineColorBlendStateCreateInfo() {
	return {
		VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
		nullptr,
		0,
		logicOpEnable,
		logicOp,
		(uint32_t) attachments.size(),
		attachments.data(),
		blendConstants[0],
		blendConstants[1],
		blendConstants[2],
		blendConstants[3]
	};
}

Info::Pipe::ShaderStage::operator VkPipelineShaderStageCreateInfo() {
	return {
		VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
		nullptr,
		0,
		stage,
		*module,
		entryPoint.c_str(),
		pConstants
	};
}

Info::Pipe::ColorBlendAttachmentState::ColorBlendAttachmentState() {
	blendEnable = VK_FALSE;
	srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
	dstColorBlendFactor = VK_BLEND_FACTOR_ONE;
	colorBlendOp = VK_BLEND_OP_ADD;
	srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
	dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
	alphaBlendOp = VK_BLEND_OP_ADD;
	colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
}