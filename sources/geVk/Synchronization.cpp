/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\Synchronization.h>
#include <geVk\Device.h>
#include <geAx\Exception.h>
#include <stdlib.h> // sleep function

using namespace ge::Vk;

/* ------ Fence Core Class ------ */
CreateInfo::Fence::operator VkFenceCreateInfo() {
	return {
		VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
		nullptr,
		(createSignaled) ? VK_FENCE_CREATE_SIGNALED_BIT : (VkFenceCreateFlags) 0
	};
}

void Fence::init(CreateInfo::Fence& createInfo) {
	if (_fence != VK_NULL_HANDLE)
		return;

	VkFenceCreateInfo info = createInfo;
	VkResult res = vkCreateFence(*createInfo.pDevice, &info, nullptr, &_fence);
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Creation("geVk Fence");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	_description = new Info::Fence(createInfo);

}

void Fence::deinit() {
	// before do anything with fence, must wait until is not used by device anymore
	waitUntilSignaled();

	// destroy vk object
	if (_fence != VK_NULL_HANDLE)
		vkDestroyFence(*_description->pDevice, _fence, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _fence = VK_NULL_HANDLE;
}

void Fence::move(Fence& withdraw) {
	// before do anything with fence, must wait until is not used by device anymore
	waitUntilSignaled();

	if (_fence != VK_NULL_HANDLE || _description != nullptr)
		deinit();

	// move ptr on resources
	_fence = withdraw._fence;
	_description = withdraw._description;

	// disvalidate withdraw's ptrs, when all set like this dtor of withdraw object will not affect moved resources
	withdraw._fence = VK_NULL_HANDLE;
	withdraw._description = nullptr;

}

VkFence Fence::passFenceToSignal() {
	_used = true;
	return _fence;
}

void Fence::waitUntilSignaled() {
	VkResult res;
	if (_used) {
		res = vkGetFenceStatus(*_description->pDevice, _fence);
		if (res == VK_NOT_READY) {
			res = vkWaitForFences(*_description->pDevice, 1, &_fence, VK_TRUE, MAX_WAIT_TIMEOUT);

		}
		else if (res != VK_SUCCESS) // device lost or something >> throw exepction
			throw ge::Ax::Exception::Bad::Execution("Fence::waitUntilSignaled -> WaitForFencesError");

		// reset signaled fence
		reset();
	}
}

void Fence::reset() {
	if (_used) {
		// reset signaled fence
		vkResetFences(*_description->pDevice, 1, &_fence);

		// set it as unused
		_used = false;
	}
}

bool ge::Vk::Fence::isUsed() {
	// critical section
	//statusMutex.lock();

	VkResult res = VK_SUCCESS;
	if (_used) {
		res = vkGetFenceStatus(*_description->pDevice, _fence);
		if (res == VK_SUCCESS)
			reset(); // reset state
	}

	// out of critical section
	//statusMutex.unlock();

	return res == VK_NOT_READY; // not_ready -> not signaled so cmd buffs are still executed
}

bool ge::Vk::Fence::isSignaled() {
	VkResult res = VK_SUCCESS;
	
	if (_used) {
		res = vkGetFenceStatus(*_description->pDevice, _fence);
		if (res == VK_SUCCESS)
			reset(); // reset state
	}

	return res == VK_SUCCESS;
}

/* ------ Semaphore Core Class ------ */
CreateInfo::Semaphore::operator VkSemaphoreCreateInfo() {
	return {
		VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
		nullptr,
		0
	};
}

void Semaphore::init(CreateInfo::Semaphore& createInfo) {
	if (_semaphore != VK_NULL_HANDLE)
		return;

	VkSemaphoreCreateInfo info = createInfo;
	VkResult res = vkCreateSemaphore(*createInfo.pDevice, &info, nullptr, &_semaphore);
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Creation("geVk Semaphore");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	_description = new Info::Semaphore(createInfo);

}

void Semaphore::deinit() {
	// destroy vk object
	if (_semaphore != VK_NULL_HANDLE)
		vkDestroySemaphore(*_description->pDevice, _semaphore, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _semaphore = VK_NULL_HANDLE;
}

void Semaphore::move(Semaphore& withdraw) {
	if (_semaphore != VK_NULL_HANDLE || _description != nullptr)
		deinit();

	// move ptr on resources
	_semaphore = withdraw._semaphore;
	_description = withdraw._description;

	// disvalidate withdraw's ptrs, when all set like this dtor of withdraw object will not affect moved resources
	withdraw._semaphore = VK_NULL_HANDLE;
	withdraw._description = nullptr;

}

/* ------ Event Info Class ------ */
ge::Vk::CreateInfo::Event::operator VkEventCreateInfo() {
	return {
		VK_STRUCTURE_TYPE_EVENT_CREATE_INFO,
		nullptr,
		0
	};
}

/* ------ Event Core Class ------ */
void ge::Vk::Event::init(CreateInfo::Event& createInfo) {
	if (_event != VK_NULL_HANDLE)
		return;

	VkEventCreateInfo info = createInfo;
	VkResult res = vkCreateEvent(*createInfo.pDevice, &info, nullptr, &_event);
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Creation("geVk Event");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	_description = new Info::Event(createInfo);

	// always for insurance reset event
	res = vkResetEvent(*createInfo.pDevice, _event);
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Creation("geVk Event");
}

void ge::Vk::Event::deinit() {
	// destroy vk object
	if (_event != VK_NULL_HANDLE)
		vkDestroyEvent(*_description->pDevice, _event, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _event = VK_NULL_HANDLE;
}

void ge::Vk::Event::move(Event& withdraw) {
	if (_event != VK_NULL_HANDLE || _description != nullptr)
		deinit();

	// move ptr on resources
	_event = withdraw._event;
	_description = withdraw._description;

	// disvalidate withdraw's ptrs, when all set like this dtor of withdraw object will not affect moved resources
	withdraw._event = VK_NULL_HANDLE;
	withdraw._description = nullptr;
}

bool ge::Vk::Event::isReset() {
	return vkGetEventStatus(*_description->pDevice, _event) == VK_EVENT_RESET;
}

bool ge::Vk::Event::isSet() {
	return vkGetEventStatus(*_description->pDevice, _event) == VK_EVENT_SET;
}

void ge::Vk::Event::waitUntilSignaled() {
	VkResult res;
	res = vkGetEventStatus(*_description->pDevice, _event);
	if (res == VK_EVENT_SET) {
		// active waiting :(
		while (vkGetEventStatus(*_description->pDevice, _event) != VK_EVENT_RESET)
			ge::Ax::sleep(50); // space for experimenting to find suitable value...
		
	}
	else if (res != VK_EVENT_RESET) // device lost or something >> throw exepction
		throw ge::Ax::Exception::Bad::Execution("Event::waitUntilSignaled -> WaitForEventError");

}

void ge::Vk::Event::set() {
	if (vkSetEvent(*_description->pDevice, _event) != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Execution("Event::signal -> cannot signal event");
}

void ge::Vk::Event::reset() {
	if (vkResetEvent(*_description->pDevice, _event) != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Execution("Event::reset -> cannot reset event");
}