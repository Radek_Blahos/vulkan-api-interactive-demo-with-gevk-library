/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\Device.h>
#include <geAx\Exception.h>

using namespace ge::Vk;
using namespace std;

/* ------- Core Class ------- */
void Device::init(CreateInfo::Device& createInfo) {
	// objekt je uz vytvoren? -> return
	if (_device != VK_NULL_HANDLE)
		return;

	/* Physical Device Selection and Compatibility check */
	selectPhysicalDevice(createInfo);
	VkDeviceCreateInfo info = createInfo;
	
	if (_physicalDevice == VK_NULL_HANDLE)
		throw ge::Ax::Exception::Bad::Select("geVk PhysicalDevice");
	
	VkResult res = vkCreateDevice(_physicalDevice, &info, nullptr, &_device);
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Creation("geVk Device");

	// initializing description object
	if (_description != nullptr)
		delete _description; // dealloc safety

	// decision to alloc futher info struct or just info
	if (createInfo.abundantInfo)
		_description = new FurtherInfo::Device(createInfo);
	else 
		_description = new Info::Device(createInfo);

	obtainQueueHandles(createInfo);
}

void Device::deinit() {
	/* delete queues class ptrs */
	for (auto& queueFamily : _qHandles) {
		for (auto& queue : queueFamily.second.availableQueues) {
			delete queue;
		}
	}		

	if (_device != VK_NULL_HANDLE)
		vkDestroyDevice(_device, nullptr);

	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _device = VK_NULL_HANDLE;
}

void Device::selectPhysicalDevice(CreateInfo::Device& createInfo) {
	// Zjistim si vycet dostupnych gpu na soucasnem hw 
	vector<VkPhysicalDevice> GPUs;
	createInfo.from->enumeratePhysicalDevices(GPUs);
	// a zkontroluju dostupne features, a ulozim vhodne GPU do vektoru
	featuresSupported(GPUs, createInfo.requieredDeviceFeatures);
	// zkontroluju dodatecne uzivatelem zadane capability nad gpu
	if (createInfo.checkGPUCapability != nullptr)
		createInfo.checkGPUCapability(GPUs);
	// na zaklade zadanych extension/layers vyberu ty gpu jez je podporuji
	extLayCompatibleGPUs(GPUs, createInfo);

	// tady najdu vhodne qFamily a upravim Info::RequestQueues vector pomoci ziskanych qFamily indexu
	std::vector<int> familyIdx;
	for (auto gpu : GPUs) {
		familyIdx.clear(); // vycistim po predchozim gpu 
		int foundCounter = 0;
		for (auto& qCInfo : createInfo.initializedQueuesInfo) {
			// find suitable family idx and assign it to crate info
			familyIdx.push_back(findSuitableQueueFamilyIndex(gpu, qCInfo.flags, qCInfo.count, createInfo.usedSurface));
			if (familyIdx.back() > -1)
				foundCounter++;
		}
		// nalezeny qfamilies pro vsechny pozadavky
		if (foundCounter == createInfo.initializedQueuesInfo.size()) {
			_physicalDevice = gpu; // nasel jsem vhodne gpu 
			break;
		}
	}
	if (_physicalDevice == VK_NULL_HANDLE) 
		throw ge::Ax::Exception::Bad::Execution("Device::selectPhysicalDevice -> Cannot find suitable GPU");
	
	// else priradim podle nalezenych family idx pro dane gpu 
	for (int i = 0; i < createInfo.initializedQueuesInfo.size(); i++) {
		createInfo.initializedQueuesInfo[i].qFamilyIdx = familyIdx[i];
	}
	// timto je create info kompletni a je i nalezeno vhodne GPU
}

void Device::extLayCompatibleGPUs(std::vector<VkPhysicalDevice>& fromGPUs, CreateInfo::Device & demands) {
	std::vector<VkPhysicalDevice> startVec(fromGPUs); // copy input vector to tmp vector
	fromGPUs.clear(); // clear res vector
	// vybrane GPU, ktere splnuji uzivatelovi pozadavky na featury co musi splnovat
	for (auto gpu : startVec) {
		if (Auxiliary::Device::checkPhysicalDeviceCapability(gpu, demands))
			fromGPUs.push_back(gpu); // gpu vyhovujici pozadavkum na extensions a validation layers se ulozi do res vectoru
	}
}

int ge::Vk::Device::findSuitableQueueFamilyIndex(VkPhysicalDevice fromGPU, VkQueueFlagBits properties, uint32_t& queuesCount, VkSurfaceKHR renderSurface) {
	unsigned queueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(fromGPU, &queueFamilyCount, nullptr);
	vector<VkQueueFamilyProperties> qFamilyProps(queueFamilyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(fromGPU, &queueFamilyCount, qFamilyProps.data());
	
	int nearestQueuesCount = 0;
	int inQueueFamily = 0;

	for (int qFamilyIdx = 0; qFamilyIdx < qFamilyProps.size(); qFamilyIdx++) {
		// presentation support inside queue
		VkBool32 presentSupport(true); // if user doesn't care it has to be true to doesn't influence logical expression below.
		if (renderSurface != VK_NULL_HANDLE) 
			vkGetPhysicalDeviceSurfaceSupportKHR(fromGPU, qFamilyIdx, renderSurface, &presentSupport);
		
		// pokud se v queue flag vyskytuje properties && ma queue presentation support && dana qfamily ma potrebny pocet volnych queues
		if (((qFamilyProps[qFamilyIdx].queueFlags & properties) == properties) && presentSupport) {
			if (qFamilyProps[qFamilyIdx].queueCount >= queuesCount) 
				return qFamilyIdx;
			
			else {
				if (nearestQueuesCount < qFamilyProps[qFamilyIdx].queueCount) {
					nearestQueuesCount = qFamilyProps[qFamilyIdx].queueCount;
					inQueueFamily = qFamilyIdx;
				}	
			}
		}		
	}

	if (nearestQueuesCount > 0) {
		cerr << "Cannot find so much queues from this family, returning at best as can be found.\n";
		queuesCount = nearestQueuesCount; // must rewrite actual demanded queues count inside create info structure...
		return inQueueFamily;
	}
	return -1;
}

void ge::Vk::Device::findSuitableQueueFamilyIndex(std::vector<VkPhysicalDevice>& fromGPUs, std::vector<int>& psDeviceQFamilies, VkQueueFlagBits properties, uint32_t queuesCount, VkSurfaceKHR renderSurface) {
	for (auto gpu : fromGPUs) 
		psDeviceQFamilies.push_back(findSuitableQueueFamilyIndex(gpu, properties, queuesCount, renderSurface));
}

void Device::obtainQueueHandles(CreateInfo::Device& createInfo) {
	for (auto QInfo : createInfo.initializedQueuesInfo) {
		// ulozim si aktualni size vektoru do ktereho budu pridavat qHandles, od ni pak dopocitam idx na ktere budu nahravat nove ziskane ptr
		uint32_t currSize = _qHandles[QInfo.qFamilyIdx].availableQueues.size();
		
		// obtain handles
		for (int i = 0; i < QInfo.count; i++) {
			// queues handles allocation
			_qHandles[QInfo.qFamilyIdx].availableQueues.push_back(new Queue());

			CreateInfo::Queue queueCI;
			queueCI.pDevice = this;
			queueCI.queueFamilyIndex = QInfo.qFamilyIdx;
			queueCI.allocationIndex = i;

			_qHandles[QInfo.qFamilyIdx].availableQueues.back()->obtain(queueCI);
		}
	}
}