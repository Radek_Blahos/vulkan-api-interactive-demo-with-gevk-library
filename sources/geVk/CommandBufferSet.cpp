/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\CommandBufferSet.h>
#include <geVk\CommandBuffer.inl>
#include <geAx\Exception.h>

using namespace ge::Vk;
using namespace ge::Ax;
using namespace std;

void CommandBufferSet::init(CreateInfo::CommandBufferSet& createInfo) {
	// create command pool
	CreateInfo::CommandPool poolCI;
	poolCI.pDevice = createInfo.pDevice;
	poolCI.queueFamilyIndex = createInfo.familyQueueIdx;
	poolCI.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT | VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
	_pool.construct(poolCI); // throws it own exception

	// create cmdbuffers in bulk
	VkCommandBufferAllocateInfo allocBuffers {
		VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
		0,
		_pool,
		VK_COMMAND_BUFFER_LEVEL_PRIMARY,
		createInfo.initialBufferCount
	};

	// allocate cmd buffers 
	vector<VkCommandBuffer> tmpVec;
	tmpVec.resize(allocBuffers.commandBufferCount);
	VkResult res = vkAllocateCommandBuffers(*createInfo.pDevice, &allocBuffers, tmpVec.data());
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Creation("geVk CommandBufferSet:AllocateBuffers");

	if (_description != nullptr)
		delete _description;

	_description = new Info::CommandBufferSet(createInfo);

	// create geVk cmdbuff from vk ones
	CreateInfo::CommandBuffer buffCI;
	buffCI.pPool = &_pool;
	buffCI.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	buffCI.createFence = _description->createFences;
	buffCI.useEvent = _description->useEvents;
	for (auto& vkcmdbuff : tmpVec) {
		_buffers.push_back(new CommandBuffer());
		_buffers.back()->adopt(vkcmdbuff, buffCI);
	}

	_bufferUsage.init(allocBuffers.commandBufferCount);

}

void CommandBufferSet::deinit() {
	// init function was not called upon object
	if (!_buffers.size())
		return;

	// before destroy attributes, must wait for queues finished work with cmdbuffers
	if (_description->createFences) {
		vector<VkFence> vkfences;
		for (auto& cmdbuffer : _buffers) {
			// only if buffer was passed into queue
			if (cmdbuffer->isSubmited()) {
				if (!cmdbuffer->getDescription().useEvent)
					vkfences.push_back(cmdbuffer->getLock().getPrimitive<Fence>().getVkFence());
			}
		}

		if (vkfences.size())
			vkWaitForFences(*_pool.getDescription().pDevice, _buffers.size(), vkfences.data(), VK_TRUE, MAX_WAIT_TIMEOUT);
	}
	
	if (_description != nullptr)
		delete _description;

	// destroy cmd buffers
	for (auto& cmdbuff : _buffers)
		delete cmdbuff;

	_buffers.clear(); _bufferUsage.reset(); _description = nullptr;

}

void CommandBufferSet::addBuffers(uint32_t count) {
	// create cmdbuffers in bulk
	VkCommandBufferAllocateInfo allocBuffers{
		VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
		0,
		_pool,
		VK_COMMAND_BUFFER_LEVEL_PRIMARY,
		count
	};

	// allocate cmd buffers 
	vector<VkCommandBuffer> tmpVec;
	tmpVec.resize(allocBuffers.commandBufferCount);
	VkResult res = vkAllocateCommandBuffers(*_pool.getDescription().pDevice, &allocBuffers, tmpVec.data());
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Creation("geVk CommandBufferSet:AllocateBuffers");

	// create geVk cmdbuff from vk ones
	CreateInfo::CommandBuffer buffCI;
	buffCI.pPool = &_pool;
	buffCI.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	buffCI.createFence = _description->createFences;
	for (auto& vkcmdbuff : tmpVec) {
		_buffers.push_back(new CommandBuffer());
		_buffers.back()->adopt(vkcmdbuff, buffCI);
	}

	_bufferUsage.resize(_buffers.size());
}

int ge::Vk::CommandBufferSet::getNextBufferIdx() {
	int freeIdx;
	while (1) {
		freeIdx = _bufferUsage.getNextFreeIndex();
		// search every available buffers and find one which is not operated with 
		if (freeIdx != -1) {
			if (!_buffers[freeIdx]->isSubmited())
				break;
			// else search continues
		}
		else {
			_buffers.front()->waitUntilExecutionFinished();
			
			freeIdx = 0;
			break;
		}
	}
	
	return freeIdx;
}

void ge::Vk::CommandBufferSet::deleteFencePointers() {
	for (auto& buffers : _buffers) {
		if (buffers->getLockPtr() != nullptr && !buffers->getDescription().useEvent)
			buffers->getLock().getPrimitivePtr() = nullptr;
	}
}