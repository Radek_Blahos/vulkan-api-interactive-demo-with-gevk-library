/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\PipelineDatabase.h>
#include <geAx\AuxInlines.inl>
#include <geAx\Exception.h>
#include <thread>

using namespace ge::Vk;
using namespace ge::Ax;
using namespace std;

/* ------- Core Class ------- */
void ge::Vk::PipelineDatabase::buildGraphicPipelines(std::vector<CreateInfo::GraphicPipeDBEntry>& graphicPipesCreateInfos) {
	if (!graphicPipesCreateInfos.size() || _description == nullptr)
		return;

	// Pipelines Creation constants
	const uint32_t maxPipesPerThread = 4;
	const uint32_t gThreadCount = ceil((double)graphicPipesCreateInfos.size() / maxPipesPerThread);

	// Caches loading
	CreateInfo::PipelineCache cacheCI;
	cacheCI.pDevice = _description->pDevice;
	cacheCI.pathToData = _description->cacheStoragePath;

	vector<PipelineCache*> caches;
	// create caches, one per thread all share same input data
	for (int i = 0; i < gThreadCount; i++) {
		caches.push_back(new PipelineCache());
		caches[i]->create(cacheCI);
	}

	// assemble graphic pipes
	vector<VkPipeline> resVec(gThreadCount);
	vector<thread> graphicWorkers;
	VkPipelineCache passTo; 
	
	// create worker threads
	for (uint32_t i = 0; i < gThreadCount; i++) {
		passTo = *caches[i];
		graphicWorkers.push_back(thread(assembleGraphicPipelinesWorker, &graphicPipesCreateInfos, _description->pDevice, i * maxPipesPerThread, (i + 1)*maxPipesPerThread, passTo, &resVec));
	}
	// join them and check results 
	for (uint32_t i = 0; i < gThreadCount; i++) {
		graphicWorkers[i].join();
		// must be 0(Vk_Success) to pass so thats why !resVec.size() -> if some error occurs during creation resVec will be cleared in worker method...
		if (resVec.size() == 0)
			throw ge::Ax::Exception::Bad::Creation("geVk PipeDB::GraphicPipelines");

		// resVec is filled with requested created pipes, so time to push them into DB
		// from i * maxPipesPerThread to resVec.size()
		int counter = i;
		for (auto& vkpipeline : resVec) {
			_graphicPipesDB[graphicPipesCreateInfos[counter].name]; // vytvorim prazdny pipeline objekt
			_graphicPipesDB[graphicPipesCreateInfos[counter].name].assemble(vkpipeline, graphicPipesCreateInfos[counter].graphicPipeCreateInfo);
			counter++;
		}
	}

	// store cache data, first merge them together into newly created empty one...
	// cacheCI.pathToData.clear(); // memory saving?
	// creating cache where all data will be merged
	caches.push_back(new PipelineCache());
	caches.back()->create(cacheCI);

	vector<VkPipelineCache> vkcaches;
	for (uint32_t i = 0; i < caches.size() - 1; i++)
		vkcaches.push_back(*caches[i]);

	VkResult res = vkMergePipelineCaches(*_description->pDevice, *caches.back(), vkcaches.size(), vkcaches.data());
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Execution("geVk PipeDB: MergePipelineCache");

	if (!caches.back()->store(_description->cacheStoragePath))
		throw ge::Ax::Exception::Bad::Execution("geVk PipeDB: StorePipelineCache");

	for (auto& cache : caches) {
		delete cache;
	}

}

void ge::Vk::PipelineDatabase::buildComputePipelines(std::vector<CreateInfo::ComputePipeDBEntry>& computePipesCreateInfos) {
	if (!computePipesCreateInfos.size() || _description == nullptr)
		return;

	// Pipelines Creation constants
	const uint32_t maxPipesPerThread = 4;
	const uint32_t cThreadCount = ceil((double)computePipesCreateInfos.size() / maxPipesPerThread);

	// Caches loading
	CreateInfo::PipelineCache cacheCI;
	cacheCI.pDevice = _description->pDevice;
	cacheCI.pathToData = _description->cacheStoragePath;

	vector<PipelineCache*> caches;
	// create caches, one per thread all share same input data
	for (int i = 0; i < cThreadCount; i++) {
		caches.push_back(new PipelineCache());
		caches[i]->create(cacheCI);
	}

	// compute ones here
	vector<VkPipeline> resVec(cThreadCount);
	vector<thread> computeWorkers;
	VkPipelineCache passTo;

	// create worker threads
	for (uint32_t i = 0; i < cThreadCount; i++) {
		passTo = *caches[i];
		computeWorkers.push_back(thread(assembleComputePipelinesWorker, &computePipesCreateInfos, _description->pDevice, i * maxPipesPerThread, (i + 1)*maxPipesPerThread, passTo, &resVec));
	}

	// join them and check results
	for (uint32_t i = 0; i < cThreadCount; i++) {
		computeWorkers[i].join();
		if (resVec.size() == 0)
			throw ge::Ax::Exception::Bad::Creation("geVk PipeDB::ComputePipelines");

		int counter = i;
		for (auto& vkpipeline : resVec) {
			_computePipesDB[computePipesCreateInfos[counter].name];
			_computePipesDB[computePipesCreateInfos[counter].name].assemble(vkpipeline, computePipesCreateInfos[counter].computePipeCreateInfo);
			counter++;
		}
	}

	// store cache data, first merge them together into newly created empty one...
	// cacheCI.pathToData.clear(); // memory saving?
	// creating cache where all data will be merged
	caches.push_back(new PipelineCache());
	caches.back()->create(cacheCI);

	vector<VkPipelineCache> vkcaches;
	for (uint32_t i = 0; i < caches.size() - 1; i++)
		vkcaches.push_back(*caches[i]);

	VkResult res = vkMergePipelineCaches(*_description->pDevice, *caches.back(), vkcaches.size(), vkcaches.data());
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Execution("geVk PipeDB: MergePipelineCache");

	if (!caches.back()->store(_description->cacheStoragePath))
		throw ge::Ax::Exception::Bad::Execution("geVk PipeDB: StorePipelineCache");

	for (auto& cache : caches) {
		delete cache;
	}

}

void PipelineDatabase::init(CreateInfo::PipelineDatabase& createInfo) {
	// Pipelines Creation constants
	const uint32_t maxPipesPerThread = 4;
	const uint32_t gThreadCount = ceil((double)createInfo.graphicPipesCreateInfos.size() / maxPipesPerThread);
	const uint32_t cThreadCount = ceil((double)createInfo.computePipesCreateInfos.size() / maxPipesPerThread);
	uint32_t reqSize = max(gThreadCount, cThreadCount);

	// Caches loading
	CreateInfo::PipelineCache cacheCI;
	cacheCI.pDevice = createInfo.pDevice;
	cacheCI.pathToData = createInfo.cacheStoragePath;

	vector<PipelineCache*> caches;
	// create caches, one per thread all share same input data
	for (int i = 0; i < reqSize; i++) {
		caches.push_back(new PipelineCache());
		caches[i]->create(cacheCI);
	}
	
	// assemble graphic pipes
	vector<VkPipeline> resVec(reqSize);
	vector<thread> graphicWorkers;
	VkPipelineCache passTo; // for some unexplained reasons visual++ thread implementation captured geVk Pipeline Cache object(caches[i]) not VkPipelineCache ptr (*caches[i]) and then destoyed it? ffs wtf? :( 
	// create worker threads
	for (uint32_t i = 0; i < gThreadCount; i++) {
		passTo = *caches[i];
		graphicWorkers.push_back(thread(assembleGraphicPipelinesWorker, &createInfo.graphicPipesCreateInfos, createInfo.pDevice, i * maxPipesPerThread, (i + 1)*maxPipesPerThread, passTo, &resVec));
	}
	// join them and check results 
	for (uint32_t i = 0; i < gThreadCount; i++) {
		graphicWorkers[i].join();
		// must be 0(Vk_Success) to pass so thats why !resVec.size() -> if some error occurs during creation resVec will be cleared in worker method...
		if (resVec.size() == 0) 
			throw ge::Ax::Exception::Bad::Creation("geVk PipeDB::GraphicPipelines");
			
		// resVec is filled with requested created pipes, so time to push them into DB
		// from i * maxPipesPerThread to resVec.size()
		int counter = i;
		for (auto& vkpipeline : resVec) {
			_graphicPipesDB[createInfo.graphicPipesCreateInfos[counter].name]; // vytvorim prazdny pipeline objekt
			_graphicPipesDB[createInfo.graphicPipesCreateInfos[counter].name].assemble(vkpipeline, createInfo.graphicPipesCreateInfos[counter].graphicPipeCreateInfo);
			counter++;
		}
	}

	// compute ones here
	vector<thread> computeWorkers;
	// create worker threads
	for (uint32_t i = 0; i < cThreadCount; i++) {
		passTo = *caches[i];
		computeWorkers.push_back(thread(assembleComputePipelinesWorker, &createInfo.computePipesCreateInfos, createInfo.pDevice, i * maxPipesPerThread, (i + 1)*maxPipesPerThread, passTo, &resVec));
	}

	// join them and check results
	for (uint32_t i = 0; i < cThreadCount; i++) {
		computeWorkers[i].join();
		if (resVec.size() == 0)
			throw ge::Ax::Exception::Bad::Creation("geVk PipeDB::ComputePipelines");

		int counter = i;
		for (auto& vkpipeline : resVec) {
			_computePipesDB[createInfo.computePipesCreateInfos[counter].name];
			_computePipesDB[createInfo.computePipesCreateInfos[counter].name].assemble(vkpipeline, createInfo.computePipesCreateInfos[counter].computePipeCreateInfo);
			counter++;
		}
	}

	// allocate description
	if (_description != nullptr)
		delete _description;

	_description = new Info::PipelineDatabase(createInfo);

	// store cache data, first merge them together into newly created empty one...
	// cacheCI.pathToData.clear(); // memory saving?
	// creating cache where all data will be merged
	caches.push_back(new PipelineCache());
	caches.back()->create(cacheCI); 

	vector<VkPipelineCache> vkcaches;
	for (uint32_t i = 0; i < caches.size()-1; i++)
		vkcaches.push_back(*caches[i]);

	VkResult res = vkMergePipelineCaches(*createInfo.pDevice, *caches.back(), vkcaches.size(), vkcaches.data());
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Execution("geVk PipeDB: MergePipelineCache");

	if (!caches.back()->store(_description->cacheStoragePath))
		throw ge::Ax::Exception::Bad::Execution("geVk PipeDB: StorePipelineCache");

	for (auto& cache : caches) {
		delete cache;
	}

}

void PipelineDatabase::deinit() {
	if (_description != nullptr)
		delete _description;

	_description = nullptr;
	
	_graphicPipesDB.clear();
	_computePipesDB.clear();
}

void PipelineDatabase::assembleGraphicPipelinesWorker(std::vector<CreateInfo::GraphicPipeDBEntry>* graphicPipesCreateInfos, Device* pDevice, uint32_t from, uint32_t to, VkPipelineCache cache, std::vector<VkPipeline>* pipesPtr) {
	// multithreaded pipeline creation
	vector<VkGraphicsPipelineCreateInfo> createInfos(graphicPipesCreateInfos->size());
	for (int i = from; i < to && i < graphicPipesCreateInfos->size(); i++) 
		createInfos[i] = graphicPipesCreateInfos->operator[](i).graphicPipeCreateInfo;
	
	pipesPtr->clear();
	pipesPtr->resize(createInfos.size());

	VkResult res = vkCreateGraphicsPipelines(*pDevice, cache, createInfos.size(), createInfos.data(), nullptr, pipesPtr->data());
	if (res != VK_SUCCESS) {
		pipesPtr->clear();
		return;
	}
}

void PipelineDatabase::assembleComputePipelinesWorker(std::vector<CreateInfo::ComputePipeDBEntry>* computePipesCreateInfos, Device* pDevice, uint32_t from, uint32_t to, VkPipelineCache cache, std::vector<VkPipeline>* pipesPtr) {
	// multithreaded pipeline creation
	vector<VkComputePipelineCreateInfo> createInfos;
	for (int i = from; i < to && i < computePipesCreateInfos->size(); i++) {
		createInfos.push_back(computePipesCreateInfos->operator[](i).computePipeCreateInfo);
	}
	pipesPtr->clear();
	pipesPtr->resize(createInfos.size());

	VkResult res = vkCreateComputePipelines(*pDevice, cache, createInfos.size(), createInfos.data(), nullptr, pipesPtr->data());
	if (res != VK_SUCCESS) {
		pipesPtr->clear();
		return;
	}
}