/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\Instance.inl>
#include <geAx\Exception.h>

// for readability purpouse
using namespace ge::Vk;

/*  ----------------  Instance Auxiliary ---------------- */
void Auxiliary::Instance::enumeratePhysicalDevices(VkInstance instance, std::vector<VkPhysicalDevice>& devices) {
	uint32_t deviceCount;
	VkResult res = vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Execution("Instance::enumeratePhysicalDevices -> Cannot obtain physical device count");

	devices.resize(deviceCount);
	res = vkEnumeratePhysicalDevices(instance, &deviceCount, devices.data());
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Execution("Instance::enumeratePhysicalDevices -> Cannot obtain physical device info");
}

void Auxiliary::Instance::enumerateAvailableExtensions(VkInstance instance, std::vector<VkExtensionProperties>& props) {
	uint32_t extensionsCount;
	VkResult res = vkEnumerateInstanceExtensionProperties(nullptr, &extensionsCount, nullptr);
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Execution("Instance::enumerateAvailableExtensions -> Cannot obtain extensions count");

	props.resize(extensionsCount);
	res = vkEnumerateInstanceExtensionProperties(nullptr, &extensionsCount, props.data());
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Execution("Instance::enumerateAvailableExtensions -> Cannot obtain extensions info");
}

void Auxiliary::Instance::enumerateAvailableLayers(VkInstance instance, std::vector<VkLayerProperties>& props) {
	uint32_t layersCount;
	VkResult res = vkEnumerateInstanceLayerProperties(&layersCount, nullptr);
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Execution("Instance::enumerateAvailableLayers -> Cannot obtain layers count");

	props.resize(layersCount);
	res = vkEnumerateInstanceLayerProperties(&layersCount, props.data());
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Execution("Instance::enumerateAvailableLayers -> Cannot obtain layers info");
}

bool Auxiliary::Instance::extensionSupported(VkInstance instance, std::string name) {
	std::vector<VkExtensionProperties> props;
	Auxiliary::Instance::enumerateAvailableExtensions(instance, props);
	for (auto entry : props) {
		if (!name.compare(entry.extensionName)) {
			return true;
		}
	}

	return false;
}

bool Auxiliary::Instance::layerSupported(VkInstance instance, std::string name) {
	std::vector<VkLayerProperties> props;
	Auxiliary::Instance::enumerateAvailableLayers(instance, props);
	for (auto entry : props) {
		if (!name.compare(entry.layerName)) {
			return true;
		}
	}

	return false;
}

void Auxiliary::Instance::extensionsSupported(VkInstance instance, std::vector<const char*>& names, std::vector<bool>& resVec) {
	for (int i = 0; i < names.size(); i++) {
		if (Auxiliary::Instance::extensionSupported(instance, names[i])) 
			resVec.push_back(true);
		else
			resVec.push_back(false);
	}
}

void Auxiliary::Instance::layersSupported(VkInstance instance, std::vector<const char*>& names, std::vector<bool>& resVec) {
	for (int i = 0; i < names.size(); i++) {
		if (Auxiliary::Instance::layerSupported(instance, names[i]))
			resVec.push_back(true);
		else
			resVec.push_back(false);
	}
}

VKAPI_ATTR VkBool32 VKAPI_CALL ge::Vk::Instance::debugCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType, uint64_t obj, size_t location, int32_t code, const char * layerPrefix, const char * msg, void * userData) {
	printf("Error inside layer %s:\n\t %s\n", layerPrefix, msg);
	return VK_FALSE;
}

VkResult ge::Vk::Instance::createDebugReportCallbackEXT(VkInstance instance, const VkDebugReportCallbackCreateInfoEXT * pCreateInfo, const VkAllocationCallbacks * pAllocator, VkDebugReportCallbackEXT * pCallback) {
	auto fun = (PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT");
	if (fun != nullptr)
		return fun(instance, pCreateInfo, pAllocator, pCallback);
	else
		return VK_ERROR_EXTENSION_NOT_PRESENT;
}

void ge::Vk::Instance::destroyDebugReportCallbackEXT(VkInstance instance, VkDebugReportCallbackEXT callback, const VkAllocationCallbacks * pAllocator) {
	auto fun = (PFN_vkDestroyDebugReportCallbackEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT");
	if (fun != nullptr)
		fun(instance, callback, pAllocator);
}

/* ----------- Create Info Class ----------- */
ge::Vk::CreateInfo::Instance::operator VkInstanceCreateInfo() {
	using namespace std;

	/*if (useValidationLayers)
		enabledExtensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME); */

	appInfo = *this; // inicializuju pomocnou vk application info strukturu

	return VkInstanceCreateInfo {
		VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
		nullptr,
		0,
		&appInfo,
		(uint32_t) enabledLayers.size(),
		enabledLayers.data(),
		(uint32_t) enabledExtensions.size(),
		enabledExtensions.data(),
	};
}

/*  ----------------  Instance ---------------- */
Instance::Instance(VkInstance instance) {
	if (_instance != VK_NULL_HANDLE || _description != nullptr)
		deinit();

	_instance = instance;

}

void Instance::init(CreateInfo::Instance& createInfo) {
	if (_instance != VK_NULL_HANDLE)
		return;

	VkInstanceCreateInfo info = createInfo;
	VkResult res = vkCreateInstance(&info, nullptr, &_instance);
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Creation("geVk Instance");

	if (_description != nullptr)
		delete _description; // dealloc safety

	// decision to alloc futher info struct or just info
	if (createInfo.abundantInfo)
		_description = new FurtherInfo::Instance(createInfo); // allokace heap struktury na heapu
	else 
		_description = new Info::Instance(createInfo); 


	if (_description->useValidationLayers) {
		VkDebugReportCallbackCreateInfoEXT debugCreate;
		debugCreate.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
		debugCreate.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;
		debugCreate.pfnCallback = debugCallback;

		VkResult res = createDebugReportCallbackEXT(_instance, &debugCreate, nullptr, &_debugCb);
		if (res != VK_SUCCESS)
			throw ge::Ax::Exception::Bad::Creation("geVk Instance::DebugCallback");
	}
}

void ge::Vk::Instance::deinit() {
	if (_description == nullptr)
		return;

	if (_description->useValidationLayers)
		destroyDebugReportCallbackEXT(_instance, _debugCb, nullptr); // destroy debug callback object 
		
	if (_instance != VK_NULL_HANDLE)
		vkDestroyInstance(_instance, nullptr);

	// if here _description != nullptr
	delete _description; // uvolnim info structuru

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _instance = VK_NULL_HANDLE;
	
}

void Instance::move(Instance& withdraw) {
	if (_instance != VK_NULL_HANDLE || _description != nullptr)
		deinit();

	// move ptr on resources
	_instance = withdraw._instance;
	_description = withdraw._description;

	// disvalidate withdraw's ptrs, when all set like this dtor of withdraw object will not affect moved resources
	withdraw._instance = VK_NULL_HANDLE;
	withdraw._description = nullptr;

}