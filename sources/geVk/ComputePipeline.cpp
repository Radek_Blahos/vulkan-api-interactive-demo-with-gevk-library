/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\ComputePipeline.h>
#include <geAx\Exception.h>
#include <utility>

using namespace ge::Vk;

/* ------- CreateInfoClass ------- */
CreateInfo::ComputePipeline::operator VkComputePipelineCreateInfo() {
	return {
		VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
		nullptr,
		flags,
		shaderStage,
		*layout,
		basePipelineHandle,
		basePipelineIndex
	};
}

/* ------- Core Class ------- */
void ComputePipeline::init(CreateInfo::ComputePipeline& createInfo) {
	if (_pipeline != VK_NULL_HANDLE)
		return;

	VkComputePipelineCreateInfo pipeCI = createInfo;
	VkResult res = vkCreateComputePipelines(*createInfo.pDevice, (VkPipelineCache)nullptr, 1, &pipeCI, nullptr, &_pipeline);
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Creation("geVk ComputePipeline");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	// decision to alloc futher info struct or just info
	if (createInfo.pDevice->abundantInfo())
		_description = new FurtherInfo::ComputePipeline(createInfo);
	else
		_description = new Info::ComputePipeline(createInfo);

}

void ComputePipeline::deinit() {
	// destroy vk object
	if (_pipeline == VK_NULL_HANDLE)
		vkDestroyPipeline(*_description->pDevice, _pipeline, nullptr);

	// destroys info.
	if (_description == nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _pipeline = VK_NULL_HANDLE;
}

bool ge::Vk::ComputePipeline::rebuild(CreateInfo::ComputePipeline* createInfo) {
	// reset 
	deinit();

	// rebuild
	if (createInfo != nullptr) 
		// $cam na osetreni vyjimky proste se vyskoci a konec...
		init(*createInfo);
	
	return true;
}

void ComputePipeline::move(ComputePipeline& withdraw) {
	// if object has allocated some resources, they has to be cleaned
	if (_pipeline != VK_NULL_HANDLE || _description != nullptr)
		deinit();

	// move ptr on resources
	_pipeline = withdraw._pipeline;
	_description = withdraw._description;

	// disvalidate withdraw's ptrs, when all set like this dtor of withdraw object will not affect moved resources
	withdraw._pipeline = VK_NULL_HANDLE;
	withdraw._description = nullptr;
	
}

bool ComputePipeline::assemble(VkPipeline vkpipe, Info::ComputePipeline& info) {
	if (_pipeline != VK_NULL_HANDLE || _description != nullptr)
		deinit();

	_description = new Info::ComputePipeline(std::move(info));
	_pipeline = vkpipe;

	return true;
}