/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\MemoryPool.h>
#include <geVk\Device.h>
#include <geVk\MemoryManager.h>
#include <geAx\Exception.h>
#include <geAx\AuxInlines.inl>
#include <geAx\Exception.h>

using namespace ge::Vk;
using namespace std;

void MemoryManager::allocatePool(CreateInfo::MemoryPool const& createInfo, MemoryPool& pool) {
	pool._memory = allocateMemory(createInfo.allocatedSpace, createInfo.memoryTypeIndex, createInfo.propertyFlags);
	if (pool._memory == VK_NULL_HANDLE)
		throw ge::Ax::Exception::Bad::Execution("MemoryManager::allocatePool -> Cannot allocate memory pool");
	
	pool._description = new Info::MemoryPool(createInfo); 

	pool._allocationAbleMap.push_back({ 0, createInfo.allocatedSpace}); // 0 ... createInfo.allocatedSpace-1 bytes indexing
}

void MemoryManager::releasePool(MemoryPool& pool) {
	if (pool._memory != VK_NULL_HANDLE)
		releaseMemory(pool._memory);

	if (_description != nullptr)
		delete pool._description;
	
	pool._allocationAbleMap.clear();

}

// public method, firstly memoryPool from allocate memory must by found
bool ge::Vk::MemoryManager::allocateMemoryBlock(Info::MemoryBlock& resBlock, ge::Vk::Info::MemoryRequierements memReqs) {
	// finds suitable pool from which allocate memory
	MemoryPool* allocPool = findSuitableMemoryPool(memReqs);

	// found no one, time to alloc it 
	if (allocPool == nullptr) {
		// Nenasel se vhodny memory pool, potreba vytvorit novy
		CreateInfo::MemoryPool createPool;
		createPool.propertyFlags = memReqs.memFlags;
		createPool.allocatedSpace = ge::Ax::max(memReqs.memReqs.size, memReqs.allocationSize);
		createPool.unitSize = memReqs.memReqs.alignment;
		createPool.memoryTypeIndex = typeBitsToIndex(memReqs.memReqs.memoryTypeBits, memReqs.memFlags);
		
		if ((int)createPool.memoryTypeIndex == -1)
			throw ge::Ax::Exception::Bad::Execution("MemoryManager::allocateMemoryBlock -> Cannot find suitable memory index");
		
		allocPool = insertPool(createPool); 
		// throws exepction which is not catches so it exits program if fail...
	}

	// returns result of memory alloc and memory block inside resBlock.
	return provideMemoryBlock(resBlock, *allocPool, memReqs.memReqs.size);
}

// private method, memoryPool from allocate memory is known here
bool MemoryManager::provideMemoryBlock(Info::MemoryBlock& resBlock, MemoryPool& pool, VkDeviceSize requieredSize) {
	// hledam prvni volny block do ktereho se vleze reqSize
	list<AllocationInfo>::iterator blocksIter = pool._allocationAbleMap.begin();

	for (; blocksIter != pool._allocationAbleMap.end(); blocksIter++) {
		// iterator se nachazi na blocku kam se vlezou pozadovana data
		if ((blocksIter->offset - blocksIter->start) >= requieredSize) 
			break;
	}

	// nenasla se zadna volna pamet
	if (blocksIter == pool._allocationAbleMap.end())
		return false;

	// priradim vysledne hodnoty do output parametru
	resBlock.allocInfo = { blocksIter->start, blocksIter->start + requieredSize }; // ulozim si odkud az kam je alokovana pamet
	resBlock.allocatedFrom = &pool;

	// blok nalezen oznacim pool jako allocated
	pool._allocationCount++;

	// a smazu block z free listu, pokud uz na nem neni zadne volne misto, nebo zmenit start volne pameti
	blocksIter->start += requieredSize; // possible +1 problem
	if ((blocksIter->offset - blocksIter->start) == 0)
		pool._allocationAbleMap.erase(blocksIter);
	
	return true; 
}

void MemoryManager::restoreMemoryBlock(Info::MemoryBlock& memory) {
	// pamet jeste nebyla prirazena
	if (memory.allocatedFrom == nullptr)
		return;

	// find the pool from which memory was allocated
	MemoryPool& pool = *memory.allocatedFrom;

	// oznacim zadany memory block v poolu jako volny
	pool._allocationAbleMap.push_back({ memory.allocInfo.start, memory.allocInfo.offset });

	// jeden volny memory block neni s cim spojovat
	if (pool._allocationAbleMap.size() > 1) {
		// snaha spojit sousedni uvolnene bloky, prvni je ale treba sornout -> poradne otestovat
		pool._allocationAbleMap.sort([](AllocationInfo& info1, AllocationInfo& info2) -> bool { return info1.start < info2.start; });

		// pokud volne bloky sousedi, spojim je do jednoho a vlozim do modified listu, ten pak priradim do _allocationAbleMap a modified list se pote dtorne
		list<AllocationInfo> modifiedList;
		for (auto blocksIter = pool._allocationAbleMap.begin(); blocksIter != pool._allocationAbleMap.end();) {
			modifiedList.push_back({ blocksIter->start, blocksIter->offset }); // ulozim puvodni zacatek/konec, kde se konec muze menit na zaklade toho jestli jsou sousedni bloky primo spojeny nebo ne. 
			// iter out of range
			if (blocksIter == pool._allocationAbleMap.end())
				break;

			for (auto searchIter = blocksIter; ;) {
				AllocationInfo ancestorCheck = *searchIter; 
				searchIter++; // posunu iterator
				if (searchIter == pool._allocationAbleMap.end()) {
					blocksIter = searchIter; // nastavim blockIter na _allocationAbleMap.end() aby se vyskocilo i s nejvrchnejsiho loopu. 
					break;
				}
				// vzdalenost o 0 -> prvky jsou sousedni
				if (searchIter->start == ancestorCheck.offset) {
					modifiedList.back().offset += searchIter->offset - searchIter->start; // zvetsi se offset o offset sousedniho bloku					
				}
				else {
					blocksIter = searchIter; // poskocim s iteratorem na prvni nesousedni memory block
					break; // bloky nesousedi konec loopu...
				}
			}

		}

		pool._allocationAbleMap = modifiedList;
		// allocation able map je serazena a pokud mozno spojita...
	}

	// musim odeznacit predany blok jako alokovany, ale prva ho musim najit, kvuli tomuhle jedinemu kroku memory block obsahuje iterator a vypada to ze ikdyz se s listem hybe, iterator zustane validni...
	pool._allocationCount--;

}