/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\CommandPool.h>
#include <geAx\Exception.h>

using namespace ge::Vk;

/* ------- CreateInfoClass ------- */
CreateInfo::CommandPool::operator VkCommandPoolCreateInfo() {
	return {
		VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
		0,
		flags,
		queueFamilyIndex
	};
}

/* ------- Core Class ------- */
void ge::Vk::CommandPool::init(CreateInfo::CommandPool& createInfo) {
	if (_pool != VK_NULL_HANDLE)
		return;

	VkCommandPoolCreateInfo poolCI = createInfo;
	VkResult res = vkCreateCommandPool(*createInfo.pDevice, &poolCI, nullptr, &_pool);
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Creation("geVk CommandPool");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	_description = new Info::CommandPool(createInfo);

}

void ge::Vk::CommandPool::deinit() {
	// destroy vk buffer object
	if (_pool != VK_NULL_HANDLE)
		vkDestroyCommandPool(*_description->pDevice, _pool, nullptr);

	// destroys info.
	if (_description == nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _pool = VK_NULL_HANDLE;
}