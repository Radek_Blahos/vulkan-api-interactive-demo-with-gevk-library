/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\Swapchain.h>

using namespace std;
using namespace ge::Vk;

/* ------- Auxiliary Class ------- */
VkSurfaceCapabilitiesKHR ge::Vk::Auxiliary::Swapchain::surfaceCapabilities(VkPhysicalDevice psDevice, VkSurfaceKHR surface) {
	VkSurfaceCapabilitiesKHR caps;
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(psDevice, surface, &caps);
	return caps;
}

bool Auxiliary::Swapchain::checkSurfaceFormatAvailability(VkPhysicalDevice psDevice, VkSurfaceKHR surface, VkSurfaceFormatKHR format) {
	uint32_t sfCount(0);
	vkGetPhysicalDeviceSurfaceFormatsKHR(psDevice, surface, &sfCount, nullptr);
	vector<VkSurfaceFormatKHR> surfaceFormats(sfCount);
	vkGetPhysicalDeviceSurfaceFormatsKHR(psDevice, surface, &sfCount, surfaceFormats.data());

	for (auto x : surfaceFormats) {
		if ((x.colorSpace == format.colorSpace) && (x.format == format.format))
			return true;
	}

	return false;
}

VkSurfaceFormatKHR Auxiliary::Swapchain::returnRandomAvailableSurfaceFormat(VkPhysicalDevice psDevice, VkSurfaceKHR surface) {
	uint32_t sfCount(0);
	vkGetPhysicalDeviceSurfaceFormatsKHR(psDevice, surface, &sfCount, nullptr);
	vector<VkSurfaceFormatKHR> surfaceFormats(sfCount);
	vkGetPhysicalDeviceSurfaceFormatsKHR(psDevice, surface, &sfCount, surfaceFormats.data());

	return surfaceFormats.front();
}

bool Auxiliary::Swapchain::checkPresentModeAvailability(VkPhysicalDevice psDevice, VkSurfaceKHR surface, VkPresentModeKHR mode) {
	uint32_t pmCount(0);
	vkGetPhysicalDeviceSurfacePresentModesKHR(psDevice, surface, &pmCount, nullptr);
	vector<VkPresentModeKHR> presentModes(pmCount);
	vkGetPhysicalDeviceSurfacePresentModesKHR(psDevice, surface, &pmCount, presentModes.data());

	for (auto x : presentModes) {
		if (x == mode)
			return true;
	}

	// else
	return false;
}

VkPresentModeKHR Auxiliary::Swapchain::returnRandomAvailablePresentMode(VkPhysicalDevice psDevice, VkSurfaceKHR surface) {
	uint32_t pmCount(0);
	vkGetPhysicalDeviceSurfacePresentModesKHR(psDevice, surface, &pmCount, nullptr);
	vector<VkPresentModeKHR> presentModes(pmCount);
	vkGetPhysicalDeviceSurfacePresentModesKHR(psDevice, surface, &pmCount, presentModes.data());

	return presentModes.front();
}

VkDisplayPropertiesKHR ge::Vk::Auxiliary::Swapchain::singleDisplayProperties(VkPhysicalDevice psDevice) {
	VkDisplayPropertiesKHR displayProps;
	uint32_t pCount(0);
	vkGetPhysicalDeviceDisplayPropertiesKHR(psDevice, &pCount, nullptr);
	if (pCount == 1) {
		vkGetPhysicalDeviceDisplayPropertiesKHR(psDevice, &pCount, &displayProps);
		return displayProps;
	}
	displayProps.display = VK_NULL_HANDLE; // sign invalid structure
	return displayProps;
}