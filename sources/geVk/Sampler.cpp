/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\Sampler.h>
#include <geVk\Device.h>
#include <geAx\Exception.h>

using namespace ge::Vk;

/* ------- CreateInfoClass ------- */
CreateInfo::Sampler::operator VkSamplerCreateInfo() {
	return {
		VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
		nullptr,
		0,
		magFilter,
		minFilter,
		mipmapMode,
		addressModeU,
		addressModeV,
		addressModeW,
		mipLodBias,
		anisotropyEnable,
		maxAnisotropy,
		compareEnable,
		compareOp,
		minLod,
		maxLod,
		borderColor,
		unnormalizedCoordinates
	};
}

/* ------- Core Class ------- */
void Sampler::init(CreateInfo::Sampler & createInfo) {
	if (_sampler != VK_NULL_HANDLE)
		return;

	VkSamplerCreateInfo samplerCI = createInfo;
	VkResult res = vkCreateSampler(*createInfo.pDevice, &samplerCI, nullptr, &_sampler);
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Creation("geVk Sampler");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	// decision to alloc futher info struct or just info
	if (createInfo.pDevice->abundantInfo())
		_description = new FurtherInfo::Sampler(createInfo);
	else
		_description = new Info::Sampler(createInfo);
}

void Sampler::deinit() {
	// destroy vk object
	if (_sampler != VK_NULL_HANDLE)
		vkDestroySampler(*_description->pDevice, _sampler, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _sampler = VK_NULL_HANDLE;
}

void Sampler::move(Sampler& withdraw) {
	// if object has allocated some resources, they has to be cleaned
	if (_sampler != VK_NULL_HANDLE || _description != nullptr)
		deinit();

	// move ptr on resources
	_sampler = withdraw._sampler;
	_description = withdraw._description;

	// disvalidate withdraw's ptrs, when all set like this dtor of withdraw object will not affect moved resources
	withdraw._sampler = VK_NULL_HANDLE;
	withdraw._description = nullptr;

}