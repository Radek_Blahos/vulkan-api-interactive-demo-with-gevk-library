/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\PipelineCache.h>
#include <geAx\FileOps.inl>
#include <geAx\Exception.h>
#include <fstream>

using namespace ge::Vk;
using namespace ge::Ax;
using namespace std;

/* ------- CreateInfoClass ------- */
CreateInfo::PipelineCache::operator VkPipelineCacheCreateInfo() {
	// load cache data
	if (pathToData.size() > 0) { // want to load data from file
		if (!File::read(pathToData, _data))
			throw Exception::Bad::Load("Cannot obtain cache data");
	}

	return {
		VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO,
		nullptr,
		0,
		_data.size(),
		_data.data()
	};
}

/* ------- Core Class ------- */
void PipelineCache::init(CreateInfo::PipelineCache& createInfo) {
	if (_cache != VK_NULL_HANDLE)
		return;

	VkPipelineCacheCreateInfo pipecacheCI = createInfo;
	VkResult res = vkCreatePipelineCache(*createInfo.pDevice, &pipecacheCI, nullptr, &_cache);
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Creation("geVk PipelineCache");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	_description = new Info::PipelineCache(createInfo);

}

void PipelineCache::deinit() {
	// destroy vk buffer object
	if (_cache != VK_NULL_HANDLE)
		vkDestroyPipelineCache(*_description->pDevice, _cache, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _cache = VK_NULL_HANDLE;
}

bool ge::Vk::PipelineCache::store(std::string path) {
	fstream file(path, fstream::out | fstream::binary);
	if (!file.is_open())
		throw ge::Ax::Exception::Bad::Execution("PipelineCache::store -> Cannot open file");

	size_t dataSize;
	vkGetPipelineCacheData(*_description->pDevice, _cache, &dataSize, nullptr);

	vector<uint8_t> data(dataSize);
	vkGetPipelineCacheData(*_description->pDevice, _cache, &dataSize, data.data());

	file.write((const char*) data.data(), dataSize);
	if (file.fail())
		throw ge::Ax::Exception::Bad::Execution("PipelineCache::store -> Can't write onto file");

	file.close();

	return true;
}