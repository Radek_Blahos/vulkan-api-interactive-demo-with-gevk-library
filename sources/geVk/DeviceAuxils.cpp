/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\Device.h>
#include <geVk\DeviceQueues.h>
#include <geAx\Exception.h>

using namespace std;
using namespace ge::Vk;

/* ------- QueuesInfoClass ------- */
Info::RequestQueues::operator VkDeviceQueueCreateInfo() {
	// rovnomerna priorita queues
	queuePriorities.resize(count);
	queuePriorities.assign(count, (float) 1 / count);

	return VkDeviceQueueCreateInfo {
		VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO, // sType
		nullptr, // pNext
		0, // flags
		qFamilyIdx, // queueFamilyIndex
		count, // queueCount
		queuePriorities.data() // pQueuePriorities
	};
}

/* ------- CreateInfoClass ------- */
CreateInfo::Device::operator VkDeviceCreateInfo() {
	// queues to init info
	for (auto& entry : initializedQueuesInfo)
		queuesInfo.push_back(entry); 

	return VkDeviceCreateInfo {
		VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
		nullptr,
		0,
		(uint32_t) queuesInfo.size(),
		queuesInfo.data(),
		(uint32_t) requieredLayerNames.size(),
		requieredLayerNames.data(),
		(uint32_t)requieredExtensionNames.size(),
		requieredExtensionNames.data(),
		&requieredDeviceFeatures
	};
}

/* ------- Auxiliary Methods ------- */
void Auxiliary::Device::enumerateAvailableExtensions(VkPhysicalDevice psDevice, std::vector<VkExtensionProperties>& props) {
	uint32_t extensionsCount;
	VkResult res = vkEnumerateDeviceExtensionProperties(psDevice, nullptr, &extensionsCount, nullptr);
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Execution("Device::enumerateAvailableExtensions -> Cannot obtain extensions count");

	props.resize(extensionsCount);
	res = vkEnumerateDeviceExtensionProperties(psDevice, nullptr, &extensionsCount, props.data());
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Execution("Device::enumerateAvailableExtensions -> Cannot obtain extensions info");

}

void Auxiliary::Device::enumerateAvailableLayers(VkPhysicalDevice psDevice, std::vector<VkLayerProperties>& props) {
	uint32_t layersCount;
	VkResult res = vkEnumerateDeviceLayerProperties(psDevice, &layersCount, nullptr);
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Execution("Device::enumerateAvailableLayers -> Cannot obtain layers count");

	props.resize(layersCount);
	res = vkEnumerateDeviceLayerProperties(psDevice, &layersCount, props.data());
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Execution("Device::enumerateAvailableLayers -> Cannot obtain layers info");
}

bool Auxiliary::Device::extensionSupported(VkPhysicalDevice psDevice, std::string name) {
	std::vector<VkExtensionProperties> props;
	Auxiliary::Device::enumerateAvailableExtensions(psDevice, props);
	for (auto entry : props) {
		if (!name.compare(entry.extensionName)) {
			return true;
		}
	}

	return false;
}

bool Auxiliary::Device::layerSupported(VkPhysicalDevice psDevice, std::string name) {
	std::vector<VkLayerProperties> props;
	Auxiliary::Device::enumerateAvailableLayers(psDevice, props);
	for (auto entry : props) {
		if (!name.compare(entry.layerName)) {
			return true;
		}
	}

	return false;
}

int Auxiliary::Device::extensionsSupported(VkPhysicalDevice psDevice, std::vector<const char*>& names) {
	for (int i = 0; i < names.size(); i++) {
		if (!Auxiliary::Device::extensionSupported(psDevice, names[i]))
			return i;
	}

	return names.size();
}

void Auxiliary::Device::extensionsSupported(VkPhysicalDevice psDevice, std::vector<const char*>& names, std::vector<bool>& resVec) {
	for (int i = 0; i < names.size(); i++) {
		if (Auxiliary::Device::extensionSupported(psDevice, names[i]))
			resVec.push_back(true);
		else
			resVec.push_back(false);
	}
}

int ge::Vk::Auxiliary::Device::layersSupported(VkPhysicalDevice psDevice, std::vector<const char*>& names) {
	for (int i = 0; i < names.size(); i++) {
		if (!Auxiliary::Device::layerSupported(psDevice, names[i]))
			return i;
	}

	return names.size();
}

void Auxiliary::Device::layersSupported(VkPhysicalDevice psDevice, std::vector<const char*>& names, std::vector<bool>& resVec) {
	for (int i = 0; i < names.size(); i++) {
		if (Auxiliary::Device::layerSupported(psDevice, names[i]))
			resVec.push_back(true);
		else
			resVec.push_back(false);
	}
}

bool Auxiliary::Device::checkPhysicalDeviceCapability(VkPhysicalDevice psDevice, CreateInfo::Device& demands) {
	// check extensions support	
	int res = Auxiliary::Device::extensionsSupported(psDevice, demands.requieredExtensionNames);
	if (res < demands.requieredExtensionNames.size())
		return false; // psdevice nema vsechny pozadovane extensions 

	if (demands.useValidationLayers) { // pokud se maji pouzit validacni vrstvy
		// check layers support
		int res = Auxiliary::Device::layersSupported(psDevice, demands.requieredLayerNames);
		if (res < demands.requieredLayerNames.size())
			return false; // psdevice nema vsechny pozadovane layers
	}

	return true;
}

bool ge::Vk::Auxiliary::Device::checkGPUFeatures(VkPhysicalDevice psDevice, VkPhysicalDeviceFeatures featuresToCheck) {
	uint32_t len = sizeof(VkPhysicalDeviceFeatures); // how many bools are in there
	// obtain psDevice features
	VkPhysicalDeviceFeatures availableFeats;
	vkGetPhysicalDeviceFeatures(psDevice, &availableFeats);
	bool* avFeatsPtr = (bool*) &availableFeats;
	bool* reqFeatsPtr = (bool*) &featuresToCheck;

	for (int i = 0; i < len; i++) {
		// requiered Feature is not available on current GPU
		if (reqFeatsPtr[i] == true && reqFeatsPtr[i] != avFeatsPtr[i]) {
			return false;
		}
	}

	return true;
}

void ge::Vk::Device::featuresSupported(std::vector<VkPhysicalDevice>& fromGPUs, VkPhysicalDeviceFeatures reqFeatures) {
	std::vector<VkPhysicalDevice> startVec(fromGPUs);
	fromGPUs.clear();
	for (auto& gpu : startVec) {
		if (Auxiliary::Device::checkGPUFeatures(gpu, reqFeatures))
			fromGPUs.push_back(gpu);
	}

}