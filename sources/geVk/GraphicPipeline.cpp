/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\GraphicPipeline.h>
#include <geVk\RenderPass.h>
#include <geAx\Exception.h>
#include <utility>

using namespace ge::Vk;

/* ------- CreateInfoClass ------- */
CreateInfo::GraphicPipeline::operator VkPipelineVertexInputStateCreateInfo() {
	inputBinding.clear(); attriburesDescription.clear();
	// create input structures
	for (auto entry : vertexInput) {
		inputBinding.push_back(entry);
		attriburesDescription.push_back(entry);
	}
	
	return {
		VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
		nullptr,
		0,
		(uint32_t) inputBinding.size(),
		inputBinding.data(),
		(uint32_t) attriburesDescription.size(),
		attriburesDescription.data()
	};
}

CreateInfo::GraphicPipeline::operator VkPipelineDynamicStateCreateInfo() {
	return {
		VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
		nullptr,
		0,
		(uint32_t) dynamicStates.size(),
		dynamicStates.data()
	};
}

CreateInfo::GraphicPipeline::operator VkGraphicsPipelineCreateInfo() {
	shaderStages.clear();
	shaderStages.resize(stagesInfo.size());
	for (unsigned i = 0; i < stagesInfo.size(); i++) 
		shaderStages[i] = stagesInfo[i];

	inputState = *this;
	assemblyState = primitivesAssembly;
	tessellationState = tessellation;
	viewportState = viewport;
	multisampleState = multisample;
	rasterizationState = rasterization;
	dynamicState = *this;
	colorBlendState = colorBlend;
	depthStencilState = depthStencil;

	return {
		VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
		nullptr,
		flags,
		(uint32_t) stagesInfo.size(),
		shaderStages.data(),
		&inputState,
		&assemblyState,
		&tessellationState,
		&viewportState,
		&rasterizationState,
		&multisampleState,
		&depthStencilState,
		&colorBlendState,
		&dynamicState,
		*layout,
		*renderPass,
		usedForSubpass,
		basePipelineHandle,
		basePipelineIndex
	};
}

/* ------- Core Class ------- */
void GraphicPipeline::init(CreateInfo::GraphicPipeline& createInfo) {
	if (_pipeline != VK_NULL_HANDLE)
		return;

	VkGraphicsPipelineCreateInfo pipeCI = createInfo;
	VkResult res = vkCreateGraphicsPipelines(*createInfo.pDevice, (VkPipelineCache)nullptr, 1, &pipeCI, nullptr, &_pipeline);
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Creation("geVk GraphicPipeline");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	// decision to alloc futher info struct or just info
	if (createInfo.pDevice->abundantInfo())
		_description = new FurtherInfo::GraphicPipeline(createInfo);
	else
		_description = new Info::GraphicPipeline(createInfo);

}

void GraphicPipeline::deinit() {
	// destroy vk object
	if (_pipeline != VK_NULL_HANDLE)
		vkDestroyPipeline(*_description->pDevice, _pipeline, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _pipeline = VK_NULL_HANDLE;
}

bool GraphicPipeline::rebuild(CreateInfo::GraphicPipeline* createInfo) {
	// reset 
	deinit();

	// rebuild
	if (createInfo != nullptr)
		init(*createInfo);

	return true;
}

bool GraphicPipeline::renderPassCompatible(RenderPass& renderPassToCheck) {
	return false;
}

void ge::Vk::GraphicPipeline::move(GraphicPipeline& withdraw) {
	// if object has allocated some resources, they has to be cleaned
	if (_pipeline != VK_NULL_HANDLE || _description != nullptr)
		deinit();

	// move ptr on resources
	_pipeline = withdraw._pipeline;
	_description = withdraw._description;

	// disvalidate withdraw's ptrs, when all set like this dtor of withdraw object will not affect moved resources
	withdraw._pipeline = VK_NULL_HANDLE;
	withdraw._description = nullptr;

}

bool GraphicPipeline::assemble(VkPipeline vkpipe, Info::GraphicPipeline& info) {
	if (_pipeline != VK_NULL_HANDLE || _description != nullptr)
		deinit();

	_description = new Info::GraphicPipeline(std::move(info));
	_pipeline = vkpipe;

	return true;
}