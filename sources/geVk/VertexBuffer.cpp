/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\VertexBuffer.h>

using namespace ge::Vk;

Utils::VertexBuffer::~VertexBuffer() {
	if (description != nullptr)
		delete description;
	
	if (data != nullptr)
		delete data;
}
