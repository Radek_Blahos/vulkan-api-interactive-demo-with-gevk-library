/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\PipelineLayout.h>
#include <geAx\Exception.h>

using namespace ge::Vk;

/* ------- CreateInfo Class ------- */
CreateInfo::PipelineLayout::operator VkPipelineLayoutCreateInfo() {
	// convert descriptorSet's info into vkdescriptorsetlayout info
	for (auto& entry : pDescriptorSetLayouts) 
		descriptorLayouts.push_back(*entry);
	
	return {
		VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
		nullptr,
		0,
		(uint32_t) descriptorLayouts.size(),
		descriptorLayouts.data(),
		(uint32_t) pushConstants.size(),
		pushConstants.data()
	};
}

/* ------- Core Class ------- */
void PipelineLayout::init(CreateInfo::PipelineLayout& createInfo) {
	if (_layout != VK_NULL_HANDLE)
		return;

	VkPipelineLayoutCreateInfo layoutCI = createInfo;
	VkResult res = vkCreatePipelineLayout(*createInfo.pDevice, &layoutCI, nullptr, &_layout);
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Creation("geVk PipelineLayout");

	// allocate info struct memory
	if (_description != nullptr) 
		delete _description; // dealloc safety

	// decision to alloc futher info struct or just info
	if (createInfo.pDevice->abundantInfo())
		_description = new FurtherInfo::PipelineLayout(createInfo);
	else
		_description = new Info::PipelineLayout(createInfo); 

}

void PipelineLayout::deinit() {
	// destroy vk object
	if (_layout != VK_NULL_HANDLE)
		vkDestroyPipelineLayout(*_description->pDevice, _layout, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _layout = VK_NULL_HANDLE;
}

void PipelineLayout::move(PipelineLayout& withdraw) {
	if (_layout != VK_NULL_HANDLE || _description != nullptr)
		deinit();

	// move ptr on resources
	_layout = withdraw._layout;
	_description = withdraw._description;

	// disvalidate withdraw's ptrs, when all set like this dtor of withdraw object will not affect moved resources
	withdraw._layout = VK_NULL_HANDLE;
	withdraw._description = nullptr;

}