/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\DescriptorManager.h>
#include <geVk\descriptorSet.inl>
#include <geAx\Exception.h>
#include <unordered_map>

using namespace ge::Vk;

void ge::Vk::DescriptorManager::init(CreateInfo::DescriptorManager& createInfo) {
	if (_description != nullptr)
		return;

	_description = new Info::DescriptorManager(createInfo);

}

void ge::Vk::DescriptorManager::deinit() {
	for (auto& pool : _pools) 
		delete pool;
	
	if (_description != nullptr)
		delete _description;

	_pools.clear();
}

bool DescriptorManager::allocateDescriptorMemory(DescriptorSet& descriptorSet) {
	// searching for pool 
	for (unsigned i = 0; i < _pools.size(); i++) {
		// from which all descriptors can be allocated
		for (auto& memReq : descriptorSet._layoutMemReqs) {
			if (!(memReq.second <= _pools[i]->_allocationAbleObjectsInfo[memReq.first])) // pool has available at least memReq.second(count) resources of type memReq.first 
				break; // not available in this poll jump to another one
		}
		
		// come into here? pool has available all resources
		descriptorSet._allocatedFrom = _pools[i];
		VkDescriptorSetAllocateInfo allocInfo {
			VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
			nullptr,
			*_pools[i],
			1,
			descriptorSet._mainLayout.getVkLayoutPtr()
		};
		VkResult res = vkAllocateDescriptorSets(*_description->pDevice, &allocInfo, &descriptorSet._memory);
		if (res == VK_SUCCESS) {
			for (auto& memReq : descriptorSet._layoutMemReqs)
				_pools[i]->_allocationAbleObjectsInfo[memReq.first] -= memReq.second; // odectu pocet alokovanych resources

			return true;
		}
		else if (res != VK_ERROR_OUT_OF_POOL_MEMORY_KHR) // nejaka jina chyba nez nedostatek pameti v poolu, nedostatek pameti muze byt bud kvuli prekroceni povoleneho max set alloc nebo fragmentaci.
			return false;
	}
	
	/* 
		Suitable pool not found, must create new one...
	*/
	CreateInfo::DescriptorPool dpCI;
	dpCI.pDevice = _description->pDevice;
	dpCI.maxSets = _maxAllocationCountperPool; // assign it to draw worker threads
	dpCI.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
	for (auto& memReq : descriptorSet._layoutMemReqs) { 
		dpCI.allocateObjectsInfo.push_back({ memReq.first, memReq.second * _maxAllocationCountperPool });
	}
	_pools.push_back(new DescriptorPool(dpCI)); 
	
	descriptorSet._allocatedFrom = _pools.back(); 
	VkDescriptorSetAllocateInfo allocInfo{
		VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
		nullptr,
		*_pools.back(),
		1,
		descriptorSet._mainLayout.getVkLayoutPtr()
	};
	VkResult res = vkAllocateDescriptorSets(*_description->pDevice, &allocInfo, &descriptorSet._memory);
	if (res == VK_SUCCESS) {
		for (auto& memReq : descriptorSet._layoutMemReqs)
			_pools.back()->_allocationAbleObjectsInfo[memReq.first] -= memReq.second; // odectu pocet alokovanych resources

		return true;
	}
	else
		return false;

	return false;
}

bool DescriptorManager::releaseDescriptorMemory(DescriptorSet& descriptorSet) {
	VkResult res = vkFreeDescriptorSets(*_description->pDevice, *descriptorSet._allocatedFrom, 1, &descriptorSet._memory);
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Execution("DescriptorManager::releaseDescriptorMemory -> Cannot free desciptor sets from pool");

	for (auto& memReq : descriptorSet._layoutMemReqs)
		descriptorSet._allocatedFrom->_allocationAbleObjectsInfo[memReq.first] += memReq.second;

	return true;
}