#include <geVk\RenderContext.h>
#include <geAx\AuxInlines.inl>

using namespace ge::Vk;
using namespace ge::Ax;
using namespace std;

/* This is how init should look like, just demonstation... user must init framebuffers with by him defined renderpass
void Utils::RenderContext::initialize(uint32_t width, uint32_t height) {
try {
	// init device
	initDevice();

	// init memory manager
	initMemoryManager();

	// init swapchain
	initSwapchain(width, height);

	// Init RenderContext Components
	initRenderComponents();

	// create swapchain's framebuffers
	swapchain.createFramebuffers(componentsDB.renderPasses["base"]);

	// build pipelines
	buildPipelineDB();

}
	// catch geVk object Creation errors
	catch (ge::Ax::Exception::Base& exception) {
	std::cerr << exception.what(); // vypise jmeno prvni vyjimky a skonci
	CATCHEXIT(-1);
	}
	// heap allocation problem
	catch (std::bad_alloc& exception) {
	std::cerr << exception.what();
	CATCHEXIT(-1);
	}
}
*/

/* Pretypovat vkstrukturu na bool* a vstup je taky bool* a nazaklade toho v device creatoru zapnu featury... */