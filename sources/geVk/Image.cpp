/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\ImageInfo.h>
#include <geVk\MemoryManager.inl>
#include <geVk\CommandBuffer.inl>
#include <geVk\CommandProcessor.h>
#include <geAx\Exception.h>

using namespace std;
using namespace ge::Vk;

/* ------- MemoryBarrier ------- */
Info::Image::MemoryBarrier::operator VkImageMemoryBarrier() {
	return {
		VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
		nullptr,
		srcAccessMask,
		dstAccessMask,
		oldLayout,
		newLayout,
		srcQueueFamilyIndex,
		dstQueueFamilyIndex,
		image,
		subresourceRange
	};
}

/* ------- ChangeLayout ------- */
Info::Image::ChangeLayout::ChangeLayout(TransferOp operation) {
	switch (operation) {
		case LOADING_TEX: {
			oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			srcAccessMask = 0;
			dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			waitToStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
			completeBeforeStage = VK_PIPELINE_STAGE_TRANSFER_BIT;

		} break;

		case USE_TEX: {
			oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
			subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			waitToStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
			completeBeforeStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;

		} break;

		case IMG_INIT: {
			oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
			srcAccessMask = 0;
			dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
			subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			waitToStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
			completeBeforeStage = VK_PIPELINE_STAGE_TRANSFER_BIT;

		} break;

		case PRE_CLEAR_IMG: {
			oldLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
			newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
			dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			waitToStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
			completeBeforeStage = VK_PIPELINE_STAGE_TRANSFER_BIT;

		} break;

		case POST_CLEAR_IMG: {
			oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
			srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
			subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			waitToStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
			completeBeforeStage = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;

		} break;

		case DEPTH_TEST: {
			oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			newLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
			srcAccessMask = 0;
			dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
			subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT; //| VK_IMAGE_ASPECT_STENCIL_BIT
			waitToStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
			completeBeforeStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;

		} break;

		case PRE_CLEAR_DEPTH_A: {
			oldLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
			newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			srcAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT;
			dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT; //| VK_IMAGE_ASPECT_STENCIL_BIT
			waitToStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
			completeBeforeStage = VK_PIPELINE_STAGE_TRANSFER_BIT;

		} break;

		case POST_CLEAR_DEPTH_A: {
			oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			newLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
			srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT;
			subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT; //| VK_IMAGE_ASPECT_STENCIL_BIT
			waitToStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
			completeBeforeStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;

		} break;
	}
}


VkAccessFlags Image::layoutToFlags(VkImageLayout layout) {
	switch (layout) {
	case VK_IMAGE_LAYOUT_PREINITIALIZED:
		return VK_ACCESS_HOST_WRITE_BIT;

	case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
		return VK_ACCESS_TRANSFER_WRITE_BIT;

	case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
		return VK_ACCESS_TRANSFER_READ_BIT;

	case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
		return VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

	case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
		return VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

	case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
		return VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_INPUT_ATTACHMENT_READ_BIT;

	case VK_IMAGE_LAYOUT_PRESENT_SRC_KHR:
		return VK_ACCESS_MEMORY_READ_BIT;
	}
	return 0;
}

/* ------- CreateInfoClass ------- */
CreateInfo::Image::Image(CreateInfo::Image::Usage imgUsage, uint32_t width, uint32_t height, bool useMipmap, bool deviceLocal) {
	// keeps the same for the next ones
	type = VK_IMAGE_TYPE_2D;
	memFlags = (deviceLocal) ? Info::Memory::location::GPU : Info::Memory::location::HOST_RAM;
	this->width = width;
	this->height = height;

	switch (imgUsage) {
		case CreateInfo::Image::Usage::COLORATTACHMENT: {
			usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_STORAGE_BIT;
			//resourceDescription.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		} break;
		case CreateInfo::Image::Usage::COLORBUFFER: {
			usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
			//resourceDescription.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		} break;
		case CreateInfo::Image::Usage::COLORSAMPLED: {
			usage = VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_STORAGE_BIT;
			//resourceDescription.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			mipLevelCount = (useMipmap) ? 1 /*+ floor(log2(max(width, height)))*/ : 1; // musim zjistit kde ma gpuengine definovane tyhle funkce a nebo si je napisu sam...
		} break;
		case CreateInfo::Image::Usage::DEPTHBUFFER: {
			usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
			format = VK_FORMAT_D32_SFLOAT;
			tiling = VK_IMAGE_TILING_OPTIMAL; 
			//resourceDescription.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
		} break;
		case CreateInfo::Image::Usage::DEPTHATTACHMENT: {
			usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
			//resourceDescription.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
		} break;
	}
}

CreateInfo::Image::operator VkImageCreateInfo() {
	return {
		VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
		nullptr,
		0,
		type,
		format,
		{ width, height, layerCount}, // extent 3D
		mipLevelCount,
		layerCount,
		samples,
		tiling,
		usage,
		VK_SHARING_MODE_EXCLUSIVE,
		0,
		nullptr,
		currentLayout
	};
}


/*  ------- Core Class ------- */
void Image::init(CreateInfo::Image& createInfo, bool createImageView, CreateInfo::ImageView* pViewCreateInfo) {
	if (_image != VK_NULL_HANDLE)
		return;

	VkImageCreateInfo imageCI = createInfo;
	VkResult res = vkCreateImage(createInfo.pManager->getVkDevice(), &imageCI, nullptr, &_image);
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Creation("geVk Image");

	// allocate info struct memory
	if (_description != nullptr) 
		delete _description; // dealloc safety

	_description = new Info::Image(createInfo);

	if (createInfo.assignMemory && !_description->pManager->assignMemoryToImage(*this, createInfo.allocationSize))
		throw ge::Ax::Exception::Bad::Allocation("geVk Image's memory");

	if (createImageView) {
		if (pViewCreateInfo != nullptr) {
			pViewCreateInfo->format = _description->format;
			pViewCreateInfo->image = this;
			_views.push_back(new ImageView(*pViewCreateInfo)); // vytvori se novy image view
		}
		else {
			CreateInfo::ImageView viewI;
			viewI.format = _description->format;
			viewI.image = this;
			_views.push_back(new ImageView(viewI)); // vytvori se novy image view
		}
	}

}

void Image::deinit() {
	// release memory block
	if (_memory.allocatedFrom != nullptr)
		_description->pManager->releaseImageMemory(*this);

	// destroy image views vector
	for (auto& view : _views) 
		delete view;
	
	// destroy vk buffer object
	if (_image != VK_NULL_HANDLE) 
		vkDestroyImage(_description->pManager->getVkDevice(), _image, nullptr);
	
	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _image = VK_NULL_HANDLE; _views.clear();
}

void Image::move(Image& withdraw) {
	if (_image != VK_NULL_HANDLE || _memory.allocatedFrom != nullptr || _description != nullptr)
		deinit();

	// move ptr on resources
	_image = withdraw._image;
	_description = withdraw._description;
	_memory = withdraw._memory;
	_views.insert(_views.begin(), withdraw._views.begin(), withdraw._views.end());

	// must change view pointers to this geVk Image instead withdraw image
	for (auto& view : _views)
		view->_description->image = this;

	// disvalidate withdraw's ptrs, when all set like this dtor of withdraw object will not affect moved resources
	withdraw._image = VK_NULL_HANDLE;
	withdraw._description = nullptr;
	withdraw._memory.allocatedFrom = nullptr;
	withdraw._views.clear();

}

void Image::copyMemory(Image& data, CommandBuffer& keepCommands, VkImageCopy* copyInfo) {
	uint32_t requieredSize = data._memory.allocInfo.size();
	// need to allocate more space
	if (requieredSize > _memory.allocInfo.size()) {
		CreateInfo::Image cI(*data._description);
		cI.pManager = _description->pManager;

		// release attributes
		deinit();

		/* 
			init with new size
			if data has an image view, create one for this object also
		*/
		CreateInfo::ImageView viewCI(data._views.front()->getDescription());
		if (data._views.size() > 0)
			init(cI, true, &viewCI);
	}

	// data copy here is guaranteed that source buffer will fit inside this buffer memory...
	if (copyInfo == nullptr) {
		VkImageCopy copyRegion;
		copyRegion.srcOffset = { 0,0,0 };
		copyRegion.dstOffset = { 0,0,0 };
		copyRegion.srcSubresource = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1 };
		copyRegion.dstSubresource = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1 };
		copyRegion.extent = { data._description->width, data._description->height, data._description->layerCount };
		vkCmdCopyImage(keepCommands, data, data._description->currentLayout, *this, _description->currentLayout, 1, &copyRegion);
	}
	else 
		vkCmdCopyImage(keepCommands, data, data._description->currentLayout, *this, _description->currentLayout, 1, copyInfo);
	
}

void Image::copyMemory(Buffer& data, CommandBuffer& keepCommands, VkBufferImageCopy* copyInfo) {
	uint32_t requieredSize = data.getMemory().allocInfo.size();
	// need to allocate more space
	if (requieredSize > _memory.allocInfo.size()) {
		return; // disallow image realloc

		/* but code for it is here for demonstration... CreateInfo::Image cI(*_description);
		cI.width = imageDimensions.width;
		cI.height = imageDimensions.height;
		cI.layerCount = imageDimensions.depth;

		// release attributes
		deinit();

		// init with new size
		if (_views.size() > 0) {
			// create image view with same parameters as before
			CreateInfo::ImageView viewCI(_views.front()->getDescription());
			init(cI, true, &viewCI);
		}
		else 
			init(cI);*/
	}

	// data copy here is guaranteed that source buffer will fit inside this buffer memory...
	if (copyInfo == nullptr) {
		VkBufferImageCopy copyRegion;
		copyRegion.imageOffset = { 0,0,0 }; // Optional
		copyRegion.imageExtent = { _description->width, _description->height, 1 };
		copyRegion.imageSubresource = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1 };
		copyRegion.bufferOffset = 0;
		copyRegion.bufferRowLength = 0;
		copyRegion.bufferImageHeight = 0;
		vkCmdCopyBufferToImage(keepCommands, data, _image, _description->currentLayout, 1, &copyRegion);
	}
	else
		vkCmdCopyBufferToImage(keepCommands, data, _image, _description->currentLayout, 1, copyInfo);

}

void Image::stashOnGPU(vector<uint8_t>& data, CommandBuffer& keepCommands, CommandProcessor& proccesCommands) {
	// unitialized object? -> return
	if (_image == VK_NULL_HANDLE || _description == nullptr)
		return;

	CreateInfo::Buffer bCI;
	bCI.pManager = _description->pManager;
	bCI.size = data.size();
	bCI.propertyFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
	bCI.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
	// create stagging buffer
	Buffer staggingBuffer(bCI);
	// if data.size != image.width * image.height -> return
	if (data.size() / _description->formatSize != _description->width * _description->height)
		throw ge::Ax::Exception::Bad::Execution("Image: stashOnGPU -> data size doesn't correspond with image dimensionality");

	// image not devicelocal -> recreate it 
	if ((_description->memFlags &  VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) != 1) {
		CreateInfo::Image info(*_description);
		info.memFlags &= VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT; // disable option for host visible memory
		info.memFlags |= VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT; 
		// recreate image
		deinit();
		init(info);
	}

	// copy data to stagging buffer
	void* sbPtr = staggingBuffer.map();
	memcpy(sbPtr, data.data(), data.size());
	staggingBuffer.unmap();

	// copy from stagging buffer to this object stored in GPU local heap.
	keepCommands.waitUntilExecutionFinished();
	keepCommands.beginRecording(); 
	copyMemory(staggingBuffer, keepCommands);
	keepCommands.finishRecording();

	Info::Methods::PostSingleBuffer postI;
	postI.pBuffer = &keepCommands;
	proccesCommands.postCommands(postI, &keepCommands.getLock().getPrimitive<Fence>());

	// need to wait until copy is done before destroying stagging buffer... 
	keepCommands.waitUntilExecutionFinished();

	// here should be object filled with data
}

void ge::Vk::Image::changeLayout(CommandBuffer& commandBuffer, Info::Image::ChangeLayout& info, CommandProcessor& proccesCommands) {
	commandBuffer.beginRecording();

	info.image = _image;
	VkImageMemoryBarrier memBarrier = info; // MemoryBarrier parrent of ChangeLayout and has convertor to vkimgbarrier
	vkCmdPipelineBarrier(commandBuffer, info.waitToStage, info.completeBeforeStage, 0, 0, VK_NULL_HANDLE, 0, VK_NULL_HANDLE, 1, &memBarrier);

	_description->currentLayout = memBarrier.newLayout;

	commandBuffer.finishRecording();

	Info::Methods::PostSingleBuffer postI;
	postI.pBuffer = &commandBuffer;
	proccesCommands.postCommands(postI, &commandBuffer.getLock().getPrimitive<Fence>());

	// because data ops need to wait until execution is finished
	commandBuffer.waitUntilExecutionFinished();
}

void Image::clearColor(VkClearColorValue clearValue, CommandBuffer& keepCommands) {
	VkImageSubresourceRange info { 
		VK_IMAGE_ASPECT_COLOR_BIT,
		0,
		1,
		0,
		1
	};

	// need to change image layout to proper one n back
	Info::Image::ChangeLayout changeL(Info::Image::ChangeLayout::PRE_CLEAR_IMG);
	changeLayout(keepCommands, changeL);
	vkCmdClearColorImage(keepCommands, _image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, &clearValue, 1, &info);
	changeL = Info::Image::ChangeLayout(Info::Image::ChangeLayout::POST_CLEAR_IMG);
	changeLayout(keepCommands, changeL);

}

void Image::clearDepthStencil(VkClearDepthStencilValue clearValue, CommandBuffer & keepCommands) {
	VkImageSubresourceRange info {
		VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT,
		0,
		1,
		0,
		1
	};

	// need to change image layout to proper one n back
	Info::Image::ChangeLayout changeL(Info::Image::ChangeLayout::PRE_CLEAR_DEPTH_A);
	changeLayout(keepCommands, changeL);
	vkCmdClearDepthStencilImage(keepCommands, _image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, &clearValue, 1, &info);
	changeL = Info::Image::ChangeLayout(Info::Image::ChangeLayout::POST_CLEAR_DEPTH_A);
	changeLayout(keepCommands, changeL);

}

void ge::Vk::Image::clearColor(VkClearColorValue clearValue, CommandBuffer& keepCommands, CommandProcessor& execCommands) {
	keepCommands.beginRecording();
	clearColor(clearValue, keepCommands);
	keepCommands.finishRecording();

	Info::Methods::PostSingleBuffer postI;
	postI.pBuffer = &keepCommands;
	execCommands.postCommands(postI, &keepCommands.getLock().getPrimitive<Fence>());

	// because data ops need to wait until execution is finished
	keepCommands.waitUntilExecutionFinished();
}

void ge::Vk::Image::clearDepthStencil(VkClearDepthStencilValue clearValue, CommandBuffer& keepCommands, CommandProcessor& execCommands) {
	keepCommands.beginRecording();
	clearDepthStencil(clearValue, keepCommands);
	keepCommands.finishRecording();

	Info::Methods::PostSingleBuffer postI;
	postI.pBuffer = &keepCommands;
	execCommands.postCommands(postI, &keepCommands.getLock().getPrimitive<Fence>());

	// because data ops need to wait until execution is finished
	keepCommands.waitUntilExecutionFinished();
}

void Image::changeLayout(VkCommandBuffer commandBuffer, Info::Image::ChangeLayout& info) {
	info.image = *this;
	VkImageMemoryBarrier memBarrier = info; // MemoryBarrier parrent of ChangeLayout and has convertor to vkimgbarrier
	vkCmdPipelineBarrier(commandBuffer, info.waitToStage, info.completeBeforeStage, 0, 0, VK_NULL_HANDLE, 0, VK_NULL_HANDLE, 1, &memBarrier);
	_description->currentLayout = memBarrier.newLayout;
}