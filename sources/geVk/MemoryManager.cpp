/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\MemoryManager.h>
#include <geVk\MemoryPool.h>
#include <geVk\Device.h>
#include <geVk\MemoryBlock.h>
#include <geVk\Image.h>
#include <geVk\Buffer.h>
#include <geAx\Exception.h>

using namespace ge::Vk;

void ge::Vk::MemoryManager::init(CreateInfo::MemoryManager& createInfo) {
	_description = new Info::MemoryManager(createInfo); 
}

void MemoryManager::deinit() {
	cleanUpStorage();
	if (_description != nullptr)
		delete _description;

}

VkDeviceMemory MemoryManager::allocateMemory(VkDeviceSize size, uint32_t memTypeIdx, VkMemoryPropertyFlags memoryFlags) {
	VkMemoryAllocateInfo allocInfo { 
		VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
		nullptr,
		size,
		(uint32_t) memTypeIdx
	};
	
	VkDeviceMemory memoryPtr;
	vkAllocateMemory(*_description->pDevice, &allocInfo, nullptr, &memoryPtr);

	return memoryPtr;
}

void MemoryManager::releaseMemory(VkDeviceMemory memory) {
	if (memory != VK_NULL_HANDLE)
		vkFreeMemory(*_description->pDevice, memory, nullptr);
}

void* MemoryManager::map(Info::MemoryBlock& memory) {
	// find the pool from which memory was allocated
	MemoryPool& pool = *memory.allocatedFrom;

	if (pool.getMemory() != VK_NULL_HANDLE && (pool.getFlags() & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) != VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) {
		void* dataPtr;
		// vkspecs state that memoryMapFlags must stay 0.
		if (vkMapMemory(*_description->pDevice, pool.getMemory(), memory.allocInfo.start, memory.allocInfo.size(), 0, &dataPtr) != VK_SUCCESS) 
			throw ge::Ax::Exception::Bad::Execution("MemoryManager::map -> cannot map memory region");
		
		return dataPtr;
	}
	return nullptr;
}

void MemoryManager::unmap(Info::MemoryBlock& memory) {
	// find the pool from which memory was allocated
	MemoryPool& pool = *memory.allocatedFrom;

	if (pool.getMemory() != VK_NULL_HANDLE)
		vkUnmapMemory(*_description->pDevice, pool.getMemory());

}

int MemoryManager::typeBitsToIndex(uint32_t typeBits, VkMemoryPropertyFlags requirements) {
	VkPhysicalDeviceMemoryProperties memProps;
	vkGetPhysicalDeviceMemoryProperties(_description->pDevice->getPhysicalDevice(), &memProps);
	for (int i = 0; i < memProps.memoryTypeCount; i++) {
		if (((1 << i) & typeBits) && ((memProps.memoryTypes[i].propertyFlags & requirements) == requirements)) {
			return i;
		}
	}

	return -1;
}

bool MemoryManager::assignMemoryToImage(Image& image, uint32_t allocationSize) {
	VkMemoryRequirements memReqs;
	vkGetImageMemoryRequirements(getVkDevice(), image, &memReqs);

	Info::MemoryRequierements reqs;
	reqs.memReqs = memReqs;
	reqs.allocationSize = allocationSize;
	reqs.memFlags = image._description->memFlags;

	bool res = allocateMemoryBlock(image._memory, reqs);

	VkDeviceMemory bindTo = image._memory.allocatedFrom->getMemory();

	return vkBindImageMemory(getVkDevice(), image._image, bindTo, image._memory.allocInfo.start) == VK_SUCCESS;;
}

bool MemoryManager::assignMemoryToBuffer(Buffer& buffer, uint32_t allocationSize) {
	VkMemoryRequirements memReqs;
	vkGetBufferMemoryRequirements(getVkDevice(), buffer._buffer, &memReqs);
	
	Info::MemoryRequierements reqs;
	reqs.memReqs = memReqs;
	reqs.allocationSize = allocationSize;
	reqs.memFlags = buffer._description->propertyFlags;

	bool res = allocateMemoryBlock(buffer._memory, reqs);

	VkDeviceMemory bindTo = buffer._memory.allocatedFrom->getMemory();

	return vkBindBufferMemory(getVkDevice(), buffer._buffer, bindTo, buffer._memory.allocInfo.start) == VK_SUCCESS;
}

void ge::Vk::MemoryManager::preAllocateMemory(Info::MemoryProperties& info) {
	CreateInfo::MemoryPool crI;
	crI.memoryTypeIndex = info.memoryTypeIndex;
	crI.allocatedSpace = info.size;
	crI.unitSize = info.alignment;
	crI.propertyFlags = info.propertyFlags;

	insertPool(crI);
}

bool ge::Vk::MemoryManager::storeIntoGPU(Info::MemoryBlock & block) {
	return false;
}