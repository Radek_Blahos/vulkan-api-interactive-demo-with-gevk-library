/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\Swapchain.h>
#include <geAx\Exception.h>
#include <geVk\MemoryManager.inl>
#include <geVk\Image.inl>
#include <geVk\CommandBuffer.inl>
#include <geVk\CommandProcessor.h>
#include <geVk\RenderComponentsDB.h>

using namespace std;
using namespace ge::Vk;

/* ------- Auxiliary Class ------- */
ge::Vk::Info::Methods::PresentImage::operator VkPresentInfoKHR() {
	if (imageIndexes.size() != waitSemaphores.size())
		throw ge::Ax::Exception::Bad::Execution("Info::Methods::PresentImage::operator VkPresentInfoKHR -> not every swapchain has and image assigned");

	VkResult* resultPtr = nullptr;
	if (checkResults) {
		// resize results vector to result of every swapchain presentation is stored here
		results.clear();
		results.resize(swapchains.size());
		resultPtr = results.data();
	}

	return {
		VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
		nullptr,
		(uint32_t) waitSemaphores.size(),
		waitSemaphores.data(),
		(uint32_t) swapchains.size(),
		swapchains.data(),
		imageIndexes.data(),
		resultPtr
	};
}

/* ------- CreateInfoClass ------- */
CreateInfo::Swapchain::operator VkSwapchainCreateInfoKHR() {
	return {
		VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
		nullptr,
		0,
		surface,
		minImageCount,
		surfaceFormat.format,
		surfaceFormat.colorSpace,
		{ width, height },
		imageArrayLayers,
		imageUsage,
		VK_SHARING_MODE_EXCLUSIVE,
		0,
		nullptr,
		preTransform,
		compositeAlpha,
		presentMode,
		clipped,
		oldSwapchain
	};
}

/* ------- Core Class ------- */
void Swapchain::init(CreateInfo::Swapchain& createInfo) {
	if (_swapchain != VK_NULL_HANDLE)
		return;

	VkSwapchainCreateInfoKHR swapchainCI = createInfo;
	VkResult res = vkCreateSwapchainKHR(createInfo.pMemManager->getDevice(), &swapchainCI, nullptr, &_swapchain);
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Creation("geVk Swapchain");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	_description = new Info::Swapchain(createInfo);

	initResources();
}

void Swapchain::deinit() {
	// clears gevkimages
	returnImages();
	
	// destroy vk buffer object
	if (_swapchain != VK_NULL_HANDLE)
		vkDestroySwapchainKHR(_description->pMemManager->getDevice(), _swapchain, nullptr);

	if (_depthbuffer != nullptr)
		delete _depthbuffer;

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _swapchain = VK_NULL_HANDLE; _depthbuffer = nullptr;

}

void Swapchain::createDepthBuffer(VkDeviceSize allocSize) {
	// create info se vyplni automaticky na zaklade konstuktoru 
	CreateInfo::Image depthbuffCI(CreateInfo::Image::Usage::DEPTHBUFFER, _description->width, _description->height, false, true);
	depthbuffCI.pManager = _description->pMemManager;
	
	// going to allocate maximal resolution size for depth buffer and with every next swapchain resize will be guaranted that depthbuffer will fit into allocated device memory. So no more realocations...
	depthbuffCI.allocationSize = allocSize;

	// alloc image object on heap
	_depthbuffer = new Image();

	// ctor image view
	CreateInfo::ImageView viewCI{ Info::ImageView {
			_depthbuffer, // cast from ge::vk::image to vkimage
			VK_IMAGE_VIEW_TYPE_2D,
			VK_FORMAT_D32_SFLOAT,
			{}, // component mapping's default initializer
			{ 
				VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT,
				0,
				1,
				0,
				1
			} // subresource range
		}
	};

	_depthbuffer->create(depthbuffCI, true, &viewCI);

}

void Swapchain::obtainImages() {
	uint32_t imgCount(0);
	vkGetSwapchainImagesKHR(_description->pMemManager->getDevice(), _swapchain, &imgCount, nullptr);
	
	_description->minImageCount = imgCount;

	vector<VkImage> swapchainImgs(imgCount);
	vkGetSwapchainImagesKHR(_description->pMemManager->getDevice(), _swapchain, &imgCount, swapchainImgs.data());

	for (auto& image: swapchainImgs) {
		_images.push_back(new Image());
		// create empty gevkimage
		_images.back()->_image = image; // assign ptr to vkimage
		_images.back()->_description = new Info::Image();
		_images.back()->_description->pManager = _description->pMemManager;
		_images.back()->_description->format = _description->surfaceFormat.format;

		// creating image views
		CreateInfo::ImageView viewCI{
			Info::ImageView {
				_images.back(),
				VK_IMAGE_VIEW_TYPE_2D,
				_description->surfaceFormat.format,
				{ VK_COMPONENT_SWIZZLE_IDENTITY, }, // component mapping's default initializer
				{
					VK_IMAGE_ASPECT_COLOR_BIT,
					0,
					1,
					0,
					1
				} // subresource range
			}
		};
		_images.back()->_views.push_back(new ImageView(viewCI));
		// memory block must stay unitialized...
	}
}

void ge::Vk::Swapchain::createFramebuffers(RenderPass& usedRenderPass) {
	CreateInfo::Framebuffer framebuffCI;
	framebuffCI.pDevice = &_description->pMemManager->getDevice();
	framebuffCI.activeRenderPass = &usedRenderPass;
	framebuffCI.width = _description->width;
	framebuffCI.height = _description->height;
	framebuffCI.layersCount = _description->imageArrayLayers;

	// for single renderpass can be used just single framebuffers, be carefull here you overwrite framebuffers for another swapchain 
	if (_description->framebuffersVecPtr->find(&usedRenderPass) != _description->framebuffersVecPtr->end())
		_description->framebuffersVecPtr->erase(&usedRenderPass);
	
	// set current renderpass to method argument one...
	_currentRenderPass = &usedRenderPass;

	for (unsigned i = 0; i < _images.size(); i++) {
		framebuffCI.attachmentRefs.clear();
		framebuffCI.attachmentRefs.insert(framebuffCI.attachmentRefs.begin(), { _images[i]->_views.front(), _depthbuffer->_views.front() });

		// create framebuff for all swapchain images
		(*_description->framebuffersVecPtr)[&usedRenderPass].push_back(std::make_shared<Framebuffer>(framebuffCI));
	}
}

void ge::Vk::Swapchain::returnImages() {
	// framebuffers are deletede inside render components DB
	deleteFramebuffers(*_currentRenderPass);

	// because geVk Image objects are not allocated normaly but via swapchain object, swapchain object must also dealoce it
	for (auto& image : _images) {
		// clear image views
		for (auto& views : image->_views)
			delete views; // object cleanup after itself
		
		// delete info
		if (image->_description != nullptr)
			delete image->_description;
	}

	_images.clear();
}

void ge::Vk::Swapchain::initResources() {
	// init sync components...
	CreateInfo::Semaphore semaphoreCI{ Info::Semaphore{ &_description->pMemManager->getDevice() } };
	_presentationLock.create(semaphoreCI);
	CreateInfo::Fence fenceCI{ Info::Fence{ &_description->pMemManager->getDevice() }, false };
	_obtainNextImageFence.create(fenceCI);

	// obtain images and create their views and framebuffers 
	obtainImages();

	// depthbuffer is created by user
	// framebuffers must be always created after renderpass!!!
}

void Swapchain::acquireNextImage() {
	VkFence waitFor = _obtainNextImageFence.getVkFence();

	// acquire image with passive waiting
	vkAcquireNextImageKHR(_description->pMemManager->getDevice(), _swapchain, MAX_WAIT_TIMEOUT, VK_NULL_HANDLE, _obtainNextImageFence.passFenceToSignal(), &_currentImageIdx);
	
	// wait for acquire completed
	_obtainNextImageFence.waitUntilSignaled();

}

bool Swapchain::presentImage() {
	VkPresentInfoKHR present_info{};
	present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	present_info.waitSemaphoreCount = 0;
	present_info.pWaitSemaphores = nullptr;
	present_info.swapchainCount = 1;
	present_info.pSwapchains = &_swapchain;
	present_info.pImageIndices = &_currentImageIdx;
	present_info.pResults = VK_NULL_HANDLE;

	return vkQueuePresentKHR(*_description->presentationQueue, &present_info) == VK_SUCCESS;
}

void Swapchain::changeResolution(uint32_t width, uint32_t height) {
	Info::Swapchain inheritInfo = *_description;
	CreateInfo::Swapchain swapchainCI(inheritInfo);
	swapchainCI.width = width;
	swapchainCI.height = height;
	swapchainCI.oldSwapchain = _swapchain; // current swapchain object

	Swapchain newSwapchain; // will be moved into old swapchain object
	newSwapchain.create(swapchainCI); // init new swapchain
	dismantle(); // this->dismantle ... delete old one
	*this = newSwapchain; // move new one over current one

}

void ge::Vk::Swapchain::move(Swapchain& withdraw) {
	// if object has allocated some resources, they has to be cleaned
	if (_swapchain != VK_NULL_HANDLE || _description != nullptr)
		deinit();

	// move ptr on resources
	_swapchain = withdraw._swapchain;
	_description = withdraw._description;
	// move resources
	_depthbuffer = withdraw._depthbuffer;
	_images.insert(_images.begin(), withdraw._images.begin(), withdraw._images.end());
	_currentRenderPass = withdraw._currentRenderPass;

	// disvalidate withdraw's ptrs, when all set like this dtor of withdraw object will not affect moved resources
	withdraw._swapchain = VK_NULL_HANDLE;
	withdraw._description = nullptr;
	withdraw._depthbuffer = nullptr;
	// clear ptr vectors
	withdraw._images.clear(); 

}

void Swapchain::transformImagesToProperLayout(CommandBuffer& postCommandsHere) {
	// change images
	Info::Image::ChangeLayout changeI(Info::Image::ChangeLayout::TransferOp::IMG_INIT);

	for (auto& image : _images) {
		changeI.image = *image;
		image->changeLayout(postCommandsHere, changeI);
	}

	// change depthbuffer
	changeI = Info::Image::ChangeLayout(Info::Image::ChangeLayout::TransferOp::DEPTH_TEST);
	changeI.image = *_depthbuffer;
	_depthbuffer->changeLayout(postCommandsHere, changeI);

}

void Swapchain::clearColors(CommandBuffer& keepCommands, CommandProcessor& execCommands) {
	keepCommands.beginRecording();
	_depthbuffer->clearDepthStencil({ 1.0f, 0, }, keepCommands);
	_images[_currentImageIdx]->clearColor({ 0.7f, 0.7f,0.7f, 1.0f }, keepCommands);
	keepCommands.finishRecording();

	Info::Methods::PostSingleBuffer postI;
	postI.pBuffer = &keepCommands;
	execCommands.postCommands(postI, &keepCommands.getLock().getPrimitive<Fence>());

	// because data ops need to wait until execution is finished
	keepCommands.waitUntilExecutionFinished();
}