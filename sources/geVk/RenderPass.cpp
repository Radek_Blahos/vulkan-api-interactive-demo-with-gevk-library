/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\RenderPass.inl>
#include <geVk\Device.h>
#include <geAx\Exception.h>

using namespace ge::Vk;

/* ------- Core Class ------- */
void RenderPass::init(CreateInfo::RenderPass& createInfo) {
	if (_renderPass != VK_NULL_HANDLE)
		return;

	VkRenderPassCreateInfo rPCI = createInfo;
	VkResult res = vkCreateRenderPass(*createInfo.pDevice, &rPCI, nullptr, &_renderPass);
	if (res != VK_SUCCESS)
		throw ge::Ax::Exception::Bad::Creation("geVk RenderPass");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	// decision to alloc futher info struct or just info
	if (createInfo.pDevice->abundantInfo())
		_description = new FurtherInfo::RenderPass(createInfo);
	else
		_description = new Info::RenderPass(createInfo);

}

void RenderPass::deinit() {
	// destroy vk buffer object
	if (_renderPass != VK_NULL_HANDLE)
		vkDestroyRenderPass(*_description->pDevice, _renderPass, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _renderPass = VK_NULL_HANDLE;
}

void RenderPass::move(RenderPass& withdraw) {
	if (_renderPass != VK_NULL_HANDLE || _description != nullptr)
		deinit();

	// move ptr on resources
	_renderPass = withdraw._renderPass;
	_description = withdraw._description;

	// disvalidate withdraw's ptrs, when all set like this dtor of withdraw object will not affect moved resources
	withdraw._renderPass = VK_NULL_HANDLE;
	withdraw._description = nullptr;

}