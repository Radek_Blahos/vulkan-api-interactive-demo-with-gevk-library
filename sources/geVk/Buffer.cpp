/**
* @author Radek Blahos
* @project geVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geVk\Buffer.h>
#include <geVk\MemoryManager.inl>
#include <geVk\CommandBuffer.inl>
#include <geVk\CommandProcessor.h>
#include <geAx\Exception.h>

using namespace ge::Vk;
using namespace ge::Ax;
using namespace std;

/* ------- CreateInfoClass ------- */
CreateInfo::Buffer::Buffer(CreateInfo::Buffer::Usage bufferUsage, uint32_t locationFlags) {
	switch (bufferUsage) {
		case CreateInfo::Buffer::Usage::VERTEXBUFFER: {
			usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
			propertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
		} break;
		case CreateInfo::Buffer::Usage::INDEXBUFFER: {
			usage = VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
			propertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
		} break; 
		case CreateInfo::Buffer::Usage::STAGGINGBUFFER: {
			usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
			propertyFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
		} break;
		case CreateInfo::Buffer::Usage::SHADERSTORAGE: {
			usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
			propertyFlags = locationFlags;
		} break;
		case CreateInfo::Buffer::Usage::INDEXEDINDIRECTDRAW: {
			usage = VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
			propertyFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
		} break;
		default:
			break;
	}
}

CreateInfo::Buffer::operator VkBufferCreateInfo() {
	return VkBufferCreateInfo {
		VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
		nullptr,
		0,
		size,
		usage,
		VK_SHARING_MODE_EXCLUSIVE,
		0,
		nullptr
	};
}

/* ------- Core Class ------- */
void Buffer::init(CreateInfo::Buffer& createInfo) {
	if (_buffer != VK_NULL_HANDLE)
		return; // buffer uz je inicializovan -> return
	
	VkBufferCreateInfo buffCI = createInfo;
	VkResult res = vkCreateBuffer(createInfo.pManager->getVkDevice(), &buffCI, nullptr, &_buffer);
	if (res != VK_SUCCESS)	
		throw Exception::Bad::Creation("geVk Buffer");
	
	// allocate info struct memory
	if(_description != nullptr) 
		delete _description; // dealloc safety
	
	_description = new Info::Buffer(createInfo); 

	// allocate Vk memory for buffer
	if (createInfo.assignMemory && !_description->pManager->assignMemoryToBuffer(*this, createInfo.allocationSize))
		throw Exception::Bad::Allocation("geVk Buffer's memory");

}

void Buffer::deinit() {
	// release memory block
	if (_memory.allocatedFrom != nullptr)
		_description->pManager->releaseBufferMemory(*this);

	// destroy vk buffer object
	if (_buffer != VK_NULL_HANDLE)
		vkDestroyBuffer(_description->pManager->getVkDevice(), _buffer, nullptr);

	// destroys info.
	if (_description == nullptr)
		delete _description; 

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _buffer = VK_NULL_HANDLE;
}

void* Buffer::map() {
	return _description->pManager->map(_memory);
}

void Buffer::unmap() {
	_description->pManager->unmap(_memory);
}

void Buffer::move(Buffer& withdraw) {
	// if object has allocated some resources, they has to be cleaned
	if (_buffer != VK_NULL_HANDLE || _memory.allocatedFrom != nullptr || _description != nullptr)
		deinit();

	// move ptr on resources
	_buffer = withdraw._buffer;
	_description = withdraw._description;
	_memory = withdraw._memory;

	// disvalidate withdraw's ptrs, when all set like this dtor of withdraw object will not affect moved resources
	withdraw._buffer = VK_NULL_HANDLE;
	withdraw._description = nullptr;
	withdraw._memory.allocatedFrom = nullptr; 

}

void Buffer::copyMemory(Buffer& data, CommandBuffer& keepCommands, VkBufferCopy* copyInfo) {
	uint32_t requieredSize = data._memory.allocInfo.size();
	// need to allocate more space
	if (requieredSize > _memory.allocInfo.size()) {
		CreateInfo::Buffer bufferCI;
		bufferCI.size = requieredSize;
		bufferCI.pManager = _description->pManager;
		bufferCI.propertyFlags = _description->propertyFlags;
		bufferCI.usage = _description->usage;

		// release attributes
		deinit();

		// init with new size
		init(bufferCI);
	}

	// data copy here is guaranteed that source buffer will fit inside this buffer memory...
	if (copyInfo == nullptr) {
		VkBufferCopy copyRegion;
		copyRegion.srcOffset = 0;
		copyRegion.dstOffset = 0;
		copyRegion.size = requieredSize;
		vkCmdCopyBuffer(keepCommands, data, *this, 1, &copyRegion);
	}
	else 
		vkCmdCopyBuffer(keepCommands, data, *this, 1, copyInfo);

}

void Buffer::stashOnGPU(vector<uint8_t>& data, CommandBuffer& keepCommands, CommandProcessor& proccesCommands, uint32_t dstBufferOffset) {
	// unitialized object? -> return
	if (_buffer == VK_NULL_HANDLE || _description == nullptr)
		return;

	CreateInfo::Buffer bCI;
	bCI.pManager = _description->pManager;
	bCI.size = data.size();
	bCI.propertyFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
	bCI.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
	// create stagging buffer
	Buffer staggingBuffer(bCI);
	// recreate this buffer object if necessary
	if (data.size() > _description->size || (_description->propertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) != VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) {
		bCI = *_description;
		bCI.size = data.size();
		bCI.propertyFlags &= VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT; // disable option for host visible memory
		bCI.propertyFlags |= VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT; 
		// to inherited flags adding obligatory one
		bCI.usage |= VK_BUFFER_USAGE_TRANSFER_DST_BIT;
		// destroy and reinit object attribs...
		deinit();
		init(bCI);
	}
	// copy data to stagging buffer
	void* sbPtr = staggingBuffer.map();
	memcpy(sbPtr, data.data(), data.size());
	staggingBuffer.unmap();

	// copy from stagging buffer to this object stored in GPU local heap.
	keepCommands.waitUntilExecutionFinished(); 
	keepCommands.beginRecording();
	VkBufferCopy copyInfo;
	copyInfo.dstOffset = dstBufferOffset;
	copyInfo.srcOffset = 0;
	copyInfo.size = data.size();
	copyMemory(staggingBuffer, keepCommands, &copyInfo);
	keepCommands.finishRecording();
	
	Info::Methods::PostSingleBuffer postI;
	postI.pBuffer = &keepCommands;
	proccesCommands.postCommands(postI, &keepCommands.getLock().getPrimitive<Fence>());

	// must wait until copy executes, because stagging buffer
	keepCommands.waitUntilExecutionFinished();

	// here should be object filled with data
}

void Buffer::getMemoryProperties(Info::MemoryProperties& result) {
	VkMemoryRequirements reqs;
	vkGetBufferMemoryRequirements(getVkDevice(), _buffer, &reqs);

	result.size = reqs.size;
	result.memoryTypeIndex = _description->pManager->typeBitsToIndex(reqs.memoryTypeBits, _description->propertyFlags);
	result.alignment = reqs.alignment;
	result.propertyFlags = _description->propertyFlags;
}

void ge::Vk::Buffer::assignMemory() {
	if (!_description->pManager->assignMemoryToBuffer(*this))
		throw Exception::Bad::Allocation("geVk Buffer's memory");
}