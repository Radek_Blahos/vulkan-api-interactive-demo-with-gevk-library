/**
* @author Radek Blahos
* @project geAx Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <geAx\UsageRegister.h>
#include <geAx\AuxInlines.inl>
#include <cstring>
#include <iostream>

using namespace ge::Ax;
using namespace std;

void UsageRegister::init(uint32_t indexCount) {
	unsigned bitSize = sizeof(uint32_t) * 8;
	size_t allocate = ceil((double)indexCount / bitSize);

	_bitField.resize(allocate);
	_indexSize = indexCount;

	memset(_bitField.data(), 0, sizeof(uint32_t) * _bitField.size());
}

void UsageRegister::resize(uint32_t indexCount) {
	unsigned bitSize = sizeof(uint32_t) * 8;
	size_t allocate = ceil((double)indexCount / bitSize);
	
	// set current indexSize
	_indexSize = indexCount;

	// if there is no need to resize bitfield
	if (allocate < _bitField.size())
		return;

	// keep track on last Uint 
	if (_bitField.size() > 0) {
		uint32_t end = _bitField.size() - 1;
		uint32_t endMap = _bitField.back();

		// resize vec to desired size
		_bitField.resize(allocate);

		// zero new memory
		memset(_bitField.data() + end, 0, (_bitField.size() - end) * sizeof(uint32_t));
		_bitField[end] = endMap; // recover rewrited Uint
	}
	else 
		// resize vec to desired size
		_bitField.resize(allocate);

}

void UsageRegister::unsetAllIndexes() {
	memset(_bitField.data(), 0, _bitField.size());
	_activeCount = 0;
	_searchStoppedIdx = -1;
}

void UsageRegister::currentlyActive(set<uint32_t>& activeIdxs) {
	activeIdxs.clear();

	unsigned bitSize = sizeof(uint32_t) * 8;

	for (uint32_t i = 0; i < _bitField.size(); i++) {
		for (uint32_t j = 0; j < bitSize; j++) {
			if ((1u << j) & _bitField[i]) {
				activeIdxs.insert(i*bitSize + j);
			}
		}
	}
}

bool UsageRegister::allActive(unsigned from, unsigned upTo) {
	unsigned bitSize = sizeof(uint32_t) * 8;
	unsigned uintStart = from / bitSize;
	unsigned bitStart = from % bitSize;
	unsigned uintStop = upTo / bitSize;
	unsigned bitStop = upTo % bitSize;

	// uintStart search
	for (uint32_t i = bitStart; i <= bitSize; i++) {
		if (!((1u << i) & _bitField[uintStart])) // bit neni nastaven na 1
			return false;
	}

	// Search between
	for (uint32_t i = uintStart + 1; i < uintStop; i++) {
		for (uint32_t j = 0; j <= bitSize; j++) {
			if (!((1u << j) & _bitField[i])) // bit neni nastaven na 1
				return false;
		}
	}

	// uintStop search
	for (uint32_t i = 0; i <= bitStop; i++) {
		if (!((1u << i) & _bitField[uintStop])) // bit neni nastaven na 1
			return false;
	}
	
	return true;
}

bool UsageRegister::allFree(unsigned from, unsigned upTo) {
	unsigned bitSize = sizeof(uint32_t) * 8;
	unsigned uintStart = from / bitSize;
	unsigned bitStart = from % bitSize;
	unsigned uintStop = upTo / bitSize;
	unsigned bitStop = upTo % bitSize;

	// uintStart search
	for (uint32_t i = bitStart; i <= bitSize; i++) {
		if (((1u << i) & _bitField[uintStart])) // bit neni nastaven na 0
			return false;
	}

	// Search between
	for (uint32_t i = uintStart + 1; i < uintStop; i++) {
		for (uint32_t j = 0; j <= bitSize; j++) {
			if (((1u << j) & _bitField[i])) // bit neni nastaven na 0
				return false;
		}
	}

	// uintStop search
	for (uint32_t i = 0; i <= bitStop; i++) {
		if (((1u << i) & _bitField[uintStop])) // bit neni nastaven na 0
			return false;
	}

	return true;
}

bool UsageRegister::isActive(unsigned index) {
	unsigned bitSize = sizeof(uint32_t) * 8;
	unsigned bit = index % bitSize;
	unsigned uint = index / bitSize;

	return (1u << bit) & _bitField[uint];
}

bool UsageRegister::isFree(unsigned index) {
	unsigned bitSize = sizeof(uint32_t) * 8;
	unsigned bit = index % bitSize;
	unsigned uint = index / bitSize;

	return !((1u << bit) & _bitField[uint]);
}

/* Core Methods */
int32_t UsageRegister::getNextActiveIndex() {
	if (_activeCount == 0) // nothing active
		return -1;

	_searchStoppedIdx = (_searchStoppedIdx + 1) % _indexSize;
	unsigned bitSize = sizeof(uint32_t) * 8;
	unsigned searchBit = _searchStoppedIdx % bitSize;
	unsigned searchUint = _searchStoppedIdx / bitSize;
	
	// search from searchStoppedIdx to end
	for (unsigned i = searchBit; i < bitSize; i++) {
		// nasel se aktivni index
		if ((1u << i) & _bitField[searchUint]) { // index set to 1
			_searchStoppedIdx = searchUint * bitSize + i;
			return _searchStoppedIdx;
		}
			
	}

	// search between whole Uints up to bitfield.size() - 1 
	for (unsigned i = searchUint + 1; i < _bitField.size() - 1; i++) {
		for (unsigned j = 0; j < bitSize; j++) {
			// nasel se aktivni index
			if ((1u << j) & _bitField[i]) { // bit set to one
				_searchStoppedIdx = i * bitSize + j;
				return _searchStoppedIdx;
			}
		}
	}
	
	// bitfield end must be scanned max up to _indexSize % bitSize
	for (unsigned i = 0; i < _indexSize%bitSize; i++) {
		// nasel se aktivni index
		if ((1u << i) & _bitField[_bitField.size()-1]) {
			_searchStoppedIdx = (_bitField.size() - 1) * bitSize + i;
			return _searchStoppedIdx;
		}
	}

	// search between whole Uints from bitfield start to searchUint - 1
	for (unsigned i = 0; i < searchUint; i++) {
		for (unsigned j = 0; j < bitSize; j++) {
			if ((1u << j) & _bitField[i]) {
				_searchStoppedIdx = i * bitSize + j;
				return _searchStoppedIdx;
			}
		}
	}

	// search from start searchUint to searchStoppedIdx
	for (unsigned i = 0; i < searchBit; i++) {
		if ((1u << i) & _bitField[searchUint]) {
			_searchStoppedIdx = searchUint * bitSize + i;
			return _searchStoppedIdx;
		}
	}
	
	return -1; // found nothing active
}

int32_t UsageRegister::getNextFreeIndex() {
	// nothing free
	if (_activeCount >= _indexSize) 
		return -1;

	_searchStoppedIdx = (_searchStoppedIdx + 1) % _indexSize;
	unsigned bitSize = sizeof(uint32_t) * 8;
	unsigned searchBit = _searchStoppedIdx % bitSize;
	unsigned searchUint = _searchStoppedIdx / bitSize;

	for (unsigned i = searchBit; i < bitSize; i++) {
		// nasel se aktivni index
		if (!((1u << i) & _bitField[searchUint])) { // index set to 0
			_searchStoppedIdx = searchUint * bitSize + i;
			setIndex(_searchStoppedIdx); // set index as used
			return _searchStoppedIdx;
		}

	}

	// search between whole Uints up to bitfield.size() - 1 
	for (unsigned i = searchUint + 1; i < _bitField.size() - 1; i++) {
		for (unsigned j = 0; j < bitSize; j++) {
			// nasel se aktivni index
			if (!((1u << j) & _bitField[i])) { // bit set to zero...
				_searchStoppedIdx = i * bitSize + j;
				setIndex(_searchStoppedIdx); // set index as used
				return _searchStoppedIdx;
			}
		}
	}

	// bitfield end must be scanned max up to _indexSize % bitSize
	for (unsigned i = 0; i < _indexSize%bitSize; i++) {
		// nasel se aktivni index
		if (!((1u << i) & _bitField[_bitField.size() - 1])) {
			_searchStoppedIdx = (_bitField.size() - 1) * bitSize + i;
			setIndex(_searchStoppedIdx); // set index as used
			return _searchStoppedIdx;
		}
	}

	// search between whole Uints from bitfield start to searchUint - 1
	for (unsigned i = 0; i < searchUint; i++) {
		for (unsigned j = 0; j < bitSize; j++) {
			if (!((1u << j) & _bitField[i])) {
				_searchStoppedIdx = i * bitSize + j;
				setIndex(_searchStoppedIdx); // set index as used
				return _searchStoppedIdx;
			}
		}
	}

	// search from start searchUint to searchStoppedIdx
	for (unsigned i = 0; i < searchBit; i++) {
		if (!((1u << i) & _bitField[searchUint])) {
			_searchStoppedIdx = searchUint * bitSize + i;
			setIndex(_searchStoppedIdx); // set index as used
			return _searchStoppedIdx;
		}
	}

	return -1; // nothing free
}

void UsageRegister::setIndex(uint32_t index) {
	unsigned bitSize = sizeof(uint32_t) * 8;
	unsigned bit = index % bitSize;
	unsigned uint = index / bitSize;

	_bitField[uint] |= (1u << bit);
	_activeCount++;
}

void UsageRegister::unsetIndex(uint32_t index) {
	if (index >= _indexSize)
		return; 

	unsigned bitSize = sizeof(uint32_t) * 8;
	unsigned bit = index % bitSize;
	unsigned uint = index / bitSize;

	_bitField[uint] &= ~(1u << bit);
	_activeCount = (_activeCount > 0) ? _activeCount - 1 : 0;
}

void UsageRegister::setIndexes(std::set<uint32_t>& indexes) {
	unsigned bitSize = sizeof(uint32_t) * 8;
	unsigned bit;
	unsigned uint;

	for (auto& index : indexes) {
		bit = index % bitSize;
		uint = index / bitSize;
		_bitField[uint] |= (1u << bit);
	}

	_activeCount += indexes.size();
}

void UsageRegister::unsetIndexes(set<uint32_t>& indexes) {
	unsigned bitSize = sizeof(uint32_t) * 8;
	unsigned bit;
	unsigned uint;

	for (auto& index : indexes) {
		bit = index % bitSize;
		uint = index / bitSize;
		_bitField[uint] &= ~(1u << bit);
	}

	_activeCount -= indexes.size();
	_activeCount = (_activeCount < 0) ? 0 : _activeCount;
}