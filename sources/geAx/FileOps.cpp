/**
* @author Radek Blahos
* @project geAx Library
*
* @brief This file contains implementation of functions defined inside ge::Ax::File namespace
*/

#include <geAx\geAx.h>
#include <fstream>

using namespace ge::Ax;
using namespace std;

bool File::read(std::string filepath, uint8_t* storage, size_t size, bool binaryRead) {
	ios_base::openmode openHow = (binaryRead) ? (ios_base::in | ios_base::binary) : ios_base::in;
	fstream file(filepath, openHow);
	if (!file.is_open())
		return false;

	file.read((char*)storage, size);

	if (file.good()) {
		file.close();
		return true;
	}
	 
	file.close();
	return false;
}

bool File::write(std::string filepath, uint8_t* storage, size_t size, bool binaryWrite) {
	ios_base::openmode openHow = (binaryWrite) ? (ios_base::out | ios_base::binary) : ios_base::out;
	fstream file(filepath.c_str(), openHow);
	if (!file.is_open())
		return false;

	file.write((const char*)storage, size);
	if (file.fail()) {
		file.close();
		return false;
	}
		
	file.close();
	return true;
}

size_t File::size(std::string filepath) {
	fstream file(filepath.c_str(), fstream::in);
	if (!file.is_open())
		return false;

	file.seekg(0, fstream::end);
	size_t size = file.tellg();
	file.seekg(0, fstream::beg);
	file.close();

	return size;
}

size_t File::size(std::fstream file) {
	file.seekg(0, fstream::end);
	size_t size = file.tellg();
	file.seekg(0, fstream::beg);
	return size;
}

bool File::create(std::string name) {
	fstream file(name, ios_base::out);
	if (!file.is_open())
		return false;

	file.close();
	return true;
}