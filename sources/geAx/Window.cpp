#include <geAx\Window.h>
#include <iostream>

using namespace std;
using namespace ge::Ax;

Window::Window(string name, uint32_t width, uint32_t height) {
#ifdef _WIN32
	hInstance = GetModuleHandle(nullptr);
	hWnd = CreateWindow(name.c_str(), name.c_str(), WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, width, height, nullptr, nullptr, hInstance, nullptr);
	
	// excepction instead
	if (hWnd == nullptr) {
		cerr << "Cannot open display\n";
		return;
	}


#elif defined VK_USE_PLATFORM_XLIB_KHR	
	// prcat, dodelat to az mi nekdo vysvetli o co tam go XLIB I XCB
	// excepction instead
	dpy = XOpenDisplay(NULL);
	if (dpy == nullptr) {
		cerr << "Cannot open display\n";
		return;
	}
	//window = XCreateSimpleWindow(dpy, nullptr, 0, 0, width, height, 1, )

#elif defined VK_USE_PLATFORM_XCB_KHR	

#endif
}

Window::~Window() {
#ifdef _WIN32
	DestroyWindow(hWnd);

#elif defined VK_USE_PLATFORM_XLIB_KHR	


#elif defined VK_USE_PLATFORM_XCB_KHR	

#endif
}
