/**
* @author Radek Blahos
* @project geVkWidget Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <QtgeVkWidget\ConfigureLightWidget\ConfigureLightWidget.h>
#include <qcolordialog.h>
#include <glm\gtx\rotate_vector.hpp>

LightWidget::LightWidget() {
	// init components
	_mainLayout = new QGridLayout();

	_buttons.resize(3);
	for (int i = 0; i < _buttons.size(); i++)
		_buttons[i] = new QPushButton();

	_groupBoxes.resize(4);
	for (int i = 0; i < _groupBoxes.size(); i++)
		_groupBoxes[i] = new QGroupBox();
		
	_spinBoxes.resize(5);
	for (int i = 0; i < _spinBoxes.size(); i++)
		_spinBoxes[i] = new QSpinBox();

	_comboBoxes.resize(2);
	for (int i = 0; i < _comboBoxes.size(); i++)
		_comboBoxes[i] = new QComboBox();

	_labels.resize(4);
	for (int i = 0; i < _labels.size(); i++)
		_labels[i] = new QLabel();

	// seting up gui
	setLayout(_mainLayout);

	// set lighting mode
	_mainLayout->addWidget(_comboBoxes[0]);
	_comboBoxes[0]->addItems({ "None", "Phong", "Blinn" });

	// set light's source type
	_mainLayout->addWidget(_comboBoxes[1]);
	_comboBoxes[1]->addItems({ "Directional", "Point", "Flashlight" });

	// set color
	_mainLayout->addWidget(_buttons[0]);
	_buttons[0]->setText("Choose light's Color");

	// position set
	QHBoxLayout* positionLayout = new QHBoxLayout();
	_mainLayout->addWidget(_groupBoxes[0]);
	_groupBoxes[0]->setLayout(positionLayout);
	positionLayout->addWidget(_labels[0]);
	_labels[0]->setText("Set light's position:");
	positionLayout->addWidget(_spinBoxes[0]);
	positionLayout->addWidget(_spinBoxes[1]);
	positionLayout->addWidget(_spinBoxes[2]);
	//_spinBoxes[0]->setValue(1);
	_spinBoxes[0]->setRange(-100, 100);
	_spinBoxes[1]->setRange(-100, 100);
	_spinBoxes[2]->setRange(-100, 100);

	// direction set
	QHBoxLayout* directionLayout = new QHBoxLayout();
	_mainLayout->addWidget(_groupBoxes[2]);
	_groupBoxes[2]->setLayout(directionLayout);
	directionLayout->addWidget(_labels[2]);
	_labels[2]->setText("Choose light's direction:");
	directionLayout->addWidget(_spinBoxes[4]);
	QChar degreeChar(0260);
	_spinBoxes[4]->setSuffix(degreeChar);
	_spinBoxes[4]->setRange(0, 360);

	// set cutoff
	QHBoxLayout* cutoffLayout = new QHBoxLayout();
	_mainLayout->addWidget(_groupBoxes[1]);
	_groupBoxes[1]->setLayout(cutoffLayout);
	cutoffLayout->addWidget(_labels[1]);
	_labels[1]->setText("Choose flashlight's cutoff:");
	cutoffLayout->addWidget(_spinBoxes[3]);
	_spinBoxes[3]->setRange(0, 360);

	// confirm buttons
	QHBoxLayout* confirmActionLayout = new QHBoxLayout();
	_mainLayout->addWidget(_groupBoxes[3]);
	_groupBoxes[3]->setLayout(confirmActionLayout);
	confirmActionLayout->addWidget(_buttons[1]);
	_buttons[1]->setText("OK");
	confirmActionLayout->addWidget(_buttons[2]);
	_buttons[2]->setText("Storno");

	// signals and slots
	connect(_buttons[0], SIGNAL(clicked()), this, SLOT(chooseColorRoutine()));
	connect(_buttons[1], SIGNAL(clicked()), this, SLOT(okBtnClicked()));
	connect(_buttons[2], SIGNAL(clicked()), this, SLOT(closeBtnClicked()));
}

LightWidget::~LightWidget() {
	for (int i = 0; i < _buttons.size(); i++)
		delete _buttons[i];
	
	for (int i = 0; i < _spinBoxes.size(); i++)
		delete _spinBoxes[i];

	for (int i = 0; i < _comboBoxes.size(); i++)
		delete _comboBoxes[i];
	
	for (int i = 0; i < _groupBoxes.size(); i++)
		delete _groupBoxes[i];

	delete _mainLayout;
	//delete _chooseDirection; deleted with layout
}

void LightWidget::closeEvent(QCloseEvent* event) {
	hide();
}

void LightWidget::chooseColorRoutine() {
	_choosedColor = QColorDialog::getColor();
}

void LightWidget::okBtnClicked() {
	hide();
	emit configurationEnded();
}

void LightWidget::closeBtnClicked() {
	hide();
}

QVector3D LightWidget::getPosition() {
	return QVector3D(_spinBoxes[0]->value(), _spinBoxes[1]->value(), _spinBoxes[2]->value());
}

uint32_t LightWidget::getSourceType() {
	if (!_comboBoxes[1]->currentText().compare("Directional")) 
		return 0;
	
	else if (!_comboBoxes[1]->currentText().compare("Point"))
		return 1;

	else if (!_comboBoxes[1]->currentText().compare("Flashlight"))
		return 2;

	return -1;
}

uint32_t LightWidget::getLightingMode() {
	if (!_comboBoxes[0]->currentText().compare("None"))
		return 0;

	else if (!_comboBoxes[0]->currentText().compare("Phong"))
		return 1;

	else if (!_comboBoxes[0]->currentText().compare("Blinn"))
		return 2;

	return -1;
}

void LightWidget::setPosition(float x, float y, float z) {
	_spinBoxes[0]->setValue(x);
	_spinBoxes[1]->setValue(y);
	_spinBoxes[2]->setValue(z);
}