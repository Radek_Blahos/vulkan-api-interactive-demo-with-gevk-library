/**
* @author Radek Blahos
* @project geVkWidget Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <QtgeVkWidget\geVkWidget.h>
#include <QtgeVkWidget\geVkWindow\geVkWindow.h>
#include <QtgeVkWidget\geVkRenderer\QgeVkRenderer.h>
#include <geAx\Exception.h>
#include <geVk\Instance.h>
#include <QtImageLoader.h>
#include <qcolor.h>
#include <qcolordialog.h>
#include <qfiledialog.h>
#include <QResizeEvent>
#include <glm\gtx\rotate_vector.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm\vec3.hpp>
#include <glm\mat4x4.hpp>
#include <QtgeVkWidget/Animations/Animations.h>
//#include <InputHandler.h>

geVkWidget::geVkWidget(uint32_t width, uint32_t height, std::string name, QWidget* parent) : QWidget(parent) {
	// central widget layout
	_widgetLayout = new QVBoxLayout();
	// set layout to this widget
	setLayout(_widgetLayout);

	// window creation
	_window = new geVkWindow(width, height, name);
	// add QVkRenderer to this widget's layout
	_widgetLayout->addWidget(_window->transformToWidget(this));

	// Renderer Init
	_renderer = new QgeVkRenderer();

	// thread init
	_workerThread = new QThread();

	_qInstance = new QVulkanInstance();
	_qInstance->setVkInstance(_renderer->instance);
	_qInstance->create();
	_window->setVulkanInstance(_qInstance);
	_window->create(); // must create because need for surface

	// nastavim surface pro renderer
	_renderer->presentSurface = QVulkanInstance::surfaceForWindow(_window);

	// light configurator
	_lightConfigurator = new LightWidget();
	
	// configure renderer widget
	_configureRendererWidget = new ConfigRendererWidget();
	
	//--- Connect Signals n Slots --- //
	// config renderer widget response
	connect(_configureRendererWidget, SIGNAL(configurationEnded()), this, SLOT(afterConfigInit()));
	// update window upon rendering completion
	connect(_renderer, SIGNAL(drawCompleted()), _window, SLOT(updateWindow()));
	// redraw request with rendering routine
	connect(this, SIGNAL(redrawRequest(geVkScene*)), _renderer, SLOT(renderNow(geVkScene*))); // renderNow inside QgeVkRenderer.h !
	// window resize events
	connect(this, SIGNAL(resizeSwapchain(int, int)), _renderer, SLOT(resizeSwapchain(int, int)));
	// light config
	connect(_lightConfigurator, SIGNAL(configurationEnded()), this, SLOT(extractLightInfo()));
	// set apps resources
	connect(_configureRendererWidget, SIGNAL(resourceFolderChoosed(QString)), this, SLOT(setResourcesFolderToRenderer(QString)));

	// continuous redraw
	//connect(_window, SIGNAL(windowUpdated()), this, SLOT(continuousRedraw()));
	// input 
	//connect(_window, SIGNAL(checkInput()), this, SLOT(processInput()));	

	// here open configure app widget
	_configureRendererWidget->exec();

}

void geVkWidget::afterConfigInit() {
	int useRoutine = _configureRendererWidget->getChoosedRoutine();
	geVkRenderer::InitInfo initInfo { 
		_window->width(),
		_window->height(),
		_configureRendererWidget->getReqThreadsCount(),
		_configureRendererWidget->getReqQueuesCount(),
		useRoutine,
		_configureRendererWidget->getReqSyncPrimitive() || useRoutine != -2, // zajisti aby vzdy sg mohl pracovat s Fences v ramci cmd buffers
	};
	_renderer->initialize(initInfo);

	// move renderer to another thread
	_renderer->moveToThread(_workerThread);

	// scene struct init -> keeps the same for all scenes
	geVkSceneCreateInfo sceneCI {
		&_renderer->memManager,
		&_renderer->commandProcessor,
		&*_renderer->componentsDB.commandBufferSets.back(),
		&geVkWidget::loadImages
	};
	// init scene object, not any scene loaded yet
	_scene = new geVkScene(sceneCI);

	// cleanup config widget
	delete _configureRendererWidget;

	// animations
	_usedAnimations = new Animations();

	// start renderer's thread
	_workerThread->start();
}

geVkWidget::~geVkWidget() {
	delete _workerThread;
	// before dtoring vulkan objects must wait for all operations to be done
	vkDeviceWaitIdle(_renderer->device);
	// before destroying window, must destroy swapchain
	_renderer->swapchain.dismantle();
	delete _window;
	delete _scene;
	delete _renderer;
	delete _qInstance;
	delete _lightConfigurator;
	delete _usedAnimations;
	// delete _widgetLayout; QWidget object deletes it
}

void geVkWidget::closeEvent(QCloseEvent* event) {
	_workerThread->quit();
	_workerThread->wait();

	QWidget::closeEvent(event);
}

void geVkWidget::resizeEvent(QResizeEvent* event) {
	_window->resize(this->size());
	if (getRenderer().useRoutineVersion() == -3) {
		_renderer->resizeSwapchain(event->size().width(), event->size().height());
		_renderer->recordCommandBuffers(_scene);
	}
	else
		emit resizeSwapchain(event->size().width(), event->size().height());

	emit redrawRequest(_scene);
}

void geVkWidget::loadScene() {
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open Scene File"),
	"/home",
	tr("Scenes (*.*)"));

	// if there is non scene to process...
	if (fileName.size() < 1)
		return;

	_scene->processScene(fileName.toStdString(), _renderer->useRoutineVersion() == -3); 
	
	// set scene's transformation and camera
	_renderer->getFOV() = 120.0f;
	_renderer->getProjectionMatrix() = glm::perspectiveFovLH(_renderer->getFOV(), (float)_window->width(), (float)_window->height(), 0.01f, 100.0f);
	_scene->sceneCamera.lookAt({ 0, 0.05, 4 }, { 0, 0.05, 1 }, { 0, 1, 0 });
	_scene->sceneLight.sourceType = 95;
	_scene->sceneLight.lightingMode = 0;
	_scene->sceneLight.position = { 0.0f, 1.0f, 1.0f };
	_scene->sceneLight.usedModel = &*_scene->activeScene->models.back();

	if (_renderer->useRoutineVersion() == -3) {
		/* Update descriptor sets */
		_renderer->updateDescriptorsSG();

		// record buffer for scene
		_renderer->recordCommandBuffers(_scene);
	}

	// start rendering
	emit redrawRequest(_scene); // is emited with resize event... :(

}

void geVkWidget::setUniformColor() {
	QColor color = QColorDialog::getColor();
	// uzivatel zadal barvu
	if (color.isValid()) {
		int r, g, b;
		color.getRgb(&r, &g, &b);
		_renderer->getPushConstants().uniformColor.r = r / 255;
		_renderer->getPushConstants().uniformColor.g = g / 255;
		_renderer->getPushConstants().uniformColor.b = b / 255;
	}

	emit redrawRequest(_scene);
}

void geVkWidget::configureLight() {
	_lightConfigurator->setPosition(_scene->sceneLight.position.x, _scene->sceneLight.position.y, _scene->sceneLight.position.z);
	_lightConfigurator->show();
}

void geVkWidget::extractLightInfo() {
	// lighting mode
	_scene->sceneLight.lightingMode = _lightConfigurator->getLightingMode();

	// source type
	_scene->sceneLight.sourceType = _lightConfigurator->getSourceType();

	// color
	QColor color = _lightConfigurator->getColor();
	_scene->sceneLight.color.r = color.red() / 255;
	_scene->sceneLight.color.g = color.green() / 255;
	_scene->sceneLight.color.b = color.blue() / 255;

	// position
	QVector3D position = _lightConfigurator->getPosition();
	_scene->sceneLight.position.x = position.x();
	_scene->sceneLight.position.y = position.y();
	_scene->sceneLight.position.z = position.z();

	// direction
	float directionAngle = _lightConfigurator->getDirection(); // x - axis rotated over z - axis on directionAngle degs...
	_scene->sceneLight.direction = glm::rotateZ(glm::vec3(1, 0, 0), directionAngle);

	// cutoff
	_scene->sceneLight.cutOff = _lightConfigurator->getCutoff();

	// update scene
	emit redrawRequest(_scene);
}

void geVkWidget::rotateCamera(char direction) {
	float rotate = 0.46;

	if (operateWith == -2)
		_scene->sceneCamera.getEyeCoords() -= _scene->sceneCamera.getCenterCoords();

	switch (direction) {
		case 'r': {
			if (operateWith == -2) {
				_scene->sceneCamera.getEyeCoords() = glm::rotate(_scene->sceneCamera.getEyeCoords(), -rotate, _scene->sceneCamera.getYaxis());
			}
			else {
				_scene->sceneLight.position = glm::rotate(_scene->sceneLight.position, rotate, _scene->sceneCamera.getYaxis());
			}
		} break;
		case 'l': {
			if (operateWith == -2) {
				_scene->sceneCamera.getEyeCoords() = glm::rotate(_scene->sceneCamera.getEyeCoords(), rotate, _scene->sceneCamera.getYaxis());
			}
			else {
				_scene->sceneLight.position = glm::rotate(_scene->sceneLight.position, -rotate, _scene->sceneCamera.getYaxis());
			}
		} break;
		case 'u': {
			if (operateWith == -2) {
				_scene->sceneCamera.getEyeCoords() = glm::rotate(_scene->sceneCamera.getEyeCoords(), -rotate, _scene->sceneCamera.getXaxis());
			}
			else {
				_scene->sceneLight.position = glm::rotate(_scene->sceneLight.position, -rotate, _scene->sceneCamera.getXaxis());
			}
		} break;
		case 'd': {
			if (operateWith == -2) {
				_scene->sceneCamera.getEyeCoords() = glm::rotate(_scene->sceneCamera.getEyeCoords(), rotate, _scene->sceneCamera.getXaxis());
			}
			else {
				_scene->sceneLight.position = glm::rotate(_scene->sceneLight.position, rotate, _scene->sceneCamera.getYaxis());
			}
		} break;
	}

	if (operateWith == -2) {
		// posunu bod na puvodni vzdalenost
		_scene->sceneCamera.getEyeCoords() += _scene->sceneCamera.getCenterCoords();

		// update view projection
		_scene->sceneCamera.lookAt(_scene->sceneCamera.getEyeCoords(), _scene->sceneCamera.getCenterCoords(), _scene->sceneCamera.getYaxis());
	}
	
	// reDraw scene
	emit redrawRequest(_scene);

}

void geVkWidget::moveCamera(char direction) {
	float translate = 0.27; 
	switch (direction) {
		case 'r': {
			if (operateWith == -2) {
				_scene->sceneCamera.getEyeCoords() += _scene->sceneCamera.getXaxis() * translate;
				_scene->sceneCamera.getCenterCoords() += _scene->sceneCamera.getXaxis() * translate;
			}
			else {
				_scene->sceneLight.position += _scene->sceneCamera.getXaxis() * translate;
			}
		} break;
		case 'l': {			
			if (operateWith == -2) {
				_scene->sceneCamera.getEyeCoords() -= _scene->sceneCamera.getXaxis() * translate;
				_scene->sceneCamera.getCenterCoords() -= _scene->sceneCamera.getXaxis() * translate;
			}
			else {
				_scene->sceneLight.position -= _scene->sceneCamera.getXaxis() * translate;
			}
		} break;
		case 'u': {
			if (operateWith == -2) {
				_scene->sceneCamera.getEyeCoords() -= _scene->sceneCamera.getYaxis() * translate;
				_scene->sceneCamera.getCenterCoords() -= _scene->sceneCamera.getYaxis() * translate;
			}
			else {
				_scene->sceneLight.position -= _scene->sceneCamera.getYaxis() * translate;
			}			
		} break;
		case 'd': {
			if (operateWith == -2) {
				_scene->sceneCamera.getEyeCoords() += _scene->sceneCamera.getYaxis() * translate;
				_scene->sceneCamera.getCenterCoords() += _scene->sceneCamera.getYaxis() * translate;
			}
			else {
				_scene->sceneLight.position += _scene->sceneCamera.getYaxis() * translate;
			}			
		} break;
		case 'f': {
			if (operateWith == -2) {
				_scene->sceneCamera.getEyeCoords() += _scene->sceneCamera.getZaxis() * translate;
				_scene->sceneCamera.getCenterCoords() += _scene->sceneCamera.getZaxis() * translate;
			}
			else {
				_scene->sceneLight.position += _scene->sceneCamera.getZaxis() * translate;
			}		
		} break;
		case 'b': {
			if (operateWith == -2) {
				_scene->sceneCamera.getEyeCoords() -= _scene->sceneCamera.getZaxis() * translate;
				_scene->sceneCamera.getCenterCoords() -= _scene->sceneCamera.getZaxis() * translate;
			}
			else {
				_scene->sceneLight.position -= _scene->sceneCamera.getZaxis() * translate;
			}
		} break;
	}

	// update view projection
	if (operateWith == -2)
		_scene->sceneCamera.lookAt(_scene->sceneCamera.getEyeCoords(), _scene->sceneCamera.getCenterCoords(), _scene->sceneCamera.getYaxis());

	// reDraw scene
	emit redrawRequest(_scene);
}

void geVkWidget::setFOV(int value) {
	_renderer->getFOV() = value;
	_renderer->getProjectionMatrix() = glm::perspectiveFovLH(_renderer->getFOV(), (float)_window->width(), (float)_window->height(), 0.01f, 100.0f);
	emit redrawRequest(_scene);
}


QWindow& geVkWidget::getWindow() {
	return *_window;
}

void geVkWidget::runBenchmark() {
	_renderer->benchmarkDraw(_scene);
}

void geVkWidget::setResourcesFolderToRenderer(QString path) {
	_renderer->resourcesFolder = new string(path.toStdString());
}

// img load funkce z QtgeSG Example
void geVkWidget::loadImages(ge::sg::Model& model, std::string& imageDir) {
	for (std::shared_ptr<ge::sg::Material> material : model.materials) {
		for (std::vector<std::shared_ptr<ge::sg::MaterialComponent>>::iterator it = material->materialComponents.begin(); it != material->materialComponents.end(); ++it) {
			if ((*it)->getType() == ge::sg::MaterialComponent::ComponentType::IMAGE) {
				ge::sg::MaterialImageComponent *img = dynamic_cast<ge::sg::MaterialImageComponent*>((*it).get());
				//cout << img->semantic << " " << img->filePath << endl;
				std::string textFile(imageDir + img->filePath);
				std::shared_ptr<QtImage> image(QtImageLoader::loadImage(textFile.c_str()));
				if (image == nullptr) 
					throw ge::Ax::Exception::Bad::Load("geVkWidget::loadImages -> Qt cannot load image with this format.");
				else 
					img->image = image;
			}
		}
	}
}
