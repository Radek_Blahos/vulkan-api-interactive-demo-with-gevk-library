cmake_minimum_required(VERSION 3.10.1)

project(geVk)

SET(CMAKE_CXX_STANDARD 14)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_INCLUDE_CURRENT_DIR_IN_INTERFACE ON)

set(MAJOR_VERSION 1)
set(MINOR_VERSION 0)
set(REVISION_VERSION 0)

include(GenerateExportHeader)

set(LIB_NAME QtgeVkWidget)

set(HEADER_PATH ${geVkDemo_SOURCE_DIR}/include/${LIB_NAME})

# set and export glsl code dir
file(GLOB SHADER_SOURCE_FILES 
    "${CMAKE_CURRENT_SOURCE_DIR}/geVkRenderer/GLSL/*.frag"
    "${CMAKE_CURRENT_SOURCE_DIR}/geVkRenderer/GLSL/*.vert"
	"${CMAKE_CURRENT_SOURCE_DIR}/geVkRenderer/GLSL/*.gsh" # geometry shader
    "${CMAKE_CURRENT_SOURCE_DIR}/geVkRenderer/GLSL/*.tsh" # tesselation shader
	"${CMAKE_CURRENT_SOURCE_DIR}/geVkRenderer/GLSL/*.csh" # compute shader
)

set (GLSL_SOURCE_FILES ${SHADER_SOURCE_FILES} PARENT_SCOPE) 

file(GLOB_RECURSE INCLUDES
		"${HEADER_PATH}/*.h"
		"${HEADER_PATH}/*.inl"
	)
	
file(GLOB_RECURSE SOURCES
		"*.cpp"
	)	
	
# Tell CMake to run moc when necessary:
set(CMAKE_AUTOMOC ON)
# As moc files are generated in the binary dir, tell CMake
# to always look for includes there:
set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(Qt5 COMPONENTS Widgets Core)

#find Qt Image Loader
find_package(QtImageLoader HINTS ${geVkDemo_SOURCE_DIR}/geAd/QtImageLoader/cmake)

#find assimp loader
find_package(AssimpModelLoader HINTS ${geVkDemo_SOURCE_DIR}/geAd/AssimpModelLoader/cmake)

#find glm
find_package(glm)

find_package(vulkan)

# add geVk
add_subdirectory(../geVk ${CMAKE_CURRENT_BINARY_DIR}/geVk)
add_subdirectory(../geSG ${CMAKE_CURRENT_BINARY_DIR}/geSG)
#add_subdirectory(../geUtil ${CMAKE_CURRENT_BINARY_DIR}/geUtil)
#add_subdirectory(../geCore ${CMAKE_CURRENT_BINARY_DIR}/geCore)

################################################
# PACKAGES
add_library(${LIB_NAME} STATIC ${SOURCES} ${INCLUDES})
add_library(${LIB_NAME}::${LIB_NAME} ALIAS ${LIB_NAME})

################################################
set(External_libs Vulkan::Vulkan Qt5::Core Qt5::Gui Qt5::Widgets QtImageLoader AssimpModelLoader glm)
set(Internal_deps geAx geVk geCore geUtil geSG geUtil)

target_link_libraries(${LIB_NAME} PUBLIC ${Internal_deps} ${External_libs})
set_target_properties(${LIB_NAME} PROPERTIES LINKER_LANGUAGE C11)

target_include_directories(${LIB_NAME} PUBLIC $<INSTALL_INTERFACE:include>)
target_include_directories(${LIB_NAME} PUBLIC $<BUILD_INTERFACE:${geVkDemo_SOURCE_DIR}/include/>)
target_include_directories(${LIB_NAME} PUBLIC $<BUILD_INTERFACE:${Qt5Widgets_INCLUDE_DIRS}>)
target_include_directories(${LIB_NAME} PUBLIC $<BUILD_INTERFACE:${Qt5Core_INCLUDE_DIRS}>)
target_include_directories(${LIB_NAME} PUBLIC $<BUILD_INTERFACE:${Qt5Gui_INCLUDE_DIRS}>)

# set default resource path
set(DEFAULT_RESOERCES_PATH "${PROJECT_BINARY_DIR}/bin")
set(APP_RESOURCES "${DEFAULT_RESOERCES_PATH}" CACHE PATH "Relative or absolute path to Application resources.")
set_target_properties(${LIB_NAME} PROPERTIES COMPILE_DEFINITIONS "APP_RESOURCES=\"${APP_RESOURCES}\"")
