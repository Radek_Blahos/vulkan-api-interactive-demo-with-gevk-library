/**
* @author Radek Blahos
* @project geVkWidget Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <QtgeVkWidget\geVkScene\geVkMesh.h>

geVkMesh::~geVkMesh() {
	for (auto& buffer : buffers)
		delete buffer.second;
}