/**
* @author Radek Blahos
* @project geVkWidget Library
*
* @brief This file contains implementation of Camera's methods
*/

#include <QtgeVkWidget\geVkScene\Camera.h>

using namespace glm;
using namespace std;

void Camera::lookAt(glm::vec3 eye, glm::vec3 center, glm::vec3 up, bool rhSystem) {
	_eye = eye;
	_center = center;

	if (rhSystem) {
		_zAxis = eye + (center * -1.0f);
		_zAxis = glm::normalize(_zAxis); 
		up = glm::normalize(up);
		_xAxis = glm::cross(_zAxis, up);
		_xAxis = glm::normalize(_xAxis);
		_yAxis = glm::cross(_xAxis, _zAxis); 
	}
	else {
		_zAxis = center + (eye * -1.0f);
		_zAxis = glm::normalize(_zAxis);
		up = glm::normalize(up);
		_xAxis = glm::cross(up, _zAxis);
		_xAxis = glm::normalize(_xAxis);
		_yAxis = glm::cross(_zAxis, _xAxis);
	}

	_viewTr = glm::mat4(1.0);

	// view projection mat creation
	_viewTr[0][0] = _xAxis.x;
	_viewTr[1][0] = _xAxis.y;
	_viewTr[2][0] = _xAxis.z;
	_viewTr[0][1] = _yAxis.x;
	_viewTr[1][1] = _yAxis.y;
	_viewTr[2][1] = _yAxis.z;
	_viewTr[0][2] = _zAxis.x;
	_viewTr[1][2] = _zAxis.y;
	_viewTr[2][2] = _zAxis.z;
	_viewTr[3][0] = -glm::dot(_xAxis, eye);
	_viewTr[3][1] = -glm::dot(_yAxis, eye);
	_viewTr[3][2] = (rhSystem) ? glm::dot(_zAxis, eye) : -glm::dot(_zAxis, eye);

}