/**
* @author Radek Blahos
* @project geVkWidget Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <QtgeVkWidget\geVkScene\geVkScene.h>
#include <geVk\CommandBufferSet.h>
#include <geVk\CommandBuffer.inl>
#include <geVk\Image.inl>
#include <geAx\ExceptionMacros.h>
#include <geAx\AuxInlines.inl>
#include <geSG\Material.h>

using namespace ge::Ax;
using namespace ge::Vk;
using namespace std;

geVkScene::~geVkScene() {
	delete activeScene;
	delete _info;

	if (_useUniformLoad) {
		// remove shared buffers for all attributes
		for (auto& attribute : gpuMeshes.begin()->second.buffers)
			delete attribute.second;

		// null their pointers inside vertexInput struct to not segfault on unalowed free
		for (auto& x : gpuMeshes) {
			for (auto& buffer : x.second.buffers)
				buffer.second = nullptr;
		}
	}
}

void geVkScene::processScene(std::string path, bool useUniformLoad) {
	_useUniformLoad = useUniformLoad;
	// cleanup if needed 
	if (activeScene != nullptr) {
		gpuMeshes.clear();
		gpuTextures.clear();
		delete activeScene;
	}
	
	activeScene = AssimpModelLoader::loadScene(path.c_str());
	if (activeScene == nullptr) 
		throw Exception::Bad::Load("geVkScene: processScene -> Cannot load a scene.");
	
	// obtain directory path 
	int idx = path.find_last_of('/');
	// if (idx == string::npos) no need to check, if file path was invalid previous throw will be executed...
	string dirPath(path.begin(), path.begin() + idx + 1); // get path without scene name... +1 because need to keep "/"

	try {
		// load images from which textures will be created
		for (auto model : activeScene->models)
			_info->loadImages(*model, dirPath);

		// process meshes
		vector<ge::sg::AttributeDescriptor::Semantic> load = { ge::sg::AttributeDescriptor::Semantic::position, ge::sg::AttributeDescriptor::Semantic::normal, ge::sg::AttributeDescriptor::Semantic::texcoord, ge::sg::AttributeDescriptor::Semantic::indices };
		processMeshes(load, useUniformLoad);

		// create textures
		vector<ge::sg::MaterialImageComponent::Semantic> loadT = { ge::sg::MaterialImageComponent::Semantic::diffuseTexture };
		storeTextures(loadT);

		// assign textures to meshes
		distributeTextureRefs();

	}
	catch (ge::Ax::Exception::Base& exepction) {
		std::cerr << exepction.what() << endl;
		CATCHEXIT(-1);
	}

	// scene loaded 
}

void geVkScene::addModelToScene(std::string path) {
	// no active scene currently created
	if (activeScene == nullptr) {
		processScene(path);
		return;
	}
		
	// if scene created add model to it
	AssimpModelLoader::addModelToScene(path, activeScene);

	// now need to add it inside geVkScene
	// obtain directory path 
	int idx = path.find_last_of('/');
	string dirPath(path.begin(), path.begin() + idx + 1); // get path without scene name... +1 because need to keep "/"

	try {
		// load images from which textures will be created
		_info->loadImages(*activeScene->models.back(), dirPath);
		
		// process meshes
		vector<ge::sg::AttributeDescriptor::Semantic> load = { ge::sg::AttributeDescriptor::Semantic::position, ge::sg::AttributeDescriptor::Semantic::normal, ge::sg::AttributeDescriptor::Semantic::texcoord, ge::sg::AttributeDescriptor::Semantic::indices };
		processMeshes(*activeScene->models.back(), load);

		// create textures
		vector<ge::sg::MaterialImageComponent::Semantic> loadT = { ge::sg::MaterialImageComponent::Semantic::diffuseTexture };
		storeTextures(*activeScene->models.back(), loadT);

		std::unordered_map<ge::sg::Mesh*, geVkMesh>::iterator x = gpuMeshes.end();
		x--;

		// assign textures to meshes
		distributeTextureRefs(x);

	}
	catch (ge::Ax::Exception::Base& exepction) {
		std::cerr << exepction.what() << endl;
		CATCHEXIT(-1);
	}

}

void geVkScene::processMeshes(std::vector<ge::sg::AttributeDescriptor::Semantic>& loadAttributes, bool useUniformLoad) {
	uint32_t currentIndicesOffset(0);
	uint32_t currentVertexOffset(0);
	for (auto model : activeScene->models) {
		if (useUniformLoad) 
			processMeshesUniformModels(*model, loadAttributes, currentIndicesOffset, currentVertexOffset);
		
		else
			processMeshes(*model, loadAttributes);
	}
}

void geVkScene::processMeshes(ge::sg::Model& model, std::vector<ge::sg::AttributeDescriptor::Semantic>& loadAttributes) {
	for (auto mesh : model.meshes) {
		int memorySize = 0;

		// create new geVkMesh object
		gpuMeshes[mesh.get()];

		// loop where maximal memory of all buffers is computed...
		for (auto& x : loadAttributes) {
			auto& ptr = mesh->getAttribute(x);
			if (ptr == nullptr) {
				if (x == ge::sg::AttributeDescriptor::Semantic::normal)
					throw ge::Ax::Exception::Bad::Load("geVkScene::processMeshes -> models inside scene dont have all requested attributes");
				else 
					continue;
			}

			memorySize += ptr->size;
		}

		for (unsigned i = 0; i < loadAttributes.size(); i++) {
			auto& attribute = mesh->getAttribute(loadAttributes[i]);
			if (attribute == nullptr)
				continue;

			// create new vertex input inside mesh
			gpuMeshes[mesh.get()].buffers[loadAttributes[i]] = new ge::Vk::Utils::VertexBuffer();

			// initialize it properly
			auto& vertIn = gpuMeshes[mesh.get()].buffers[loadAttributes[i]];
			vertIn->description = new Info::Pipe::VertexInputDescription();
			*vertIn->description = convertFrom(*attribute);
			vertIn->description->location = i; 
			CreateInfo::Buffer bufferCI;
			// pri prvni alokaci pro buffer naalokuju pamet pro vsechny buffery dane mesh, a memManager mi v ni pak priradi pamet 
			if (i == 0)
				bufferCI.allocationSize = memorySize;

			bufferCI.size = attribute->size;
			bufferCI.propertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
			bufferCI.pManager = _info->pManager;
			switch (attribute->semantic) {
				case ge::sg::AttributeDescriptor::Semantic::normal:
					bufferCI.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
					break;

				case ge::sg::AttributeDescriptor::Semantic::position:
					bufferCI.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
					break;

				case ge::sg::AttributeDescriptor::Semantic::texcoord:
					bufferCI.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
					break;

				case ge::sg::AttributeDescriptor::Semantic::color:
				case ge::sg::AttributeDescriptor::Semantic::tangent:
				case ge::sg::AttributeDescriptor::Semantic::binormal:
					bufferCI.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
					break;

				case ge::sg::AttributeDescriptor::Semantic::indices:
					bufferCI.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
					break;
			}
			// init buffer and copy data from ge::sg::attribute
			vertIn->data = new Buffer(bufferCI);
			vector<uint8_t> vecData(bufferCI.size);
			memcpy(vecData.data(), attribute->data.get(), bufferCI.size);

			// obtain cmd buff
			CommandBuffer& keepComands = _info->pCmdSet->getNextBuffer();

			// fills this buffer with data provided in vector
			vertIn->data->stashOnGPU(vecData, keepComands, *_info->pCmdProcessor);

			_info->pCmdSet->returnCurrentBuffer();
		}
	}
}

void geVkScene::processMeshesUniformModels(ge::sg::Model& model, std::vector<ge::sg::AttributeDescriptor::Semantic>& loadAttributes, uint32_t& currentIndicesOffset, uint32_t& currentVertexOffset) {
	int memorySize = 0;

	// calculate mem reqs for attributes
	for (auto mesh : model.meshes) {
		// loop where maximal memory of all buffers is computed...
		for (auto& x : loadAttributes) {
			auto& ptr = mesh->getAttribute(x);
			if (ptr == nullptr) {
				if (x == ge::sg::AttributeDescriptor::Semantic::normal)
					throw ge::Ax::Exception::Bad::Load("geVkScene::processMeshesUniformModels -> models inside scene dont have all requested attributes");
				else
					continue;
			}

			memorySize += ptr->size;
			attribBufferSize[x].allocSize += ptr->size;
		}
	}

	for (unsigned i = 0; i < loadAttributes.size(); i++) {
		attribBufferSize[loadAttributes[i]].vertexBuffPtr = new ge::Vk::Utils::VertexBuffer();
		// since all models should share same attributes, description of vertex buffer can be created from first one
		if (model.meshes.size() == 0)
			break;

		auto mesh = &*model.meshes.front();
		auto attribute = mesh->getAttribute(loadAttributes[i]);
		// init it properly
		auto& vertIn = attribBufferSize[loadAttributes[i]].vertexBuffPtr;
		vertIn->description = new Info::Pipe::VertexInputDescription();
		*vertIn->description = convertFrom(*attribute);
		vertIn->description->location = i;
		CreateInfo::Buffer bufferCI;
		// pri prvni alokaci pro buffer naalokuju pamet pro vsechny buffery dane mesh, a memManager mi v ni pak priradi pamet 
		if (i == 0)
			bufferCI.allocationSize = memorySize;

		bufferCI.size = attribBufferSize[loadAttributes[i]].allocSize;
		bufferCI.propertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
		bufferCI.pManager = _info->pManager;
		switch (attribute->semantic) {
			case ge::sg::AttributeDescriptor::Semantic::normal:
				bufferCI.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
				break;

			case ge::sg::AttributeDescriptor::Semantic::position:
				bufferCI.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
				break;

			case ge::sg::AttributeDescriptor::Semantic::texcoord:
				bufferCI.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
				break;

			case ge::sg::AttributeDescriptor::Semantic::color:
			case ge::sg::AttributeDescriptor::Semantic::tangent:
			case ge::sg::AttributeDescriptor::Semantic::binormal:
				bufferCI.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
				break;

			case ge::sg::AttributeDescriptor::Semantic::indices:
				bufferCI.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
				break;
		}
		// init buffer
		vertIn->data = new Buffer(bufferCI);

		// alloc size is recicled for offset counting inside next stage...
		attribBufferSize[loadAttributes[i]].allocSize = 0;
	}

	int index(currentVertexOffset);
	int indicesOffset(currentIndicesOffset);
	for (auto mesh : model.meshes) {
		// create new geVkMesh object
		gpuMeshes[mesh.get()];

		// assign proper index to it
		gpuMeshes[mesh.get()].offset = indicesOffset; // incing index with it

		for (unsigned i = 0; i < loadAttributes.size(); i++) {
			auto& attribute = mesh->getAttribute(loadAttributes[i]);
			if (attribute == nullptr)
				continue;

			// assign proper vertex buffer to mesh
			gpuMeshes[mesh.get()].buffers[loadAttributes[i]] = attribBufferSize[loadAttributes[i]].vertexBuffPtr;
			
			// copy data from ge::sg::attribute
			vector<uint8_t> vecData(attribute->size);
			memcpy(vecData.data(), attribute->data.get(), attribute->size);

			// for indices must be corrected added offset to be rendered properly
			if (loadAttributes[i] == ge::sg::AttributeDescriptor::Semantic::indices) {
				uint32_t* view = (uint32_t*) vecData.data();
				uint32_t stop = vecData.size() / sizeof(uint32_t);
				for (uint32_t i = 0; i < stop; i++) 
					view[i] += index;
				
			}

			// obtain cmd buff
			CommandBuffer& keepComands = _info->pCmdSet->getNextBuffer();

			// fills this buffer with data provided in vector
			gpuMeshes[mesh.get()].buffers[loadAttributes[i]]->data->stashOnGPU(vecData, keepComands, *_info->pCmdProcessor, attribBufferSize[loadAttributes[i]].allocSize);
			attribBufferSize[loadAttributes[i]].allocSize += vecData.size();

			_info->pCmdSet->returnCurrentBuffer();
		}

		index += mesh.get()->count; // next model will start on index index + model mesh count * this mesh vertex count.
		indicesOffset += mesh.get()->indicesCount;
	}
	currentVertexOffset = index;
	currentIndicesOffset = indicesOffset;
}

void geVkScene::storeTextures(std::vector<ge::sg::MaterialImageComponent::Semantic>& loadTextures) {
	for (auto model : activeScene->models) 
		storeTextures(*model, loadTextures);
	
}

void geVkScene::storeTextures(ge::sg::Model& model, std::vector<ge::sg::MaterialImageComponent::Semantic>& loadTextures) {
	for (auto& material : model.materials) {
		//continue if we already processed this material
		for (auto& materialComponent : material->materialComponents) {
			if (materialComponent->getType() == ge::sg::MaterialComponent::ComponentType::IMAGE) {
				ge::sg::MaterialImageComponent *img = static_cast<ge::sg::MaterialImageComponent*>(materialComponent.get());
				// jestli se ma dany typ textury nahrat
				if (!ge::Ax::in(img->semantic, loadTextures.data(), loadTextures.size()))
					continue; // if not -->> skip

				 //if the image of MaterialImageComponent hasn't been created
				if (img && !img->image) {
					throw ge::Ax::Exception::Bad::Load("geVkScene::storeTextures -> No data provided for an image");
				}

				// map material component to string, for searching
				gpuTextures[img] = img->filePath;

				// pokud byl img jiz nahran, tak se jeho nahrani preskoci
				if (texStorage.find(img->filePath) != texStorage.end())
					continue;

				// buffer texture into GPU
				CreateInfo::Image imageCI;
				imageCI.pManager = _info->pManager;
				imageCI.currentLayout = VK_IMAGE_LAYOUT_UNDEFINED;
				imageCI.width = img->image->getWidth();
				imageCI.height = img->image->getHeight();
				imageCI.tiling = VK_IMAGE_TILING_OPTIMAL;
				imageCI.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
				imageCI.memFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

				// assignes image format from ge::sg::Image::Format -> VkFormat
				imageCI.formatSize = assignFormat(img->image->getFormat(), imageCI.format);

				// create default geVkImage object and create it
				texStorage[img->filePath].create(imageCI, true);

				// copy data to image object
				vector<uint8_t> vecData(imageCI.width * imageCI.height * imageCI.formatSize); // potrebuju tam pridat * data size (uint32_t) aby to fungovalo!!!!
				memcpy(vecData.data(), img->image->getBits(), vecData.size());

				// obtain cmd buff
				CommandBuffer& keepComands = _info->pCmdSet->getNextBuffer();

				// must change layout to optimal for memcopy
				Info::Image::ChangeLayout changeI(Info::Image::ChangeLayout::TransferOp::LOADING_TEX);
				texStorage[img->filePath].changeLayout(keepComands, changeI, *_info->pCmdProcessor);

				// fills this buffer with data provided in vector
				texStorage[img->filePath].stashOnGPU(vecData, keepComands, *_info->pCmdProcessor);

				// changing layout to optimal for shader reading
				changeI = Info::Image::ChangeLayout(Info::Image::ChangeLayout::TransferOp::USE_TEX);
				texStorage[img->filePath].changeLayout(keepComands, changeI, *_info->pCmdProcessor);

				_info->pCmdSet->returnCurrentBuffer();
			}
		}
	}
}

void geVkScene::distributeTextureRefs() {
	// projdu vsechny mesh a upravim jejich ref na textury
	for (auto& mesh : gpuMeshes) {
		// prochazim textury
		for (auto& texRef : mesh.first->material->materialComponents) {
			if (texRef->getType() != ge::sg::MaterialComponent::ComponentType::IMAGE)
				continue;

			ge::sg::MaterialImageComponent* texture = (ge::sg::MaterialImageComponent*) texRef.get();
			mesh.second.usedTextures[texture->semantic] = &texStorage[gpuTextures[texture]];
		}
	}
}

void geVkScene::distributeTextureRefs(std::unordered_map<ge::sg::Mesh*, geVkMesh>::iterator& fromMesh) {
	for (auto iter = fromMesh; iter != gpuMeshes.end(); iter++) {
		for (auto& texRef : iter->first->material->materialComponents) {
			if (texRef->getType() != ge::sg::MaterialComponent::ComponentType::IMAGE)
				continue;

			ge::sg::MaterialImageComponent* texture = (ge::sg::MaterialImageComponent*) texRef.get();
			iter->second.usedTextures[texture->semantic] = &texStorage[gpuTextures[texture]];
		}
	}
}

// returns format size
size_t geVkScene::assignFormat(ge::sg::Image::Format sgImageFormat, VkFormat& outVkformat) {
	// assign proper format
	switch (sgImageFormat) {
	case ge::sg::Image::Format::BGR:
		outVkformat = VK_FORMAT_B8G8R8_UNORM; //VK_FORMAT_R8G8B8A8_UNORM
		return 3; // 3 bytes

	case ge::sg::Image::Format::BGRA:
		outVkformat = VK_FORMAT_B8G8R8A8_UNORM;
		return 4;

	case ge::sg::Image::Format::R:
		outVkformat = VK_FORMAT_R8_UNORM;
		return 1;

	case ge::sg::Image::Format::RG:
		outVkformat = VK_FORMAT_R8G8_UNORM;
		return 2;

	case ge::sg::Image::Format::RGB:
		outVkformat = VK_FORMAT_R8G8B8_UNORM;
		return 3;

	case ge::sg::Image::Format::RGBA:
		outVkformat = VK_FORMAT_R8G8B8A8_UNORM;
		return 4;

	}

	return 0;
}

Info::Pipe::VertexInputDescription geVkScene::convertFrom(ge::sg::AttributeDescriptor& attributeInfo) {
	Info::Pipe::VertexInputDescription tmp;
	tmp.offset = attributeInfo.offset;
	tmp.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

	switch (attributeInfo.type) {
	case ge::sg::AttributeDescriptor::DataType::BYTE:
		tmp.format = VK_FORMAT_R8_SINT;
		break;
	case ge::sg::AttributeDescriptor::DataType::DOUBLE:
		tmp.format = VK_FORMAT_R64_SFLOAT;
		break;
	case ge::sg::AttributeDescriptor::DataType::FLOAT:
		tmp.format = VK_FORMAT_R32_SFLOAT;
		break;
	case ge::sg::AttributeDescriptor::DataType::INT:
		tmp.format = VK_FORMAT_R32_SINT;
		break;
	case ge::sg::AttributeDescriptor::DataType::SHORT:
		tmp.format = VK_FORMAT_R16_SINT;
		break;
	case ge::sg::AttributeDescriptor::DataType::UNSIGNED_BYTE:
		tmp.format = VK_FORMAT_R8_UINT;
		break;
	case ge::sg::AttributeDescriptor::DataType::UNSIGNED_INT:
		tmp.format = VK_FORMAT_R32_UINT;
		break;
	case ge::sg::AttributeDescriptor::DataType::UNSIGNED_SHORT:
		tmp.format = VK_FORMAT_R16_SINT;
		break;
	}

	return tmp;
}