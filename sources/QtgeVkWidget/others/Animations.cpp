/**
* @author Radek Blahos
* @project geVkWidget Library
*
* @brief This file contains implementation of renderer's animations
*/

#include <QtgeVkWidget/Animations/Animations.h>
#include <QtgeVkWidget\geVkWidget.h>
#include <QtgeVkWidget\geVkRenderer\QgeVkRenderer.h>
#include <geAx\AuxInlines.inl>
#include <geAx\ExceptionMacros.h>
#include <chrono>
#include <geSG/Animation.h>
#include <geSG/AnimationChannel.h>
#include <geSG/AnimationManager.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm\gtx\rotate_vector.hpp>
#include <random>

using namespace std;
using namespace ge::core;
using namespace glm;

Animations::Animations() : manager(new ge::sg::AnimationManager) {
	animation = make_shared<ge::sg::Animation>();
	cameraMoveCh = make_shared<AnimationChannel<float>>(); //movement channel
	animation->channels.push_back(cameraMoveCh); // connects movement channel to this animation - one channel can be shared with multiple animations
}

void geVkWidget::playCameraAnimation() {
	// create n set interpolated target
	std::shared_ptr<float> move = std::make_shared<float>();
	_usedAnimations->cameraMoveCh->setTarget(move);

	// used time points
	ge::core::time_point t0(time_unit(0));
	ge::core::time_point t1(time_unit(70));
	ge::core::time_point t2(time_unit(100));
	ge::core::time_point t3(time_unit(200));
	ge::core::time_point t4(time_unit(250));
	ge::core::time_point t5(time_unit(300));
	ge::core::time_point t6(time_unit(360));
	ge::core::time_point t7(time_unit(520));

	// obtain a seed for random generator
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

	std::mt19937 generator(seed);  // random generator

	// setting keyframes
	_usedAnimations->cameraMoveCh->KF.emplace_back(t0, 0.01 + ((generator() % 1000) / 1000));
	_usedAnimations->cameraMoveCh->KF.emplace_back(t1, -0.105 + ((generator() % 1000) / 1000));
	_usedAnimations->cameraMoveCh->KF.emplace_back(t2, 0.007 + ((generator() % 1000) / 1000));
	_usedAnimations->cameraMoveCh->KF.emplace_back(t3, -0.02 + ((generator() % 1000) / 1000));
	_usedAnimations->cameraMoveCh->KF.emplace_back(t4, 0.03 + ((generator() % 1000) / 1000));
	_usedAnimations->cameraMoveCh->KF.emplace_back(t5, -0.1 + ((generator() % 1000) / 1000));
	_usedAnimations->cameraMoveCh->KF.emplace_back(t6, 0.017 + ((generator() % 1000) / 1000));
	_usedAnimations->cameraMoveCh->KF.emplace_back(t7, -0.02 + ((generator() % 1000) / 1000));
	
	//begin animation
	_usedAnimations->manager->playAnimation(_usedAnimations->animation, time_unit(0));
	
	// animation steps
	for (int i = 2; i <= 520; i += 1) {
		_usedAnimations->manager->update(time_point(time_unit(i)));

		// update camera 
		if (generator() % 7 == 0) { // move 
			_scene->sceneCamera.getEyeCoords() += _scene->sceneCamera.getZaxis() * *move;
			_scene->sceneCamera.getCenterCoords() += _scene->sceneCamera.getZaxis() * *move;
		}
		else { // rotate
			_scene->sceneCamera.getEyeCoords() = glm::rotate(_scene->sceneCamera.getEyeCoords(), -*move, _scene->sceneCamera.getYaxis());

			/*if (generator() % 7 == 0) 
				_scene->sceneCamera.getEyeCoords() = glm::rotate(_scene->sceneCamera.getEyeCoords(), *move, _scene->sceneCamera.getXaxis());
			else 
				_scene->sceneCamera.getEyeCoords() = glm::rotate(_scene->sceneCamera.getEyeCoords(), -*move, _scene->sceneCamera.getYaxis());*/

		}
		
		_scene->sceneCamera.lookAt(_scene->sceneCamera.getEyeCoords(), _scene->sceneCamera.getCenterCoords(), _scene->sceneCamera.getYaxis());
	
		emit redrawRequest(_scene);
		
		if (i != 520) 
			ge::Ax::sleep(20);
	}
}

void geVkWidget::playLightSourceAnimation() {
	if (_scene->sceneLight.lightingMode == 0)
		return;

	// create n set interpolated target
	std::shared_ptr<float> move = std::make_shared<float>();
	_usedAnimations->cameraMoveCh->setTarget(move);

	// used time points
	ge::core::time_point t0(time_unit(0));
	ge::core::time_point t1(time_unit(70));
	ge::core::time_point t2(time_unit(100));
	ge::core::time_point t3(time_unit(200));
	ge::core::time_point t4(time_unit(250));
	ge::core::time_point t5(time_unit(300));
	ge::core::time_point t6(time_unit(360));
	ge::core::time_point t7(time_unit(520));

	// obtain a seed for random generator
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

	std::mt19937 generator(seed);  // random generator

	// setting keyframes
	_usedAnimations->cameraMoveCh->KF.emplace_back(t0, 0.01 + ((generator() % 1000) / 1000));
	_usedAnimations->cameraMoveCh->KF.emplace_back(t1, -0.105 + ((generator() % 1000) / 1000));
	_usedAnimations->cameraMoveCh->KF.emplace_back(t2, 0.007 + ((generator() % 1000) / 1000));
	_usedAnimations->cameraMoveCh->KF.emplace_back(t3, -0.02 + ((generator() % 1000) / 1000));
	_usedAnimations->cameraMoveCh->KF.emplace_back(t4, 0.03 + ((generator() % 1000) / 1000));
	_usedAnimations->cameraMoveCh->KF.emplace_back(t5, -0.1 + ((generator() % 1000) / 1000));
	_usedAnimations->cameraMoveCh->KF.emplace_back(t6, 0.017 + ((generator() % 1000) / 1000));
	_usedAnimations->cameraMoveCh->KF.emplace_back(t7, -0.02 + ((generator() % 1000) / 1000));

	//begin animation
	_usedAnimations->manager->playAnimation(_usedAnimations->animation, time_unit(0));

	// animation steps
	for (int i = 2; i <= 520; i += 1) {
		_usedAnimations->manager->update(time_point(time_unit(i)));

		// update camera 
		if (generator() % 3 == 0) { // move 
			_scene->sceneLight.position += _scene->sceneCamera.getXaxis() * *move;
		}
		else if (generator() % 2 == 0) {
			_scene->sceneLight.position += _scene->sceneCamera.getZaxis() * *move;
		}
		else { // rotate
			if (generator()%2 == 0)
				_scene->sceneLight.position = glm::rotate(_scene->sceneLight.position, *move, _scene->sceneCamera.getYaxis());
			else 
				_scene->sceneLight.position = glm::rotate(_scene->sceneLight.position, *move, _scene->sceneCamera.getXaxis());
		}

		emit redrawRequest(_scene);

		if (i != 520) 
			ge::Ax::sleep(20);
		
	}
}
