/**
* @author Radek Blahos
* @project geVkWidget Library
*
* @brief This file contains implementation of renderer's benchmark methods
*/

#include <QtgeVkWidget\geVkRenderer\QgeVkRenderer.h>
#include <geAx\AuxInlines.inl>
#include <geAx\ExceptionMacros.h>
#include <fstream>
#include <chrono>

#define BENCHMARKPASES 60

using namespace std;
using namespace ge::Vk;
using namespace ge::Ax;

void QgeVkRenderer::benchmarkDraw(geVkScene* renderScene) {
	if (!renderScene->gpuMeshes.size())
		return;

	// obtain name of current GPU and write it into file
	VkPhysicalDeviceProperties props;
	vkGetPhysicalDeviceProperties(device.getPhysicalDevice(), &props);

	string tmp;
	if (resourcesFolder == nullptr) 
		tmp = APP_RESOURCES"/others/";
	else
		tmp = *resourcesFolder + string("/others/");

	tmp.append(props.deviceName);
	tmp.append("_benchmark.txt");

	// open file
	fstream output(tmp.c_str(), fstream::out | fstream::app); // append results
	if (!output.is_open())
		throw ge::Ax::Exception::Bad::Load("geVkRenderer::benchmarkDraw -> output file");

	// extract some info about scene
	// obtain vertex count
	unsigned vertexCount(0);
	for (auto& mesh : renderScene->gpuMeshes)
		vertexCount += mesh.first->count;

	output << "\nVC: " << vertexCount << endl; // write vertex count

	// obtain used textures count	
	output << "TC: " << renderScene->texStorage.size() << endl;

	// phong lighting technique is used ?

	output << "PL: " << (bool)renderScene->sceneLight.lightingMode << endl;

	double avgDuration(0);
	chrono::high_resolution_clock::time_point drawStart;
	chrono::high_resolution_clock::time_point drawFinished;
	chrono::high_resolution_clock::time_point drawStartBM = chrono::high_resolution_clock::now();

	for (int drawPass = 0; drawPass < BENCHMARKPASES; drawPass++) {
		// start measuring
		drawStart = chrono::high_resolution_clock::now();

		// run draw routine
		draw(renderScene);

		// complete measuring
		drawFinished = chrono::high_resolution_clock::now();

		//cerr << "running Time" << chrono::duration_cast<chrono::duration<double>>(drawFinished - drawStart).count() << endl;
		avgDuration += chrono::duration_cast<chrono::duration<double>>(drawFinished - drawStart).count(); 
	}

	chrono::high_resolution_clock::time_point drawFinishedBM = chrono::high_resolution_clock::now();

	// avarage drawing time
	output << "ADTime: " << (avgDuration/ BENCHMARKPASES) * 1000 << " ms\n";
	output << "FPS: " << (double)BENCHMARKPASES / chrono::duration_cast<chrono::duration<double>>(drawFinished - drawStart).count() << endl;

	output.close();
}