#version 450
#extension GL_ARB_separate_shader_objects : enable

out gl_PerVertex {
    vec4 gl_Position;
};

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec2 inTexCoords;

layout(location = 0) out vec2 texCoords;
layout(location = 1) out vec3 fragmentPosition;
layout(location = 2) out vec3 normal;

layout(binding = 0) uniform ubo1 {
	mat4 viewProjection;
	mat4 worldSpaceTr;
	mat3 normalsFix;
} ubo;

void main() {
	// final position of vertex in clip space
	gl_Position = ubo.viewProjection * ubo.worldSpaceTr * vec4(inPosition, 1.0);

    // frag position for fs computation
	fragmentPosition = (ubo.worldSpaceTr * vec4(inPosition, 1.0)).xyz; // position for diffuse lighting computation in world space cs

	// texture coords
	texCoords = inTexCoords;

    // fix normals heading after world space transformation
    normal = inNormal; //ubo.normalsFix * vec4(inNormal, 0.0)).xyz; 
}

/*
	Momentalne nefunkcni korekce normal, kvuli spatnemu predavani matice normalsFix shaderu pres DS, resp. ma vertexuniforms
	stuktura ma odlisny layout nez ubo definovane zde v shaderu, kvuli odlisnemu zarovnavani pameti. 
*/