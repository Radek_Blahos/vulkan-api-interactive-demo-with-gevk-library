#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 texCoords;
layout(location = 1) in vec3 fragmentPosition;
layout(location = 2) in vec3 normal;

layout(location = 0) out vec4 finalColor;

layout(binding = 2) uniform sampler2D texSampler;
layout(binding = 1) uniform ubo1 {
	vec3 color;
	vec3 position;
	vec3 direction;
	float cutOff;
	vec3 eyePosition;
} lightInfo;

layout(push_constant) uniform ubo2 {
	uint lightSourceType;
	uint lightingMode;
} pushConstants;

/* Light computation functions */
vec3 computeAmbientComponent(float ambientLightStrength) {
	return ambientLightStrength * lightInfo.color;
}

vec3 computeDiffuseComponent(vec3 directionFromLight, vec3 normalizedNormal) {
	float diffuseComponentStrength = max(dot(normalizedNormal, directionFromLight), 0);
	return diffuseComponentStrength * lightInfo.color;
}

vec3 computeSpectacularComponent(float SpectacularStrength, uint SpectacularFactor, vec3 directionFromLight, vec3 normalizedNormal) { 
	vec3 viewDirection = normalize(lightInfo.eyePosition - fragmentPosition); 
	vec3 reflectDirection = reflect(-directionFromLight, normalizedNormal); 

	float SpectacularLightStrength;
	if (pushConstants.lightingMode == 1) { // Phong
		SpectacularLightStrength = pow(max(dot(viewDirection, reflectDirection), 0), SpectacularFactor);
	}
	else if (pushConstants.lightingMode == 2) { // Blinn
		vec3 halfWayVector = normalize(viewDirection + reflectDirection);
		SpectacularLightStrength = pow(max(dot(halfWayVector, normalizedNormal), 0), SpectacularFactor);
	}

	return SpectacularStrength * SpectacularLightStrength * lightInfo.color;
}

void main(void) {
	finalColor = texture(texSampler, texCoords); 
		
	if (pushConstants.lightingMode == 0) { // if lighting is off
		return;
	}
		
	/* Vars used in functions below */
	vec3 normalizedNormal = normalize(normal); 
	vec3 directionFromLight;
	float spotlightIntensity;

	if (pushConstants.lightSourceType == 0)  // directional light
		directionFromLight = normalize(-lightInfo.direction);

	else if (pushConstants.lightSourceType == 1)  // pointlight
		directionFromLight = normalize(lightInfo.position - fragmentPosition);

	else if (pushConstants.lightSourceType == 2) { // spotlight
		directionFromLight = normalize(lightInfo.position - fragmentPosition);
		float cosAngle = dot(directionFromLight, normalize(-lightInfo.direction));
		float epsilon = 10; 
		spotlightIntensity = clamp((lightInfo.cutOff + 10 - cosAngle) / epsilon, 0.0, 1.0);
	}

	// attenuation computation constants
	float distance = length(directionFromLight);
	// computational polynom coeficients
	float quadratic = 0.77; float linear = 0.27; float constant = 1;
	// light intensity fading over distance
	float attenuation = 1.0 / (quadratic * distance * distance + linear * distance + constant);

	// ambient component
	vec3 ambient_component = computeAmbientComponent(0.1);

	// diffuse component
	vec3 diffuse_component = computeDiffuseComponent(directionFromLight, normalizedNormal);

	// spectacular component
	vec3 spect_component = computeSpectacularComponent(0.4, 32, directionFromLight, normalizedNormal);

	vec3 lightSourceContribution = ambient_component + diffuse_component + spect_component;
	if (pushConstants.lightSourceType == 1) // pointlight
		lightSourceContribution *= attenuation;
	if (pushConstants.lightSourceType == 2) // flashlight source
		lightSourceContribution *= spotlightIntensity * attenuation;

	finalColor = vec4(lightSourceContribution, 1.0) * finalColor;
}
