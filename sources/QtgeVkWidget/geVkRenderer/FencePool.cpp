/**
* @author Radek Blahos
* @project geVkWidget Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <QtgeVkWidget\geVkRenderer\FencePool.h>

using namespace std;
using namespace ge::Vk;
using namespace ge::Ax;

int FencePool::getNextFenceIdx() {
	int freeIdx;
	while (1) {
		freeIdx = _fenceUsage.getNextFreeIndex();
		// traverse all fences and return idx to first free
		if (freeIdx != -1) {
			if (!_fences[freeIdx]->isUsed())
				break;
			// else search continues
		}
		else {
			// reset all fences and set them unset
			for (auto& fence:_fences)
				fence->waitUntilSignaled();

			int save = _fenceUsage.lastAssigned();
			_fenceUsage.unsetAllIndexes();
			_fenceUsage.setSearchPosition(save);

			// return idx to first fence
			freeIdx = 0;
			break;
		}
	}

	return freeIdx;
}

void FencePool::addFences(ge::Vk::CreateInfo::Fence& info, uint32_t fenceCount) {
	for (int i = 0; i < fenceCount; i++) 
		_fences.push_back(std::make_shared<Fence>(info));

	_fenceUsage.resize(fenceCount);
	
}

ge::Vk::Fence& FencePool::getNextFence() {
	int idx = getNextFenceIdx();
	_fences[idx]->markAsUsed();
	return *_fences[idx];
}