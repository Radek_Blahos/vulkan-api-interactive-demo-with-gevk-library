/**
* @author Radek Blahos
* @project geVkWidget Library
*
* @brief This file contains implementation of auxiliary geVkRenderer functions
*/

#include <QtgeVkWidget\geVkRenderer\geVkRenderer.h>
#include <geVk\Image.inl>
#include <geAx\AuxInlines.inl>
#include <geAx\ExceptionMacros.h>
#include <geVk\Swapchain.h>
#include <geVk\CommandBuffer.inl>
#include <ste/DAG.h>
#include <thread>

using namespace std;
using namespace ge::Vk;
using namespace ge::Ax;

void geVkRenderer::checkGPUFeatures(vector<VkPhysicalDevice>& hostGPUs) {
	vector<VkPhysicalDevice> start_vec(hostGPUs); hostGPUs.clear();
	for (auto const& gpu : start_vec) { 
		// properties
		/*
		Tady uzivatel kontroluje dostupne pozadavky:
		-> Pokud zarizeni vyhovuje serii pozadavkum (if statements) ulozi se ptr na gpu do res vectoru HostGPUs
		se kterymi se dale operuje po skonceni teto funkce
		*/
		VkPhysicalDeviceProperties props;
		vkGetPhysicalDeviceProperties(gpu, &props);

		if (props.deviceType != VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
			continue; 
		}

		hostGPUs.push_back(gpu);
	}
}

void geVkRenderer::updateDescriptors(geVkScene* scene, ge::sg::Mesh* sgMesh, uint32_t setIdx) {
	Info::Methods::UpdateDescriptorSets updateInfo;
	updateInfo.buffersInfo.resize(2);
	
	// vsUniforms
	updateInfo.buffersInfo[0].buffer = vsUniforms[setIdx].getBuffer();
	updateInfo.buffersInfo[0].offset = 0;
	updateInfo.buffersInfo[0].range = sizeof(VertexShaderBuffer);

	// fsUniforms
	updateInfo.buffersInfo[1].buffer = fsUniforms[setIdx].getBuffer();
	updateInfo.buffersInfo[1].offset = 0;
	updateInfo.buffersInfo[1].range = sizeof(FragmentShaderBuffer);

	/* write set infos */
	updateInfo.writeInfo.resize(2);

	// vsUniforms
	updateInfo.writeInfo[0].dstSet = componentsDB.descrSets[setIdx]->getMemory();
	updateInfo.writeInfo[0].dstBinding = 0;
	updateInfo.writeInfo[0].dstArrayElement = 0;
	updateInfo.writeInfo[0].descriptorCount = 1;
	updateInfo.writeInfo[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	updateInfo.writeInfo[0].pBufferInfo = &updateInfo.buffersInfo[0];

	// fsUniforms
	updateInfo.writeInfo[1].dstSet = componentsDB.descrSets[setIdx]->getMemory();
	updateInfo.writeInfo[1].dstBinding = 1;
	updateInfo.writeInfo[1].dstArrayElement = 0;
	updateInfo.writeInfo[1].descriptorCount = 1;
	updateInfo.writeInfo[1].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	updateInfo.writeInfo[1].pBufferInfo = &updateInfo.buffersInfo[1];
	
	if (scene->gpuMeshes[sgMesh].usedTextures.size() > 0) {
		// texture
		updateInfo.imagesInfo.push_back(VkDescriptorImageInfo{
			*componentsDB.samplers[0],
			scene->gpuMeshes[sgMesh].usedTextures[ge::sg::MaterialImageComponent::Semantic::diffuseTexture]->getVkView(),
			VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
			});

		updateInfo.writeInfo.push_back({ // texture
			componentsDB.descrSets[setIdx]->getMemory(),
			2,
			0,
			1,
			VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
			&updateInfo.imagesInfo[0],
			nullptr,
			nullptr
		});
	}

	componentsDB.descrSets[setIdx]->update(updateInfo);
}

void geVkRenderer::updateDescriptors(DrawWorkerInfo* info) {
	Info::Methods::UpdateDescriptorSets updateInfo;
	// vsUniforms
	updateInfo.buffersInfo.push_back({
		info->thisPtr->vsUniforms[info->dispatchIdx].getBuffer(),
		0,
		sizeof(VertexShaderBuffer)
		});
	// fsUniforms
	updateInfo.buffersInfo.push_back({
		info->thisPtr->fsUniforms[info->dispatchIdx].getBuffer(),
		0,
		sizeof(FragmentShaderBuffer)
		});

	/* write set infos */
	updateInfo.writeInfo.push_back({ // vsUniforms
		info->thisPtr->componentsDB.descrSets[info->dispatchIdx]->getMemory(),
		0,
		0,
		1,
		VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		nullptr,
		&updateInfo.buffersInfo[0],
		nullptr
		});

	updateInfo.writeInfo.push_back({ // fsUniforms
		info->thisPtr->componentsDB.descrSets[info->dispatchIdx]->getMemory(),
		1,
		0,
		1,
		VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		nullptr,
		&updateInfo.buffersInfo[1],
		nullptr
		});

	if (info->mesh->usedTextures.size() > 0) {
		// texture
		updateInfo.imagesInfo.push_back(VkDescriptorImageInfo{
			*info->thisPtr->componentsDB.samplers[0],
			info->mesh->usedTextures[ge::sg::MaterialImageComponent::Semantic::diffuseTexture]->getVkView(),
			VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
			});

		updateInfo.writeInfo.push_back({ // texture
			info->thisPtr->componentsDB.descrSets[info->dispatchIdx]->getMemory(),
			2,
			0,
			1,
			VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
			&updateInfo.imagesInfo[0],
			nullptr,
			nullptr
			});
	}

	info->thisPtr->componentsDB.descrSets[info->dispatchIdx]->update(updateInfo);
}