/**
* @author Radek Blahos
* @project geVkWidget Library
*
* @brief This file contains implementation of geVkRenderer init methods
*/

#include <QtgeVkWidget\geVkRenderer\geVkRenderer.h>
#include <geVk\CommandBuffer.inl>
#include <geAx\AuxInlines.inl>
#include <geAx\ExceptionMacros.h>
#include <qimage.h>
#include <glm\gtc\matrix_transform.inl>
#include <QtgeVkWidget/geVkScene/geVkMesh.h>

#define DEBUG
#define SWAPCHAIN_IMG_COUNT 4

using namespace std;
using namespace ge::Vk;
using namespace ge::Ax;

void geVkRenderer::initialize(InitInfo& info) {
	_drawingThreadsCount = info.drawThreadsCount;
	_useRoutineVersion = info.useRoutineVersion;
	_useFenceAsSyncPrimitive = info.fenceAsSyncPrimitive;

	try {
		/* init device */
		initDevice(info.useQueuesCount);

		// init memory manager
		initMemoryManager();

		// init command sets
		buildCommandBufferSets();

		// init command processor
		CreateInfo::CommandProcessor cpCI;
		cpCI.pDevice = &device;
		commandProcessor.startUp(cpCI);

		// init DescriptorManager
		CreateInfo::DescriptorManager mCI;
		mCI.pDevice = &device;
		descManager.setUp(mCI);
		descManager.setCurrentAllocationCountperPool(_drawingThreadsCount);

		// init swapchain, before creating render pass 
		createSwapchain(info.width, info.height);

		// create uniforms, referencing swapchain widht,height so must be inited here
		createUniforms();

		/* Init RenderContext Components, must init swapchain before */
		initRenderComponents();

		// init swapchain's images, framebuffer and depth buffer. Converts them into proper layouts aftewards...
		initSwapchain(info.width, info.height);

		// build pipelines, needs to be done per scene by user...
		buildPipelineDB(); 

		// if uniform model rendering is enabled
		if (info.useRoutineVersion == -3) {
			// create indirect draw buffer
			CreateInfo::Buffer idBCI(CreateInfo::Buffer::Usage::INDEXEDINDIRECTDRAW);
			idBCI.size = sizeof(VkDrawIndexedIndirectCommand);
			idBCI.pManager = &memManager;

			indirectDrawStorage.create(idBCI);
		}
	}
	// catch geVk object Creation errors
	catch (ge::Ax::Exception::Base& exception) {
		std::cerr << exception.what(); // vypise jmeno prvni vyjimky a skonci
		CATCHEXIT(-1);
	}
	// heap allocation problem
	catch (std::bad_alloc& exception) {
		std::cerr << exception.what();
		CATCHEXIT(-1);
	}
}

void geVkRenderer::initInstance() {
	try {
		// creating geVk instance
		CreateInfo::Instance info;
		info.appName = "VK Demo";
		info.appVersion = Auxiliary::Instance::makeVersion(0, 0, 1);
		info.engineName = "GPU Engine";
		info.engineVersion = Auxiliary::Instance::makeVersion(0, 0, 0);
		info.vkVersion = Auxiliary::Instance::makeVersion(1, 0, 57);
		info.enabledExtensions = { "VK_KHR_surface", VK_EXT_DEBUG_REPORT_EXTENSION_NAME };
		#if defined(_WIN32)
			info.enabledExtensions.push_back("VK_KHR_win32_surface");
		#else
			info.enabledExtensions.push_back("VK_KHR_xcb_surface");
		#endif

#ifdef DEBUG
		info.enabledLayers = { "VK_LAYER_LUNARG_standard_validation" };
		info.useValidationLayers = true;
		info.verboseMode = true;
		info.abundantInfo = true;
#endif // DEBUG

		// create instance
		instance.create(info);
	}
	// catch geVk object Creation errors
	catch (ge::Ax::Exception::Base& exception) {
		std::cerr << exception.what(); // vypise jmeno prvni vyjimky a skonci
		CATCHEXIT(-1);
	}
	// heap allocation problem
	catch (std::bad_alloc& exception) {
		std::cerr << exception.what();
		CATCHEXIT(-1);
	}
}

void geVkRenderer::initDevice(uint32_t useQueues) {
	/* Vytvorim device objekt */
	CreateInfo::Device deviceInfo;
	deviceInfo.from = &instance;
	deviceInfo.abundantInfo = instance.getDescription().abundantInfo;
	/* Musim vybrat vhodne GPU pro spolupraci s Vk */
	// Navolim si features ktere chci aby gpu splnovalo.
	deviceInfo.requieredDeviceFeatures.samplerAnisotropy = true; // chci aby podporovalo anis. samplovani
	deviceInfo.checkGPUCapability = checkGPUFeatures; // function ptr na dodatecnou kontrolu vlastnosti GPU
	// Zvolim jake extensions a validation layers musi gpu podporovat
	deviceInfo.requieredExtensionNames.insert(deviceInfo.requieredExtensionNames.end(), { VK_KHR_SWAPCHAIN_EXTENSION_NAME }); 	// requiered extensions, emplacing names into vector
	deviceInfo.useValidationLayers = true; // wanna use validation layers
	// if (deviceInfo.useValidationLayers)
	// requiered layers, only with validation layers activated!
	if (instance.getDescription().useValidationLayers)
		deviceInfo.requieredLayerNames.insert(deviceInfo.requieredLayerNames.end(), { "VK_LAYER_LUNARG_standard_validation" });
	/*
		Zvolit jake vlastnosti musi Queue splnovat a pote vyberu z GPUs to spravne, resp podle tech
		vlastnosti prohledam vsechny gpu a podivam se jestli maji Queues, ktere pozaduji, jinak zkousim dalsi GPU
	*/
	// add requested physical device queues info, want 3 queues from family which can perform rendering, computing and data transfering.
	deviceInfo.initializedQueuesInfo.insert(deviceInfo.initializedQueuesInfo.end(), {
		Info::RequestQueues{ 0, useQueues, VkQueueFlagBits(VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_COMPUTE_BIT | VK_QUEUE_TRANSFER_BIT) }
	});
	// getting surface
	deviceInfo.usedSurface = presentSurface;

	// creating map of allocated queueFamily 
	componentsDB.queueFamilyTags[VkQueueFlagBits(VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_COMPUTE_BIT | VK_QUEUE_TRANSFER_BIT)] = deviceInfo.initializedQueuesInfo[0].qFamilyIdx;

	// create device
	device.create(deviceInfo);
}

void geVkRenderer::initMemoryManager() {
	// Init Memory Manager
	CreateInfo::MemoryManager memManagerCI;
	memManagerCI.pDevice = &device;

	memManager.setUp(memManagerCI);
}

void geVkRenderer::createUniforms() {
	// allocated memory pools for uniform buffers
	CreateInfo::Buffer vsUCI;
	vsUCI.pManager = &memManager;
	vsUCI.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
	vsUCI.propertyFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

	vsUniforms.resize(_drawingThreadsCount);
	fsUniforms.resize(_drawingThreadsCount);

	for (unsigned i = 0; i < _drawingThreadsCount; i++) {
		vsUniforms[i].create(vsUCI);
		fsUniforms[i].create(vsUCI);
	}
}

void geVkRenderer::buildShaders() {
	// shaders
	CreateInfo::ShaderModule basicCI;
	basicCI.pDevice = &device;

	string dirName;
	if (resourcesFolder == nullptr)
		dirName = APP_RESOURCES;
	else
		dirName = *resourcesFolder;

	basicCI.path = dirName + string("/shaders/textured_vert.spv");
	componentsDB.shaders["basV"].create(basicCI);

	basicCI.path = dirName + string("/shaders/textured_frag.spv");
	componentsDB.shaders["basF"].create(basicCI);

	basicCI.path = dirName + string("/shaders/nonTextured_vert.spv");
	componentsDB.shaders["ntV"].create(basicCI);

	basicCI.path = dirName + string("/shaders/nonTextured_frag.spv");
	componentsDB.shaders["ntF"].create(basicCI);

	basicCI.path = dirName + string("/shaders/debug_vert.spv");
	componentsDB.shaders["debugV"].create(basicCI);

	basicCI.path = dirName + string("/shaders/debug_frag.spv");
	componentsDB.shaders["debugF"].create(basicCI);
}

void geVkRenderer::buildPipelineLayouts() {
	// textured version
	CreateInfo::PipelineLayout layoutCI;
	layoutCI.pDevice = &device;

	CreateInfo::PipelineLayout debugI = layoutCI;
	
	// debug version
	componentsDB.pipeLayouts["debug"].describe(debugI);

	// textured version of pipeline
	layoutCI.pDescriptorSetLayouts.push_back(&componentsDB.descrSets[0]->getLayout());
	layoutCI.pushConstants.resize(1);
	layoutCI.pushConstants[0].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
	layoutCI.pushConstants[0].offset = 0;
	layoutCI.pushConstants[0].size = sizeof(uint32_t) * 2;

	componentsDB.pipeLayouts["base"].describe(layoutCI);

	// non textured version
	layoutCI.pDescriptorSetLayouts[0] = &*componentsDB.descrSets[0]->getLayoutViews()[0]; // reference to descriptor set view
	layoutCI.pushConstants[0].size = sizeof(PushConstants); // uniformColor included...

	componentsDB.pipeLayouts["nonTex"].describe(layoutCI);

}

void geVkRenderer::buildRenderPasses() {
	CreateInfo::RenderPass rpCI;
	rpCI.pDevice = &device;

	rpCI.attachments.resize(2);
	// swapchain img
	rpCI.attachments[0].flags = 0;
	rpCI.attachments[0].format = swapchain.getImageFormat();
	rpCI.attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;
	rpCI.attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
	rpCI.attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	rpCI.attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	rpCI.attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	rpCI.attachments[0].initialLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
	rpCI.attachments[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

	// depth image
	rpCI.attachments[1].flags = 0;
	rpCI.attachments[1].format = VK_FORMAT_D32_SFLOAT;
	rpCI.attachments[1].samples = VK_SAMPLE_COUNT_1_BIT;
	rpCI.attachments[1].loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
	rpCI.attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	rpCI.attachments[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	rpCI.attachments[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	rpCI.attachments[1].initialLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
	rpCI.attachments[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	rpCI.attachmentReferences.resize(2);
	rpCI.attachmentReferences[0] = { 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL };
	rpCI.attachmentReferences[1] = { 1, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL };

	rpCI.subpasses.resize(1);
	rpCI.subpasses[0].pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	rpCI.subpasses[0].colorAttachmentCount = 1;
	rpCI.subpasses[0].pColorAttachments = &rpCI.attachmentReferences[0];
	rpCI.subpasses[0].pDepthStencilAttachment = &rpCI.attachmentReferences[1];

	componentsDB.renderPasses["base"].describe(rpCI);

}

void geVkRenderer::buildCommandBufferSets() {
	CreateInfo::CommandBufferSet setInfo;
	setInfo.pDevice = &device;
	setInfo.familyQueueIdx = componentsDB.queueFamilyTags[VkQueueFlagBits(VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_COMPUTE_BIT | VK_QUEUE_TRANSFER_BIT)];

	if (_useRoutineVersion == -2) {
		setInfo.initialBufferCount = 2; // every thread have at least 2 switching cmd buffers 
		setInfo.createFences = false;
		setInfo.useEvents = !_useFenceAsSyncPrimitive;
		// create command set, one per drawThread
		for (int i = 0; i < _drawingThreadsCount; i++)
			componentsDB.commandBufferSets.push_back(std::make_shared<CommandBufferSet>(setInfo));
	}
	else {
		setInfo.initialBufferCount = SWAPCHAIN_IMG_COUNT;
		setInfo.createFences = true;
		setInfo.useEvents = false;
		componentsDB.commandBufferSets.push_back(std::make_shared<CommandBufferSet>(setInfo)); // textured cmds 
		//componentsDB.commandBufferSets.push_back(new CommandBufferSet(setInfo)); // nontextured cmds
	}

	// create command set for usage outside drawThreads -> always componentsDB.commandBufferSets.front()
	setInfo.initialBufferCount = 1;
	setInfo.createFences = true;
	setInfo.useEvents = false;
	componentsDB.commandBufferSets.push_back(std::make_shared<CommandBufferSet>(setInfo));
}

void geVkRenderer::buildDescriptorSets() {
	CreateInfo::DescriptorSet info;
	info.pManager = &descManager;
	info.mainLayoutInfo.pDevice = &device;
	info.mainLayoutInfo.layoutBindings.resize(3);

	// vertex shader uniformbuffer
	info.mainLayoutInfo.layoutBindings[0].binding = 0;
	info.mainLayoutInfo.layoutBindings[0].descriptorCount = 1;
	info.mainLayoutInfo.layoutBindings[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	info.mainLayoutInfo.layoutBindings[0].pImmutableSamplers = VK_NULL_HANDLE;
	info.mainLayoutInfo.layoutBindings[0].stageFlags  = VK_SHADER_STAGE_VERTEX_BIT;
	
	// fragment shader uniformbuffer
	info.mainLayoutInfo.layoutBindings[1].binding = 1;
	info.mainLayoutInfo.layoutBindings[1].descriptorCount = 1;
	info.mainLayoutInfo.layoutBindings[1].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	info.mainLayoutInfo.layoutBindings[1].pImmutableSamplers = VK_NULL_HANDLE;
	info.mainLayoutInfo.layoutBindings[1].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

	// fragment shader image
	info.mainLayoutInfo.layoutBindings[2].binding = 2;
	info.mainLayoutInfo.layoutBindings[2].descriptorCount = 1;
	info.mainLayoutInfo.layoutBindings[2].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	info.mainLayoutInfo.layoutBindings[2].pImmutableSamplers = VK_NULL_HANDLE;
	info.mainLayoutInfo.layoutBindings[2].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

	// create layout view
	CreateInfo::DescriptorSetLayout viewInfo = info.mainLayoutInfo;
	viewInfo.layoutBindings.pop_back(); // remove sampler info
	
	// create descriptor set, one per drawThread
	for (int i = 0; i < _drawingThreadsCount; i++) {
		componentsDB.descrSets.push_back(std::make_shared<DescriptorSet>(info));
		// create layout views, used with nontextured meshes
		componentsDB.descrSets[i]->addLayoutView(viewInfo);
	}
}

void geVkRenderer::buildSamplers() {
	CreateInfo::Sampler ci;
	ci.pDevice = &device;

	componentsDB.samplers.push_back(std::make_shared<Sampler>(ci));
}

void geVkRenderer::buildSyncComponents() {
	// create semaphore used for sync of color attachment and depth buffer access
	CreateInfo::Semaphore semCI;
	semCI.pDevice = &device;
	Semaphore syncAttachments(semCI);

	componentsDB.semaphores["aSync"].create(semCI);

	// create spare fences 
	CreateInfo::Fence fenceI;
	fenceI.pDevice = &device;

	drawCallFences.addFences(fenceI, 8);

}

void geVkRenderer::initRenderComponents() {
	buildDescriptorSets(); // must be congruent with models inside geVkScene 
	buildSamplers();
	buildSyncComponents();
	buildShaders();
	buildPipelineLayouts(); // must be congruent with models inside geVkScene
	buildRenderPasses();
}

void geVkRenderer::fillPipelineDBCreateInfo(CreateInfo::PipelineDatabase& createInfo) {
	// assign device for pipelineDB
	createInfo.pDevice = &device;
	// cache storage path
	createInfo.cacheStoragePath = (resourcesFolder == nullptr) ?  APP_RESOURCES"/others/pipeline.cache" : *resourcesFolder + string("/others/pipeline.cache");

	// create graphic pipelines here
	createInfo.graphicPipesCreateInfos.resize(3);
	createInfo.graphicPipesCreateInfos[0].name = "basic";
	createInfo.graphicPipesCreateInfos[1].name = "nonTex";
	createInfo.graphicPipesCreateInfos[2].name = "debug";
	texturePipeInfo(createInfo.graphicPipesCreateInfos[0].graphicPipeCreateInfo);
	nonTexturePipeInfo(createInfo.graphicPipesCreateInfos[1].graphicPipeCreateInfo);
	createInfo.graphicPipesCreateInfos[1].graphicPipeCreateInfo.basePipelineIndex = 0; // basic is parrent of nontex
	debugPipeInfo(createInfo.graphicPipesCreateInfos[2].graphicPipeCreateInfo);
	createInfo.graphicPipesCreateInfos[2].graphicPipeCreateInfo.basePipelineIndex = 0; // basic is parrent of debug

	// create compute pipelines here

}

// general create graphic pipeline structure fill
void geVkRenderer::texturePipeInfo(CreateInfo::GraphicPipeline& fill) {
	fill.pDevice = &device;
	fill.layout = &componentsDB.pipeLayouts["base"];
	fill.renderPass = &componentsDB.renderPasses["base"];

	fill.vertexInput.resize(3);
	// position
	fill.vertexInput[0].location = 0;
	fill.vertexInput[0].stride	= 3 * sizeof(float);

	// normal
	fill.vertexInput[1].location = 1;
	fill.vertexInput[1].stride = 3 * sizeof(float);

	// texture coords
	fill.vertexInput[2].location = 2;
	fill.vertexInput[2].stride = 2 * sizeof(float);
	fill.vertexInput[2].format = VK_FORMAT_R32G32_SFLOAT;

	fill.stagesInfo.resize(2);
	fill.stagesInfo[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
	fill.stagesInfo[0].module = &componentsDB.shaders["basV"];

	fill.stagesInfo[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	fill.stagesInfo[1].module = &componentsDB.shaders["basF"];

	fill.viewport.scissors.resize(1);
	fill.viewport.scissors[0].extent = { 0, 0 };
	fill.viewport.scissors[0].offset = { (int32_t)swapchain.getDescription().width, (int32_t)swapchain.getDescription().height };

	fill.viewport.viewports.resize(1);
	fill.viewport.viewports[0].width = (float)swapchain.getDescription().width;
	fill.viewport.viewports[0].height = (float)swapchain.getDescription().height;
	fill.viewport.viewports[0].minDepth = fill.viewport.viewports[0].x = fill.viewport.viewports[0].y = 0.0f;
	fill.viewport.viewports[0].maxDepth = 1.0f;

	// dynamic states
	fill.dynamicStates.insert(fill.dynamicStates.begin(), { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR });

	// blend attachment states
	fill.colorBlend.attachments.push_back(Info::Pipe::ColorBlendAttachmentState());

	// depth test
	fill.depthStencil.depthTestEnable = VK_TRUE;
	fill.depthStencil.depthWriteEnable = VK_TRUE;
}

void geVkRenderer::nonTexturePipeInfo(CreateInfo::GraphicPipeline& fill) {
	fill.pDevice = &device;
	fill.layout = &componentsDB.pipeLayouts["nonTex"];
	fill.renderPass = &componentsDB.renderPasses["base"];

	fill.vertexInput.resize(2);
	// position
	fill.vertexInput[0].location = 0;
	fill.vertexInput[0].stride = 3 * sizeof(float);

	// normal
	fill.vertexInput[1].location = 1;
	fill.vertexInput[1].stride = 3 * sizeof(float);

	// shader stages
	fill.stagesInfo.resize(2);
	fill.stagesInfo[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
	fill.stagesInfo[0].module = &componentsDB.shaders["ntV"];

	fill.stagesInfo[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	fill.stagesInfo[1].module = &componentsDB.shaders["ntF"];

	fill.viewport.scissors.resize(1);
	fill.viewport.scissors[0].extent = { 0, 0 };
	fill.viewport.scissors[0].offset = { (int32_t)swapchain.getDescription().width, (int32_t)swapchain.getDescription().height };

	fill.viewport.viewports.resize(1);
	fill.viewport.viewports[0].width = (float)swapchain.getDescription().width;
	fill.viewport.viewports[0].height = (float)swapchain.getDescription().height;
	fill.viewport.viewports[0].minDepth = fill.viewport.viewports[0].x = fill.viewport.viewports[0].y = 0.0f;
	fill.viewport.viewports[0].maxDepth = 1.0f;
	
	// dynamic states
	fill.dynamicStates.insert(fill.dynamicStates.begin(), { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR });

	// blend attachment states
	fill.colorBlend.attachments.push_back(Info::Pipe::ColorBlendAttachmentState());

	// depth test
	fill.depthStencil.depthTestEnable = VK_TRUE;
	fill.depthStencil.depthWriteEnable = VK_TRUE;

}

void geVkRenderer::debugPipeInfo(CreateInfo::GraphicPipeline& fill) {
	fill.pDevice = &device;
	fill.layout = &componentsDB.pipeLayouts["debug"];
	fill.renderPass = &componentsDB.renderPasses["base"];

	// shader stages
	fill.stagesInfo.resize(2);
	fill.stagesInfo[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
	fill.stagesInfo[0].module = &componentsDB.shaders["debugV"];

	fill.stagesInfo[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	fill.stagesInfo[1].module = &componentsDB.shaders["debugF"];

	fill.viewport.scissors.resize(1);
	fill.viewport.scissors[0].extent = { 0, 0 };
	fill.viewport.scissors[0].offset = { (int32_t)swapchain.getDescription().width, (int32_t)swapchain.getDescription().height };

	fill.viewport.viewports.resize(1);
	fill.viewport.viewports[0].width = (float)swapchain.getDescription().width;
	fill.viewport.viewports[0].height = (float)swapchain.getDescription().height;
	fill.viewport.viewports[0].minDepth = fill.viewport.viewports[0].x = fill.viewport.viewports[0].y = 0.0f;
	fill.viewport.viewports[0].maxDepth = 1.0f;

	// dynamic states
	fill.dynamicStates.insert(fill.dynamicStates.begin(), { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR });

	// blend attachment states
	fill.colorBlend.attachments.push_back(Info::Pipe::ColorBlendAttachmentState());

}

void geVkRenderer::buildPipelineDB() {
	CreateInfo::PipelineDatabase createInfo;
	fillPipelineDBCreateInfo(createInfo);
	pipelineDB.build(createInfo);
}

void geVkRenderer::createSwapchain(uint32_t width, uint32_t height) {
	// swapchain creation
	// desired surface format
	VkSurfaceFormatKHR reqSurfaceFormat{ VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
	// getting surface capabilities
	VkSurfaceCapabilitiesKHR surfCaps = Auxiliary::Swapchain::surfaceCapabilities(device.getPhysicalDevice(), presentSurface);

	CreateInfo::Swapchain swapchainCI;
	swapchainCI.presentationQueue = device.getQueueHandles(componentsDB.queueFamilyTags[VkQueueFlagBits(VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_COMPUTE_BIT | VK_QUEUE_TRANSFER_BIT)]).availableQueues[0]; // assign one queue to be presentation one
	swapchainCI.pMemManager = &memManager;
	swapchainCI.surface = presentSurface;
	swapchainCI.surfaceFormat = (Auxiliary::Swapchain::checkSurfaceFormatAvailability(device.getPhysicalDevice(), presentSurface, reqSurfaceFormat)) ? reqSurfaceFormat : Auxiliary::Swapchain::returnRandomAvailableSurfaceFormat(device.getPhysicalDevice(), presentSurface);// check surface format capability ...  :(
	swapchainCI.presentMode = (Auxiliary::Swapchain::checkPresentModeAvailability(device.getPhysicalDevice(), presentSurface, VK_PRESENT_MODE_MAILBOX_KHR)) ? VK_PRESENT_MODE_MAILBOX_KHR : Auxiliary::Swapchain::returnRandomAvailablePresentMode(device.getPhysicalDevice(), presentSurface); // check present mode availibility...  :(
	swapchainCI.minImageCount = ge::Ax::min(surfCaps.maxImageCount, (uint32_t)SWAPCHAIN_IMG_COUNT);
	swapchainCI.width = ge::Ax::min(width, surfCaps.maxImageExtent.width);
	swapchainCI.height = ge::Ax::min(height, surfCaps.maxImageExtent.height);
	swapchainCI.imageArrayLayers = 1;
	swapchainCI.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
	swapchainCI.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
	swapchainCI.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	swapchainCI.clipped = VK_TRUE;
	swapchainCI.oldSwapchain = VK_NULL_HANDLE;
	swapchainCI.framebuffersVecPtr = &componentsDB.framebuffers;

	// init swapchain, resources will be created later with render pass known
	swapchain.create(swapchainCI);

	// init depthbuffer
	swapchain.createDepthBuffer(1920 * 1080); // zmenit za nejake definovane promenne napr max height, max width

}

void geVkRenderer::initSwapchain(uint32_t width, uint32_t height) {
	try {
		// create swapchain's framebuffers 
		swapchain.createFramebuffers(componentsDB.renderPasses["base"]);

		swapchainImagesLayoutTransfer();
	}
	// catch geVk object Creation errors
	catch (ge::Ax::Exception::Base& exception) {
		std::cerr << exception.what(); // vypise jmeno prvni vyjimky a skonci
		CATCHEXIT(-1);
	}
	// heap allocation problem
	catch (std::bad_alloc& exception) {
		std::cerr << exception.what();
		CATCHEXIT(-1);
	}
}

void geVkRenderer::swapchainImagesLayoutTransfer() {
	CommandBuffer& cmdbuff = componentsDB.commandBufferSets.back()->getNextBuffer();
	cmdbuff.beginRecording();
	// must convert swapchain img from undefined layout -> presentKHR
	swapchain.transformImagesToProperLayout(cmdbuff);
	cmdbuff.finishRecording();
	Info::Methods::PostSingleBuffer postI;
	postI.pBuffer = &cmdbuff;
	commandProcessor.postCommands(postI, &cmdbuff.getLock().getPrimitive<Fence>());
	componentsDB.commandBufferSets.back()->returnCurrentBuffer();
}

void geVkRenderer::updateDescriptorsSG() {
	Info::Methods::UpdateDescriptorSets updateInfo;
	updateInfo.buffersInfo.resize(2);

	// vsUniforms
	updateInfo.buffersInfo[0].buffer = vsUniforms[0].getBuffer();
	updateInfo.buffersInfo[0].offset = 0;
	updateInfo.buffersInfo[0].range = sizeof(VertexShaderBuffer);

	// fsUniforms
	updateInfo.buffersInfo[1].buffer = fsUniforms[0].getBuffer();
	updateInfo.buffersInfo[1].offset = 0;
	updateInfo.buffersInfo[1].range = sizeof(FragmentShaderBuffer);

	/* write set infos */
	updateInfo.writeInfo.resize(2);

	// vsUniforms
	updateInfo.writeInfo[0].dstSet = componentsDB.descrSets[0]->getMemory();
	updateInfo.writeInfo[0].dstBinding = 0;
	updateInfo.writeInfo[0].dstArrayElement = 0;
	updateInfo.writeInfo[0].descriptorCount = 1;
	updateInfo.writeInfo[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	updateInfo.writeInfo[0].pBufferInfo = &updateInfo.buffersInfo[0];

	// fsUniforms
	updateInfo.writeInfo[1].dstSet = componentsDB.descrSets[0]->getMemory();
	updateInfo.writeInfo[1].dstBinding = 1;
	updateInfo.writeInfo[1].dstArrayElement = 0;
	updateInfo.writeInfo[1].descriptorCount = 1;
	updateInfo.writeInfo[1].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	updateInfo.writeInfo[1].pBufferInfo = &updateInfo.buffersInfo[1];

	componentsDB.descrSets[0]->update(updateInfo);
}

void geVkRenderer::recordCommandBuffers(geVkScene* renderScene) {
	if (_useRoutineVersion == -2 || renderScene->gpuMeshes.size() == 0)
		return;

	for (int i = 0; i < swapchain.getDescription().minImageCount; i++) {
		RecordIndexedIndirectDrawCommands rI;

		// pipeline for textured models
		rI.renderWidth = swapchain.getDescription().width;
		rI.renderHeight = swapchain.getDescription().height;
		rI.pGraphicPipeline = &pipelineDB.obtainGraphicPipeline("nonTex");
		// render pass info
		rI.renderPassInfo.pFramebuffer = &swapchain.getFramebuffer(i); // return framebuffer for current image
		rI.renderPassInfo.pRenderPass = &componentsDB.renderPasses["base"];
		rI.renderPassInfo.renderArea.extent = { rI.renderWidth, rI.renderHeight };
		// index buffer
		rI.indexBufferInfo.pBuffer = renderScene->gpuMeshes.begin()->second.buffers[ge::sg::AttributeDescriptor::Semantic::indices]->data;
		// vertex buffers
		rI.vertexBuffersInfo.firstBinding = 0;

		// store ptr to buffers, find each of them inside ge::sg::Mesh
		// position
		rI.vertexBuffersInfo.pBuffers.push_back(*renderScene->attribBufferSize[ge::sg::AttributeDescriptor::Semantic::position].vertexBuffPtr->data);
		rI.vertexBuffersInfo.offsets.push_back(0);
		// normals
		rI.vertexBuffersInfo.pBuffers.push_back(*renderScene->attribBufferSize[ge::sg::AttributeDescriptor::Semantic::normal].vertexBuffPtr->data);
		rI.vertexBuffersInfo.offsets.push_back(0);

		// descriptors
		rI.descriptorSetsInfo.bindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		rI.descriptorSetsInfo.firstSet = 0;
		rI.descriptorSetsInfo.pPipelineLayout = &componentsDB.pipeLayouts["nonTex"];
		rI.descriptorSetsInfo.pDescriptorSets.push_back(&*componentsDB.descrSets[0]);

		// push constants
		rI.pushConstants.offset = 0;
		rI.pushConstants.usedInStages = VK_SHADER_STAGE_FRAGMENT_BIT;
		rI.pushConstants.pValues = &_pushConstants;
		rI.pushConstants.pLayout = &componentsDB.pipeLayouts["nonTex"];
		rI.pushConstants.size = sizeof(PushConstants);

		// draw info
		rI.drawInfo.buffer = indirectDrawStorage.getBuffer();
		rI.drawInfo.drawCount = 1;
		rI.drawInfo.offset = 0;
		rI.drawInfo.stride = 0;

		recordIndexedIndirectDrawCommands(componentsDB.commandBufferSets[0]->getBuffer(i), rI);
	}
}