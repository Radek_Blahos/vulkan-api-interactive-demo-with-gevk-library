/**
* @author Radek Blahos
* @project geVkWidget Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <QtgeVkWidget\geVkRenderer\QgeVkRenderer.h>
#include <QtgeVkWidget\geVkScene\geVkScene.h>

void QgeVkRenderer::renderNow(geVkScene* scene) {
	geVkRenderer::draw(scene);
	emit drawCompleted(); 
}

void QgeVkRenderer::drawTriangle() {
	debugDraw();
	emit drawCompleted();
}