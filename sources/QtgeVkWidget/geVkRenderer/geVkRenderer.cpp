/**
* @author Radek Blahos
* @project geVkWidget Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <QtgeVkWidget\geVkRenderer\geVkRenderer.h>
#include <geVk\Image.inl>
#include <geAx\AuxInlines.inl>
#include <geAx\ExceptionMacros.h>
#include <geVk\Swapchain.h>
#include <geVk\CommandBuffer.inl>
#include <ste/DAG.h>
#include <thread>

using namespace std;
using namespace ge::Vk;
using namespace ge::Ax;

geVkRenderer::~geVkRenderer() {
	if (_useRoutineVersion != -2 || !_useFenceAsSyncPrimitive)
		return;

	for (int i = 0; i < componentsDB.commandBufferSets.size() - 1; i++)
		componentsDB.commandBufferSets[i]->deleteFencePointers();

	delete resourcesFolder;
}

void geVkRenderer::recreateSwapchain(uint32_t width, uint32_t height) {
	/* 
		must wait to all ops upon device(all its queues) are completed, because honestly i had no time to implement 
		watchdog which tracks which command buffer operates with swapchains framebuffers... :(
	*/
	vkDeviceWaitIdle(device);

	// recreate Swapchain
	VkSurfaceCapabilitiesKHR surfCaps = Auxiliary::Swapchain::surfaceCapabilities(device.getPhysicalDevice(), presentSurface);
	width = ge::Ax::min(width, surfCaps.maxImageExtent.width);
	height = ge::Ax::min(height, surfCaps.maxImageExtent.height);
	swapchain.changeResolution(width, height);

	// recreate depth buffer
	swapchain.createDepthBuffer();

	// recreates swapchain's framebuffers
	swapchain.createFramebuffers(componentsDB.renderPasses["base"]);
	
	// transfer images to proper format
	swapchainImagesLayoutTransfer();

	// truncate orphan pools
	//memManager.truncatePools();

}

void geVkRenderer::drawWorker(DrawWorkerInfo* info) {
	//uint32_t currentCommandBufferSetIdx = info->dispatchIdx;
	/* Update Logic Somehow */

	/* Update Matrices */
	info->thisPtr->vsUniforms[info->dispatchIdx].accessData().viewProjection = info->thisPtr->projection * info->scene->sceneCamera.getViewProjection(); // reverse orde because row major order in glm
	//info->thisPtr->vsUniforms[info->dispatchIdx].accessData().worldSpaceTr = glm::mat4(1.0); // after proper consultations, here will be parsing scene graph and computing transformation for each mesh
	//info->thisPtr->vsUniforms[info->dispatchIdx].accessData().normalsFix = glm::mat3(1.0); // == in current state glm::transpose(glm::inverse(glm::mat3(info->thisPtr->vsUniforms[info->dispatchIdx].accessData().worldSpaceTr)));

	// store current cmd buffer idx, to wait for its fence before starting update descriptor sets
	int idx = info->thisPtr->componentsDB.commandBufferSets[info->dispatchIdx]->getCurrentBufferIdx();	
	CommandBuffer& recordBuffer = info->thisPtr->componentsDB.commandBufferSets[info->dispatchIdx]->getNextBuffer();
	// wait until last mesh will be rendered
	if (idx > -1) {
		if (info->thisPtr->_useFenceAsSyncPrimitive) {
			info->thisPtr->componentsDB.commandBufferSets[info->dispatchIdx]->getBuffer(idx).getLock().getPrimitive<Fence>().waitUntilSignaled();
			// remove fence ptr
			info->thisPtr->componentsDB.commandBufferSets[info->dispatchIdx]->getBuffer(idx).getLock().getPrimitivePtr() = nullptr;
		}
		else 
			info->thisPtr->componentsDB.commandBufferSets[info->dispatchIdx]->getBuffer(idx).getLock().getPrimitive<Event>().waitUntilSignaled();

		// return current buffer
		info->thisPtr->componentsDB.commandBufferSets[info->dispatchIdx]->returnBuffer(idx);
	}

	/* Update descriptor sets */
	updateDescriptors(info);

	/* Record command buffers */
	// indixed or unindexed draw ... 
	if (info->mesh->buffers.find(ge::sg::AttributeDescriptor::Semantic::indices) != info->mesh->buffers.end()) {
		RecordIndexedDrawCommands rI;
		rI.renderWidth = info->thisPtr->swapchain.getDescription().width;
		rI.renderHeight = info->thisPtr->swapchain.getDescription().height;
		rI.pGraphicPipeline = (info->mesh->usedTextures.size() > 0) ? &info->thisPtr->pipelineDB.obtainGraphicPipeline("basic") : &info->thisPtr->pipelineDB.obtainGraphicPipeline("nonTex");
		// render pass info
		rI.renderPassInfo.pFramebuffer = &info->thisPtr->swapchain.getFramebuffer(); // return framebuffer for current image
		rI.renderPassInfo.pRenderPass = &info->thisPtr->componentsDB.renderPasses["base"];
		rI.renderPassInfo.renderArea.extent = { rI.renderWidth, rI.renderHeight };
		// index buffer
		rI.indexBufferInfo.pBuffer = info->mesh->buffers[ge::sg::AttributeDescriptor::Semantic::indices]->data;
		// vertex buffers
		rI.vertexBuffersInfo.firstBinding = 0;

		// store ptr to buffers, find each of them inside ge::sg::Mesh
		// position
		rI.vertexBuffersInfo.pBuffers.push_back(*info->mesh->buffers[ge::sg::AttributeDescriptor::Semantic::position]->data);
		rI.vertexBuffersInfo.offsets.push_back(0);
		// normals
		rI.vertexBuffersInfo.pBuffers.push_back(*info->mesh->buffers[ge::sg::AttributeDescriptor::Semantic::normal]->data);
		rI.vertexBuffersInfo.offsets.push_back(0);
		// textures
		if (info->mesh->usedTextures.size() > 0) {
			rI.vertexBuffersInfo.pBuffers.push_back(*info->mesh->buffers[ge::sg::AttributeDescriptor::Semantic::texcoord]->data);
			rI.vertexBuffersInfo.offsets.push_back(0);
		}

		// descriptors info, tady bacha!!!! otazka je jestli to bude spravne fungovat jestlize nabinduju do descriptor setu jeste neaktualizovany buffer? Nebude!!! prvni update descriptoru, pak buffer record :(
		rI.descriptorSetsInfo.bindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		rI.descriptorSetsInfo.firstSet = 0;
		rI.descriptorSetsInfo.pPipelineLayout = (info->mesh->usedTextures.size() > 0) ? &info->thisPtr->componentsDB.pipeLayouts["base"] : &info->thisPtr->componentsDB.pipeLayouts["nonTex"];
		rI.descriptorSetsInfo.pDescriptorSets.push_back(&*info->thisPtr->componentsDB.descrSets[info->dispatchIdx]);

		// push constants
		rI.pushConstants.offset = 0;
		rI.pushConstants.usedInStages = VK_SHADER_STAGE_FRAGMENT_BIT;
		rI.pushConstants.pValues = &info->thisPtr->_pushConstants;
		if (info->mesh->usedTextures.size() == 0) { // non textured version )
			rI.pushConstants.pLayout = &info->thisPtr->componentsDB.pipeLayouts["nonTex"];
			rI.pushConstants.size = sizeof(PushConstants);
		}
		else {
			rI.pushConstants.pLayout = &info->thisPtr->componentsDB.pipeLayouts["base"];
			rI.pushConstants.size = sizeof(uint32_t) * 2;
		}

		// draw info
		rI.drawInfo.firstIndex = 0;
		rI.drawInfo.firstInstance = 0;
		rI.drawInfo.indexCount = info->sgMesh->getAttribute(ge::sg::AttributeDescriptor::Semantic::indices)->itemsCount();
		rI.drawInfo.instanceCount = 1;
		rI.drawInfo.vertexOffset = 0;
		rI.useEvent = !info->thisPtr->_useFenceAsSyncPrimitive;

		info->thisPtr->recordIndexedDrawCommands(recordBuffer, rI);
	}
	else {
		RecordDrawCommands rI;
		rI.renderWidth = info->thisPtr->swapchain.getDescription().width;
		rI.renderHeight = info->thisPtr->swapchain.getDescription().height;
		rI.pGraphicPipeline = (info->mesh->usedTextures.size() > 0) ? &info->thisPtr->pipelineDB.obtainGraphicPipeline("basic") : &info->thisPtr->pipelineDB.obtainGraphicPipeline("nonTex");
		// render pass info
		rI.renderPassInfo.pFramebuffer = &info->thisPtr->swapchain.getFramebuffer(); // return framebuffer for current image
		rI.renderPassInfo.pRenderPass = &info->thisPtr->componentsDB.renderPasses["base"];
		rI.renderPassInfo.renderArea.extent = { rI.renderWidth, rI.renderHeight };
		// vertex buffers
		rI.vertexBuffersInfo.firstBinding = 0;

		// store ptr to buffers, find each of them inside ge::sg::Mesh
		// position
		rI.vertexBuffersInfo.pBuffers.push_back(*info->mesh->buffers[ge::sg::AttributeDescriptor::Semantic::position]->data);
		rI.vertexBuffersInfo.offsets.push_back(0);
		// normals
		rI.vertexBuffersInfo.pBuffers.push_back(*info->mesh->buffers[ge::sg::AttributeDescriptor::Semantic::normal]->data);
		rI.vertexBuffersInfo.offsets.push_back(0);
		// textures
		if (info->mesh->usedTextures.size() > 0) {
			rI.vertexBuffersInfo.pBuffers.push_back(*info->mesh->buffers[ge::sg::AttributeDescriptor::Semantic::texcoord]->data);
			rI.vertexBuffersInfo.offsets.push_back(0);
		}

		// descriptors info
		rI.descriptorSetsInfo.bindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		rI.descriptorSetsInfo.firstSet = 0;
		rI.descriptorSetsInfo.pPipelineLayout = (info->mesh->usedTextures.size() > 0) ? &info->thisPtr->componentsDB.pipeLayouts["base"] : &info->thisPtr->componentsDB.pipeLayouts["nonTex"];
		rI.descriptorSetsInfo.pDescriptorSets.push_back(&*info->thisPtr->componentsDB.descrSets[info->dispatchIdx]);

		// push constants
		rI.pushConstants.offset = 0;
		rI.pushConstants.usedInStages = VK_SHADER_STAGE_FRAGMENT_BIT;
		rI.pushConstants.pValues = &info->thisPtr->_pushConstants;
		if (info->mesh->usedTextures.size() == 0) { // non textured version )
			rI.pushConstants.pLayout = &info->thisPtr->componentsDB.pipeLayouts["nonTex"];
			rI.pushConstants.size = sizeof(PushConstants);
		}
		else {
			rI.pushConstants.pLayout = &info->thisPtr->componentsDB.pipeLayouts["base"];
			rI.pushConstants.size = sizeof(uint32_t) * 2;
		}

		// draw info
		rI.drawInfo.firstVertex = 0;
		rI.drawInfo.firstInstance = 0;
		rI.drawInfo.vertexCount = info->sgMesh->count;
		rI.drawInfo.instanceCount = 1;
		rI.useEvent = !info->thisPtr->_useFenceAsSyncPrimitive;

		info->thisPtr->recordDrawCommands(recordBuffer, rI);
	}
}

void geVkRenderer::mtDraw(geVkScene* renderScene) {
	/* Create tmp rendering components */
	vector<thread> workers;
	vector<DrawWorkerInfo> tInfos(_drawingThreadsCount);
	uint32_t availableQueuesCount = device.getQueueHandles(componentsDB.commandBufferSets[0]->getBuffer(0).getFamilyIndex()).availableQueues.size();
	vector<Fence*> submitFences(availableQueuesCount);

	// obtain current aux cmd buffer from last cmdset which is used for non draw cmd recording...
	CommandBuffer& auxCmdBuffer = componentsDB.commandBufferSets.back()->getNextBuffer();

	/* Get n Clear swapchain image */
	swapchain.acquireNextImage();
	swapchain.clearColors(auxCmdBuffer, commandProcessor);
	
	componentsDB.commandBufferSets.back()->returnCurrentBuffer();
	
	// --- Update variables per Scene --- //
	/* Update push constants since they represent static scene state for all rendered meshes */
	_pushConstants.lightingMode = renderScene->sceneLight.lightingMode;
	_pushConstants.lightSourceType = renderScene->sceneLight.sourceType;

	// update light properties
	for (int i = 0; i < _drawingThreadsCount; i++) {
		fsUniforms[i].accessData().color = renderScene->sceneLight.color;
		fsUniforms[i].accessData().cutOff = renderScene->sceneLight.cutOff;
		fsUniforms[i].accessData().direction = renderScene->sceneLight.direction;
		fsUniforms[i].accessData().position = renderScene->sceneLight.position;
		fsUniforms[i].accessData().eyePos = renderScene->sceneCamera.getEyeCoords();
	}

	/* Dispatch drawing worker Threads */
	unsigned dispatchIdx(0);
	unsigned gpuMeshIdx(0);

	for (auto model : renderScene->activeScene->models) {
		// --- Update variables per model --- //
		for (int i = 0; i < model->meshes.size() || dispatchIdx > 0;) { // if there is something to draw or submit to queue			
			// if all worker thread have work to do or there is not enought meshes to start another thread
			if (dispatchIdx == _drawingThreadsCount || gpuMeshIdx == renderScene->gpuMeshes.size() || i == model->meshes.size()) {				
				// before posting cmd buffers waiting to all threads ended their exec -> join all threads
				for (auto& thread : workers)
					thread.join();

				// --- Post commands from threads here, iterationCount == posted worker threads --- // 
				// all resources should be properly updated right here :) // 
				// if there is only one mesh to render 
				if (renderScene->gpuMeshes.size() == 1) {
					Info::Methods::PostSingleBuffer postTemplate;
					//fillSubmitInfo(postTemplate, 's');
					postTemplate.pBuffer = &componentsDB.commandBufferSets[0]->getCurrentBuffer();
					commandProcessor.postCommands(postTemplate, &drawCallFences.getNextFence());
					postTemplate.pBuffer->getLock().getPrimitivePtr() = &drawCallFences.getCurrentFence();
					break; // jump off
				}

				// create postInfo
				Info::Methods::PostCommandBuffers postI; // submit info
				for (int j = 0; j < dispatchIdx; j++) {
					Info::Methods::PostSingleBuffer postTemplate;
					if ((gpuMeshIdx == _drawingThreadsCount || _drawingThreadsCount >= model->meshes.size()) && j == 0) // first mesh
						fillSubmitInfo(postTemplate, 'f');
						
					else if (gpuMeshIdx == renderScene->gpuMeshes.size() && j == (dispatchIdx - 1)) // last mesh
						fillSubmitInfo(postTemplate, 'l');

					else // others
						fillSubmitInfo(postTemplate, 'r');

					postTemplate.pBuffer = &componentsDB.commandBufferSets[j]->getCurrentBuffer();
					postI.bulk.push_back(postTemplate);
				}

				if (_useFenceAsSyncPrimitive) {
					// need to find free fence for every available queue
					for (int q = 0; q < availableQueuesCount; q++)
						submitFences[q] = &drawCallFences.getNextFence();

					// post buffers
					commandProcessor.multiQueueCommandsPost(postI, submitFences);
				}
				else {
					// post buffers
					commandProcessor.multiQueueCommandsPost(postI);
				}

				// clear thread vector
				workers.clear(); 
				dispatchIdx = 0;
			}
			// dispatch as many threads as possible
			else { // (dispatchIdx < _drawingThreadsCount)				
				tInfos[dispatchIdx].thisPtr = this;
				tInfos[dispatchIdx].dispatchIdx = dispatchIdx;
				tInfos[dispatchIdx].sgMesh = &*model->meshes[i];
				tInfos[dispatchIdx].mesh = &renderScene->gpuMeshes[&*model->meshes[i]];
				tInfos[dispatchIdx].scene = renderScene;
				// tady jeste potreba ukladat jaky je soucasny prochazeny model 

				workers.push_back(thread(drawWorker, &tInfos[dispatchIdx]));
		
				gpuMeshIdx++; dispatchIdx++; i++;
			}
		}
	}
	
	for (int i = 0; i < componentsDB.commandBufferSets.size() - 1; i++) {
		if (componentsDB.commandBufferSets[i]->getCurrentBufferIdx() > -1) {
			// need to wait for all draw cmd buffers were succefully posted and image is whole rendered  
			componentsDB.commandBufferSets[i]->getCurrentBuffer().waitUntilExecutionFinished();

			// return last used buffers
			componentsDB.commandBufferSets[i]->returnCurrentBuffer(); 
		}
	}

	// --- Present swapchain image --- //
	swapchain.presentImage();
	
}

void geVkRenderer::meshSGRoutine1(ge::sg::Mesh* mesh, geVkScene& renderScene, uint32_t currentImageIdx) {
	// obtain previous cmd buffer and wait until his completion
	//componentsDB.commandBufferSets[(_previousMeshWasTextured) ? 0 : 1]->getBuffer(currentImageIdx).waitUntilExecutionFinished();
	componentsDB.commandBufferSets[0]->getBuffer(currentImageIdx).waitUntilExecutionFinished();

	//_previousMeshWasTextured = (renderScene.gpuMeshes[mesh.get()].usedTextures.size() > 0) ? true : false;
	auto& currentCmdBuff = componentsDB.commandBufferSets[0]->getBuffer(currentImageIdx); //componentsDB.commandBufferSets[(_previousMeshWasTextured) ? 0 : 1]->getBuffer(currentImageIdx);

	/* Update Matrices */
	vsUniforms[0].accessData().viewProjection = projection * renderScene.sceneCamera.getViewProjection(); // reverse orde because row major order in glm
	//vsUniforms[0].accessData().worldSpaceTr = glm::mat4(1.0); // after proper consultations, here will be parsing scene graph and computing transformation for each mesh
	//vsUniforms[0].accessData().normalsFix = glm::mat3(1.0); // == in current state glm::transpose(glm::inverse(glm::mat3(info->thisPtr->vsUniforms[info->dispatchIdx].accessData().worldSpaceTr)));

	/* update indirect draw parameters */
	indirectDrawStorage.accessData().indexCount = mesh->indicesCount;
	indirectDrawStorage.accessData().instanceCount = 1;
	indirectDrawStorage.accessData().firstIndex = renderScene.gpuMeshes[mesh].offset;
	indirectDrawStorage.accessData().firstInstance = 0;
	indirectDrawStorage.accessData().vertexOffset = 0; // ?

	/* submit command buffers */
	Info::Methods::PostSingleBuffer postTemplate;
	postTemplate.pBuffer = &currentCmdBuff;
	commandProcessor.postCommands(postTemplate, &currentCmdBuff.getLock().getPrimitive<Fence>());
}

void geVkRenderer::meshSGRoutine2(ge::sg::Mesh* mesh, geVkScene& renderScene, uint32_t currentImageIdx) {
	// wait until previous cmd buffer finish execution
	componentsDB.commandBufferSets[0]->getBuffer(0).waitUntilExecutionFinished();

	/* Update Matrices */
	vsUniforms[0].accessData().viewProjection = projection * renderScene.sceneCamera.getViewProjection(); // reverse orde because row major order in glm
	//vsUniforms[0].accessData().worldSpaceTr = glm::mat4(1.0); // after proper consultations, here will be parsing scene graph and computing transformation for each mesh
	//vsUniforms[0].accessData().normalsFix = glm::mat3(1.0); // == in current state glm::transpose(glm::inverse(glm::mat3(info->thisPtr->vsUniforms[info->dispatchIdx].accessData().worldSpaceTr)));

	/* Update descriptors */
	updateDescriptors(&renderScene, mesh, 0);

	/* record commands */
	geVkMesh& gmesh = renderScene.gpuMeshes[mesh];
	RecordIndexedDrawCommands rI;
	rI.renderWidth = swapchain.getDescription().width;
	rI.renderHeight = swapchain.getDescription().height;
	rI.pGraphicPipeline = (gmesh.usedTextures.size() > 0) ? &pipelineDB.obtainGraphicPipeline("basic") : &pipelineDB.obtainGraphicPipeline("nonTex");
	// render pass info
	rI.renderPassInfo.pFramebuffer = &swapchain.getFramebuffer(); // return framebuffer for current image
	rI.renderPassInfo.pRenderPass = &componentsDB.renderPasses["base"];
	rI.renderPassInfo.renderArea.extent = { rI.renderWidth, rI.renderHeight };
	// index buffer
	rI.indexBufferInfo.pBuffer = gmesh.buffers[ge::sg::AttributeDescriptor::Semantic::indices]->data;
	// vertex buffers
	rI.vertexBuffersInfo.firstBinding = 0;

	// store ptr to buffers, find each of them inside ge::sg::Mesh
	// position
	rI.vertexBuffersInfo.pBuffers.push_back(*gmesh.buffers[ge::sg::AttributeDescriptor::Semantic::position]->data);
	rI.vertexBuffersInfo.offsets.push_back(0);
	// normals
	rI.vertexBuffersInfo.pBuffers.push_back(*gmesh.buffers[ge::sg::AttributeDescriptor::Semantic::normal]->data);
	rI.vertexBuffersInfo.offsets.push_back(0);
	// textures
	if (gmesh.usedTextures.size() > 0) {
		rI.vertexBuffersInfo.pBuffers.push_back(*gmesh.buffers[ge::sg::AttributeDescriptor::Semantic::texcoord]->data);
		rI.vertexBuffersInfo.offsets.push_back(0);
	}

	// descriptors info, tady bacha!!!! otazka je jestli to bude spravne fungovat jestlize nabinduju do descriptor setu jeste neaktualizovany buffer? Nebude!!! prvni update descriptoru, pak buffer record :(
	rI.descriptorSetsInfo.bindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	rI.descriptorSetsInfo.firstSet = 0;
	rI.descriptorSetsInfo.pPipelineLayout = (gmesh.usedTextures.size() > 0) ? &componentsDB.pipeLayouts["base"] : &componentsDB.pipeLayouts["nonTex"];
	rI.descriptorSetsInfo.pDescriptorSets.push_back(&*componentsDB.descrSets[0]);

	// push constants
	rI.pushConstants.offset = 0;
	rI.pushConstants.usedInStages = VK_SHADER_STAGE_FRAGMENT_BIT;
	rI.pushConstants.pValues = &_pushConstants;
	if (gmesh.usedTextures.size() == 0) { // non textured version )
		rI.pushConstants.pLayout = &componentsDB.pipeLayouts["nonTex"];
		rI.pushConstants.size = sizeof(PushConstants);
	}
	else {
		rI.pushConstants.pLayout = &componentsDB.pipeLayouts["base"];
		rI.pushConstants.size = sizeof(uint32_t) * 2;
	}

	// draw info
	rI.drawInfo.firstIndex = 0;
	rI.drawInfo.firstInstance = 0;
	rI.drawInfo.indexCount = mesh->getAttribute(ge::sg::AttributeDescriptor::Semantic::indices)->itemsCount();
	rI.drawInfo.instanceCount = 1;
	rI.drawInfo.vertexOffset = 0;

	recordIndexedDrawCommands(componentsDB.commandBufferSets[0]->getBuffer(0), rI);

	/* submit command buffers */
	Info::Methods::PostSingleBuffer postTemplate;
	postTemplate.pBuffer = &componentsDB.commandBufferSets[0]->getBuffer(0);
	commandProcessor.postCommands(postTemplate, &componentsDB.commandBufferSets[0]->getBuffer(0).getLock().getPrimitive<Fence>());

}

// recursive solution of traversing tree ... using preOrder
void geVkRenderer::sgDraw(ge::sg::MatrixTransformNode* currentNode, geVkScene& renderScene, uint32_t currentImageIdx, ge::util::MatrixStack& stack) {
	// add matrix to stack for children to process it
	if (currentNode->data->getRefMatrix() != nullptr)
		stack.push(*currentNode->data->getRefMatrix());

	// compute matrix
	

	// draw meshes connected with node
	for (auto mesh : currentNode->data->meshes) {
		/* Update logic per mesh*/

		// draw mesh routines
		if (_useRoutineVersion == -3) 
			meshSGRoutine1(&*mesh, renderScene, currentImageIdx);
		else 
			meshSGRoutine2(&*mesh, renderScene, currentImageIdx);
	}

	// traverse childrens
	for (auto& child : currentNode->children)
		sgDraw(&*child, renderScene, currentImageIdx, stack);

	// subtree processed here, poping matrix
	stack.pop();
}

void geVkRenderer::draw(geVkScene* renderScene) {
	if (renderScene == nullptr || renderScene->activeScene == nullptr || renderScene->activeScene->models.size() == 0)
		return;
	
	if (_useRoutineVersion != -2) {
		ge::util::MatrixStack matrixStack;

		// obtain current aux cmd buffer from last cmdset which is used for non draw cmd recording...
		CommandBuffer& auxCmdBuffer = componentsDB.commandBufferSets.back()->getNextBuffer();

		/* Get n Clear swapchain image */
		swapchain.acquireNextImage();
		swapchain.clearColors(auxCmdBuffer, commandProcessor);

		componentsDB.commandBufferSets.back()->returnCurrentBuffer();

		// update compotents per scene
		/* Update push constants since they represent static scene state for all rendered meshes */
		_pushConstants.lightingMode = renderScene->sceneLight.lightingMode;
		_pushConstants.lightSourceType = renderScene->sceneLight.sourceType;

		// update light properties
		fsUniforms[0].accessData().color = renderScene->sceneLight.color;
		fsUniforms[0].accessData().cutOff = renderScene->sceneLight.cutOff;
		fsUniforms[0].accessData().direction = renderScene->sceneLight.direction;
		fsUniforms[0].accessData().position = renderScene->sceneLight.position;
		fsUniforms[0].accessData().eyePos = renderScene->sceneCamera.getEyeCoords();

		// draw scene
		sgDraw(&*renderScene->activeScene->rootNode, *renderScene, swapchain.getCurrentImageIdx(), matrixStack);

		// --- Present swapchain image --- //
		swapchain.presentImage();
	}
	else
		mtDraw(renderScene);
}

void geVkRenderer::debugDraw() {
	// obtain current aux cmd buffer from last cmdset which is used for non draw cmd recording...
	CommandBuffer& auxCmdBuffer = componentsDB.commandBufferSets.back()->getNextBuffer();

	/* Get n Clear swapchain image */
	swapchain.acquireNextImage();
	swapchain.clearColors(auxCmdBuffer, commandProcessor);

	componentsDB.commandBufferSets.back()->returnCurrentBuffer();

	// record commnads
	componentsDB.commandBufferSets[0]->returnCurrentBuffer();
	CommandBuffer& recordBuffer = componentsDB.commandBufferSets[0]->getNextBuffer();

	// render pass info
	ge::Vk::Info::Commands::BeginRenderPass renderPassInfo;
	renderPassInfo.pFramebuffer = &swapchain.getFramebuffer(); // return framebuffer for current image
	renderPassInfo.pRenderPass = &componentsDB.renderPasses["base"];
	renderPassInfo.renderArea.extent = { swapchain.getDescription().width, swapchain.getDescription().height };

	/* begin recording */
	recordBuffer.beginRecording();

	/* begin renderPass */
	recordBuffer.beginRenderPass(renderPassInfo);

	/* bind Pipeline */
	recordBuffer.bindGraphicPipeline(pipelineDB.obtainGraphicPipeline("debug"));

	/* set viewport and scissor test */
	Info::Commands::SetViewport viewport;
	viewport.width = swapchain.getDescription().width;
	viewport.height = swapchain.getDescription().height;
	recordBuffer.setViewport(viewport);

	Info::Commands::SetScissor scissor;
	scissor.width = swapchain.getDescription().width;
	scissor.height = swapchain.getDescription().height;
	recordBuffer.setScissor(scissor);

	/* record draw command */
	recordBuffer.draw(3);

	/* end renderpass */
	recordBuffer.endRenderPass();

	/* finish recording */
	recordBuffer.finishRecording();

	/* post commands */
	Info::Methods::PostSingleBuffer postTemplate;
	postTemplate.pBuffer = &componentsDB.commandBufferSets[0]->getCurrentBuffer();
	commandProcessor.postCommands(postTemplate, &postTemplate.pBuffer->getLock().getPrimitive<Fence>());
	
	/* Present swapchain image */
	swapchain.presentImage();

	componentsDB.commandBufferSets[0]->returnCurrentBuffer();
}

void geVkRenderer::fillSubmitInfo(ge::Vk::Info::Methods::PostSingleBuffer& info, char meshPosition) {
	info.insideStages.push_back(VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT);
	switch (meshPosition) {
		case 's': // single mesh
			break;

		case 'f': // first mesh
			info.signalUponFinish.push_back(componentsDB.semaphores["aSync"]);
			break;

		case 'r': // between meshes
			info.signalUponFinish.push_back(componentsDB.semaphores["aSync"]);
			info.waitOn.push_back(componentsDB.semaphores["aSync"]);
			break;

		case 'l': // last mesh
			info.waitOn.push_back(componentsDB.semaphores["aSync"]);
			break;

		default:
			break;
	}
}