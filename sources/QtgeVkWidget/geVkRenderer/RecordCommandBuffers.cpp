/**
* @author Radek Blahos
* @project geVkWidget Library
*
* @brief This file contains implementation of command buffer recording
*/

#include <QtgeVkWidget\geVkRenderer\geVkRenderer.h>
#include <geVk\Image.inl>
#include <geAx\AuxInlines.inl>
#include <geAx\ExceptionMacros.h>
#include <geVk\Swapchain.h>
#include <geVk\CommandBuffer.inl>
#include <ste/DAG.h>
#include <thread>

using namespace std;
using namespace ge::Vk;
using namespace ge::Ax;

void geVkRenderer::recordCoreCommands(ge::Vk::CommandBuffer& keepCommands, RecordDrawCommandsCore& info) {
	/* bind Pipeline */
	keepCommands.bindGraphicPipeline(*info.pGraphicPipeline);

	/* set viewport and scissor test */
	Info::Commands::SetViewport viewport;
	viewport.width = info.renderWidth;
	viewport.height = info.renderHeight;
	keepCommands.setViewport(viewport);

	Info::Commands::SetScissor scissor;
	scissor.width = info.renderWidth;
	scissor.height = info.renderHeight;
	keepCommands.setScissor(scissor);

	/* bind vertexBuffers */
	keepCommands.bindVertexBuffers(info.vertexBuffersInfo);

	/* bind descriptors */
	if (info.descriptorSetsInfo.pDescriptorSets.size() > 0)
		keepCommands.bindDescriptorSets(info.descriptorSetsInfo);

	/* push constants */
	keepCommands.pushConstants(info.pushConstants);
}

void geVkRenderer::recordDrawCommands(ge::Vk::CommandBuffer& keepCommands, RecordDrawCommands& info) {
	/* begin recording */
	keepCommands.beginRecording((_useRoutineVersion != -2) ? 0 : VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);

	// if syncing via events, submit a command to signal it
	if (info.useEvent) {
		// set event right now to signalize it is used  
		vkSetEvent(*keepCommands.getDescription().pPool->getDescription().pDevice, keepCommands.getLock().getPrimitive<Event>());
		keepCommands.resetEvent(keepCommands.getLock().getPrimitive<Event>(), VK_PIPELINE_STAGE_ALL_COMMANDS_BIT); // event is reset after queue finishes with cmd buffer
	}

	/* begin renderPass */
	keepCommands.beginRenderPass(info.renderPassInfo);

	/* record shared commands */
	recordCoreCommands(keepCommands, info);

	/* record draw command */
	keepCommands.draw(info.drawInfo);

	/* end renderpass */
	keepCommands.endRenderPass();

	/* finish recording */
	keepCommands.finishRecording();

	//cerr << "recorded buffer: " << keepCommands.getVkBuffer() << endl;
}

void geVkRenderer::recordIndexedDrawCommands(ge::Vk::CommandBuffer& keepCommands, RecordIndexedDrawCommands& info) {
	/* begin recording */
	keepCommands.beginRecording((_useRoutineVersion != -2) ? 0 : VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);

	// if syncing via events, submit a command to signal it
	if (info.useEvent) {
		//cerr << keepCommands.getLock().getPrimitive<Event>() << endl;
		// set event right now to signalize it is used  
		Event& currEvent = keepCommands.getLock().getPrimitive<Event>();
		vkSetEvent(*keepCommands.getDescription().pPool->getDescription().pDevice, currEvent);
		keepCommands.resetEvent(currEvent, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT); // event is reset after queue finishes with cmd buffer
	}

	/* begin renderPass */
	keepCommands.beginRenderPass(info.renderPassInfo);

	/* record shared commands */
	recordCoreCommands(keepCommands, info);

	/* bind indexBuffer */
	keepCommands.bindIndexBuffer(info.indexBufferInfo);

	/* record draw command */
	keepCommands.drawIndexed(info.drawInfo);

	/* end renderpass */
	keepCommands.endRenderPass();

	/* finish recording */
	keepCommands.finishRecording();

	//cerr << "recorded buffer: " << keepCommands.getVkBuffer() << endl;
}

void geVkRenderer::recordIndexedIndirectDrawCommands(ge::Vk::CommandBuffer& keepCommands, RecordIndexedIndirectDrawCommands& info) {
	/* begin recording */
	keepCommands.beginRecording((_useRoutineVersion != -2) ? 0 : VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);

	/* begin renderPass */
	keepCommands.beginRenderPass(info.renderPassInfo);

	/* record shared commands */
	recordCoreCommands(keepCommands, info);

	/* bind indexBuffer */
	keepCommands.bindIndexBuffer(info.indexBufferInfo);

	/* draw command */
	keepCommands.drawIndexedIndirect(info.drawInfo);

	/* end renderpass */
	keepCommands.endRenderPass();

	/* finish recording */
	keepCommands.finishRecording();
}
