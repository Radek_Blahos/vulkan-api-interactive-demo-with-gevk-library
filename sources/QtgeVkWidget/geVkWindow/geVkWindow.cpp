/**
* @author Radek Blahos
* @project geVkWidget Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <QtgeVkWidget\geVkWindow\geVkWindow.h>
#include <iostream>
#include <QtgeVkWidget\geVkWidget.h>

using namespace std;

geVkWindow::geVkWindow() {
	QWindow::setSurfaceType(QSurface::VulkanSurface);
}

geVkWindow::geVkWindow(uint32_t width, uint32_t height, std::string name, QWindow* parent)
	: QWindow(parent)
{
	// Window Init
	QWindow::setTitle(name.c_str());
	QWindow::resize(width, height);
	QWindow::setSurfaceType(QSurface::VulkanSurface);

	// init input handler
	//_inputHandler = new InputHandler();
}

geVkWindow::~geVkWindow() {
	//delete _inputHandler;
}

void geVkWindow::updateWindow() {
	QWindow::requestUpdate();
	// if want to continuous redraw
	emit windowUpdated();
}