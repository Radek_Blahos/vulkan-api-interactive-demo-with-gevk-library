/**
* @author Radek Blahos
* @project geVkWidget Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <QtgeVkWidget/ConfigureRendererWidget/ConfigureRendererWidget.h>
#include <qcolordialog.h>
#include <glm\gtx\rotate_vector.hpp>
#include <qfiledialog.h>

ConfigRendererWidget::ConfigRendererWidget() {
	// init components
	_mainLayout = new QGridLayout();

	_buttons.resize(3);
	for (int i = 0; i < _buttons.size(); i++)
		_buttons[i] = new QPushButton();

	_spinBoxes.resize(2);
	for (int i = 0; i < _spinBoxes.size(); i++)
		_spinBoxes[i] = new QSpinBox();

	_labels.resize(4);
	for (int i = 0; i < _labels.size(); i++)
		_labels[i] = new QLabel();

	_groupBoxes.resize(5);
	for (int i = 0; i < _groupBoxes.size(); i++)
		_groupBoxes[i] = new QGroupBox();

	_checkboxes.resize(5);
	for (int i = 0; i < _checkboxes.size(); i++)
		_checkboxes[i] = new QCheckBox();

	_bg.resize(2);
	for (int i = 0; i < _bg.size(); i++)
		_bg[i] = new QButtonGroup();

	setWindowTitle("Configure geVkRenderer");

	// seting up gui
	setLayout(_mainLayout);

	// choose if use scene graph or not
	QHBoxLayout* useSGLayout = new QHBoxLayout();
	_mainLayout->addWidget(_groupBoxes[4]);
	_groupBoxes[4]->setLayout(useSGLayout);
	_bg[1] = new QButtonGroup();
	_bg[1]->setExclusive(true);
	useSGLayout->addWidget(_labels[3]);
	_labels[3]->setText("Choose version of rendering routine"); // Choose if use Scene Graph during rendering
	_checkboxes[2] = new QCheckBox("Multi-Threaded");
	_checkboxes[3] = new QCheckBox("PreInit everything possible");
	_checkboxes[4] = new QCheckBox("Versatile");
	_checkboxes[4]->setChecked(true);
	_bg[1]->addButton(_checkboxes[2]);
	_bg[1]->addButton(_checkboxes[3]);
	_bg[1]->addButton(_checkboxes[4]);
	useSGLayout->addWidget(_checkboxes[2]);
	useSGLayout->addWidget(_checkboxes[3]);
	useSGLayout->addWidget(_checkboxes[4]);

	// choose resource folder
	_mainLayout->addWidget(_buttons[2]);
	_buttons[2]->setText("Choose resource folder");

	// threads
	QHBoxLayout* threadLayout = new QHBoxLayout();
	_mainLayout->addWidget(_groupBoxes[0]);
	_groupBoxes[0]->setLayout(threadLayout);
	threadLayout->addWidget(_labels[0]);
	_labels[0]->setText("Set number of running threads:");
	threadLayout->addWidget(_spinBoxes[1]);
	_spinBoxes[1]->setMinimum(1); _spinBoxes[1]->setMaximum(12);
	_spinBoxes[1]->setValue(3);

	// queues
	QHBoxLayout* queueLayout = new QHBoxLayout();
	_mainLayout->addWidget(_groupBoxes[1]);
	_groupBoxes[1]->setLayout(queueLayout);
	queueLayout->addWidget(_labels[1]);
	_labels[1]->setText("Set number of working queues:");
	queueLayout->addWidget(_spinBoxes[0]);
	_spinBoxes[0]->setMinimum(1); _spinBoxes[0]->setMaximum(12);
	_spinBoxes[0]->setValue(1);

	// choose what sync primitive to use during draw 
	QHBoxLayout* syncprimitiveLayout = new QHBoxLayout();
	_mainLayout->addWidget(_groupBoxes[3]);
	_groupBoxes[3]->setLayout(syncprimitiveLayout);
	_bg[0] = new QButtonGroup();
	_bg[0]->setExclusive(true);
	syncprimitiveLayout->addWidget(_labels[2]);
	_labels[2]->setText("Set which sync primitive use during draw routine:");
	_checkboxes[0] = new QCheckBox("Event");
	_checkboxes[1] = new QCheckBox("Fence");
	_checkboxes[1]->setChecked(true);
	_bg[0]->addButton(_checkboxes[0]);
	_bg[0]->addButton(_checkboxes[1]);
	syncprimitiveLayout->addWidget(_checkboxes[0]);
	syncprimitiveLayout->addWidget(_checkboxes[1]);

	// confirm buttons
	QHBoxLayout* confirmActionLayout = new QHBoxLayout();
	_mainLayout->addWidget(_groupBoxes[2]);
	_groupBoxes[2]->setLayout(confirmActionLayout);
	confirmActionLayout->addWidget(_buttons[0]);
	_buttons[0]->setText("OK");
	confirmActionLayout->addWidget(_buttons[1]);
	_buttons[1]->setText("Storno");

	// signals and slots
	connect(_buttons[0], SIGNAL(clicked()), this, SLOT(okbtnClicked()));
	connect(_buttons[1], SIGNAL(clicked()), this, SLOT(closebtnClicked()));
	connect(_buttons[2], SIGNAL(clicked()), this, SLOT(chooseResourcesSource()));
}

ConfigRendererWidget::~ConfigRendererWidget() {
	for (int i = 0; i < _groupBoxes.size(); i++)
		delete _groupBoxes[i];

	for (int i = 0; i < _bg.size(); i++)
		delete _bg[i];

	delete _mainLayout;
}

void ConfigRendererWidget::okbtnClicked() {
	hide();
	emit configurationEnded();
}

void ConfigRendererWidget::closeEvent(QCloseEvent* event) {
	closebtnClicked();
}

void ConfigRendererWidget::closebtnClicked() {
	hide();
	_spinBoxes[0]->setValue(1);
	_spinBoxes[1]->setValue(3);
	emit configurationEnded();
}

void ConfigRendererWidget::chooseResourcesSource() {
	QString dirName = QFileDialog::getExistingDirectory(this, tr("Set Directory"),
		"/home",
		QFileDialog::ShowDirsOnly
		| QFileDialog::DontResolveSymlinks);

	// if there is non scene to process...
	if (dirName.size() < 1)
		return;

	emit resourceFolderChoosed(dirName);
}