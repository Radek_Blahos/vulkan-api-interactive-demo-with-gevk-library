set(SMODULE_NAME "ste") #whatever it might be ...
set(SMODULE_DIR "${CMAKE_CURRENT_LIST_DIR}/..")

#If target was already defined then return. Perhaps it was included from another package.
IF(TARGET ${SMODULE_NAME})
   return()
ENDIF()

file(GLOB "${SMODULE_NAME}_INCLUDES"
		"${SMODULE_DIR}/ste/*.h"
	)

add_library(${SMODULE_NAME} INTERFACE)

target_include_directories(${SMODULE_NAME} INTERFACE "${SMODULE_DIR}/")

