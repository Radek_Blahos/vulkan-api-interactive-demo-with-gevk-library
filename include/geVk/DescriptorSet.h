/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Descriptor Set wrapper
*/

#ifndef GEVKDESCRIPTOR_H
#define GEVKDESCRIPTOR_H

#include <geAx\PtrVector.inl>
#include <geVk\DescriptorManager.h>
#include <geVk\DescriptorSetLayout.h>
#include <geVk\DescriptorSetAuxils.h>
#include <vector>
#include <unordered_map>

/* ----------- Info Class ----------- */
class ge::Vk::Info::DescriptorSet {
// attributes
public:
	ge::Vk::DescriptorManager* pManager;

};

/* ----------- Create Info Class ----------- */
class ge::Vk::CreateInfo::DescriptorSet : public ge::Vk::Info::DescriptorSet {
// attributes
public:
	CreateInfo::DescriptorSetLayout mainLayoutInfo;

};

/* ------- Core Class ------- */
class ge::Vk::DescriptorSet {
// friend class 
	friend class ge::Vk::DescriptorManager;

// attributes
private:
	Info::DescriptorSet* _description = nullptr;
	std::unordered_map<VkDescriptorType, uint32_t> _layoutMemReqs;

	DescriptorPool* _allocatedFrom;
	DescriptorSetLayout _mainLayout; // layout which can be allocated from pool in memory manager
	ge::Ax::PtrVector<DescriptorSetLayout> _layoutViews; // contains layouts which are parents of main layout(ommiting some descriptor from main layout...)
	VkDescriptorSet _memory = VK_NULL_HANDLE; // ptrs to GPU memory where layouts are stored -> is initialized after passing them into desciptors manager

// methods
private:
	void init(CreateInfo::DescriptorSet& createInfo);
	void deinit();
	//bool initCompleted();
	void move(DescriptorSet& withdraw);

public:
	/* Ctors 'n Dtor */
	DescriptorSet() {}
	DescriptorSet(CreateInfo::DescriptorSet& createInfo) { init(createInfo); }
	DescriptorSet(DescriptorSet& copy) { move(copy); }
	~DescriptorSet() { deinit(); }

	/* Getters */
	DescriptorSetLayout& getLayout() { return _mainLayout; }
	VkDescriptorSet getMemory() { return _memory; }
	ge::Ax::PtrVector<DescriptorSetLayout>& getLayoutViews() { return _layoutViews; }

	/* Operators */
	DescriptorSet& operator=(DescriptorSet& assign) { move(assign); return *this; }

	/* Core Methods */
	void create(CreateInfo::DescriptorSet& createInfo) { init(createInfo); }
	void destroy() { deinit(); }
	// update descriptor set data
	inline void update(Info::Methods::UpdateDescriptorSets& info);
	void addLayoutView(CreateInfo::DescriptorSetLayout& layoutInfo) { _layoutViews.push_back(std::make_shared<DescriptorSetLayout>(layoutInfo)); }

	/* Operators */
	operator VkDescriptorSet() { return _memory; }

	/* Status Checkers */

};
#endif