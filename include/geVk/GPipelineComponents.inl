/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Pipeline componets used inside CreateInfo::GraphicPipeline ...
*/

#ifndef GEVKPIPECOMPONENTS_INL
#define GEVKPIPECOMPONENTS_INL

#include <geVk\PipelineComponents.h>

using namespace ge::Vk;

inline Info::Pipe::VertexInputDescription::operator VkVertexInputBindingDescription() {
	return VkVertexInputBindingDescription {
		location,
		stride,
		inputRate
	};
}

inline Info::Pipe::VertexInputDescription::operator VkVertexInputAttributeDescription() {
	return VkVertexInputAttributeDescription{
		location,
		location,
		format,
		offset
	};
}

inline Info::Pipe::InputAssemblyState::operator VkPipelineInputAssemblyStateCreateInfo() {
	return VkPipelineInputAssemblyStateCreateInfo {
		VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
		nullptr,
		0,
		topology,
		primitiveRestartEnable
	};
}

inline Info::Pipe::DepthStencilState::operator VkPipelineDepthStencilStateCreateInfo() {
	return VkPipelineDepthStencilStateCreateInfo {
		VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
		nullptr,
		0,
		depthTestEnable,
		depthWriteEnable,
		depthCompareOp,
		depthBoundsTestEnable,
		stencilTestEnable,
		front,
		back,
		minDepthBounds,
		maxDepthBounds
	};
}

inline Info::Pipe::MultisampleState::operator VkPipelineMultisampleStateCreateInfo() {
	return VkPipelineMultisampleStateCreateInfo{
		VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
		nullptr,
		0,
		rasterizationSamples,
		sampleShadingEnable,
		minSampleShading,
		&SampleMask,
		alphaToCoverageEnable,
		alphaToOneEnable,
	};
}

inline Info::Pipe::RasterizationState::operator VkPipelineRasterizationStateCreateInfo() {
	return VkPipelineRasterizationStateCreateInfo{
		VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
		nullptr,
		0,
		depthClampEnable,
		rasterizerDiscardEnable,
		polygonMode,
		cullMode,
		frontFace,
		depthBiasEnable,
		depthBiasConstantFactor,
		depthBiasClamp,
		depthBiasSlopeFactor,
		lineWidth
	};
}

inline Info::Pipe::TessellationState::operator VkPipelineTessellationStateCreateInfo() {
	return VkPipelineTessellationStateCreateInfo{
		VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO,
		nullptr,
		0,
		patchControlPoints
	};
}

inline Info::Pipe::StencilOpState::operator VkStencilOpState() {
	return VkStencilOpState {
		failOp,
		passOp,
		depthFailOp,
		compareOp,
		compareMask,
		writeMask,
		reference
	};
}

inline Info::Pipe::ViewportState::operator VkPipelineViewportStateCreateInfo() {
	return VkPipelineViewportStateCreateInfo{
		VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
		nullptr,
		0,
		(uint32_t) viewports.size(),
		viewports.data(),
		(uint32_t)scissors.size(),
		scissors.data()
	};
}

#endif