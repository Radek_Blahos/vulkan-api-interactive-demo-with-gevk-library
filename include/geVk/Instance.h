/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Instance wrapper
*/

#ifndef GEVK_INSTANCE_H
#define GEVK_INSTANCE_H

#include <geVk/geVk.h>
#include <string>
#include <vector>

/* ----------- Info Class ----------- */
class ge::Vk::Info::Instance {
/* attributes */
public:
	bool useValidationLayers = false;
	bool abundantInfo = false;
	bool verboseMode = false;

// virtual dtor
public:
	virtual ~Instance() = default;

};

class ge::Vk::FurtherInfo::Instance : public ge::Vk::Info::Instance {
/* attributes */
public:
	std::string appName;
	uint32_t appVersion;
	std::string engineName;
	uint32_t engineVersion;
	uint32_t vkVersion;
	std::vector<const char*> enabledExtensions;
	std::vector<const char*> enabledLayers;

};

/* ----------- Create Info Class ----------- */
class ge::Vk::CreateInfo::Instance : public ge::Vk::FurtherInfo::Instance {
/* attributes */
private:
	VkApplicationInfo appInfo;

/* methods */
public:
	/* Operators */
	inline operator VkApplicationInfo();
	operator VkInstanceCreateInfo();

};

/* ------- Auxiliary Class ------- */
class ge::Vk::Auxiliary::Instance {
// methods
public:
	// makes version from specified numbers
	static inline uint32_t makeVersion(uint16_t major, uint16_t minor, uint16_t patch);
	static inline std::string getVersion(uint32_t encodedIn);

	// enumerate all available PhysicalDevices
	static void enumeratePhysicalDevices(VkInstance instance, std::vector<VkPhysicalDevice>& devices);
	// enumerate extensions
	static void enumerateAvailableExtensions(VkInstance instance, std::vector<VkExtensionProperties>& props);
	// enumerate layers
	static void enumerateAvailableLayers(VkInstance instance, std::vector<VkLayerProperties>& props);
	// Query for Support
	static bool extensionSupported(VkInstance instance, std::string name);
	static void extensionsSupported(VkInstance instance, std::vector<const char*>& names, std::vector<bool>& resVec);
	static bool layerSupported(VkInstance instance, std::string name);
	static void layersSupported(VkInstance instance, std::vector<const char*>& names, std::vector<bool>& resVec);
};

/* ------- Core Class ------- */
class ge::Vk::Instance : public ge::Vk::Auxiliary::Instance {
// attributes
private:
	VkInstance _instance = VK_NULL_HANDLE;
	ge::Vk::Info::Instance* _description = nullptr;
	VkDebugReportCallbackEXT _debugCb;

// methods
private:
	void init(CreateInfo::Instance& createInfo);
	void deinit();
	//bool initCompleted() { return _instance != VK_NULL_HANDLE && _description != nullptr; } // Checks if Object was properly created
	void move(Instance& withdraw);

	/* Debug Callback Handling */
	static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
		VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType,
		uint64_t obj, size_t location, int32_t code,
		const char* layerPrefix, const char* msg, void* userData);
	VkResult createDebugReportCallbackEXT(VkInstance instance, const VkDebugReportCallbackCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugReportCallbackEXT* pCallback);
	void destroyDebugReportCallbackEXT(VkInstance instance, VkDebugReportCallbackEXT callback, const VkAllocationCallbacks* pAllocator);
	
public:
	/* Ctors 'n Dtor */
	Instance() { ; }
	Instance(CreateInfo::Instance& createInfo) { init(createInfo); }
	Instance(Instance& copy) { move(copy); }
	Instance(VkInstance instance); // kvuli $**!#j Qt rakovln... 
	~Instance() { deinit(); }

	/* Getters */
	VkInstance getVkInstance() { return _instance; }
	Info::Instance& getDescription() { return *_description; }
	bool useValidationLayers() { return _description->useValidationLayers; }
	bool abundantInfo() { return _description->abundantInfo; }
	bool verboseActivated() { return _description->verboseMode; }

	/* Operators */
	Instance& operator=(Instance& assign) { move(assign); return *this; }
	operator VkInstance() { return _instance; }

	/* Core Methods */
	// Initialization on demand
	void create(CreateInfo::Instance& createInfo) { init(createInfo); }
	// Objects dtor on demand
	void destroy() { deinit(); }

	/* Capatibility checkers */
	// enumerate Psdevices
	void enumeratePhysicalDevices(std::vector<VkPhysicalDevice>& devices) { ge::Vk::Auxiliary::Instance::enumeratePhysicalDevices(_instance, devices); }
	// enumerate extensions
	void enumerateAvailableExtensions(std::vector<VkExtensionProperties>& props) { ge::Vk::Auxiliary::Instance::enumerateAvailableExtensions(_instance, props); }
	// enumerate layers
	void enumerateAvailableLayers(std::vector<VkLayerProperties>& props) { ge::Vk::Auxiliary::Instance::enumerateAvailableLayers(_instance, props); }
	// Query for Support
	bool extensionSupported(std::string name) { return ge::Vk::Auxiliary::Instance::extensionSupported(_instance, name); }
	// v resVec je ulozena bool hodnota jestli je extension podporovano nebo ne
	void extensionsSupported(std::vector<const char*>& names, std::vector<bool>& resVec) { ge::Vk::Auxiliary::Instance::extensionsSupported(_instance, names, resVec); }
	bool layerSupported(std::string name) { return ge::Vk::Auxiliary::Instance::layerSupported(_instance, name); }
	void layersSupported(std::vector<const char*>& names, std::vector<bool>& resVec) { ge::Vk::Auxiliary::Instance::layersSupported(_instance, names, resVec); }

	/* Status Checkers */

};

#endif
