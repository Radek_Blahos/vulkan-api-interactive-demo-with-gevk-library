/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Info structures for Descriptor Set methods
*/

#include <geVk\geVk.h>
#include <vector>

/* Auxiliary Class */
class ge::Vk::Info::WriteDescriptorSet {
// attributes
public:
	VkDescriptorSet dstSet;
	uint32_t dstBinding;
	uint32_t dstArrayElement;
	uint32_t descriptorCount;
	VkDescriptorType descriptorType;
	const VkDescriptorImageInfo* pImageInfo = nullptr;
	const VkDescriptorBufferInfo* pBufferInfo = nullptr;
	const VkBufferView* pTexelBufferView = nullptr;

// methods
public:
	operator VkWriteDescriptorSet();
	
};

class ge::Vk::Info::Methods::UpdateDescriptorSets {
// attributes
public:
	std::vector<VkDescriptorBufferInfo> buffersInfo;
	std::vector<VkDescriptorImageInfo> imagesInfo;
	std::vector<ge::Vk::Info::WriteDescriptorSet> writeInfo; // write info references items from buffer or images Infos.

};

