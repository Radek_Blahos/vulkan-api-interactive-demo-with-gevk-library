/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Info structure used with Memory manager methods.
*/

#ifndef GEVKMEMORYBLOCK_H
#define GEVKMEMORYBLOCK_H

#include <geVk\geVk.h>
#include <list>

/* ---- Aux Memory Alloc Map Struct ---- */
struct AllocationInfo {
	VkDeviceSize start;
	VkDeviceSize offset;

	VkDeviceSize size() { return offset - start; }
};

/* ------- Info Classes ------- */
class ge::Vk::Info::MemoryBlock {
//attributes
public:
	AllocationInfo allocInfo; 
	ge::Vk::MemoryPool* allocatedFrom = nullptr;

// methods
public:

};

class ge::Vk::Info::MemoryRequierements {
//attributes
public:
	VkMemoryRequirements memReqs;
	VkMemoryPropertyFlags memFlags;
	VkDeviceSize allocationSize;

};

class ge::Vk::Info::MemoryProperties {
//attributes
public:
	VkDeviceSize size;
	VkDeviceSize alignment;
	uint32_t memoryTypeIndex;
	VkMemoryPropertyFlags propertyFlags;

};

#endif
