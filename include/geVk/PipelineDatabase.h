/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Pipeline Database object
*/

#ifndef GEVKPIPEDB_H
#define GEVKPIPEDB_H

#include <geVk/geVk.h>
#include <geVk\Device.h>
#include <geVk\PipelineCache.h>
#include <geVk\GraphicPipeline.h>
#include <geVk\ComputePipeline.h>
#include <string>
#include <unordered_map>
#include <vector>

/* Aux structs */
struct ge::Vk::CreateInfo::GraphicPipeDBEntry {
	ge::Vk::CreateInfo::GraphicPipeline graphicPipeCreateInfo;
	std::string name;
};

struct ge::Vk::CreateInfo::ComputePipeDBEntry {
	ge::Vk::CreateInfo::ComputePipeline computePipeCreateInfo;
	std::string name;
};

/* ------- Info Class ------- */
class ge::Vk::Info::PipelineDatabase {
// attributes
public:
	ge::Vk::Device* pDevice;
	std::string cacheStoragePath;

};


/* ------- CreateInfoClass ------- */
class ge::Vk::CreateInfo::PipelineDatabase : public ge::Vk::Info::PipelineDatabase {
// attributes
public:
	// potreba pred pouzitim resiznout...
	std::vector<GraphicPipeDBEntry> graphicPipesCreateInfos; // string will be name used inside DB
	std::vector<ComputePipeDBEntry> computePipesCreateInfos;

// methods
public:

};

/* ------- Core Class ------- */
class ge::Vk::PipelineDatabase {
// attributes
private:
	std::unordered_map<std::string, GraphicPipeline> _graphicPipesDB;
	std::unordered_map<std::string, ComputePipeline> _computePipesDB;
	Info::PipelineDatabase* _description = nullptr;


// methods
private:
	void init(CreateInfo::PipelineDatabase& createInfo);
	void deinit();
	//bool initCompleted();

	// pipelines ctor
	static void assembleGraphicPipelinesWorker(std::vector<CreateInfo::GraphicPipeDBEntry>* graphicPipesCreateInfos, Device* pDevice, uint32_t from, uint32_t to, VkPipelineCache cache = VK_NULL_HANDLE, std::vector<VkPipeline>* pipesPtr = nullptr);
	static void assembleComputePipelinesWorker(std::vector<CreateInfo::ComputePipeDBEntry>* computePipesCreateInfos, Device* pDevice, uint32_t from, uint32_t to, VkPipelineCache cache = VK_NULL_HANDLE, std::vector<VkPipeline>* pipesPtr = nullptr);

public:
	/* Ctors 'n Dtor */
	PipelineDatabase() {}
	PipelineDatabase(CreateInfo::PipelineDatabase& createInfo) { init(createInfo); }
	~PipelineDatabase() { deinit(); }

	/* Getters */

	/* Core Methods */
	void build(CreateInfo::PipelineDatabase& createInfo) { init(createInfo); }
	void destroy() { deinit(); }
	// in order to be able to properly use these methods DB object must be already initialized! 
	void buildGraphicPipelines(std::vector<CreateInfo::GraphicPipeDBEntry>& graphicPipesCreateInfos);
	void buildComputePipelines(std::vector<CreateInfo::ComputePipeDBEntry>& computePipesCreateInfos);
	void destroyGraphicPipelines() { _graphicPipesDB.clear(); }
	void destroyComputePipelines() { _computePipesDB.clear(); }

	// adds object to pipeline, referenced object will be moved into DB -> DB responds for objects deallocation
	bool addComputePipeline(std::string name, ComputePipeline& pipeline) { _computePipesDB[name] = pipeline; } // ano vidim ze tady se musi objekt vytvorit a jeste se pak do nej ta pipeline movne ale v insertu se taky stejne vola pipeline.move() a na emplace nemam CreateInfo te pipeline ... takze tohle je jedine reseni 
	bool addGraphicPipeline(std::string name, GraphicPipeline& pipeline) { _graphicPipesDB[name] = pipeline; }
	// returns and pop pipeline object from DB, referenced geVk must be uninitialized
	ComputePipeline& obtainComputePipeline(std::string name) { return _computePipesDB[name]; }
	GraphicPipeline& obtainGraphicPipeline(std::string name) { return _graphicPipesDB[name]; }
	// just destroy pipeline with provided key if existed
	void popComputePipeline(std::string name) { _computePipesDB.erase(name); }
	void popGraphicPipeline(std::string name) { _graphicPipesDB.erase(name); }

	/* Operators */

	/* Status Checkers */
	

};

#endif