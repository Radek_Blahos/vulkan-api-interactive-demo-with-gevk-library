/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Inlines for memory manager's methods
*/

#ifndef GEVKMEMANAGER_INL
#define GEVKMEMANAGER_INL

#include <geVk\MemoryManager.h>
#include <geVk\Buffer.h>
#include <geVk\Image.h>

using namespace ge::Vk;

inline void MemoryManager::releaseImageMemory(Image& image) {
	restoreMemoryBlock(image.getMemory()); 
}

inline void MemoryManager::releaseBufferMemory(Buffer& buffer) {
	restoreMemoryBlock(buffer.getMemory()); 
}

#endif