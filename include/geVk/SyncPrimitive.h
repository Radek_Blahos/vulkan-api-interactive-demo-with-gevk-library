/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Abstract factory for Vk Sync primitives
*/

#ifndef GEVKSYNCPRIMITIVE_H
#define GEVKSYNCPRIMITIVE_H

#include <geVk\Event.h>
#include <geVk\Fence.h>

/* ------ Sync Primitive ------ */
class ge::Vk::Utils::SyncPrimitive {
// attributes
public:
	void* _lockPtr = nullptr; // stores ptr to Fence or Event

// methods
public:
	SyncPrimitive() {};
	SyncPrimitive(ge::Vk::Device& device) {} // if createFence false -> event is used
	virtual ~SyncPrimitive() {}

	virtual void create(ge::Vk::Device& device) = 0;
	virtual void destroy() = 0;

	template<typename T>
	T& getPrimitive() { return *(T*)_lockPtr; }
	void*& getPrimitivePtr() { return _lockPtr; }

	virtual void waitUntilSignaled() = 0;
	virtual void reset() = 0;
	virtual void signal() = 0;
	virtual void markAsUsed() = 0;
	virtual bool isUsed() = 0;
	virtual bool isSignaled() = 0;

};

class FencePrimitive : public ge::Vk::Utils::SyncPrimitive {
public:
	FencePrimitive() {};
	FencePrimitive(ge::Vk::Device& device) { create(device); }
	virtual ~FencePrimitive() { destroy(); }

	virtual void create(ge::Vk::Device& device);
	virtual void destroy();

	virtual void waitUntilSignaled() { ge::Vk::Fence* ptr = (ge::Vk::Fence*)_lockPtr; ptr->waitUntilSignaled(); }
	virtual void reset() { ge::Vk::Fence* ptr = (ge::Vk::Fence*)_lockPtr; ptr->reset(); }
	virtual void signal() { return; }
	virtual void markAsUsed() { ge::Vk::Fence* ptr = (ge::Vk::Fence*)_lockPtr; ptr->markAsUsed(); }
	virtual bool isUsed() { ge::Vk::Fence* ptr = (ge::Vk::Fence*)_lockPtr; return ptr->isUsed(); }
	virtual bool isSignaled() { ge::Vk::Fence* ptr = (ge::Vk::Fence*)_lockPtr; return ptr->isSignaled(); }

};

class EventPrimitive : public ge::Vk::Utils::SyncPrimitive {
public:
	EventPrimitive() {};
	EventPrimitive(ge::Vk::Device& device) { create(device); }
	virtual ~EventPrimitive() { destroy(); }

	virtual void create(ge::Vk::Device& device);
	virtual void destroy();

	virtual void waitUntilSignaled() { ge::Vk::Event* ptr = (ge::Vk::Event*)_lockPtr; ptr->waitUntilSignaled(); }
	virtual void reset() { ge::Vk::Event* ptr = (ge::Vk::Event*)_lockPtr; ptr->reset(); }
	virtual void signal() { ge::Vk::Event* ptr = (ge::Vk::Event*)_lockPtr; ptr->set(); }
	virtual void markAsUsed() { return; }
	virtual bool isUsed() { ge::Vk::Event* ptr = (ge::Vk::Event*)_lockPtr; return ptr->isSet(); }
	virtual bool isSignaled() { ge::Vk::Event* ptr = (ge::Vk::Event*)_lockPtr; return ptr->isSet(); }

};

#endif