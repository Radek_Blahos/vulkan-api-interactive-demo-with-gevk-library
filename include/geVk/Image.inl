/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Inlines for Image's methods
*/

#ifndef GEVKIMAGEVIEW_INL
#define GEVKIMAGEVIEW_INL

#include <geVk\Image.h>
#include <geVk\ImageInfo.h>
#include <geVk\ImageView.h>
#include <geAx\Exception.h>

using namespace ge::Vk;

inline void Image::getMemoryProperties(Info::MemoryProperties& result) {
	VkMemoryRequirements reqs;
	vkGetImageMemoryRequirements(getVkDevice(), _image, &reqs);

	result.size = reqs.size;
	result.memoryTypeIndex = _description->pManager->typeBitsToIndex(reqs.memoryTypeBits, _description->memFlags);
	result.alignment = reqs.alignment;
	result.propertyFlags = _description->memFlags;
}

inline void ge::Vk::Image::assignMemory() {
	if (!_description->pManager->assignMemoryToImage(*this))
		throw ge::Ax::Exception::Bad::Allocation("geVk Image's memory");
}

#endif