/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Render Context -> This class is something like template to geVk users. Encapsulates all geVk objets 
	required to Start rendering. And defines abstract methods which user have to implement in
	order to geVk Start doing something(Render to a screen).
*/

#ifndef GEVK_RENDERCONTEXT_H
#define GEVK_RENDERCONTEXT_H

#include <geVk\Instance.inl>
#include <geVk\Device.h>
#include <geVk\MemoryManager.inl>
#include <geVk\RenderComponentsDB.h>
#include <geVk\PipelineDatabase.h>
#include <geVk\Swapchain.h>
#include <geVk\CommandProcessor.h>
#include <geVk\CommandBufferSet.h>
#include <geVk\DescriptorManager.h>

/* 
	
*/

/* ------- Abstract Class ------- */
class ge::Vk::Utils::RenderContext {
// attributes
public:
	//ge::Ax::Window window; // OS window include/inherit in external class together with render context 
	
	// Vulkan Objects
	VkSurfaceKHR presentSurface; // save here created surface obtained from 3rd party lib or when its done from geAx Window
	ge::Vk::Instance instance;
	ge::Vk::Device device;
	ge::Vk::MemoryManager memManager;
	ge::Vk::DescriptorManager descManager;
	ge::Vk::PipelineDatabase pipelineDB;
	ge::Vk::Utils::RenderComponentsDB componentsDB;
	ge::Vk::Swapchain swapchain;
	ge::Vk::CommandProcessor commandProcessor;

// methods
private:
public:
	/* Ctors 'n Dtor */
	
	/* Getters */

	/* Core Methods -> which user should implement inside his renderer */
	//virtual void initialize(uint32_t width, uint32_t height, uint32_t drawThreadsCount) = 0;
	//virtual void initInstance() = 0;
	//virtual void initDevice() = 0;
	//virtual void initMemoryManager() = 0;
	//virtual void initSwapchain(uint32_t width, uint32_t height) = 0;
	//virtual void buildPipelineLayouts() = 0;
	//virtual void buildShaders() = 0;
	//virtual void buildDescriptorSets() = 0;
	//virtual void buildRenderPasses() = 0;
	//virtual void buildCommandBufferSets() = 0;
	//virtual void buildSamplers() = 0;
	//virtual void initRenderComponents() = 0;
	//virtual void fillPipelineDBCreateInfo(ge::Vk::CreateInfo::PipelineDatabase& createInfo) = 0;
	//virtual void buildPipelineDB() = 0;

	/* Ideas */

	/* Operators */

	/* Status Checkers */

};

#endif