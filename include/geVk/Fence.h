/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Fence wrapper object
*/

#ifndef GEVKFENCE_H
#define GEVKFENCE_H

#include <geVk/geVk.h>
//#include <mutex>

/* ------ Info Classes ------ */
class ge::Vk::Info::Fence {
// attributes
public:
	ge::Vk::Device* pDevice;

};

class ge::Vk::CreateInfo::Fence : public ge::Vk::Info::Fence {
// attributes
public:
	bool createSignaled = false;

// methods
public:
	Fence() {}
	Fence(const Info::Fence& info, bool signaled = false) : Info::Fence(info), createSignaled(signaled) {}
	operator VkFenceCreateInfo();

};

/* ------ Fence Core Class ------ */
class ge::Vk::Fence {
// attributes
private:
	VkFence _fence = VK_NULL_HANDLE;
	Info::Fence* _description = nullptr;
	bool _used = false; // fence usage flag
	//std::mutex statusMutex; // used when fence is retrieving info about signal status from Vk driver

// methods
private:
	void init(CreateInfo::Fence& createInfo);
	void deinit();
	//bool initCompleted() { return _fence != VK_NULL_HANDLE; }
	void move(Fence& withdraw);

public:
	/* Ctors 'n Dtor */
	Fence() {}
	Fence(CreateInfo::Fence& createInfo) { init(createInfo); }
	Fence(Fence& copy) { move(copy); }
	~Fence() { deinit(); }

	/* Getters */
	VkFence getVkFence() { return _fence; }
	VkFence* getVkFencePtr() { return &_fence; }
	Info::Fence& getDescription() { return *_description; }

	/* Core Methods */
	void create(CreateInfo::Fence& createInfo) { init(createInfo); }
	void destroy() { deinit(); }

	VkFence passFenceToSignal();
	void waitUntilSignaled();
	void reset();
	void markAsUsed() { _used = true; }

	/* Operators */
	Fence& operator=(Fence& assign) { move(assign); return *this; }
	operator VkFence() { return _fence; }

	/* Status Checkers */
	bool isUsed();
	bool isSignaled();

};

#endif