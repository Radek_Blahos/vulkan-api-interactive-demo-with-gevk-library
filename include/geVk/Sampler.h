/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Sampler wrapper
*/

#ifndef GEVKSAMPLER_H
#define GEVKSAMPLER_H

#include <geVk/geVk.h>

/* ------- Info Class ------- */
class ge::Vk::Info::Sampler {
// attributes
public:
	ge::Vk::Device* pDevice;

};

class ge::Vk::FurtherInfo::Sampler : public ge::Vk::Info::Sampler {
// attributes
public:
	VkFilter magFilter = VK_FILTER_LINEAR;
	VkFilter minFilter = VK_FILTER_LINEAR;
	VkSamplerMipmapMode mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	VkSamplerAddressMode addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	VkSamplerAddressMode addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	VkSamplerAddressMode addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	float mipLodBias = 0;
	VkBool32 anisotropyEnable = VK_FALSE;
	float maxAnisotropy = 1;
	VkBool32 compareEnable = VK_FALSE; 
	VkCompareOp compareOp = VK_COMPARE_OP_ALWAYS;
	float minLod = 0;
	float maxLod = 0;
	VkBorderColor borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
	VkBool32 unnormalizedCoordinates = VK_FALSE;

};

/* ------- CreateInfoClass ------- */
class ge::Vk::CreateInfo::Sampler : public ge::Vk::FurtherInfo::Sampler {
// attributes
public:
// methods
public:
	operator VkSamplerCreateInfo();

};

/* ------- Core Class ------- */
class ge::Vk::Sampler {
// attributes
private:
	Info::Sampler* _description = nullptr;
	VkSampler _sampler = VK_NULL_HANDLE;

// methods
private:
	void init(CreateInfo::Sampler& createInfo);
	void deinit();
	void move(Sampler& withdraw);

public:
	/* Ctors 'n Dtor */
	Sampler() {}
	Sampler(CreateInfo::Sampler& createInfo) { init(createInfo); }
	Sampler(Sampler& copy) { move(copy); }
	~Sampler() { deinit(); }

	/* Getters */

	/* Core Methods */
	void create(CreateInfo::Sampler& createInfo) { init(createInfo); }
	void destroy() { deinit(); }

	/* Operators */
	Sampler& operator=(Sampler& assign) { move(assign); return *this; }
	operator VkSampler() { return _sampler; }
	VkSampler getVkSampler() { return _sampler; }
	Info::Sampler& getDescription() { return *_description; }

};

#endif
