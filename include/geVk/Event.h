/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Event object
*/

#ifndef GEVKEVENT_H
#define GEVKEVENT_H

#include <geVk/geVk.h>

/* ------ Event Info Classes ------ */
class ge::Vk::Info::Event {
// attributes
public:
	ge::Vk::Device* pDevice;

};

class ge::Vk::CreateInfo::Event : public ge::Vk::Info::Event {
// methods
public:
	Event() {}
	Event(const Info::Event& info) : Info::Event(info) {}
	operator VkEventCreateInfo();

};

/* ------ Event Core Class ------ */
class ge::Vk::Event {
// attributes
private:
	VkEvent _event = VK_NULL_HANDLE;
	Info::Event* _description = nullptr;

// methods
private:
	void init(CreateInfo::Event& createInfo);
	void deinit();
	//bool initCompleted() { return _event != VK_NULL_HANDLE; }
	void move(Event& withdraw);

public:
	/* Ctors 'n Dtor */
	Event() {}
	Event(CreateInfo::Event& createInfo) { init(createInfo); }
	Event(Event& copy) { move(copy); }
	~Event() { deinit(); }

	/* Getters */
	VkEvent getVkEvent() { return _event; } // _used = true; 
	VkEvent* getVkEventPtr() { return &_event; }
	Info::Event& getDescription() { return *_description; }

	/* Core Methods */
	void create(CreateInfo::Event& createInfo) { init(createInfo); }
	void destroy() { deinit(); }
	void waitUntilSignaled();
	void set();
	void reset();

	/* Operators */
	operator VkEvent() { return getVkEvent(); }
	Event& operator=(Event& assign) { move(assign); return *this; }

	/* Status Checkers */
	bool isSet();
	bool isReset();

};

#endif
