/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Semaphore object
*/

#ifndef GEVKSEMAPHORE_H
#define GEVKSEMAPHORE_H

#include <geVk/geVk.h>

/* ------ Semaphore Info Classes ------ */
class ge::Vk::Info::Semaphore {
// attributes
public:
	ge::Vk::Device* pDevice;

};

class ge::Vk::CreateInfo::Semaphore : public ge::Vk::Info::Semaphore {
// methods
public:
	Semaphore() {}
	Semaphore(const Info::Semaphore& info) : Info::Semaphore(info) {}
	operator VkSemaphoreCreateInfo();

};

/* ------ Semaphore Core Class ------ */
class ge::Vk::Semaphore {
// attributes
private:
	VkSemaphore _semaphore = VK_NULL_HANDLE;
	Info::Semaphore* _description = nullptr;

// methods
private:
	void init(CreateInfo::Semaphore& createInfo);
	void deinit();
	//bool initCompleted() { return _semaphore != VK_NULL_HANDLE; }
	void move(Semaphore& withdraw);

public:
	/* Ctors 'n Dtor */
	Semaphore() {}
	Semaphore(CreateInfo::Semaphore& createInfo) { init(createInfo); }
	Semaphore(Semaphore& copy) { move(copy); }
	~Semaphore() { deinit(); }

	/* Getters */
	VkSemaphore getVkSemaphore() { return _semaphore; } 
	VkSemaphore* getVkSemaphorePtr() { return &_semaphore; }

	/* Core Methods */
	void create(CreateInfo::Semaphore& createInfo) { init(createInfo); }
	void destroy() { deinit(); }

	/* Operators */
	operator VkSemaphore() { return getVkSemaphore(); }
	Semaphore& operator=(Semaphore& assign) { move(assign); return *this; }

};

#endif
