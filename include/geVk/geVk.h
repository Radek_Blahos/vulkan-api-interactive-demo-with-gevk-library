﻿/**
* @author Radek Blahos
* @project geVk Library
*
* @brief geVk Forward declarations
*/

#ifndef GEVK_H
#define GEVK_H

/*
*
*
# geVk's Basic Object structure
	If you know or will read something about Vulkan you'll find out that vulkan has several
	API defined object which most of them have their counterpart inside geVk implementation.
	Some object, these which requires allocating resources from pools, are merged together with
	them into single geVk object which handles their memory management.
	All class list you can read below with brief description in form of Doxygen commentary.
	
	Every geVk object has 3 types of classes defining objects.
	Core, CreateInfo, Info. Info class is inherited by CreateInfo and is also stored 
	as private _description attribute inside Core class. Why so? because some attributes needs to be
	in izomorf relationship between CreateInfo and Core class. In Core class their mostly representing 
	static read-only status where user can obtain runtime info about object depending createInfo input.
	Its more programming friendly for me as library writer. And object conversions are making my life easier here to.
*/

#include <vulkan\vulkan.h> // vulkan API
#include <cstdint> // base types (uint32_t eg.)
#include <geVk\UsefulMacros.h>

namespace ge {
	namespace Vk {
		// wrapper classes
		class Instance;
		class Device;
		class Queue;
		class CommandProcessor;
		class CommandBuffer;
		class CommandPool;
		class CommandBufferSet;
		class Pipeline;
		class GraphicPipeline;
		class ComputePipeline;
		class PipelineLayout;
		class PipelineDatabase;
		class PipelineCache;
		class DescriptorManager;
		class DescriptorSet;
		class DescriptorSetLayout;
		class DescriptorPool;
		class RenderPass;
		class MemoryManager;
		class MemoryPool;
		class Buffer;
		class BufferView;
		class Image;
		class ImageView;
		class Swapchain;
		class Framebuffer;
		class Semaphore;
		class Fence;
		class Event;
		class ShaderModule;
		class Sampler;
		
		namespace CreateInfo {
			class Core; // abstract class
			class Instance;
			class Device;
			class Queue;
			class CommandProcessor;
			class CommandBuffer;
			class CommandPool;
			class CommandBufferSet;
			class Pipeline;
			class GraphicPipeline;
			class ComputePipeline;
			class PipelineLayout;
			class PipelineCache;
			class PipelineDatabase;
			struct GraphicPipeDBEntry;
			struct ComputePipeDBEntry;
			class DescriptorSet;
			class DescriptorSetLayout;
			class DescriptorPool;
			class DescriptorManager;
			class RenderPass;
			class MemoryManager;
			class MemoryPool;
			class MemoryPoolStorage;
			class Buffer;
			class Image;
			class ImageView;
			class Framebuffer;
			class Swapchain;
			class Event;
			class Semaphore;
			class Fence;
			class ShaderModule;
			class Sampler;
			//class RenderContext;

		}

		namespace FurtherInfo {
			class Instance;
			class Device;
			class ComputePipeline;
			class GraphicPipeline;
			class DescriptorSetLayout;
			class RenderPass;
			class Framebuffer;
			class PipelineLayout;
			class Sampler;

		}

		namespace Info {
			class Instance;
			class Device;
			class Queue;
			class QueueFamily;
			class MemoryBlock;
			class DescriptorManager;
			class DescriptorPool;
			class DescriptorSet;
			class DescriptorSetLayout;
			class CommandBuffer;
			class CommandPool;
			class CommandBufferSet;
			class CommandProcessor;
			class Pipeline;
			class ComputePipeline;
			class GraphicPipeline;
			class PipelineCache;
			class PipelineDatabase;
			class PipelineLayout;
			class Buffer;
			class Image;
			class ImageView;
			class RenderPass;
			class Framebuffer;
			class Swapchain;
			class RequestQueues; // used as Info for geVkQueue
			class MemoryPool;
			class MemoryManager;
			class MemoryRequierements;
			class MemoryProperties;
			class Semaphore;
			class Event;
			class Fence;
			class ShaderModule;
			class Sampler;

			// class inhering and modifiing original vk structures
			class WriteDescriptorSet;

			namespace Commands {
				class BeginRenderPass;
				class PushConstants;
				class BindDescriptorSets;
				class BindVertexBuffers;
				class BindIndexBuffer;
				class Draw;
				class DrawIndexed;
				class DrawIndexedIndirect;
				class SetViewport;
				class SetScissor;
			}
			
			namespace Methods {
				class PostCommandBuffers;
				class PostSingleBuffer;
				class UpdateDescriptorSets;
				class PresentImage;
			}

			// vk flags shortcuts
			namespace Memory {
				namespace location {
					const uint32_t GPU = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
					const uint32_t HOST_RAM = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
				}
			}

			namespace Pipe {
				class ShaderStage;
				class VertexInputDescription;
				class InputAssemblyState;
				class TessellationState;
				class ViewportState;
				class RasterizationState;
				class MultisampleState;
				class DepthStencilState;
				class StencilOpState;
				class ColorBlendAttachmentState;
				class ColorBlendState;
				using DynamicState = VkDynamicState; 
			}
		}
		
		// Utility 
		namespace Utils {
			//class FlexiblePipe;
			//class FlexiblePipeHolder;
			class SyncPrimitive;
			class GPUPropertiesViewer;
			class RenderContext;
			class RenderComponentsDB;
			class VertexBuffer;
			template <typename T> class UniformBuffer;
		}

		// auxilliary classes
		namespace Auxiliary {
			class Instance;
			class Device;
			class Swapchain;
		}
	}
}

#endif