/**
* @author Radek Blahos
* @project geVk Library
*
* @brief GPU properties viewer
*/

#ifndef GEVKPROPERTIESVIEW_H
#define GEVKPROPERTIESVIEW_H

#include <geVk\Instance.h>
#include <geVk\Device.h>

class ge::Vk::Utils::GPUPropertiesViewer : 
	public ge::Vk::Auxiliary::Instance, 
	public ge::Vk::Auxiliary::Device
{

};

#endif
