/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Pipeline Layout object
*/

#ifndef GEVKPIPELAYOUT_H
#define GEVKPIPELAYOUT_H

#include <geVk\DescriptorSet.inl>
#include <geVk/geVk.h>
#include <vector>

/* ------- Info Class ------- */
class ge::Vk::Info::PipelineLayout {
// attributes
public:
	ge::Vk::Device* pDevice;

// virtual dtor
public:
	virtual ~PipelineLayout() = default;

};

class ge::Vk::FurtherInfo::PipelineLayout : public ge::Vk::Info::PipelineLayout {
// attributes
public:
	std::vector<ge::Vk::DescriptorSetLayout*> pDescriptorSetLayouts; // extract layouts and other info about descriptor
	std::vector<VkPushConstantRange> pushConstants;

};


/* ------- CreateInfoClass ------- */
class ge::Vk::CreateInfo::PipelineLayout : public ge::Vk::FurtherInfo::PipelineLayout {
// attributes
private:
	std::vector<VkDescriptorSetLayout> descriptorLayouts;

// methods
public:
	operator VkPipelineLayoutCreateInfo();

};

/* ------- Core Class ------- */
class ge::Vk::PipelineLayout {
// attributes
private:
	ge::Vk::Info::PipelineLayout* _description = nullptr;
	VkPipelineLayout _layout = VK_NULL_HANDLE;


// methods
private:	
	void init(CreateInfo::PipelineLayout& createInfo);
	void deinit();
	//bool initCompleted() { return _layout != VK_NULL_HANDLE && _description != nullptr; }
	void move(PipelineLayout& withdraw);

public:
	/* Ctors 'n Dtor */
	PipelineLayout() {}
	PipelineLayout(CreateInfo::PipelineLayout& createInfo) { init(createInfo); }
	PipelineLayout(PipelineLayout& copy) { move(copy); }
	~PipelineLayout() { deinit(); }

	/* Getters */
	VkPipelineLayout getVkLayout() { return _layout; }

	/* Operators */
	PipelineLayout& operator=(PipelineLayout& assign) { move(assign); return *this; }
	operator VkPipelineLayout() { return _layout; }

	/* Core */
	void describe(CreateInfo::PipelineLayout& createInfo) { init(createInfo); }
	void destroy() { deinit(); }

};

#endif

