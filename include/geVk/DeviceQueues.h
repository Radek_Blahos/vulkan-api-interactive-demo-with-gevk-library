/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Wrapper over Device's Queues
*/

#ifndef DQUEUE_H
#define DQUEUE_H

#include <geVk/geVk.h>
#include <geAx\UsageRegister.h>
#include <geVk\Synchronization.h>
#include <vector>

/* ------- Info Class ------- */
class ge::Vk::Info::RequestQueues {
// attributes
public:
	uint32_t qFamilyIdx;
	uint32_t count;
	VkQueueFlagBits flags;
	std::vector<float> queuePriorities;

// methods
public:
	operator VkDeviceQueueCreateInfo();

};

class ge::Vk::Info::Queue {
// attributes
public:
	ge::Vk::Device* pDevice;
	uint32_t queueFamilyIndex;
	uint32_t allocationIndex;

};

class ge::Vk::CreateInfo::Queue : public ge::Vk::Info::Queue {
// attributes
public:

};

class ge::Vk::Queue {
// attributes
private:
	VkQueue _queue = VK_NULL_HANDLE;
	Info::Queue* _description = nullptr;

// methods
private:
	void init(CreateInfo::Queue& createInfo);
	void deinit();
	//bool initCompleted() { return _queue != VK_NULL_HANDLE && _description != nullptr; }

public:
	/* Ctors 'n Dtor */
	Queue() {}
	Queue(CreateInfo::Queue& createInfo) { init(createInfo); };
	~Queue() { deinit(); }

	/* Getters */
	VkQueue getVkQueue() { return _queue; }
	const Info::Queue& getDescription() { return *_description; }

	/* Core Methods */
	void obtain(CreateInfo::Queue& createInfo) { init(createInfo); }
	void release() { deinit(); }

	/* Operators */
	operator VkQueue() { return _queue; }

	/* Status Checkers */
	

};

class ge::Vk::Info::QueueFamily {
// attributes
public:
	std::vector<ge::Vk::Queue*> availableQueues;
	int lastUsedQueue = -1;

// methods
public:

};

#endif