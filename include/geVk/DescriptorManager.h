/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Descriptor Manager, stores descriptor pool and redistributes their memory.
*/

#ifndef GEVKDESCRIPTORSMANAGER_H
#define GEVKDESCRIPTORSMANAGER_H

#include <geVk\DesciptorsPool.h>
#include <geVk\Device.h>
#include <vector>

/* ----------- Info Class ----------- */
class ge::Vk::Info::DescriptorManager {
// attributes
public:
	ge::Vk::Device* pDevice;

};

/* ----------- Create Info Class ----------- */
class ge::Vk::CreateInfo::DescriptorManager : public ge::Vk::Info::DescriptorManager {
// attributes
public:

// methods
public:

};

/* ------- Core Class ------- */
class ge::Vk::DescriptorManager {
// attributes
private:
	ge::Vk::Info::DescriptorManager* _description = nullptr; // create info description
	std::vector<DescriptorPool*> _pools; // allowing possibly unlimited allocation of sets
	uint32_t _maxAllocationCountperPool = 1;

// methods
private:
	void init(CreateInfo::DescriptorManager& createInfo);
	void deinit();
	//bool initCompleted() { return _description != nullptr; }

public:
	/* Ctors 'n Dtor */
	DescriptorManager() {}
	DescriptorManager(CreateInfo::DescriptorManager& createInfo) { init(createInfo); }
	~DescriptorManager() { deinit(); }

	/* Core Methods */
	void setUp(CreateInfo::DescriptorManager& createInfo) { init(createInfo); }
	void shutdown() { deinit(); }

	// geVkDesciptors allocation
	bool allocateDescriptorMemory(DescriptorSet& descriptorSet);
	bool releaseDescriptorMemory(DescriptorSet& descriptorSet);

	/* Getters */
	ge::Vk::Device& getDevice() { return *_description->pDevice; }

	/* Setters */
	// will influence only descriptor pools created after this call!
	void setCurrentAllocationCountperPool(uint32_t allocationCount) { _maxAllocationCountperPool = allocationCount; }

	/* Status Checkers */

};

#endif
