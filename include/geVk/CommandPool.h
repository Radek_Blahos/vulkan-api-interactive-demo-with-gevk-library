/**
* @author Radek Blahos
* @project geVk Library
*
* @brief CommandPool object 
*/

#ifndef GEVKCOMMANDPOOL_H
#define GEVKCOMMANDPOOL_H

#include <geVk/geVk.h>
#include <geVk\Device.h>

/* ------- Info Class ------- */
class ge::Vk::Info::CommandPool {
// attributes
public:
	ge::Vk::Device* pDevice;
	VkCommandPoolCreateFlags flags = 0;
	uint32_t queueFamilyIndex;

};

/* ------- CreateInfoClass ------- */
class ge::Vk::CreateInfo::CommandPool : public ge::Vk::Info::CommandPool {
// attributes
public:

// methods
public:
	operator VkCommandPoolCreateInfo();

};

/* ------- Core Class ------- */
class ge::Vk::CommandPool {
//attributes
private:
	VkCommandPool _pool = VK_NULL_HANDLE;
	Info::CommandPool* _description = nullptr;

//methods
private:
	void init(CreateInfo::CommandPool& createInfo);
	void deinit();
	//bool initCompleted() { return _pool != VK_NULL_HANDLE && _description != nullptr; }
	//void move(CommandPool& withdraw);

public:
	/* Ctors 'n Dtor */
	CommandPool() {}
	CommandPool(CreateInfo::CommandPool& createInfo) { init(createInfo); }
	~CommandPool() { deinit(); }

	/* Getters */
	VkCommandPool getVkPool() { return _pool; }
	Info::CommandPool& getDescription() { return *_description; }

	/* Core Methods */
	void construct(CreateInfo::CommandPool& createInfo) { init(createInfo); }
	void wipeout() { deinit(); }

	/* Operators */
	operator VkCommandPool() { return _pool; }

	/* Status Checkers */
	
};

#endif
