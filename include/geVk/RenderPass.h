/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Render Pass wrapper
*/

#ifndef GEVKRENDERPASS_H
#define GEVKRENDERPASS_H

#include <geVk/geVk.h>
#include <vector>

/* ------- Info Class ------- */
class ge::Vk::Info::RenderPass {
// attributes
public:
	// parent Device
	ge::Vk::Device* pDevice; 

// virtual dtor
public:
	virtual ~RenderPass() = default;

};

class ge::Vk::FurtherInfo::RenderPass : public ge::Vk::Info::RenderPass {
// attributes
public:
	std::vector<VkAttachmentReference> attachmentReferences; // what attachment(idx) will have during subpass which layout
	std::vector<VkAttachmentDescription> attachments; //attachments available during this renderpass
	std::vector<VkSubpassDescription> subpasses;
	std::vector<VkSubpassDependency> subpassDependencies;

};

/* ------- CreateInfoClass ------- */
class ge::Vk::CreateInfo::RenderPass : public ge::Vk::FurtherInfo::RenderPass {
// attributes
public:

// methods
public:
	inline operator VkRenderPassCreateInfo();

};

/* ------- Core Class ------- */
class ge::Vk::RenderPass {
// attributes
private:
	ge::Vk::Info::RenderPass* _description = nullptr;
	VkRenderPass _renderPass = VK_NULL_HANDLE;

// methods
private:
	void init(CreateInfo::RenderPass& createInfo);
	void deinit();
	//bool initCompleted() { return _renderPass != VK_NULL_HANDLE && _description != nullptr; }
	void move(RenderPass& withdraw);

public:
	/* Ctors 'n Dtor */
	RenderPass() {}
	RenderPass(CreateInfo::RenderPass& createInfo) { init(createInfo); }
	RenderPass(RenderPass& copy) { move(copy); }
	~RenderPass() { deinit(); }

	/* Operators */
	operator VkRenderPass() { return _renderPass; }
	RenderPass& operator=(RenderPass& assign) { move(assign); return *this; }

	/* Getters */
	VkRenderPass getVkRenderPass() { return _renderPass; }

	/* Core Methods */
	void describe(CreateInfo::RenderPass& createInfo) { init(createInfo); }
	void dismantle() { deinit(); }

	/* Status Checkers */

};
#endif