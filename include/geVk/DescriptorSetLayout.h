/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Descriptor Set Layout wrapper
*/

#ifndef GEVKDESCRIPTORSETLAYOUT_H
#define GEVKDESCRIPTORSETLAYOUT_H

#include <geVk/geVk.h>
#include <vector>

/* ----------- Info Class ----------- */
class ge::Vk::Info::DescriptorSetLayout {
// attributes
public:
	ge::Vk::Device* pDevice;

// virtual dtor
public:
	virtual ~DescriptorSetLayout() = default;

};


class ge::Vk::FurtherInfo::DescriptorSetLayout : public ge::Vk::Info::DescriptorSetLayout {
// attributes
public:
	std::vector<VkDescriptorSetLayoutBinding> layoutBindings; // bindings

};

/* ----------- Create Info Class ----------- */
class ge::Vk::CreateInfo::DescriptorSetLayout : public ge::Vk::FurtherInfo::DescriptorSetLayout {
// attributes
public:

// methods
public:
	operator VkDescriptorSetLayoutCreateInfo();

};

/* ------- Core Class ------- */
class ge::Vk::DescriptorSetLayout {
// attributes
private:
	Info::DescriptorSetLayout* _description = nullptr;
	VkDescriptorSetLayout _layout = VK_NULL_HANDLE;

// methods
private:
	void init(CreateInfo::DescriptorSetLayout& createInfo);
	void deinit();
	//bool initCompleted();
	void move(DescriptorSetLayout& withdraw);

public:
	DescriptorSetLayout() {}
	DescriptorSetLayout(CreateInfo::DescriptorSetLayout& createInfo) { init(createInfo); }
	DescriptorSetLayout(DescriptorSetLayout& copy) { move(copy); }
	~DescriptorSetLayout() { deinit(); }

	/* Getters */
	VkDescriptorSetLayout& getVkLayout() { return _layout; }
	VkDescriptorSetLayout* getVkLayoutPtr() { return &_layout; }

	/* Core Methods */
	void describe(CreateInfo::DescriptorSetLayout& createInfo) { init(createInfo); }
	void destroy() { deinit(); }

	/* Operators */
	operator VkDescriptorSetLayout() { return _layout; }

};

#endif

