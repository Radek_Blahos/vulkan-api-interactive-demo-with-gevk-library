/**
* @author Radek Blahos
* @project geVk Library
*
* @brief usefull macros used inside this lib
*/

#ifndef GEVK_MACROS_H
#define GEVK_MACROS_H

#include <iostream>
#include <limits>

const uint32_t MAX_WAIT_TIMEOUT = std::numeric_limits<uint32_t>::max();

// uniform vk funs result check 
#define vkRC(f_res, err_msg, ret_res, verbose_mode) if (f_res != VK_SUCCESS && verbose_mode) { \
		std::cerr << err_msg << std::endl; \
		return ret_res; \
	} 

#endif#pragma once
