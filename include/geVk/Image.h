/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Image wrapper
*/

#ifndef GEVKIMAGE_H
#define GEVKIMAGE_H

#include <geVk\MemoryManager.h>
#include <geVk\MemoryBlock.h>
#include <geVk\ImageView.h>
#include <vector>

/* ------- Info Class ------- */
class ge::Vk::Info::Image {
public:
// same namespace classes
	class MemoryBarrier;
	class ChangeLayout;

// attributes
public:
	ge::Vk::MemoryManager* pManager = nullptr;
	
	VkImageType type = VK_IMAGE_TYPE_2D;
	VkFormat format;
	size_t formatSize; // user must define format size
	uint32_t width;
	uint32_t height;
	VkImageTiling tiling;
	VkImageUsageFlags usage;
	VkMemoryPropertyFlags memFlags = 0;
	VkSampleCountFlagBits samples = VK_SAMPLE_COUNT_1_BIT;
	uint32_t mipLevelCount = 1;
	uint32_t layerCount = 1;
	VkImageLayout currentLayout = VK_IMAGE_LAYOUT_UNDEFINED;

};

/* ------- CreateInfoClass ------- */
class ge::Vk::CreateInfo::Image : public ge::Vk::Info::Image {
public:
	enum Usage { GENERAL, DEPTHBUFFER, COLORBUFFER, DEPTHATTACHMENT, COLORATTACHMENT, COLORSAMPLED };

// attributes
public:
	bool assignMemory = true;
	VkDeviceSize allocationSize = 0;

// methods
public:
	// Ctor
	Image(ge::Vk::CreateInfo::Image::Usage usage = ge::Vk::CreateInfo::Image::Usage::GENERAL, uint32_t width = 0, uint32_t height = 0, bool use_mipmap = false, bool device_local = true);
	Image(Info::Image& info) : Info::Image(info) {}

	// Convertor
	operator VkImageCreateInfo();
	
};

/* ------- Core Class ------- */
class ge::Vk::Image {
//friend class
	friend class MemoryManager;
	friend class Swapchain;

// attributes
private:
	// Img Description
	ge::Vk::Info::Image* _description = nullptr;

	// vk Image object
	VkImage _image = VK_NULL_HANDLE;

	// views storage
	std::vector<ImageView*> _views; 

	// GPU Memory info
	Info::MemoryBlock _memory;

// methods
private:
	void init(CreateInfo::Image& createInfo, bool createImageView = false, CreateInfo::ImageView* pViewCreateInfo = nullptr);  // vytvori zaroven imageview
	void deinit();
	//bool initCompleted() { return _image != VK_NULL_HANDLE && _memory.allocatedFrom != nullptr; }
	void move(Image& withdraw);

	// changing layout
	VkAccessFlags layoutToFlags(VkImageLayout layout);


public:
	/* Ctors 'n Dtor */
	Image() {}
	Image(CreateInfo::Image& createInfo, bool createImageView = false, CreateInfo::ImageView* pViewCreateInfo = nullptr) { init(createInfo, createImageView, pViewCreateInfo); }
	Image(Image& copy) { move(copy); }
	~Image() { deinit(); }
	// when using this, image must have flag DeviceLocal
	void stashOnGPU(std::vector<uint8_t>& data, CommandBuffer& keepCommands, CommandProcessor& proccesCommands);

	/* Core Methods */
	// object lifetime handles
	void create(CreateInfo::Image& createInfo, bool createImageView = false, CreateInfo::ImageView* pViewCreateInfo = nullptr) { return init(createInfo, createImageView, pViewCreateInfo); }
	void discard() { deinit(); }
		
	// set data
	void setData(void* data);
	
	// changing layout
	void changeLayout(VkCommandBuffer commandBuffer, Info::Image::ChangeLayout& info);
	void changeLayout(CommandBuffer& commandBuffer, Info::Image::ChangeLayout& info, CommandProcessor& proccesCommands);

	/*
		post commands to copy memory from one geVkBuffer to this Image, source data keeps untouched,
		its user responsibility to begin command buffer before posting it to this method and after this method's call cmd buffer must be finished and posted by user...
	*/
	void copyMemory(Buffer& data, CommandBuffer& keepCommands, VkBufferImageCopy* copyInfo = nullptr);
	// Image -> Image
	void copyMemory(Image& data, CommandBuffer& keepCommands, VkImageCopy* copyInfo = nullptr);

	// clear Image's color
	// only add proper commands to keepCommands buffer
	void clearColor(VkClearColorValue clearValue, CommandBuffer& keepCommands);
	// it also begin and finish cmd buffer and execute it instantly
	void clearColor(VkClearColorValue clearValue, CommandBuffer& keepCommands, CommandProcessor& execCommands);

	// clear depth stencil image
	// only add proper commands to keepCommands buffer
	void clearDepthStencil(VkClearDepthStencilValue clearValue, CommandBuffer& keepCommands);
	// it also begin and finish cmd buffer and execute it instantly
	void clearDepthStencil(VkClearDepthStencilValue clearValue, CommandBuffer& keepCommands, CommandProcessor& execCommands);

	// getting buffer's memory requierements 
	inline void getMemoryProperties(Info::MemoryProperties& result);
	inline void assignMemory();

	/* Operators */
	operator VkImage() { return _image; }
	Image& operator=(Image& assign) { move(assign); return *this; }
	
	/* Getters */
	VkImage getVkImage() { return _image; }
	VkImageView getVkView() { return *_views.back(); } // vetsinou jediny view -> samostatny getter
	std::vector<ImageView*>& getViews() { return _views; } 
	Info::MemoryBlock& getMemory() { return _memory; }
	Device& getDevice() { return _description->pManager->getDevice(); }
	VkDevice getVkDevice() { return _description->pManager->getVkDevice(); }
	const Info::Image& getDescription() { return *_description; }
	VkDeviceMemory getDeviceMemory() { return _memory.allocatedFrom->getMemory(); }

	/* Status Checkers */

};

#endif
