/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Infos for Image's methods
*/

#ifndef GEVKIMAGEINFO_H
#define GEVKIMAGEINFO_H

#include <geVk/Image.h>

class ge::Vk::Info::Image::MemoryBarrier {
public:
	VkAccessFlags srcAccessMask;
	VkAccessFlags dstAccessMask;
	VkImageLayout oldLayout;
	VkImageLayout newLayout;
	uint32_t srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	uint32_t dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	VkImage image;
	VkImageSubresourceRange subresourceRange = { 0, 0, 1, 0, 1 };

public:
	operator VkImageMemoryBarrier();

};

class ge::Vk::Info::Image::ChangeLayout : public ge::Vk::Info::Image::MemoryBarrier {
public:
// ctor enum
enum TransferOp { IMG_INIT = 0, DEPTH_TEST, LOADING_TEX, USE_TEX, PRE_CLEAR_IMG, POST_CLEAR_IMG, PRE_CLEAR_DEPTH_A, POST_CLEAR_DEPTH_A };

public:
	VkPipelineStageFlags waitToStage;
	VkPipelineStageFlags completeBeforeStage;

public:
	ChangeLayout() {}
	ChangeLayout(TransferOp operation);

};

#endif
