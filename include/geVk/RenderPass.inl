/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Inlines for RenderPass
*/

#ifndef GEVKRENDERPASS_INL
#define GEVKRENDERPASS_INL

#include <geVk\RenderPass.h>

/* ------- CreateInfoClass ------- */
inline ge::Vk::CreateInfo::RenderPass::operator VkRenderPassCreateInfo() {
	return {
		VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
		nullptr,
		0,
		(uint32_t)attachments.size(),
		attachments.data(),
		(uint32_t)subpasses.size(),
		subpasses.data(),
		(uint32_t)subpassDependencies.size(),
		subpassDependencies.data()
	};
}

#endif
