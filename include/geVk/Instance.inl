/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Inlines for Instance methods 
*/

#ifndef GEVK_INSTANCE_INL
#define GEVK_INSTANCE_INL

#include <geVk\Instance.h>

inline uint32_t ge::Vk::Auxiliary::Instance::makeVersion(uint16_t major, uint16_t minor, uint16_t patch) {
	return (((major) << 22) | ((minor) << 12) | (patch));
}

inline std::string ge::Vk::Auxiliary::Instance::getVersion(uint32_t encodedIn) {
	return std::string(std::to_string(encodedIn >> 22) + "." + std::to_string((encodedIn << 10) >> 22) + "." + std::to_string((encodedIn << 20) >> 20));
}

inline ge::Vk::CreateInfo::Instance::operator VkApplicationInfo() {
	return VkApplicationInfo{
		VK_STRUCTURE_TYPE_APPLICATION_INFO,
		nullptr,
		appName.c_str(),
		appVersion,
		engineName.c_str(),
		engineVersion,
		vkVersion
	};
}

#endif // !
