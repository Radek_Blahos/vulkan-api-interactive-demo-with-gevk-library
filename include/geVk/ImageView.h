/**
* @author Radek Blahos
* @project geVk Library
*
* @brief ImageView wrapper
*/

#ifndef GEVKIMAGEVIEW_H
#define GEVKIMAGEVIEW_H

#include <geVk/geVk.h>

/* ------- Info Class ------- */
class ge::Vk::Info::ImageView {
// attributes
public:
	ge::Vk::Image* image; // view to
	VkImageViewType viewType = VK_IMAGE_VIEW_TYPE_2D;
	VkFormat format;
	VkComponentMapping componentMaping = { VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY };
	VkImageSubresourceRange subresourceRange = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };

};

/* ------- CreateInfoClass ------- */
class ge::Vk::CreateInfo::ImageView : public ge::Vk::Info::ImageView {
// attributes
public:

// methods
public:
	ImageView() {}
	ImageView(const Info::ImageView& parent) : Info::ImageView(parent) {}
	operator VkImageViewCreateInfo();

};

/* ------- Core Class ------- */
class ge::Vk::ImageView { 
// friend class
	friend class Image;

// attributes
private:
	VkImageView _view = VK_NULL_HANDLE;
	ge::Vk::Info::ImageView* _description = nullptr;

// methods
private:
	void init(CreateInfo::ImageView& createInfo);
	void deinit();
	//bool initCompleted() { return(_view != VK_NULL_HANDLE && _description != nullptr); }
	void move(ImageView& withdraw);

public:
	/* Ctors 'n Dtor */
	ImageView() {}
	ImageView(CreateInfo::ImageView& createInfo) { init(createInfo); }
	ImageView(ImageView& copy) { move(copy); }
	~ImageView() { deinit(); }

	/* Getters */
	const Info::ImageView& getDescription() { return *_description; }
	VkImageView getVkView() { return _view; }

	/* Core Methods */
	void create(CreateInfo::ImageView& createInfo) { init(createInfo); }
	void destroy(){ deinit(); }

	/* Operators */
	ImageView& operator=(ImageView& assign) { move(assign); return *this; }
	operator VkImageView() { return _view; }

	/* Status Checkers */
	

};

#endif