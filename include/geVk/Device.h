/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Device wrapper object
*/

#ifndef GEVK_DEVICE_H
#define GEVK_DEVICE_H

#include <geVk\DeviceQueues.h>
#include <geVk\Instance.h>
#include <string>
#include <vector>
#include <unordered_map>

/* ------- Auxiliary Class ------- */
class ge::Vk::Auxiliary::Device {
// methods
public:
	// enumerate extensions
	static void enumerateAvailableExtensions(VkPhysicalDevice psDevice, std::vector<VkExtensionProperties>& props);
	// enumerate layers
	static void enumerateAvailableLayers(VkPhysicalDevice psDevice, std::vector<VkLayerProperties>& props);
	// Query for Support
	static bool extensionSupported(VkPhysicalDevice psDevice, std::string name);
	static int extensionsSupported(VkPhysicalDevice psDevice, std::vector<const char*>& names);
	static void extensionsSupported(VkPhysicalDevice psDevice, std::vector<const char*>& names, std::vector<bool>& resVec);
	static bool layerSupported(VkPhysicalDevice psDevice, std::string name);
	static int layersSupported(VkPhysicalDevice psDevice, std::vector<const char*>& names);
	static void layersSupported(VkPhysicalDevice psDevice, std::vector<const char*>& names, std::vector<bool>& resVec);
	// checking physical devices capability
	static bool checkPhysicalDeviceCapability(VkPhysicalDevice psDevice, ge::Vk::CreateInfo::Device& demands);
	static bool checkGPUFeatures(VkPhysicalDevice psDevice, VkPhysicalDeviceFeatures featuresToCheck);

};

/* ----------- Info Class ----------- */
class ge::Vk::Info::Device {
// attributes
public:
	VkSurfaceKHR usedSurface = VK_NULL_HANDLE;
	bool useValidationLayers;
	bool abundantInfo;
	bool verboseMode;

// virtual dtor
public:
	virtual ~Device() = default;

};

class ge::Vk::FurtherInfo::Device : public ge::Vk::Info::Device {
// attributes
public:
	std::vector<Info::RequestQueues> initializedQueuesInfo;
	VkPhysicalDeviceFeatures requieredDeviceFeatures;
	std::vector<const char*> requieredExtensionNames;
	std::vector<const char*> requieredLayerNames;

};

/* ------- CreateInfoClass ------- */
class ge::Vk::CreateInfo::Device : public ge::Vk::FurtherInfo::Device {
// attributes
private:
	std::vector<VkDeviceQueueCreateInfo> queuesInfo;

public:
	ge::Vk::Instance* from; // will be needed when searching for device's physical device
	void(*checkGPUCapability)(std::vector<VkPhysicalDevice>& hostGPUs);
	//void* FunAdditional_data;

// methods
public:
	Device() { memset(&requieredDeviceFeatures, 0, sizeof(requieredDeviceFeatures)); }
	operator VkDeviceCreateInfo();

};

/* ------- Core Class ------- */
class ge::Vk::Device : private Auxiliary::Device {
// attributes
private:
	std::unordered_map<uint32_t, Info::QueueFamily> _qHandles; // uint32_t qFamilyIdx
	VkPhysicalDevice _physicalDevice = VK_NULL_HANDLE;
	ge::Vk::Info::Device* _description = nullptr;
	VkDevice _device = VK_NULL_HANDLE;
	

// methods
private:
	void init(CreateInfo::Device& createInfo);
	void deinit();
	//bool initCompleted();
	
	// Fills handles vector based upon selected vkDevice and current qFamilyIdx, where queues_count must be equal to
	void obtainQueueHandles(CreateInfo::Device& createInfo);
	// Based upon CreateInfo selects proper GPU to host Vulkan
	void selectPhysicalDevice(CreateInfo::Device& createInfo);
	// select gpus throught extensions and layer compatibility
	void extLayCompatibleGPUs(std::vector<VkPhysicalDevice>& fromGPUs, CreateInfo::Device& demands);
	int findSuitableQueueFamilyIndex(VkPhysicalDevice fromGPU, VkQueueFlagBits properties, uint32_t& queuesCount, VkSurfaceKHR renderSurface = VK_NULL_HANDLE);
	void findSuitableQueueFamilyIndex(std::vector<VkPhysicalDevice>& fromGPUs, std::vector<int>& psDeviceQFamilies, VkQueueFlagBits properties, uint32_t queuesCount = 1, VkSurfaceKHR renderSurface = VK_NULL_HANDLE);
	void featuresSupported(std::vector<VkPhysicalDevice>& fromGPUs, VkPhysicalDeviceFeatures reqFeatures);

public:
	// Ctor 'n Dtor
	Device() {}
	Device(CreateInfo::Device& createInfo) { init(createInfo); }
	~Device() { deinit(); }

	/* Getters  -> make them const */
	VkDevice getVkDevice() { return _device; }
	VkPhysicalDevice getPhysicalDevice() { return _physicalDevice; }
	const Info::QueueFamily& getQueueHandles(uint32_t familyIdx) { return _qHandles[familyIdx]; };
	const Info::Device& getDescription() { return *_description; }
	bool verboseActivated() { return _description->verboseMode; }
	bool abundantInfo() { return _description->abundantInfo; }

	/* Casters */
	operator VkDevice() { return _device; }

	/* Core Methods */
	// Initialization on demand
	void create(CreateInfo::Device& createInfo) { init(createInfo); }
	// Objects dtor on demand
	void destroy() { deinit(); } 

	/* Capatibility checkers */
	// enumerate extensions
	void enumerateAvailableExtensions(std::vector<VkExtensionProperties>& props) { Auxiliary::Device::enumerateAvailableExtensions(_physicalDevice, props); }
	// enumerate layers
	void enumerateAvailableLayers(std::vector<VkLayerProperties>& props) { Auxiliary::Device::enumerateAvailableLayers(_physicalDevice, props); }
	// Query for Support
	bool extensionSupported(std::string name) { return Auxiliary::Device::extensionSupported(_physicalDevice, name); }
	void extensionsSupported(std::vector<const char*>& names, std::vector<bool>& resVec) { ge::Vk::Auxiliary::Device::extensionsSupported(_physicalDevice, names, resVec); }
	// returns number of available extensions from provided names
	int extensionsSupported(std::vector<const char*>& names) { return ge::Vk::Auxiliary::Device::extensionsSupported(_physicalDevice, names); }
	bool layerSupported(std::string name) { return Auxiliary::Device::layerSupported(_physicalDevice, name); }
	void layersSupported(std::vector<const char*>& names, std::vector<bool>& resVec) { ge::Vk::Auxiliary::Device::layersSupported(_physicalDevice, names, resVec); }
	int layersSupported(std::vector<const char*>& names) { return ge::Vk::Auxiliary::Device::layersSupported(_physicalDevice, names); }
	bool checkGPUFeatures(VkPhysicalDeviceFeatures featuresToCheck) { return ge::Vk::Auxiliary::Device::checkGPUFeatures(_physicalDevice, featuresToCheck); };

	/* Status Checkers */
	
};

#endif
