/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Command buffer Info sturctures used for CommandBuffer's methods
*/

#ifndef GEVKCOMMANDBUFFERINFOSTRUCTS_H
#define GEVKCOMMANDBUFFERINFOSTRUCTS_H

#include <geVk\geVk.h>
#include <vector>

class ge::Vk::Info::Commands::BeginRenderPass {
// attributes
public:
	ge::Vk::RenderPass* pRenderPass = nullptr;
	ge::Vk::Framebuffer* pFramebuffer = nullptr;
	VkRect2D renderArea = { { 0,0 }, };
	std::vector<VkClearValue> clearColors;
	VkSubpassContents subpassContents = VK_SUBPASS_CONTENTS_INLINE; // defines if during first subpass will be used secondary cmd buffers or not

// methods
public:
	operator VkRenderPassBeginInfo();

};

class ge::Vk::Info::Commands::BindIndexBuffer {
// attributes
public:
	ge::Vk::Buffer* pBuffer = nullptr;
	VkDeviceSize offset = 0;
	VkIndexType indexType = VK_INDEX_TYPE_UINT32;

};

class ge::Vk::Info::Commands::BindVertexBuffers {
// attributes
public:
	uint32_t firstBinding;
	std::vector<VkBuffer> pBuffers;
	std::vector<VkDeviceSize> offsets;

};

class ge::Vk::Info::Commands::BindDescriptorSets {
// attributes
public:
	std::vector<ge::Vk::DescriptorSet*> pDescriptorSets;
	uint32_t firstSet;
	ge::Vk::PipelineLayout* pPipelineLayout = nullptr;
	VkPipelineBindPoint bindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	std::vector<uint32_t> dynamicOffsets;

// methods
public:

};

class ge::Vk::Info::Commands::Draw {
// attributes
public:
	uint32_t vertexCount;
	uint32_t firstVertex = 0;
	uint32_t instanceCount = 1;
	uint32_t firstInstance = 0;

};

class ge::Vk::Info::Commands::DrawIndexed {
// attributes
public:
	uint32_t indexCount;
	uint32_t firstIndex = 0;
	int32_t vertexOffset = 0;
	uint32_t instanceCount = 1;
	uint32_t firstInstance = 0;

};

class ge::Vk::Info::Commands::DrawIndexedIndirect {
// attributes
public:
	VkBuffer buffer;
	VkDeviceSize offset;
	uint32_t drawCount;
	uint32_t stride;

};

class ge::Vk::Info::Commands::PushConstants {
// attributes
public:
	ge::Vk::PipelineLayout* pLayout;
	VkShaderStageFlags usedInStages;
	uint32_t offset = 0;
	uint32_t size;
	const void* pValues;

};



class ge::Vk::Info::Commands::SetViewport {
// attributes
public:
	float x = 0;
	float y = 0;
	float width;
	float height;
	float minDepth = 0;
	float maxDepth = 1;

// methods
public:
	operator VkViewport() {
		return { x, y, width, height, minDepth, maxDepth };
	}
};

class ge::Vk::Info::Commands::SetScissor {
// attributes
public:
	int32_t x = 0;
	int32_t y = 0;
	uint32_t width;
	uint32_t height;

// methods
public:
	operator VkRect2D() { return { { x, y }, { width, height } }; }

};

#endif
