/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Set builded over Command buffers...
*/

#ifndef GEVKCOMMANDBUFFERSET_H
#define GEVKCOMMANDBUFFERSET_H

#include <geVk/geVk.h>
#include <geVk\CommandPool.h>
#include <geVk\Device.h>
#include <geAx\UsageRegister.h>
#include <geVk\Synchronization.h>

/* ------- Info Class ------- */
class ge::Vk::Info::CommandBufferSet {
// attributes
public:
	bool createFences = true;
	bool useEvents = false; // if false Fences are used

};

/* ------- CreateInfoClass ------- */
class ge::Vk::CreateInfo::CommandBufferSet : public ge::Vk::Info::CommandBufferSet {
// attributes
public:
	ge::Vk::Device* pDevice;
	uint32_t familyQueueIdx; // to which queues can be commands submited
	uint32_t initialBufferCount; // how many buffers should be allocated at start

};

/* ------- Core Class ------- */
class ge::Vk::CommandBufferSet {
// attributes
private:
	CommandPool _pool;
	std::vector<CommandBuffer*> _buffers;
	ge::Ax::UsageRegister _bufferUsage;
	Info::CommandBufferSet* _description = nullptr;

// methods
private:
	void init(CreateInfo::CommandBufferSet& createInfo);
	void deinit();
	//bool initCompleted();
	int getNextBufferIdx(); // returns idx of the first free cmd buff

public:
	/* Ctors 'n Dtor */
	CommandBufferSet() {}
	CommandBufferSet(CreateInfo::CommandBufferSet& createInfo) { init(createInfo); }
	~CommandBufferSet() { deinit(); }
	
	/* Getters */
	CommandBuffer& getBuffer(unsigned idx) { return *_buffers[idx]; }
	
	// return cmd buffer which idx has been lastly assigned by getNextBuffer
	// User cannot call to consequence getCurrentBuffer calls without calling returnCurrentBuffer between them !
	CommandBuffer& getNextBuffer() { return *_buffers[getNextBufferIdx()]; }
	CommandBuffer& getCurrentBuffer() { return *_buffers[_bufferUsage.lastAssigned()]; } // neni osetreno, protoze chci aby to vyhodilo vyjimku, jelikoz to to znamena ze uzivatel pouziva cmd set spatne!
	int getCurrentBufferIdx() { return _bufferUsage.lastAssigned(); }
	void returnBuffer(uint32_t idx) { _bufferUsage.unsetIndex(idx); }
	void returnCurrentBuffer() { _bufferUsage.unsetIndex(_bufferUsage.lastAssigned()); } // mark buffer on idx as unused

	/* Core Methods */
	void create(CreateInfo::CommandBufferSet& createInfo) { init(createInfo); }
	void destroy() { return deinit(); }

	// insert count more cmd buffers into set
	void addBuffers(uint32_t count);

	// reset _bufferUsage 
	void usageRegisterReset() { _bufferUsage.unsetAllIndexes(); }
	void deleteFencePointers();

	/* Operators */

	/* Status Checkers */
	

};

#endif
