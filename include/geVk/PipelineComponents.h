/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Pipeline Componets used inside CreateInfo::Pipeline
*/

#ifndef GEVKPIPECOMPONENTS_H
#define GEVKPIPECOMPONENTS_H

#include <geVk\geVk.h>
#include <geVk\ShaderModule.h>
#include <geVk\Device.h>
#include <vector>
#include <string>

class ge::Vk::Info::Pipe::ShaderStage {
// attributes
public:
	VkShaderStageFlagBits stage;
	ge::Vk::ShaderModule* module = nullptr;
	std::string entryPoint = "main";
	VkSpecializationInfo* pConstants = nullptr; // shader constants, dynamic allocation only when needed

// methods
public:
	~ShaderStage() { if (pConstants != nullptr) delete pConstants; }
	operator VkPipelineShaderStageCreateInfo();

};

class ge::Vk::Info::Pipe::VertexInputDescription {
// attributes
public:
	//uint32_t binding; 
	uint32_t location = -1; // unassigned
	uint32_t stride;
	VkVertexInputRate inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
	VkFormat format = VK_FORMAT_R32G32B32_SFLOAT;
	uint32_t offset = 0;

// methods
public:
	inline operator VkVertexInputBindingDescription();
	inline operator VkVertexInputAttributeDescription();

};

class ge::Vk::Info::Pipe::InputAssemblyState {
// attributes
public:
	VkPrimitiveTopology topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	bool primitiveRestartEnable = VK_FALSE;

// methods
public:
	inline operator VkPipelineInputAssemblyStateCreateInfo();

};

class ge::Vk::Info::Pipe::ColorBlendAttachmentState 
	: public VkPipelineColorBlendAttachmentState 
{
public:
	ColorBlendAttachmentState();

};

class ge::Vk::Info::Pipe::ColorBlendState {
// attributes
public:
	VkBool32 logicOpEnable = VK_FALSE;
	VkLogicOp logicOp = VK_LOGIC_OP_COPY;
	std::vector<ColorBlendAttachmentState> attachments;
	float blendConstants[4] = { 0, };

// methods
public:
	operator VkPipelineColorBlendStateCreateInfo();

};

class ge::Vk::Info::Pipe::StencilOpState {
	// attributes
public:
	VkStencilOp failOp;
	VkStencilOp passOp;
	VkStencilOp depthFailOp;
	VkCompareOp compareOp;
	uint32_t compareMask;
	uint32_t writeMask;
	uint32_t reference;

	// methods
public:
	inline operator VkStencilOpState();

};

class ge::Vk::Info::Pipe::DepthStencilState {
// attributes
public:
	bool depthTestEnable = VK_FALSE;
	bool depthWriteEnable = VK_FALSE;
	VkCompareOp depthCompareOp = VK_COMPARE_OP_LESS;
	bool depthBoundsTestEnable = VK_FALSE;
	bool stencilTestEnable = VK_FALSE;
	Info::Pipe::StencilOpState front;
	Info::Pipe::StencilOpState back;
	float minDepthBounds;
	float maxDepthBounds;

// methods
public:
	inline operator VkPipelineDepthStencilStateCreateInfo();

};

class ge::Vk::Info::Pipe::MultisampleState {
// attributes
public:
	VkSampleCountFlagBits rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	bool sampleShadingEnable = VK_FALSE;
	float minSampleShading = 1.0f;
	VkSampleMask SampleMask = 0;
	bool alphaToCoverageEnable = VK_FALSE;
	bool alphaToOneEnable = VK_FALSE;

// methods
public:
	inline operator VkPipelineMultisampleStateCreateInfo();

};
class ge::Vk::Info::Pipe::RasterizationState {
// attributes
public:
	bool depthClampEnable = VK_FALSE;
	bool rasterizerDiscardEnable = VK_FALSE;
	VkPolygonMode polygonMode = VK_POLYGON_MODE_FILL;
	VkCullModeFlags cullMode = VK_CULL_MODE_NONE;
	VkFrontFace frontFace;
	bool depthBiasEnable = VK_FALSE;
	float depthBiasConstantFactor = 0.0f;
	float depthBiasClamp = 0.0f;
	float depthBiasSlopeFactor = 0.0f;
	float lineWidth = 1.0f;

// methods
public:
	inline operator VkPipelineRasterizationStateCreateInfo();

};

class ge::Vk::Info::Pipe::TessellationState {
// attributes
public:
	uint32_t patchControlPoints;

// methods
public:
	inline operator VkPipelineTessellationStateCreateInfo();

};

class ge::Vk::Info::Pipe::ViewportState {
// attributes
public:
	std::vector<VkViewport> viewports;
	std::vector<VkRect2D> scissors;

// methods
public:
	inline operator VkPipelineViewportStateCreateInfo();

};

#endif