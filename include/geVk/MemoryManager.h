/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Memory Manager class
*/

#ifndef GEVKMEMORYMANAGER_H
#define GEVKMEMORYMANAGER_H

#include <geVk\MemoryPool.h>
#include <geVk\Device.h>
#include <unordered_map>

/* ------- Info Class ------- */
class ge::Vk::Info::MemoryManager {
// attributes
public:
	ge::Vk::Device* pDevice = nullptr; // ptr to device
};

/* ------- CreateInfoClass ------- */
class ge::Vk::CreateInfo::MemoryManager : public ge::Vk::Info::MemoryManager {

};

/* ------- Execution Class ------- */
class ge::Vk::MemoryManager {
// attributes
private:
	ge::Vk::Info::MemoryManager* _description = nullptr;
	std::unordered_map<uint32_t, std::vector<ge::Vk::MemoryPool*>> _poolStorage; // uint32_t MemoryTypeIdx

public:

// methods
private:
	void init(CreateInfo::MemoryManager& createInfo);
	void deinit();
	//bool initCompleted() { return _description->pDevice != nullptr; }

	/* Memory Pool Events */
	void allocatePool(ge::Vk::CreateInfo::MemoryPool const& createInfo, ge::Vk::MemoryPool& pool);
	void releasePool(ge::Vk::MemoryPool& pool);
	bool provideMemoryBlock(ge::Vk::Info::MemoryBlock& resBlock, ge::Vk::MemoryPool& pool, VkDeviceSize requieredSize);

	/* Memory Pool Storage */
	ge::Vk::MemoryPool* insertPool(ge::Vk::CreateInfo::MemoryPool const& createInfo);
	void cleanUpStorage();   
	// Find suitable pool for storing resource
	ge::Vk::MemoryPool* findSuitableMemoryPool(ge::Vk::Info::MemoryRequierements& reqs);
	

public:
	/* Ctors 'n Dtor */
	MemoryManager() {}
	MemoryManager(CreateInfo::MemoryManager& createInfo) { init(createInfo); }
	~MemoryManager() { deinit(); }

	// Auxiliary
	int typeBitsToIndex(uint32_t typeBits, VkMemoryPropertyFlags requirements);

	/* Core Methods */
	// Init 
	void setUp(CreateInfo::MemoryManager& createInfo) { init(createInfo); } //return _description = new Info::MemoryManager(createInfo)
	void shutdown() { deinit(); }
	
	// alocate memory to pool
	VkDeviceMemory allocateMemory(VkDeviceSize size, uint32_t memTypeIdx, VkMemoryPropertyFlags memoryFlags);
	void releaseMemory(VkDeviceMemory memory);

	// assign or release memory from pool
	bool allocateMemoryBlock(ge::Vk::Info::MemoryBlock& resBlock, ge::Vk::Info::MemoryRequierements memReqs);
	void restoreMemoryBlock(ge::Vk::Info::MemoryBlock& memory); // memory pool ptr is part of memory block

	// assign memory block to object
	bool assignMemoryToImage(Image& image, uint32_t allocationSize = 0); // preAllocationCount -- kolikrat se ma baseRscSize naalokovat 
	bool assignMemoryToBuffer(Buffer& buffer, uint32_t allocationSize = 0);
	inline void releaseImageMemory(Image& image);
	inline void releaseBufferMemory(Buffer& buffer);
	
	// store data from host coherent memory into device local memory(GPU) -> copy between proper memory pools
	bool storeIntoGPU(Info::MemoryBlock& block); // block will be modified to point into GPU memory, host coherent mem will be freed... 

	// assign host|device shared memory to resource 
	void setSubData(void* data, VkDeviceSize size, VkDeviceSize offset = 0);
	void* map(Info::MemoryBlock& memory);
	void unmap(Info::MemoryBlock& memory);

	// auxiliary
	//bool findProperFormatForBuffer(Info::Buffer& buffer); // VkPhysicalDevice psDevice, VkFormat reqFormat
	//bool findProperFormatForImage(Info::Image& img);

	// preallocate memory pool 
	void preAllocateMemory(Info::MemoryProperties& info);

	// Truncate, mempools which dont store any data
	void truncatePools();

	/* Getters */
	ge::Vk::Device& getDevice() { return *_description->pDevice; }
	VkDevice getVkDevice() { return *_description->pDevice; }

	/* Status Checkers */
	
};

#endif