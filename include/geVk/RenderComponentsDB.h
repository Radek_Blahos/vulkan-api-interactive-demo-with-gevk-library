/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Render Compontents storage
*/

#ifndef GEVKPIPECOMPONENTSDB_H
#define GEVKPIPECOMPONENTSDB_H

#include <geVk/geVk.h>
#include <geVk\CommandBufferSet.h>
#include <geVk\DescriptorSet.inl>
#include <geVk\PipelineLayout.h>
#include <geVk\ShaderModule.h>
#include <geVk\Sampler.h>
#include <geVk\RenderPass.inl>
#include <geAx\PtrVector.inl>
#include <unordered_map>
#include <string>

class ge::Vk::Utils::RenderComponentsDB {
//attributes
public:
	std::unordered_map<std::string, PipelineLayout> pipeLayouts;
	std::unordered_map<std::string, ShaderModule> shaders;
	std::unordered_map<std::string, RenderPass> renderPasses;
	ge::Ax::PtrVector<CommandBufferSet> commandBufferSets; // no need to name commandBufferSets
	ge::Ax::PtrVector<DescriptorSet> descrSets; // or descriptor sets
	std::unordered_map<std::string, Semaphore> semaphores;
	ge::Ax::PtrVector<Sampler> samplers; 
	std::unordered_map<VkQueueFlagBits, uint32_t> queueFamilyTags; // keeps mapping from flags to queueFamilyIndex
	std::unordered_map<RenderPass*, ge::Ax::PtrVector<Framebuffer>> framebuffers; // framebuffers per renderpass and single swapchain

//methods
private:
public:
	/* Ctors 'n Dtor */
	//~RenderComponentsDB();

	/* Getters */

	/* Core Methods */

	/* Operators */

	/* Status Checkers */

};

#endif