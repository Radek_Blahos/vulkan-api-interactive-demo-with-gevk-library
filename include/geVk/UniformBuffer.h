/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Utility uniform buffer object -> This class will contain only geVkBuffer located in host memory(RAM)
	but will be treated as typename T
*/

#ifndef GEVKUNIFORMBUFFER_H
#define GEVKUNIFORMBUFFER_H

#include <geVk\geVk.h>
#include <geVk\Buffer.h>

template <typename T>
class ge::Vk::Utils::UniformBuffer {
// attributes
private:
	T* _viewPtr = nullptr; 
	ge::Vk::Buffer _buffer;

public:
	UniformBuffer() {}
	UniformBuffer(CreateInfo::Buffer& createInfo);
	~UniformBuffer() { if (_viewPtr != nullptr) _buffer.unmap(); }
	void create(CreateInfo::Buffer& createInfo);
	
	
	ge::Vk::Buffer& getBuffer() { return _buffer; }
	T& accessData() { return *_viewPtr; }
	void copy(T& data);

};

#endif
