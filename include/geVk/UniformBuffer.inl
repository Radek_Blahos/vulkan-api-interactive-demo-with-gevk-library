/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Uniform buffer's inlines
*/

#ifndef GEVKUNIFORMBUFFER_INL
#define GEVKUNIFORMBUFFER_INL

#include <geVk\UniformBuffer.h>

using namespace ge::Vk;

template<typename T>
Utils::UniformBuffer<T>::UniformBuffer(CreateInfo::Buffer& createInfo) {
	createInfo.size = sizeof(T);
	createInfo.assignMemory = true;
	_buffer.create(createInfo);
	
	// initialize data memory to default for object T
	_viewPtr = (T*)_buffer.map();
	T init;
	memcpy(_viewPtr, &init, sizeof(T));
}

template<typename T>
void Utils::UniformBuffer<T>::create(CreateInfo::Buffer& createInfo) {
	createInfo.size = sizeof(T);
	createInfo.assignMemory = true;
	_buffer.create(createInfo);

	// initialize data memory to default for object T
	_viewPtr = (T*)_buffer.map();
	T init;
	memcpy(_viewPtr, &init, sizeof(T));
}

template<typename T>
void ge::Vk::Utils::UniformBuffer<T>::copy(T& data) {
	memcpy(_viewPtr, &data, sizeof(T));
}

#endif // !GEVKUNIFORMBUFFER_INL
