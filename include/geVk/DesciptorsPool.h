/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Descriptor Pool wrapper object
*/

#ifndef GEVKDESCRIPTORPOOL_H
#define GEVKDESCRIPTORPOOL_H

#include <geVk\Device.h>
#include <vector>
#include <unordered_map>

/* ----------- Info Class ----------- */
class ge::Vk::Info::DescriptorPool {
// attributes
public:
	ge::Vk::Device* pDevice;
	uint32_t maxSets; // maximum number of sets which can be allocated from pool
	VkDescriptorPoolCreateFlags flags = 0;

};

/* ----------- Create Info Class ----------- */
class ge::Vk::CreateInfo::DescriptorPool : public ge::Vk::Info::DescriptorPool {
// attributes
public:
	std::vector<VkDescriptorPoolSize> allocateObjectsInfo;
	uint32_t maxAllocationCount = 1;

// methods
public:
	operator VkDescriptorPoolCreateInfo();

};

/* ------- Core Class ------- */
class ge::Vk::DescriptorPool {
// friend class 
	friend class ge::Vk::DescriptorManager;

// attributes
private:
	Info::DescriptorPool* _description = nullptr;
	VkDescriptorPool _pool = VK_NULL_HANDLE;
	std::unordered_map<VkDescriptorType, uint32_t> _allocationAbleObjectsInfo;

// methods
private:
	void init(CreateInfo::DescriptorPool& createInfo);
	void deinit();
	//bool initCompleted();

public:
	/* Ctors 'n Dtor */
	DescriptorPool() {}
	DescriptorPool(CreateInfo::DescriptorPool& createInfo) { init(createInfo); }
	~DescriptorPool() { deinit(); }

	/* Core Methods */
	void construct(CreateInfo::DescriptorPool& createInfo) { init(createInfo); }
	void wipeout() { deinit(); }

	/* Getters */
	VkDescriptorPool getVkPool() { return _pool; }

	/* Operators */
	operator VkDescriptorPool() { return _pool; }

	/* Status Checkers */
	
};


#endif
