/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Compute Pipeline class
*/

#ifndef GEVKCOMPUTEPIPE_H
#define GEVKCOMPUTEPIPE_H

#include <geVk\Pipeline.h>
#include <geVk\PipelineLayout.h>
#include <geVk\GPipelineComponents.inl>

/* ------- Info Class ------- */
class ge::Vk::Info::ComputePipeline : public ge::Vk::Info::Pipeline {
// attributes
public:

// virtual methods
public:
	virtual ~ComputePipeline() = default;
	// virtual ComputePipeline& operator=(ComputePipeline& assign) = default; nemusi byt posunuju akorat ptr na strukturu v tech objektech ...

};

class ge::Vk::FurtherInfo::ComputePipeline : public ge::Vk::Info::ComputePipeline {
// attributes
public:
	// shader stage info
	ge::Vk::Info::Pipe::ShaderStage shaderStage;

};

/* ------- CreateInfoClass ------- */
class ge::Vk::CreateInfo::ComputePipeline : public ge::Vk::FurtherInfo::ComputePipeline, public ge::Vk::CreateInfo::Pipeline {
// attributes
public:

// methods
public:
	operator VkComputePipelineCreateInfo();

};

/* ------- Core Class ------- */
class ge::Vk::ComputePipeline : private ge::Vk::Pipeline {
// attributes
private:
	// pipeline's info
	ge::Vk::Info::ComputePipeline* _description = nullptr;


// methods
private:
	void init(CreateInfo::ComputePipeline& createInfo);
	void deinit();
	//bool initCompleted() { return _pipeline != VK_NULL_HANDLE && _description != nullptr; }
	void move(ComputePipeline& withdraw);

public:
	// Ctor 'n Dtor
	ComputePipeline() {}
	ComputePipeline(CreateInfo::ComputePipeline& createInfo) { init(createInfo); } // testnout jak se bude chovat vector na move-assignment(p
	ComputePipeline(ComputePipeline& copy) { move(copy); }
	~ComputePipeline() { deinit(); }

	/* read-only Getters */
	const Info::ComputePipeline& getDescription() { return *_description; }

	/* operators */
	ComputePipeline& operator=(ComputePipeline& assign) { move(assign); return *this; }
	operator VkPipeline() { return Pipeline::operator VkPipeline(); }

	/* Core Methods */
	// object's lifetime
	void assemble(CreateInfo::ComputePipeline& createInfo) { init(createInfo); }
	bool assemble(VkPipeline vkpipe, Info::ComputePipeline& info); // used in pipelineDB
	void dismantle() { deinit(); }

	// Explicit Object's Reset, object is keept in same memory, but attributes are restored in state as in the Start of Ctor. 
	bool rebuild(CreateInfo::ComputePipeline* createInfo); // if createInfo == nullptr -> reset else rebuild via createInfo

	/* Status Checkers */
	
};

#endif
