/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Header containing all sync primitives
*/

#ifndef GEVKSYNC_H
#define GEVKSYNC_H

#include <geVk/SyncPrimitive.h>
#include <geVk\Semaphore.h>

#endif