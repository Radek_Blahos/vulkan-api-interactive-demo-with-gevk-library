/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Framebuffer wrapper
*/

#ifndef GEVKFRAMEBUFFER_H
#define GEVKFRAMEBUFFER_H

#include <geVk/geVk.h>
#include <vector>

/* ------- Info Class ------- */
class ge::Vk::Info::Framebuffer {
// attributes
public:
	ge::Vk::Device* pDevice;
	ge::Vk::RenderPass* activeRenderPass;
	uint32_t width;
	uint32_t height;
	uint32_t layersCount; // or dimensions of framebuffer if you want

// virtual dtor
public:
	virtual ~Framebuffer() = default;

};

class ge::Vk::FurtherInfo::Framebuffer : public ge::Vk::Info::Framebuffer {
// attributes
public:
	std::vector<ge::Vk::ImageView*> attachmentRefs;

};

/* ------- CreateInfoClass ------- */
class ge::Vk::CreateInfo::Framebuffer : public ge::Vk::FurtherInfo::Framebuffer {
// attributes
private:
	std::vector<VkImageView> _createInfoAttachmentRefs;

// methods
public:
	operator VkFramebufferCreateInfo();

};

/* ------- Core Class ------- */
class ge::Vk::Framebuffer {
// attributes
private:
	ge::Vk::Info::Framebuffer* _description = nullptr;
	VkFramebuffer _framebuffer = VK_NULL_HANDLE;

public:

// methods
private:
	void init(CreateInfo::Framebuffer& createInfo);
	void deinit();
	//bool initCompleted() { return _framebuffer != VK_NULL_HANDLE && _description != nullptr; }
	void move(Framebuffer& withdraw);

public:
	/* Ctors 'n Dtor */
	Framebuffer() {}
	Framebuffer(CreateInfo::Framebuffer& createInfo) { init(createInfo); }
	Framebuffer(Framebuffer& copy) { move(copy); }
	~Framebuffer() { deinit(); }

	/* Getters */
	Info::Framebuffer& getDescription() { return *_description; }
	VkFramebuffer getVkFrameBuffer() { return _framebuffer; }

	/* Operators */
	Framebuffer& operator=(Framebuffer& assign) { move(assign); return *this; }
	operator VkFramebuffer() { return _framebuffer; }

	/* Core Methods */
	// nickname for init
	void assemble(CreateInfo::Framebuffer& createInfo) { init(createInfo); }
	void dismantle() { deinit(); };
	
	/* Status Checkers */
	
}; 

#endif
