/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Command buffer object
*/

#ifndef GEVKCOMMANDBUFFER_H
#define GEVKCOMMANDBUFFER_H

#include <geVk/geVk.h>
#include <geVk\CommandPool.h>
#include <geVk\Synchronization.h>

/* ------- Info Class ------- */
class ge::Vk::Info::CommandBuffer {
// attributes
public:
	ge::Vk::CommandPool* pPool;
	VkCommandBufferLevel level;
	bool useEvent = false; // if false, fence is used as sync primitive

};

/* ------- CreateInfoClass ------- */
class ge::Vk::CreateInfo::CommandBuffer : public ge::Vk::Info::CommandBuffer {
// attributes
public:
	bool createFence = true;

// methods
public:
	operator VkCommandBufferAllocateInfo();

};

/* ------- Core Class ------- */
class ge::Vk::CommandBuffer {
// attributes
private:
	Info::CommandBuffer* _description = nullptr;
	VkCommandBuffer _buffer = VK_NULL_HANDLE;
	Utils::SyncPrimitive* _lock = nullptr;

public:
	VkCommandBufferInheritanceInfo* inheritanceInfo = nullptr; // only if _description->level == Secondary

// methods
private:
	void init(CreateInfo::CommandBuffer& createInfo);
	void deinit();
	//bool initCompleted() { return _buffer != VK_NULL_HANDLE && _description != nullptr; }
	void move(CommandBuffer& withdraw);

public:
	/* Ctors 'n Dtor */
	CommandBuffer() {}
	CommandBuffer(CreateInfo::CommandBuffer& createInfo) { init(createInfo); }
	CommandBuffer(CommandBuffer& copy) { move(copy); }
	CommandBuffer(VkCommandBuffer buffer, CreateInfo::CommandBuffer info) { adopt(buffer, info); }
	~CommandBuffer() { deinit(); }

	/* Getters */
	VkCommandBuffer getVkBuffer() { return _buffer; }
	VkCommandBuffer* getVkBufferPtr() { return &_buffer; }
	const Info::CommandBuffer& getDescription() { return *_description; }
	uint32_t getFamilyIndex() { return _description->pPool->getDescription().queueFamilyIndex; }
	Utils::SyncPrimitive& getLock() { return *_lock; }
	Utils::SyncPrimitive* getLockPtr() { return _lock; }

	/* Core Methods */
	void create(CreateInfo::CommandBuffer& createInfo) { init(createInfo); }
	bool create(VkCommandBuffer vkbuffer, CreateInfo::CommandBuffer& createInfo);
	void discard() { deinit(); }

	// adopt and disinherit vkcommandbuffer object
	void adopt(VkCommandBuffer buffer, CreateInfo::CommandBuffer info);
	void disinherit();

	/* Custom VkCommandBuffer Commands */
	bool beginRecording(VkCommandBufferUsageFlags usage = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT); // secondary buffers must preinit their inheritance struct before calling this...
	bool finishRecording() { return vkEndCommandBuffer(_buffer) == VK_SUCCESS; }
	bool reset(VkCommandBufferResetFlags resetFlag = VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT) { return VK_SUCCESS == vkResetCommandBuffer(_buffer, resetFlag); }
	void beginRenderPass(Info::Commands::BeginRenderPass& beginInfo);
	void endRenderPass() { vkCmdEndRenderPass(_buffer); }
	void setViewport(Info::Commands::SetViewport& info);
	void setScissor(Info::Commands::SetScissor& info);
	void bindVertexBuffers(Info::Commands::BindVertexBuffers& info);
	void bindIndexBuffer(Info::Commands::BindIndexBuffer& info);
	void bindIndexBuffer(VkBuffer indexBuffer, VkDeviceSize offset, VkIndexType indexType = VK_INDEX_TYPE_UINT32);
	void bindPipeline(Pipeline& pipeline, VkPipelineBindPoint bindPoint); 
	void bindGraphicPipeline(GraphicPipeline& pipeline);
	void bindComputePipeline(ComputePipeline& pipeline);
	void bindDescriptorSets(Info::Commands::BindDescriptorSets& info);
	void pushConstants(Info::Commands::PushConstants& info);
	void setEvent(VkEvent event, VkPipelineStageFlags stageMask);
	void resetEvent(VkEvent event, VkPipelineStageFlags stageMask);
	// in future, waitEvent, but this command not only waits for event to be signaled but also with it activate memory barrier

	// variations of draw commands
	void draw(Info::Commands::Draw& info) { vkCmdDraw(_buffer, info.vertexCount, info.instanceCount, info.firstVertex, info.firstInstance); }
	void draw(uint32_t vertexCount, uint32_t firstVertex = 0, uint32_t instanceCount = 1, uint32_t firstInstance = 0) { vkCmdDraw(_buffer, vertexCount, instanceCount, firstVertex, firstInstance); }
	void drawIndexed(Info::Commands::DrawIndexed& info) { vkCmdDrawIndexed(_buffer, info.indexCount, info.instanceCount, info.firstIndex, info.vertexOffset, info.firstInstance); }
	void drawIndexed(uint32_t indexCount, uint32_t firstIndex = 0, int32_t vertexOffset = 0, uint32_t instanceCount = 1, uint32_t firstInstance = 0) { vkCmdDrawIndexed(_buffer, indexCount, instanceCount, firstIndex, vertexOffset, firstInstance); }
	void drawIndexedIndirect(VkBuffer buffer, VkDeviceSize offset, uint32_t drawCount, uint32_t stride);
	void drawIndexedIndirect(Info::Commands::DrawIndexedIndirect& info) { drawIndexedIndirect(info.buffer, info.offset, info.drawCount, info.stride); }

	/* Operators */
	CommandBuffer& operator=(CommandBuffer& assign) { move(assign); return *this; }
	operator VkCommandBuffer() { return _buffer; }

	/* Lock using operations */
	void waitUntilExecutionFinished();
	bool isSubmited();

};

#endif
