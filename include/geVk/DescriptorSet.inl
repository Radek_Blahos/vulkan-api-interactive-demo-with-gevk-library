/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Inlines for Descriptor Set
*/

#ifndef GEVKDESCRIPTOR_INL
#define GEVKDESCRIPTOR_INL

#include <geVk\DescriptorSet.h>

inline ge::Vk::Info::WriteDescriptorSet::operator VkWriteDescriptorSet() {
	return {
		VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
		nullptr,
		dstSet,
		dstBinding,
		dstArrayElement,
		descriptorCount,
		descriptorType,
		pImageInfo,
		pBufferInfo,
		pTexelBufferView
	};
}

/* --------- Core --------- */
void ge::Vk::DescriptorSet::update(Info::Methods::UpdateDescriptorSets& info) {
	std::vector<VkWriteDescriptorSet> writeInfos;
	for (auto& writeI : info.writeInfo)
		writeInfos.push_back(writeI);

	vkUpdateDescriptorSets(_description->pManager->getDevice(), writeInfos.size(), writeInfos.data(), 0, VK_NULL_HANDLE);
}

#endif