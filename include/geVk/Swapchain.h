/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Swapchain wrapper
*/

#ifndef GEVKSWAPCHAIN_H
#define GEVKSWAPCHAIN_H

#include <geVk/geVk.h>
#include <geVk\Framebuffer.h>
#include <geVk\Image.h>
#include <geVk\Synchronization.h>
#include <geAx\PtrVector.inl> 

/* ------- Auxiliary Class ------- */
class ge::Vk::Auxiliary::Swapchain {
// methods
public:
	static bool checkSurfaceFormatAvailability(VkPhysicalDevice psDevice, VkSurfaceKHR surface, VkSurfaceFormatKHR format);
	static VkSurfaceFormatKHR returnRandomAvailableSurfaceFormat(VkPhysicalDevice psDevice, VkSurfaceKHR surface);
	static bool checkPresentModeAvailability(VkPhysicalDevice psDevice, VkSurfaceKHR surface, VkPresentModeKHR mode);
	static VkPresentModeKHR returnRandomAvailablePresentMode(VkPhysicalDevice psDevice, VkSurfaceKHR surface);
	static VkSurfaceCapabilitiesKHR surfaceCapabilities(VkPhysicalDevice psDevice, VkSurfaceKHR surface);
	static VkDisplayPropertiesKHR singleDisplayProperties(VkPhysicalDevice psDevice);
};

class ge::Vk::Info::Methods::PresentImage {
// methods
public:
	std::vector<VkSemaphore> waitSemaphores;
	std::vector<VkSwapchainKHR> swapchains;
	std::vector<uint32_t> imageIndexes;
	bool checkResults = false;
	std::vector<VkResult> results;

	operator VkPresentInfoKHR();

};

/* ------- Info Class ------- */
class ge::Vk::Info::Swapchain {
// attributes
public:
	ge::Vk::MemoryManager* pMemManager; // parrent Device ptr, used when creating depth buffer, includes ptr to Device
	ge::Vk::Queue* presentationQueue;

	VkSwapchainCreateFlagsKHR createFlags;
	VkSurfaceKHR surface;
	uint32_t minImageCount;
	VkSurfaceFormatKHR surfaceFormat;
	uint32_t width;
	uint32_t height;
	uint32_t imageArrayLayers;
	VkImageUsageFlags imageUsage;
	VkSurfaceTransformFlagBitsKHR preTransform;
	VkCompositeAlphaFlagBitsKHR compositeAlpha;
	VkPresentModeKHR presentMode;
	VkBool32 clipped;
	std::unordered_map<ge::Vk::RenderPass*, ge::Ax::PtrVector<ge::Vk::Framebuffer>>* framebuffersVecPtr;
	//VkSurfaceCapabilitiesKHR surfaceCaps;

	// advanced usage
	//VkSharingMode imageSharingMode = VK_SHARING_MODE_EXCLUSIVE; // Image share between numerous queues
};

/* ------- CreateInfoClass ------- */
class ge::Vk::CreateInfo::Swapchain : public ge::Vk::Info::Swapchain {
// attributes
public:
	VkSwapchainKHR oldSwapchain; // for recreating swapchain via old one

// methods
public:
	Swapchain() {}
	Swapchain(const Info::Swapchain& info) : Info::Swapchain(info) {}
	operator VkSwapchainCreateInfoKHR();

};

/* ------- Core Class ------- */
class ge::Vk::Swapchain {
// attributes
private:
	std::vector<Image*> _images; // imageView is part of ge::Vk::Image
	Image* _depthbuffer = nullptr;

	uint32_t _currentImageIdx = -1; // max uint number -> none image acquired.
	Info::Swapchain* _description = nullptr; // custom alloc in ctor
	VkSwapchainKHR _swapchain = VK_NULL_HANDLE;
	
	RenderPass* _currentRenderPass; // signals for which renderpass use framebuffers
	Semaphore _presentationLock; // On GPU Synchronization wait for presentation of previous image comes to the end
	Fence _obtainNextImageFence; // to create it once not every obtainNextImage method call...

// methods
private:
	void init(CreateInfo::Swapchain& createInfo);
	void deinit();
	void move(Swapchain& withdraw);
	//bool initCompleted();

	// obtain swapchain images and init image views for them
	void obtainImages(); 
	// before creating framebuffers used render pass must be created
	void returnImages(); // clears all gevkimages(set them null, because swapchain cares of dtoring them)
	// dtory na objekty nejsou treba... zaridi si to objekty sami ;) 
	
public:
	/* Ctors 'n Dtor */
	Swapchain() {}
	Swapchain(CreateInfo::Swapchain& createInfo) { init(createInfo); }
	Swapchain(Swapchain& copy) { move(copy); }
	~Swapchain() { deinit(); };

	/* Object's Lifetime Methods */
	void create(CreateInfo::Swapchain& createInfo) { init(createInfo); }
	void dismantle() { deinit(); }
	
	/* Getters */
	void acquireNextImage(); // rekne swapchain aby ziskala dalsi image, vrati se s presentImage...
	Image& getImage() { if (_currentImageIdx != -1)  return *_images[_currentImageIdx]; }
	Framebuffer& getFramebuffer() { if (_currentImageIdx != -1) return *(*_description->framebuffersVecPtr)[_currentRenderPass][_currentImageIdx]; }
	Framebuffer& getFramebuffer(uint32_t idx) { return *(*_description->framebuffersVecPtr)[_currentRenderPass][idx]; }
	Image& getDepthbuffer() { return *_depthbuffer; }	
	VkFormat getImageFormat() { return _images.front()->getDescription().format; }
	VkSurfaceKHR getPresentationSurface() { return _description->surface; }
	const Info::Swapchain& getDescription() { return *_description; }
	VkSwapchainKHR getVkSwapchain() { return _swapchain; }
	int getCurrentImageIdx() { return _currentImageIdx; }

	/* Core Methods */
	void initResources();
	bool presentImage(); //bool waitPresentationEnd = false
	bool presentImage(Info::Methods::PresentImage& info) { VkPresentInfoKHR tmp = info; return vkQueuePresentKHR(*_description->presentationQueue, &tmp) == VK_SUCCESS; }
	void changeResolution(uint32_t width, uint32_t height);
	void createFramebuffers(RenderPass& usedRenderPass);
	void deleteFramebuffers(RenderPass& usedRenderPass) { if (_description != nullptr) _description->framebuffersVecPtr->erase(&usedRenderPass); } // delete framebuffers for defined renderpass
	void setCurrentRenderPass(RenderPass& usedRenderPass) { if (_description->framebuffersVecPtr->find(&usedRenderPass) != _description->framebuffersVecPtr->end()) _currentRenderPass = &usedRenderPass; } // assign renderpass only if framebuffers for it exists ...
	void createDepthBuffer(VkDeviceSize allocSize = 0);
	void transformImagesToProperLayout(CommandBuffer& postCommandsHere);
	void clearColors(CommandBuffer& keepCommands, CommandProcessor& execCommands); // clear colors for swapchain image and deptbuffer

	/* Operators */
	operator VkSwapchainKHR() { return _swapchain; }
	Swapchain& operator=(Swapchain& assign) { move(assign); return *this; }

	/* Status Checkers */
	
};

#endif

/* 
// Choose Swapchain's surface compatibility with GPU, zada se pozadovany, a v res vectoru se vrati nalezeny pro kazde gpu nebo prazdna struktura
void SurfaceCompatibleGPUs(VkPhysicalDevice psDevice, VkSurfaceFormatKHR& format);
*/