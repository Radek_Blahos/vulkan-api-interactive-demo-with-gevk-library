/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Graphic pipeline class

Chovani objektu odpovida chovani VkPipeline objektu jak je ve specifikaci.
Objektu se v konstruktoru preda, povinne, struktura popisujici pipeline a v ramci
behu ctoru se vytvori finalni VkPipe. Pote uz objekt slouzi akorat ke cteni parametru pipeline,
ziskani VkPipeline ptr. Je zde pridana moznost, zmenit kompletne pipeline na zaklade rebuild metody.
Ta ma ale velkou rezii, jelikoz se musi z GPU odstranit cela puvodni pipeline a nahrat se tam nova

*/

#ifndef GEVKGRAPHICPIPE_H
#define GEVKGRAPHICPIPE_H

#include <geVk\GPipelineComponents.inl>
#include <geVk\PipelineLayout.h>
#include <geVk\Pipeline.h>
#include <vector>

/* ------- Info Class ------- */
class ge::Vk::Info::GraphicPipeline : public ge::Vk::Info::Pipeline {
// attributes
public:
	// renderpass
	ge::Vk::RenderPass* renderPass;

	// used in subpass no.
	uint32_t usedForSubpass;

// virtual dtor
public:
	virtual ~GraphicPipeline() = default;

};

class ge::Vk::FurtherInfo::GraphicPipeline : public ge::Vk::Info::GraphicPipeline {
// attributes
public:
	std::vector<Info::Pipe::ShaderStage> stagesInfo;
	std::vector<Info::Pipe::VertexInputDescription> vertexInput; // must be congruent with shaders, so in current state cant use geVkMesh to fill it, must be hardcored. I going to solve this somehow in my future work.
	ge::Vk::Info::Pipe::InputAssemblyState primitivesAssembly;
	ge::Vk::Info::Pipe::TessellationState tessellation;
	ge::Vk::Info::Pipe::ViewportState viewport;
	ge::Vk::Info::Pipe::RasterizationState rasterization;
	ge::Vk::Info::Pipe::MultisampleState multisample;
	ge::Vk::Info::Pipe::DepthStencilState depthStencil;
	ge::Vk::Info::Pipe::StencilOpState stencilFrontOp;
	ge::Vk::Info::Pipe::StencilOpState stencilBackOp;
	ge::Vk::Info::Pipe::ColorBlendState colorBlend;
	std::vector<ge::Vk::Info::Pipe::DynamicState> dynamicStates;

};

/* ------- CreateInfoClass ------- */
class ge::Vk::CreateInfo::GraphicPipeline : public ge::Vk::FurtherInfo::GraphicPipeline, public ge::Vk::CreateInfo::Pipeline {
// attributes
private:
	std::vector<VkPipelineShaderStageCreateInfo> shaderStages;
	std::vector<VkVertexInputBindingDescription> inputBinding;
	std::vector<VkVertexInputAttributeDescription> attriburesDescription;
	VkPipelineVertexInputStateCreateInfo inputState;
	VkPipelineInputAssemblyStateCreateInfo assemblyState;
	VkPipelineTessellationStateCreateInfo tessellationState;
	VkPipelineViewportStateCreateInfo viewportState;
	VkPipelineRasterizationStateCreateInfo rasterizationState;
	VkPipelineMultisampleStateCreateInfo multisampleState;
	VkPipelineDepthStencilStateCreateInfo depthStencilState;
	VkPipelineColorBlendStateCreateInfo colorBlendState;
	VkPipelineDynamicStateCreateInfo dynamicState;

// methods
public:
	operator VkGraphicsPipelineCreateInfo();
	operator VkPipelineVertexInputStateCreateInfo();
	operator VkPipelineDynamicStateCreateInfo();
};

/* ------- Core Class ------- */
class ge::Vk::GraphicPipeline : private ge::Vk::Pipeline {
// attributes
private:
	// pipeline's info
	ge::Vk::Info::GraphicPipeline* _description = nullptr;

	
// methods
private:
	void init(CreateInfo::GraphicPipeline& createInfo);
	void deinit();
	//bool initCompleted() { return _pipeline != VK_NULL_HANDLE && _description != nullptr; }
	void move(GraphicPipeline& withdraw);

public:
	// Ctor 'n Dtor
	GraphicPipeline() {}
	GraphicPipeline(CreateInfo::GraphicPipeline& createInfo) { init(createInfo); } // testnout jak se bude chovat vector na move-assignment(p
	GraphicPipeline(GraphicPipeline& copy) { move(copy); }
	~GraphicPipeline() { deinit(); }
	
	/* read-only Getters */
	const Info::GraphicPipeline& getDescription() { return *_description; }

	/* operators */
	GraphicPipeline& operator=(GraphicPipeline& copy) { move(copy); return *this; }
	operator VkPipeline() { return Pipeline::operator VkPipeline(); }

	/* Core Methods */
	// object's lifetime
	void assemble(CreateInfo::GraphicPipeline& createInfo) { init(createInfo); }
	bool assemble(VkPipeline vkpipe, Info::GraphicPipeline& info); // used in pipelineDB
	void dismantle() { deinit(); }
	
	// Explicit Object's Reset, object is keept in same memory, but attributes are restored in state as in the Start of Ctor. 
	bool rebuild(CreateInfo::GraphicPipeline* createInfo); // if createInfo == nullptr -> reset else rebuild via createInfo

	// renderpass compatibility check
	bool renderPassCompatible(ge::Vk::RenderPass& renderPassToCheck);

	/* Status Checkers */

};

#endif