/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Class wrapping Vulkan Buffer object. 
*/

#ifndef GEVKBUFFER_H
#define GEVKBUFFER_H

#include <geVk\MemoryManager.h>
#include <geVk\MemoryBlock.h>
#include <geVk/geVk.h>
#include <vector>

/* ------- Info Class ------- */
class ge::Vk::Info::Buffer {
// attributes
public:
	ge::Vk::MemoryManager* pManager = nullptr; // zpristupnuje memory operace a zaroven urcuje odkud se vezne pamet
	VkDeviceSize size; // buffer's size
	VkBufferUsageFlags usage;
	VkMemoryPropertyFlags propertyFlags;

};

/* ------- CreateInfoClass ------- */
class ge::Vk::CreateInfo::Buffer : public ge::Vk::Info::Buffer {
public:
	enum Usage { GENERAL, VERTEXBUFFER, INDEXBUFFER, STAGGINGBUFFER, SHADERSTORAGE, INDEXEDINDIRECTDRAW };

// attributes
public:
	bool assignMemory = true;
	VkDeviceSize allocationSize = 0; // if 0 Info::size memory will be allocated else defines how much memory will be allocated from pool

// methods
public:
	// Ctor
	Buffer(Info::Buffer const& info) : Info::Buffer(info) {}
	Buffer(Usage bufferUsage = Usage::GENERAL, uint32_t locationFlags = ge::Vk::Info::Memory::location::GPU);
	
	operator VkBufferCreateInfo();

};

/* ------- Core Class ------- */
class ge::Vk::Buffer {
//friend class
	friend class MemoryManager;

// attributes
private:
	Info::Buffer* _description = nullptr;
	VkBuffer _buffer = VK_NULL_HANDLE;
	Info::MemoryBlock _memory;

// methods
private:
	void init(CreateInfo::Buffer& createInfo);
	void deinit();
	//bool initCompleted() { return _buffer != VK_NULL_HANDLE && _memory.allocatedFrom != nullptr; }
	void move(Buffer& withdraw);

public:
	/* Ctors 'n Dtor */
	Buffer() {}
	Buffer(CreateInfo::Buffer& createInfo) { init(createInfo); }
	Buffer(Buffer& copy) { move(copy); } // same as move ctor
	//Buffer(Buffer&& copy) { move(copy); } // move ctor
	~Buffer() { deinit(); }
	// when using this, buffer must have flag DeviceLocal
	void stashOnGPU(std::vector<uint8_t>& data, CommandBuffer& keepCommands, CommandProcessor& proccesCommands, uint32_t dstBufferOffset = 0);

	/* Getters */
	VkBuffer getVkBuffer() { return _buffer; }
	Info::MemoryBlock& getMemory() { return _memory; }
	Device& getDevice() { return _description->pManager->getDevice(); }
	VkDevice getVkDevice() { return _description->pManager->getVkDevice(); }

	/* Operators */
	operator VkBuffer() { return _buffer; }
	Buffer& operator=(Buffer& copy) { move(copy); return *this; } // "move" -> shallow assign

	/* Core Methods */
	// object lifetime
	void create(CreateInfo::Buffer& createInfo) { init(createInfo); }
	void discard() { deinit(); }

	/* 
		post commands to copy memory from one geVkBuffer to another, source's data keeps untouched,
		its user responsibility to begin command buffer before posting it to this method and after this method's call cmd buffer must be finished and posted by user...
	*/
	void copyMemory(Buffer& data, CommandBuffer& keepCommands, VkBufferCopy* copyInfo = nullptr);

	// memory maping VRAM -> RAM
	void* map();
	void unmap();

	// getting buffer's memory requierements 
	void getMemoryProperties(Info::MemoryProperties& result);
	void assignMemory();

	// changing layout

	/* Status Checkers */
	
};

#endif
