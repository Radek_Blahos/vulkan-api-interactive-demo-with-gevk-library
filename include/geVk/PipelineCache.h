/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Pipeline Cache object
*/

#ifndef GEVKPIPECACHE_H
#define GEVKPIPECACHE_H

#include <geVk/geVk.h>
#include <geVk\Device.h>
#include <vector>
#include <string>

/* ------- Info Class ------- */
class ge::Vk::Info::PipelineCache {
// attributes
public:
	ge::Vk::Device* pDevice;

};


/* ------- CreateInfoClass ------- */
class ge::Vk::CreateInfo::PipelineCache : public ge::Vk::Info::PipelineCache {
// attributes
private:
	std::vector<uint8_t> _data;

public:
	std::string pathToData;

// methods
public:
	operator VkPipelineCacheCreateInfo();

};

/* ------- Core Class ------- */
class ge::Vk::PipelineCache {
// attributes
private:
	Info::PipelineCache* _description = nullptr;
	VkPipelineCache _cache = VK_NULL_HANDLE;

// methods
private:
	void init(CreateInfo::PipelineCache& createInfo);
	void deinit();
	//bool initCompleted() { return _cache != VK_NULL_HANDLE; }

public:
	/* Ctors 'n Dtor */
	PipelineCache() {}
	PipelineCache(CreateInfo::PipelineCache& createInfo) { init(createInfo); }
	~PipelineCache() { deinit(); }

	/* Operators */
	operator VkPipelineCache() { return _cache; }

	/* Getters */
	VkPipelineCache getVkCache() { return _cache; }
	
	/* Core Methods */
	void create(CreateInfo::PipelineCache& createInfo) { init(createInfo); }
	void destroy() { deinit(); }
	bool store(std::string path);


	/* Status Checkers */
	
};

#endif
