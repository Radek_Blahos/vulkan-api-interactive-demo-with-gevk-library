/**
* @author Radek Blahos
* @project geVk Library
*
* @brief
*/

#ifndef GEVKCOMMANDPROCESSOR_H
#define GEVKCOMMANDPROCESSOR_H

#include <geVk/geVk.h>
#include <geVk\Device.h>
#include <vector>
#include <unordered_map>

/**
* @author Radek Blahos
* @project geVk Library
*
* @brief 
	This class serves as interface between commandBufferSets or single cmdbuffers and device's queues
	User will via postCommands, pass command buffer which want to execute in queue and CmdProcessor object
	will find first suitable queue to post commands in (must have same queueFamilyIdx and optionaly should idle(so commands
	will be executed as soon as GPU allowes it, it will be upon user to define this behavior).

	Use notes:
	Should be one per application
*/

/* ------- Info Classes ------- */
class ge::Vk::Info::Methods::PostSingleBuffer {
// attributes
public:
	ge::Vk::CommandBuffer* pBuffer; // which cmd buffer to post
	// separate vectors, inside vk Submit info it is also described separately
	std::vector<VkSemaphore> waitOn;
	std::vector<VkPipelineStageFlags> insideStages;
	std::vector<VkSemaphore> signalUponFinish;

// methods
public:
	operator VkSubmitInfo();

};

class ge::Vk::Info::Methods::PostCommandBuffers {
// attributes
public:
	std::vector<PostSingleBuffer> bulk;

};

class ge::Vk::Info::CommandProcessor {
// attributes
public:
	ge::Vk::Device* pDevice; // ptr to parent device(the one which has compatible queues), ptr because user doesn't have to specify it in object's ctor calling

};

/* ------- CreateInfoClass ------- */
class ge::Vk::CreateInfo::CommandProcessor : public ge::Vk::Info::CommandProcessor {
// attributes
public:

};

/* ------- Core Class ------- */
class ge::Vk::CommandProcessor {
// attributes
private:
	Info::CommandProcessor* _description = nullptr;

// methods
private:
	void init(CreateInfo::CommandProcessor& createInfo);
	void deinit();
	//bool initCompleted() { return _description != nullptr; }

	VkQueue findQueueToSubmit(uint32_t queueFamilyIdx);

public:
	/* Ctors 'n Dtor */
	CommandProcessor() {}
	CommandProcessor(CreateInfo::CommandProcessor& createInfo) { init(createInfo); }
	~CommandProcessor() { deinit(); }

	/* Getters */


	/* Setters */

	/* Core Methods */
	void startUp(CreateInfo::CommandProcessor& createInfo) { init(createInfo); }
	void shutdown() { deinit(); }

	// post cmd buffer into queue and when it is processed, then return it (make it available) into CommandBufferSet
	bool postCommands(Info::Methods::PostSingleBuffer& buffer, Fence* signalUponFinish = nullptr);
	/* 
		unconditionally all commands must be created from same pool, otherwise use variant with single Info::CommandsExec
		singleQueueCommandPost posts all cmd buffers into single queue, multiQueueCommandPost redistribute cmd buffers over all available queues from QFamily
		if useFences:
			for every queue where commands will be posted is provided one fence from intern pool and that fence
			is than assigned to cmd buffer object which can check its status afterwards...
	*/

	// version without signaling fences per each queue's work done
	bool multiQueueCommandsPost(Info::Methods::PostCommandBuffers& buffers);
	// user must guarantee that pFenceVec.size() >= min(availableQueues[acrossAllFamilies].size(), buffers.size())
	bool multiQueueCommandsPost(Info::Methods::PostCommandBuffers& buffers, std::vector<Fence*>& pFenceVec);
	bool singleQueueCommandsPost(Info::Methods::PostCommandBuffers& buffers, Fence* signalUponFinish = nullptr);

	/* Operators */	

	/* Status Checkers */
	
};


#endif