/**
* @author Radek Blahos
* @project geVk Library
*
* @brief utility vertex buffer object
* Class pairing ge::Vk::Info::Pipe::VertexInputDescription with its ge::Vk::Buffer representation.
* Used by geVkScene class.
*/

#ifndef GEVKVERTEXINPUT_H
#define GEVKVERTEXINPUT_H

#include <geVk\Buffer.h>
#include <geVk\GPipelineComponents.inl>

class ge::Vk::Utils::VertexBuffer {
// attributes
public:
	Info::Pipe::VertexInputDescription* description = nullptr;
	ge::Vk::Buffer* data = nullptr;

// methods
public:
	VertexBuffer() {}
	VertexBuffer(ge::Vk::Buffer* Data, Info::Pipe::VertexInputDescription* Description) : data(Data), description(Description) {}
	~VertexBuffer();

};

#endif
