/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Shader module wrapper
*/

#ifndef GEVKSHADERMODULE_H
#define GEVKSHADERMODULE_H

#include <geVk/geVk.h>
#include <string>
#include <vector>

/* ----------- Info Class ----------- */
class ge::Vk::Info::ShaderModule {
	friend class ge::Vk::CreateInfo::ShaderModule;
	friend class ge::Vk::ShaderModule;

// attributes
private:
	std::vector<uint32_t> data;

public:
	ge::Vk::Device* pDevice;
	std::string path;

};

/* ----------- Create Info Class ----------- */
class ge::Vk::CreateInfo::ShaderModule : public ge::Vk::Info::ShaderModule {
// attributes
private:
public:

// methods
public:
	/* Operators */
	operator VkShaderModuleCreateInfo();

};

/* ------- Core Class ------- */
class ge::Vk::ShaderModule {
// attributes
private:
	Info::ShaderModule* _description = nullptr;
	VkShaderModule _shader = VK_NULL_HANDLE;

// methods
private:
	void init(CreateInfo::ShaderModule& createInfo);
	void deinit();
	//bool initCompleted() { return _shader != VK_NULL_HANDLE && _description != nullptr; }
	void move(ShaderModule& withdraw);

public:
	/* Ctors 'n Dtor */
	ShaderModule() {}
	ShaderModule(CreateInfo::ShaderModule& createInfo) { init(createInfo); }
	ShaderModule(ShaderModule& copy) { move(copy); }
	~ShaderModule() { deinit(); }

	/* Getters */
	VkShaderModule getVkShaderModule() { return _shader; }

	/* Core Methods */
	void create(CreateInfo::ShaderModule& createInfo) { init(createInfo); }
	void destroy() { deinit(); }
	
	/* Operators */
	operator VkShaderModule() { return _shader; }
	ShaderModule& operator=(ShaderModule& assign) { move(assign); return *this; }

	/* Status Checkers */

};

#endif