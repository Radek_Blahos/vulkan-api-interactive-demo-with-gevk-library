/**
* @author Radek Blahos
* @project geVk Library
*
* @brief MemoryPoll object
*/

#ifndef GEVKMEMORYPOOL_H
#define GEVKMEMORYPOOL_H

#include <geVk\geVk.h>
#include <geVk\MemoryManager.h>
#include <geVk\MemoryBlock.h>
#include <list>

/* ------- Info Class ------- */
class ge::Vk::Info::MemoryPool {
// attributes
public:
	//ge::Vk::Device* pDevice;
	VkDeviceSize allocatedSpace = 0; // how much memory will be allocated inside pool
	VkDeviceSize unitSize; // size of baseResource for which was pool originaly allocated
	uint32_t memoryTypeIndex;
	VkMemoryPropertyFlags propertyFlags;

};

class ge::Vk::CreateInfo::MemoryPool : public ge::Vk::Info::MemoryPool {
// attributes
public:

};

class ge::Vk::MemoryPool {
//friend classes
	friend class MemoryManager;

// attributes
private:
	ge::Vk::Info::MemoryPool* _description = nullptr;

	/* Memory Allocation Info */
	//std::list<AllocationInfo> _allocationMap; // VkDeviceSize Start, VkDeviceSize offset
	std::list<AllocationInfo> _allocationAbleMap; // free spaces inside memory

	VkDeviceMemory _memory = VK_NULL_HANDLE;
	uint32_t _allocationCount = 0; // used in deallocation

// methods
private:
public:
	/* Ctors 'n Dtor */
	MemoryPool() {}

	/* Getters */
	VkDeviceMemory getMemory() { return _memory; }
	VkMemoryPropertyFlags getFlags() { return _description->propertyFlags; }
	std::list<AllocationInfo>const& getFreeSpaceInfo() { return _allocationAbleMap; }
	ge::Vk::Info::MemoryPool const& getDescription() { return *_description; }
	uint32_t getAllocationCount() { return _allocationCount; }

	/* Core Methods */

	/* Status Checkers */
	bool initialized() { return _memory != VK_NULL_HANDLE; };
};

#endif
