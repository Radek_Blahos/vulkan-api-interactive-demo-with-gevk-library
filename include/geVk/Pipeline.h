/**
* @author Radek Blahos
* @project geVk Library
*
* @brief Pipeline abstract class
*/

#ifndef GEVKPIPE_H
#define GEVKPIPE_H

#include <geVk/geVk.h>

/* ------- Info Class ------- */
class ge::Vk::Info::Pipeline {
// attributes
public:
	// parent device
	ge::Vk::Device* pDevice;

	// used shader layout
	ge::Vk::PipelineLayout* layout;

	// flags
	VkPipelineCreateFlags flags = 0;

};

/* ------- CreateInfoClass ------- */
class ge::Vk::CreateInfo::Pipeline {
// attributes
public:
	// pipes derivations
	VkPipeline basePipelineHandle = VK_NULL_HANDLE;
	int32_t basePipelineIndex = -1;

};

/* ------- Core Class ------- */
class ge::Vk::Pipeline {
// attributes
public:
	VkPipeline _pipeline = VK_NULL_HANDLE;

// methods
public:
	/* Ctors 'n Dtor */

	/* Getters */
	VkPipeline getVkPipeline() { return _pipeline; }

	/* Core Methods */

	/* Operators */
	operator VkPipeline() { return _pipeline; }

};


#endif
