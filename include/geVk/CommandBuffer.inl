/**
* @author Radek Blahos
* @project geVk Library
*
* @brief inlines for command buffer.h
*/

#ifndef GEVKCOMMANDBUFFERINFO_INL
#define GEVKCOMMANDBUFFERINFO_INL

#include <geVk\CommandBufferInfo.h>
#include <geVk\CommandBuffer.h>
#include <geVk\RenderPass.h>
#include <geVk\Framebuffer.h>
#include <geVk\GraphicPipeline.h>
#include <geVk\ComputePipeline.h>
#include <geVk\Buffer.h>

using namespace ge::Vk;
using namespace std;

/* info inlines */
inline Info::Commands::BeginRenderPass::operator VkRenderPassBeginInfo() {
	return {
		VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
		nullptr,
		*pRenderPass,
		*pFramebuffer,
		renderArea,
		(uint32_t)clearColors.size(),
		clearColors.data()
	};
}

/* Commands */
inline void CommandBuffer::beginRenderPass(Info::Commands::BeginRenderPass& beginInfo) {
	VkRenderPassBeginInfo info = beginInfo;
	vkCmdBeginRenderPass(_buffer, &info, beginInfo.subpassContents);
}

inline void CommandBuffer::bindPipeline(Pipeline& pipeline, VkPipelineBindPoint bindPoint) {
	vkCmdBindPipeline(_buffer, bindPoint, pipeline); 
}

inline void CommandBuffer::bindGraphicPipeline(GraphicPipeline& pipeline) {
	vkCmdBindPipeline(_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
}

inline void CommandBuffer::bindComputePipeline(ComputePipeline& pipeline) {
	vkCmdBindPipeline(_buffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline);
}

inline void CommandBuffer::setViewport(Info::Commands::SetViewport& info) {
	VkViewport tmp = info;
	vkCmdSetViewport(_buffer, 0, 1, &tmp); 
}

inline void CommandBuffer::setScissor(Info::Commands::SetScissor& info) {
	VkRect2D tmp = info;
	vkCmdSetScissor(_buffer, 0, 1, &tmp); 
}

inline void CommandBuffer::bindVertexBuffers(Info::Commands::BindVertexBuffers& info) {
	vkCmdBindVertexBuffers(_buffer, info.firstBinding, info.pBuffers.size(), info.pBuffers.data(), info.offsets.data()); 
}

inline void ge::Vk::CommandBuffer::bindIndexBuffer(Info::Commands::BindIndexBuffer& info) {
	vkCmdBindIndexBuffer(_buffer, *info.pBuffer, info.offset, info.indexType);
}

inline void CommandBuffer::bindIndexBuffer(VkBuffer indexBuffer, VkDeviceSize offset, VkIndexType indexType) {
	vkCmdBindIndexBuffer(_buffer, indexBuffer, offset, indexType); 
}

inline void CommandBuffer::pushConstants(Info::Commands::PushConstants& info) {
	vkCmdPushConstants(_buffer, *info.pLayout, info.usedInStages, info.offset, info.size, info.pValues);
}

inline void CommandBuffer::setEvent(VkEvent event, VkPipelineStageFlags stageMask) {
	vkCmdSetEvent(_buffer, event, stageMask);
}

inline void CommandBuffer::resetEvent(VkEvent event, VkPipelineStageFlags stageMask) {
	vkCmdResetEvent(_buffer, event, stageMask);
}

inline void ge::Vk::CommandBuffer::drawIndexedIndirect(VkBuffer buffer, VkDeviceSize offset, uint32_t drawCount, uint32_t stride) {
	vkCmdDrawIndexedIndirect(_buffer, buffer, offset, drawCount, stride);
}

#endif
