#ifndef GEVKFLEXIBLEPIPE_H
#define GEVKFLEXIBLEPIPE_H

#include <geVk\geVk.h>

/* ------- Flexible Pipe ------- */
/*
Class obsahuje pipeline jak je definovana nahore, spolu s createInfo, ktere je modifikovatelne
tim umoznuje rychle upravit jemne detaily na pipeline podle potreby. Tj ma to stejnou funkcionalitu jako
pipeline v geVu. Proc? protoze ve Vulkanu musi byt vsechny pipeline predem popsane a jakmile se vytvori, nelze
je zpetne modifikovat, leda smazat. Diky tomu je tam prakticky nulovy driver overhead predem je vsechno definovano
a zustava konstantni behem renderingu. Toto chovani implementuje basic geVk Pipeline. Pro vetsi uzivatelsky komfort
ale zavadim tuhle class. Btw pozornemu ctenari neunikne ze v nasledujicim objektu budou za behu 2 pipecreateinfo
objekty ale! to je aby to splnovalo cil meho navrhu ze jeden objekt vzdycky bude popisovat soucasny stav pipeline
jak je sestavena na GPU a je read-only, kdezto Description lze modifikovat a na jejim zaklade lze Rebuildnout
geVkPipeline's.VkPipeline na GPU
*/
class ge::Vk::Utils::FlexiblePipe {
	// attributes
private:
	ge::Vk::Pipeline _pipe;

public:
	ge::Vk::CreateInfo::Pipeline* Description; // new()

											   // methods
public:
	// ctor
	FlexiblePipe(ge::Vk::CreateInfo::Pipeline& description);
	// dtor netreba kompilator vygeneruje volani dtoru atributu, ty se postaraji o vse

	/* Core Methods */
	inline bool SubmitChanges() { _pipe.rebuild(Description); } // na zaklade Description rebuildne _Pipe

};

/*
Implementace kde jak pipeline tak description jsou predany z venci a objekt se akorat stara o jejich propojeni a submit zmen
Atributy jsou uchovavany jako private, jelikoz z venci na ne pres objekt nema nikdo pravo sahat, uzivatel pokud chce provest zmeny
v description, necht tak ucini pres referencovany objekt a pak pouze postne zmeny.
*/
class ge::Vk::Utils::FlexiblePipeHolder {
	// attributes
private:
	ge::Vk::Pipeline* _Pipe;
	ge::Vk::CreateInfo::Pipeline* _description; // ptr

												// methods
public:
	// ctor
	FlexiblePipeHolder(ge::Vk::CreateInfo::Pipeline& description, ge::Vk::Pipeline& pipe) :
		_Pipe(&pipe), _description(&description) {};

	/* Core Methods */
	inline bool SubmitChanges() { _Pipe->Rebuild(_description); } // na zaklade Description rebuildne _Pipe

};

#endif