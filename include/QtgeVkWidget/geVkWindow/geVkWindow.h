/**
* @author Radek Blahos
* @project geVkWidget Library
*
* @brief
*/

#ifndef GEVKWINDOW_H
#define GEVKWINDOW_H

#include <qwindow.h>
#include <qwidget.h>
#include <string>
//#include <InputHandler.h>

class geVkWindow : public QWindow {
	Q_OBJECT

friend class MainWindow;

// attributes
private:
	//InputHandler* _inputHandler;

public:
	// renderer must have initialized instance before
	geVkWindow();
	geVkWindow(uint32_t width, uint32_t height, std::string name = "", QWindow* parent = nullptr);
	~geVkWindow();

	/* Core */
	

	/* Getters */
	QWidget* transformToWidget(QWidget* parent = nullptr) { return QWidget::createWindowContainer(this, parent); }

	/* Operators */

signals:
	void windowUpdated();
	void checkInput();

public slots:
	void updateWindow();
	void processInput() { emit checkInput(); }

};

#endif
