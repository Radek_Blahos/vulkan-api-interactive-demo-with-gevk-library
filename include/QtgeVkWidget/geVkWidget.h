/**
* @author Radek Blahos
* @project geVkWidget Library
*
* @brief
*/

#ifndef GEVKWIDGET_H
#define GEVKWIDGET_H

#include <QtgeVkWidget\geVkScene\geVkScene.h>
#include <qthread.h>
#include <qvulkaninstance.h>
#include <qboxlayout.h>
#include <qwidget.h>
#include <QtgeVkWidget\ConfigureLightWidget\ConfigureLightWidget.h>
#include <QtgeVkWidget/ConfigureRendererWidget/ConfigureRendererWidget.h>

class geVkWindow;
class QgeVkRenderer;
class InputHandler;
class Animations;

class geVkWidget : public QWidget {
	Q_OBJECT

// attributes
private:
	ge::Ax::PtrVector<geVkScene> _models;
	geVkScene* _scene;
	geVkWindow* _window;
	QgeVkRenderer* _renderer;
	QThread* _workerThread;
	QVulkanInstance* _qInstance;
	QVBoxLayout* _widgetLayout;
	LightWidget* _lightConfigurator;
	ConfigRendererWidget* _configureRendererWidget;
	int operateWith = -2; // -2 ... camera | -3 ... light -> this ids are provided by Qt's button group class
	bool _showLightBulb = true;
	Animations* _usedAnimations;

// methods
private:
	static void loadImages(ge::sg::Model& model, std::string& imageDir);

public:
	/* Object's Lifetime */
	geVkWidget(uint32_t width = 1366, uint32_t height = 720, std::string name = "geVk Demo", QWidget* parent = nullptr);
	~geVkWidget();

	/* Core */

	/* Events */
	void closeEvent(QCloseEvent* event) override;
	void resizeEvent(QResizeEvent* event) override;	

	/* Getters */
	QgeVkRenderer& getRenderer() { return *_renderer; }
	QWindow& getWindow();
	geVkScene& getScene() { return *_scene; }
	bool& getShowLightBulb() { return _showLightBulb; }

signals:
	void redrawRequest(geVkScene*);
	void resizeSwapchain(int, int);

private slots:
	void loadScene();
	void setUniformColor();
	void configureLight();
	void extractLightInfo();
	void continuousRedraw() { emit redrawRequest(_scene); }
	void setResourcesFolderToRenderer(QString path);
	//void processInput(); // waits for following input, to catch almost simultaneous emit of several keys or mouse events

public slots:
	//void renderScene(geVkScene* scene);
	void afterConfigInit();
	void rotateCamera(char direction); // l-eft, r-ight, u-p, d-own
	void moveCamera(char direction); // l, r, u, d, f-orward, b-ackward
	void setFOV(int value);
	void changeMovableObject(int id) { operateWith = id; }
	void runBenchmark(); 
	void playCameraAnimation();
	void playLightSourceAnimation();

};

#endif