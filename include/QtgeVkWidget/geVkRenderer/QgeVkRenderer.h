/**
* @author Radek Blahos
* @project geVkWidget Library
*
* @brief
*/

#ifndef QGEVKRENDERER_H
#define QGEVKRENDERER_H

#include <QtgeVkWidget\geVkRenderer\geVkRenderer.h>
#include <qobject.h>
#include <string>

/* Wrapper class around geVkRenderer, so Qt is able to use it inside its threads and signals and slots */

class QgeVkRenderer : public QObject, public geVkRenderer {
	Q_OBJECT

// attributes
private:

// methods
public:
	void benchmarkDraw(geVkScene* renderScene);

signals:
	void drawCompleted();

public slots:
	void renderNow(geVkScene* scene);
	void drawTriangle();
	void resizeSwapchain(int width, int height) { geVkRenderer::recreateSwapchain(width, height); }

};

#endif