/**
* @author Radek Blahos
* @project geVkWidget Library
*
* @brief
*/

#ifndef GEVKRENDERER_H
#define GEVKRENDERER_H

#include <geAx\PtrVector.h>
#include <geVk\RenderContext.h>
#include <geVk\CommandBuffer.inl>
#include <geVk\UniformBuffer.inl>
#include <QtgeVkWidget\geVkScene\geVkScene.h>
#include <QtgeVkWidget\geVkRenderer\FencePool.h>
#include <geUtil/MatrixStack.h>
#include <string>
#include <glm/mat4x4.hpp>
#include <glm/mat3x3.hpp>
#include <string>

/* Auxiliary */
class RecordDrawCommandsCore {
// attributes
public:
	ge::Vk::Info::Commands::BeginRenderPass renderPassInfo;
	ge::Vk::Info::Commands::BindVertexBuffers vertexBuffersInfo;
	ge::Vk::Info::Commands::BindDescriptorSets descriptorSetsInfo;
	ge::Vk::Info::Commands::PushConstants pushConstants;
	ge::Vk::GraphicPipeline* pGraphicPipeline;
	uint32_t renderWidth;
	uint32_t renderHeight;

	bool useEvent = false;
};
 
class RecordDrawCommands : public RecordDrawCommandsCore {
// attributes
public:
	ge::Vk::Info::Commands::Draw drawInfo;
	
// methods
public:


};

class RecordIndexedDrawCommands : public RecordDrawCommandsCore {
// attributes
public:
	ge::Vk::Info::Commands::BindIndexBuffer indexBufferInfo;
	ge::Vk::Info::Commands::DrawIndexed drawInfo;

};

class RecordIndexedIndirectDrawCommands : public RecordDrawCommandsCore {
// attributes
public:
	ge::Vk::Info::Commands::BindIndexBuffer indexBufferInfo;
	ge::Vk::Info::Commands::DrawIndexedIndirect drawInfo;

};

struct VertexShaderBuffer {
	glm::mat4 viewProjection;
	glm::mat4 worldSpaceTr;
	glm::mat3 normalsFix;

	VertexShaderBuffer() : viewProjection(glm::mat4(1.0)), worldSpaceTr(glm::mat4(1.0)), normalsFix(glm::mat3(1.0)) {}
};

struct FragmentShaderBuffer {
	glm::vec3 color;
	glm::vec3 position;
	glm::vec3 direction;
	float cutOff;
	glm::vec3 eyePos;

	//FragmentShaderBuffer() { memset(this, 0, sizeof(VertexShaderBuffer)); }
};

struct PushConstants {
	uint32_t lightSourceType;
	uint32_t lightingMode;
	glm::vec3 uniformColor;
	//bool isLightSource;
};

/* Core */
class geVkRenderer : public ge::Vk::Utils::RenderContext {
// derived objects
	struct DrawWorkerInfo {
		geVkRenderer* thisPtr;
		uint32_t dispatchIdx;
		geVkMesh* mesh;
		ge::sg::Mesh* sgMesh;
		geVkScene* scene; // used for model transform computations
	};

public:
	struct InitInfo {
		uint32_t width;
		uint32_t height;
		uint32_t drawThreadsCount = 3;
		uint32_t useQueuesCount = 1;
		int useRoutineVersion = false;
		bool fenceAsSyncPrimitive = true;
	};

// attributes
protected:
	// projection matrix
	glm::mat4 projection;
	float currentFOV = 90;

	uint32_t _drawingThreadsCount = 1;
	FencePool drawCallFences;
	// uniformbuffers for shaders one per running drawThread
	std::vector<ge::Vk::Utils::UniformBuffer<VertexShaderBuffer>> vsUniforms; 
	std::vector<ge::Vk::Utils::UniformBuffer<FragmentShaderBuffer>> fsUniforms;
	// must have exclusive access
	PushConstants _pushConstants; // for nontextured object, can be defined from qt gui
	// indirect draw buffer
	ge::Vk::Utils::UniformBuffer<VkDrawIndexedIndirectCommand> indirectDrawStorage;

	// state attributes
	int _useRoutineVersion;
	bool _useFenceAsSyncPrimitive; // if false event is used
	bool _previousMeshWasTextured; // used in traversing SceneGraph for proper cmd buffer usage decision

public:
	std::string* resourcesFolder = nullptr;

// methods
private:
	void initDevice(uint32_t useQueues = 1);
	void createSwapchain(uint32_t width, uint32_t height);
	void initMemoryManager();
	void createUniforms();
	void buildPipelineLayouts();
	void buildShaders();
	void buildDescriptorSets();
	void buildRenderPasses();
	void buildCommandBufferSets();
	void buildSamplers();
	void initRenderComponents();
	void fillPipelineDBCreateInfo(ge::Vk::CreateInfo::PipelineDatabase& createInfo);
	void texturePipeInfo(CreateInfo::GraphicPipeline& fill);
	void nonTexturePipeInfo(CreateInfo::GraphicPipeline& fill);
	void debugPipeInfo(CreateInfo::GraphicPipeline& fill);
	void buildPipelineDB(); // pipelines must be congruent with models inside geVkScene, user programmer responsibility...
	void swapchainImagesLayoutTransfer();
	void buildSyncComponents();
	static void updateDescriptors(DrawWorkerInfo* info);
	void updateDescriptors(geVkScene* scene, ge::sg::Mesh* sgMesh, uint32_t setIdx);

	/* 
		fills submit info acording to mesh position inside scene, 
		if s -> single mesh should be drawn, 
		   l -> last mesh should be drawn,
		   r -> meshes between...
		   f -> first mesh 
	*/
	void fillSubmitInfo(ge::Vk::Info::Methods::PostSingleBuffer& info, char meshPosition);
	void recordCoreCommands(ge::Vk::CommandBuffer& keepCommands, RecordDrawCommandsCore& info);
	void recordDrawCommands(ge::Vk::CommandBuffer& keepCommands, RecordDrawCommands& info);
	void recordIndexedDrawCommands(ge::Vk::CommandBuffer& keepCommands, RecordIndexedDrawCommands& info);
	void recordIndexedIndirectDrawCommands(ge::Vk::CommandBuffer& keepCommands, RecordIndexedIndirectDrawCommands& info);
	void meshSGRoutine1(ge::sg::Mesh* mesh, geVkScene& renderScene, uint32_t currentImageIdx);
	void meshSGRoutine2(ge::sg::Mesh* mesh, geVkScene& renderScene, uint32_t currentImageIdx);

public:
	geVkRenderer() { initInstance(); }
	~geVkRenderer();

	void initInstance(); // must be initialized first, because QWindow
	// initialize device, memManager and commandProcessor with sets
	void initSwapchain(uint32_t width, uint32_t height);
	void initialize(InitInfo& info); // geVk RenderContext override method
	void recreateSwapchain(uint32_t width, uint32_t height); // must specify renderpass for recreating framebuffers
	void recordCommandBuffers(geVkScene* renderScene);

	virtual void draw(geVkScene* renderScene); // here I present some function code to do basic randering, user can override it
	static void drawWorker(DrawWorkerInfo* info);
	void mtDraw(geVkScene* renderScene);
	void sgDraw(ge::sg::MatrixTransformNode* currentNode, geVkScene& renderScene, uint32_t currentImageIdx, ge::util::MatrixStack& stack);
	void updateDescriptorsSG();

	// auxiliary draw call, used for debuging purpouse
	void debugDraw();

	/* Utility */
	static void checkGPUFeatures(std::vector<VkPhysicalDevice>& hostGPUs);

	PushConstants& getPushConstants() { return _pushConstants; }

	float& getFOV() { return currentFOV; }
	// set current view projection 
	glm::mat4& getProjectionMatrix() { return projection; }
	int useRoutineVersion() { return _useRoutineVersion; }

};

#endif // GEVKWIDGET_H
