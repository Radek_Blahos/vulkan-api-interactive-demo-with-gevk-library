/**
* @author Radek Blahos
* @project geVkWidget Library
*
* @brief
*/

#ifndef GEVKFENCEPOOL_H
#define GEVKFENCEPOOL_H

#include <geVk\Fence.h>
#include <geAx\PtrVector.inl>
#include <geAx\UsageRegister.h>

class FencePool {
// attributes
private:
	ge::Ax::PtrVector<ge::Vk::Fence> _fences;
	ge::Ax::UsageRegister _fenceUsage;

// methods
private:
	int getNextFenceIdx(); 

public:
	FencePool() {}
	FencePool(uint32_t fenceCount) : _fenceUsage(fenceCount) { _fences.resize(fenceCount); }
	void addFences(ge::Vk::CreateInfo::Fence& info, uint32_t fenceCount);

	ge::Vk::Fence& getFence(unsigned idx) { return *_fences[idx]; }
	ge::Vk::Fence& getNextFence();
	ge::Vk::Fence& getCurrentFence() { return *_fences[_fenceUsage.lastAssigned()]; }
	int getCurrentFenceIdx() { return _fenceUsage.lastAssigned(); }
	void returnFence(uint32_t idx) { _fenceUsage.unsetIndex(idx); }
	void returnCurrentFence() { _fenceUsage.unsetIndex(_fenceUsage.lastAssigned()); } // mark fence on idx as unused
	void usageRegisterReset() { _fenceUsage.unsetAllIndexes(); }
	void clear() { _fences.clear(); } // need to be called before dtoring device 

};

#endif
