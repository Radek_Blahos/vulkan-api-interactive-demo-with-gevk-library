/**
* @author Radek Blahos
* @project geVkWidget Library
*
* @brief
*/

#ifndef LIGHTWIDGET_H
#define LIGHTWIDGET_H

#include <qlabel.h>
#include <qcheckbox.h>
#include <qgridlayout.h>
#include <QHBoxLayout>
#include <qvector3d.h>
#include <qspinbox.h>
#include <qcombobox.h>
#include <vector>
#include <qpushbutton.h>
#include <qgroupbox.h>

class LightWidget : public QWidget {
	Q_OBJECT

// attributes
private:
	QGridLayout* _mainLayout;
	QColor _choosedColor = Qt::white; 
	std::vector<QPushButton*> _buttons;
	std::vector<QGroupBox*> _groupBoxes;
	std::vector<QSpinBox*> _spinBoxes;
	std::vector<QComboBox*> _comboBoxes; 
	std::vector<QLabel*> _labels;

// methods
public:
	LightWidget();
	~LightWidget();

	void closeEvent(QCloseEvent* event) override;

	/* setters */
	void setPosition(float x, float y, float z);

	/* getters */
	QColor& getColor() { return _choosedColor; }
	int getDirection() { return _spinBoxes[4]->value(); }
	QVector3D getPosition();
	float getCutoff() { return _spinBoxes[3]->value(); } 
	uint32_t getSourceType();
	uint32_t getLightingMode();

signals:
	void configurationEnded();

public slots:
	void chooseColorRoutine();
	void okBtnClicked();
	void closeBtnClicked();

};

#endif