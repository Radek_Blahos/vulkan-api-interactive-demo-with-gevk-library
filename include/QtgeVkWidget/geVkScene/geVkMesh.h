/**
* @author Radek Blahos
* @project geVkWidget Library
*
* @brief
*/

#ifndef GEADVERTEXINPUT_H
#define GEADVERTEXINPUT_H

#include <geVk\VertexBuffer.h>
#include <geVk\GraphicPipeline.h>
#include <geSG\Material.h>
#include <geSG\AttributeDescriptor.h>
#include <unordered_map>

class geVkMesh {
// attributes
public:
	std::unordered_map<ge::sg::AttributeDescriptor::Semantic, ge::Vk::Utils::VertexBuffer*> buffers;
	ge::Vk::GraphicPipeline* usedPipeline = nullptr; // ptr to assigned graphic pipeline
	std::unordered_map<ge::sg::MaterialImageComponent::Semantic, ge::Vk::Image*> usedTextures; // map texture type(diffuse, ambient...) to geVkImage Object.
	uint32_t offset; // used in draw routine, every same mesh attribut are baked into same buffer object and via indexed buffer they're during draw referenced

// methods
public:
	geVkMesh() {}
	~geVkMesh();

};

#endif