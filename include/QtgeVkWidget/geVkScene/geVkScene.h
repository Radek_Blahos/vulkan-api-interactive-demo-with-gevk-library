/**
* @author Radek Blahos
* @project geVkWidget Library
*
* @brief Maps geSG Scene Meshes and Images into geVk provided structures.
* This class provides the link between implementation independent scene graph
* and Vulkan.
* Meshes are mapped to a collection of geVkBuffers and MaterialImageComponents
* are mapped to geVkImages.
*/

#ifndef GEADSCENE_H
#define GEADSCENE_H

#include <QtgeVkWidget\geVkScene\geVkMesh.h>
#include <QtgeVkWidget\geVkScene\Camera.h>
#include <geVk\Image.h>
#include <geSG\Image.h>
#include <AssimpModelLoader.h>
#include <string>

struct SceneLight {
	glm::vec3 color;
	glm::vec3 position;
	glm::vec3 direction;
	float cutOff;
	uint32_t sourceType; // DIRECTIONAL = 0, POINT = 1, SPOTLIGHT = 2
	uint32_t lightingMode; // NONE = 0, PHONG = 1, PHONG_BLINN = 2
	ge::sg::Model* usedModel = nullptr;
};

struct geVkSceneCreateInfo {
	// these object will be needed for Vulkan's ge::sg::scene representation 
	ge::Vk::MemoryManager* pManager = nullptr;
	ge::Vk::CommandProcessor* pCmdProcessor = nullptr;
	ge::Vk::CommandBufferSet* pCmdSet = nullptr;
	// image load function ptr
	void(*loadImages)(ge::sg::Model& model, std::string& directoryPath) = nullptr;

};

class geVkScene {
	struct attribInfo {
		uint32_t allocSize = 0;
		ge::Vk::Utils::VertexBuffer* vertexBuffPtr = nullptr;
	};

// attributes
private:
	geVkSceneCreateInfo* _info = nullptr; // need to keep ptrs to some geVk Objects
	// used with SG
	bool _useUniformLoad = false;

public:
	// camera
	Camera sceneCamera;

	// scene's light
	SceneLight sceneLight; 

	ge::sg::Scene* activeScene = nullptr; // heap alloc, obtained from AssimpModelLoader::loadScene...
	std::unordered_map<ge::sg::Mesh*, geVkMesh> gpuMeshes;
	std::unordered_map<std::string, ge::Vk::Image> texStorage; // aby se stejne textury nenacitali viceX
	std::unordered_map<ge::sg::MaterialImageComponent*, std::string> gpuTextures;
	std::unordered_map<ge::sg::AttributeDescriptor::Semantic, attribInfo> attribBufferSize;


// methods
private:
	ge::Vk::Info::Pipe::VertexInputDescription convertFrom(ge::sg::AttributeDescriptor& attributeInfo);
	static size_t assignFormat(ge::sg::Image::Format sgImageFormat, VkFormat& outVkformat);

public:
	geVkScene() {};
	geVkScene(geVkSceneCreateInfo& createInfo) { init(createInfo); }
	~geVkScene();

	/* Core */
	void init(geVkSceneCreateInfo& createInfo) { _info = new geVkSceneCreateInfo(createInfo); }

	void processScene(std::string path, bool useUniformLoad = false); // delete previous scene if exists and replace it with defined one
	void addModelToScene(std::string path);
	
	void processMeshes(std::vector<ge::sg::AttributeDescriptor::Semantic>& loadAttributes, bool useUniformLoad = false);
	void processMeshes(ge::sg::Model& model, std::vector<ge::sg::AttributeDescriptor::Semantic>& loadAttributes);
	void processMeshesUniformModels(ge::sg::Model& model, std::vector<ge::sg::AttributeDescriptor::Semantic>& loadAttributes, uint32_t& currentIndicesOffset, uint32_t& currentVertexOffset);
	// typeflags defines which textures to load
	void storeTextures(std::vector<ge::sg::MaterialImageComponent::Semantic>& loadTextures); 
	void storeTextures(ge::sg::Model& model, std::vector<ge::sg::MaterialImageComponent::Semantic>& loadTextures);
	// to every geVkMesh assign texture references which belongs to it...
	void distributeTextureRefs();
	void distributeTextureRefs(std::unordered_map<ge::sg::Mesh*, geVkMesh>::iterator& fromMesh);	

};

#endif