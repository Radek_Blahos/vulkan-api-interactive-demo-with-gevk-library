/**
* @author Radek Blahos
* @project geVkWidget Library
*
* @brief
*/

#ifndef USEDANIMATIONS_H
#define USEDANIMATIONS_H

#include <QtgeVkWidget\geVkWidget.h>
#include <geSG/Animation.h>
#include <geSG/AnimationChannel.h>
#include <geSG/AnimationManager.h>
#include <memory>

template <class T>
class AnimationChannel : public ge::sg::AnimationChannel {
// attribute
private:
	std::shared_ptr<T> _target;

public:
	using KeyFrame = ge::sg::AnimationKeyFrameTemplate<T>;

	std::vector<KeyFrame> KF;
	std::unique_ptr<ge::sg::KeyframeInterpolator<std::vector<KeyFrame>>> Interpolator;

// methods
public:	
	AnimationChannel();
	~AnimationChannel() override = default;

	void update(const ge::core::time_point &t) override;
	inline void setTarget(std::shared_ptr<T> &target) { _target = target; }
	inline std::shared_ptr<T> &getTarget() { return _target; }

	ge::core::time_unit getDuration() const override;

};

template<class T>
AnimationChannel<T>::AnimationChannel() : Interpolator(new ge::sg::LinearKeyframeInterpolator<std::vector<KeyFrame>>()) {}

template<class T>
ge::core::time_unit AnimationChannel<T>::getDuration() const {
	ge::core::time_point end = KF.empty() ? ge::core::time_point() : KF.back().getTime();
	return std::chrono::duration_cast<ge::core::time_unit>(end.time_since_epoch());
}

template<class T>
void AnimationChannel<T>::update(const ge::core::time_point &t) {
	T p = Interpolator->interpolate(KF, ge::core::TPtoFP(t));

	if (_target) 
		*_target = p;
}

class Animations {
	// attributes
private:

public:
	std::shared_ptr<ge::sg::Animation> animation;
	std::shared_ptr<AnimationChannel<float>> cameraMoveCh; 
	std::unique_ptr<ge::sg::AnimationManager> manager;

	// methods
public:
	Animations();
	~Animations() {}

	/* setters */

	/* getters */
};

#endif