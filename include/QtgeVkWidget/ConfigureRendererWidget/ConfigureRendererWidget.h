/**
* @author Radek Blahos
* @project geVkWidget Library
*
* @brief
*/

#ifndef RENDERCONFIGWIDGET_H
#define RENDERCONFIGWIDGET_H

#include <qlabel.h>
#include <qcheckbox.h>
#include <qgridlayout.h>
#include <QHBoxLayout>
#include <qvector3d.h>
#include <qspinbox.h>
#include <qcombobox.h>
#include <vector>
#include <qpushbutton.h>
#include <qgroupbox.h>
#include <qdialog.h>
#include <qbuttongroup.h>
#include <qstring.h>

class ConfigRendererWidget : public QDialog {
	Q_OBJECT

// attributes
private:
	QGridLayout* _mainLayout;
	std::vector<QPushButton*> _buttons;
	std::vector<QGroupBox*> _groupBoxes;
	std::vector<QLabel*> _labels;
	std::vector<QSpinBox*> _spinBoxes; 
	std::vector<QCheckBox*> _checkboxes;
	std::vector<QButtonGroup*> _bg;

// methods
public:
	ConfigRendererWidget();
	~ConfigRendererWidget();

	void closeEvent(QCloseEvent* event) override;

	/* Getters */
	int getReqQueuesCount() { return _spinBoxes[0]->value(); }
	int getReqThreadsCount() { return _spinBoxes[1]->value(); }
	int getChoosedRoutine() { return _bg[1]->checkedId(); } // if id != -2 Mt -3 preinit -4 versatile
	int getReqSyncPrimitive() { return _bg[0]->checkedId() != -2; } // if id != -2 -> makes fence default, if id eq -3 event is used

signals:
	void configurationEnded();
	void dialogReject();
	void resourceFolderChoosed(QString);

public slots:
	void okbtnClicked();
	void closebtnClicked();
	void chooseResourcesSource();

};

#endif