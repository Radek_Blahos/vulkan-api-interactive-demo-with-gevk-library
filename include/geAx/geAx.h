/**
* @author Radek Blahos
* @project geAx Library
*
* @brief Forward declaration of geAx 
*/

#ifndef GEAX_H
#define GEAX_H

#include <vector>
#include <string>
#include <memory>
#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

namespace ge {
	namespace Ax {
		class Window;
		class UsageRegister;
		template<typename T>
		using PtrVector = std::vector<std::shared_ptr<T>>;

		template<typename T>
		inline T min(T a, T b);

		template<typename T>
		inline T max(T a, T b);

		template<typename T>
		inline T floor(T number);

		template<typename T>
		inline T ceil(T number);

		template<typename T>
		inline T round(T number);

		template<typename T>
		inline bool in(T val, T* array, int arr_size);
		template<>
		bool in<char*>(char* val, char** array, int arr_size);
		template<>
		bool in<std::string>(std::string val, std::string* array, int arr_size);
	
		namespace File {
			template<typename T>
			bool read(std::string filepath, std::vector<T>& storage, bool binaryRead = true);
			bool read(std::string filepath, uint8_t* storage, size_t size, bool binaryRead = true);
			bool write(std::string filepath, uint8_t* storage, size_t size, bool binaryWrite = true);
			size_t size(std::string filepath);
			size_t size(std::fstream file);
			bool create(std::string name);
		}
		namespace Exception {
			class Base; // base exepction
			namespace Bad {
				class Allocation; // Heap Alloc
				class Creation; // crating ge(Vk) objects -> althought it includes heap alloc as well...
				class Load; // loading from file
				class Select;
				class Execution;
			}
			//class  
		}
		void sleep(uint32_t miliseconds);
	}
}

inline void ge::Ax::sleep(uint32_t miliseconds) {
#ifdef _WIN32
	Sleep(miliseconds);
#else
	usleep(miliseconds * 1000);
#endif
}

#endif