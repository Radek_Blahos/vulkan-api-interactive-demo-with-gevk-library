/**
* @author Radek Blahos
* @project geAx Library
*
* @brief Usage Register class
*/

#ifndef GEVKUSAGE_REGISTER_H
#define GEVKUSAGE_REGISTER_H

#include <geAx\geAx.h>
#include <iostream>
#include <vector>
#include <set>
#include <mutex>

class ge::Ax::UsageRegister {
private:
	std::vector<uint32_t> _bitField;
	uint32_t _indexSize; // count of available resource indexes -> size of bitfield
	uint32_t _activeCount = 0;
	int _searchStoppedIdx = -1;

public:
	UsageRegister() {}
	UsageRegister(uint32_t indexCount) { init(indexCount); }

	// Object's lifetime
	void init(uint32_t indexCount);
	void resize(uint32_t indexCount); // keeps current register state
	void reset() { _bitField.clear(); _indexSize = 0; _activeCount = 0; _searchStoppedIdx = -1; }

	// Status queries
	uint32_t countCurrentlyActive() { return _activeCount; }
	void currentlyActive(std::set<uint32_t>& activeIdxs);
	uint32_t lastAssigned() { return _searchStoppedIdx; }
	bool isFull() { return _activeCount >= _indexSize; }
	bool allActive(unsigned from, unsigned upTo);
	bool allFree(unsigned from, unsigned upTo);
	bool isActive(unsigned index);
	bool isFree(unsigned index);

	// Index Getters
	int32_t getNextActiveIndex();
	int32_t getNextFreeIndex(); 
	
	// Setters
	void setIndex(uint32_t index);
	void unsetIndex(uint32_t index);
	void setIndexes(std::set<uint32_t>& indexes);
	void unsetIndexes(std::set<uint32_t>& indexes);
	void unsetAllIndexes(); // set bitfiled to 0
	void setSearchPosition(uint32_t index) { if (index < _indexSize) _searchStoppedIdx = index; }

};

#endif
