/**
* @author Radek Blahos
* @project geAx Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#ifndef GEAXINLINES_H
#define GEAXINLINES_H

#include <geAx\geAx.h>

template<typename T>
T ge::Ax::min(T a, T b) {
	return (a < b) ? a : b;
}

template<typename T>
T ge::Ax::max(T a, T b) {
	return (a > b) ? a : b;
}

template<typename T>
T ge::Ax::floor(T number) {
	return T(int(number));
}

template<typename T>
T ge::Ax::ceil(T number) {
	if (number == int(number)) 
		return T(number);

	return T(int(number + 1));
}

template<typename T>
T ge::Ax::round(T number) {
	return T(int(number + 0.5));
}

template<typename T>
bool ge::Ax::in(T val, T* array, int arr_size) {
	for (int i = 0; i < arr_size; i++) {
		if (val == array[i])
			return true;
	}

	return false;
}

#endif
