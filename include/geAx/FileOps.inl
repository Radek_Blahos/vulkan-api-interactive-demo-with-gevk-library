/**
* @author Radek Blahos
* @project geAx Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#ifndef GEAXFILEOPS_H
#define GEAXFILEOPS_H

#include <geAx\geAx.h>
#include <fstream>

using namespace std;

template<typename T>
bool ge::Ax::File::read(std::string filepath, std::vector<T>& storage, bool binaryRead) {
	ios_base::openmode openHow = (binaryRead) ? (ios_base::in | ios_base::binary) : ios_base::in;
	fstream file(filepath, openHow);
	if (!file.is_open())
		return false; // file doesn't exist

	file.seekg(0, fstream::end);
	unsigned size = file.tellg();
	file.seekg(0, fstream::beg);

	storage.clear();
	storage.resize(size / sizeof(T)); 

	file.read((char*)storage.data(), size);

	if (file.good()) {
		file.close();
		return true;
	}

	storage.clear();
	file.close();
	return false;
}

#endif
