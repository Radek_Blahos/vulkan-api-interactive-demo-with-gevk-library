#ifndef GEVXWINDOW_H
#define GEVXWINDOW_H

#include <geAx\geAx.h>
#include <vulkan\vulkan.h>
#include <string>

#ifdef _WIN32 //VK_USE_PLATFORM_WIN32_KHR
#include <Windows.h>

#elif defined VK_USE_PLATFORM_XLIB_KHR	
#include <X11/Xlib.h>

#elif defined VK_USE_PLATFORM_XCB_KHR	
#include <xcb/xcb.h>

#endif

class ge::Ax::Window {
// attributes
public:
#ifdef _WIN32
	HINSTANCE hInstance;
	HWND hWnd;

#elif defined VK_USE_PLATFORM_XLIB_KHR	
	Display* dpy;
	Window window;

#elif defined VK_USE_PLATFORM_XCB_KHR	
	xcb_connection_t* connection;
	xcb_window_t window;

#endif // VK_USE_PLATFORM_WIN32_KHR

// methods
private:


public:
	Window(std::string name = "Vulkan Demo", uint32_t width = 1366, uint32_t height = 720);
	~Window();

};

#endif
