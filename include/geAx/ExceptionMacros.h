/**
* @author Radek Blahos
* @project geAx Library
*
* @brief excepction macros
*/

#ifndef GEAXEXCEPTIONMACROS_H
#define GEAXEXCEPTIONMACROS_H

#include <geAx\Exception.h>
#include <iostream>

#define CATCHEXIT(returnCode) \
		std::cout << "press any key to exit..."; \
		std::cin.get(); \
		exit(-1) // vypustim ; kvuli zvyku ho stejne za to makro v kodu napsat...

#endif
