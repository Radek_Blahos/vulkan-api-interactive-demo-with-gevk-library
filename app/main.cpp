/**
* @author Radek Blahos
* @project geVkDemo Application
*
* @brief Main entry point for whole application
*/

#include <include\MainWindow.h>
#include <qapplication.h>

int main(int argc, char *argv[]) {
	QApplication app(argc, argv);
	MainWindow demo(1366, 720);
	demo.show();
    return app.exec();
}
