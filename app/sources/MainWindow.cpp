/**
* @author Radek Blahos
* @project geVkDemo Application
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <include\MainWindow.h>
#include <QtgeVkWidget\geVkRenderer\QgeVkRenderer.h>
#include <qwindow.h>
#include <qtoolbar.h>
#include <qmenubar.h>
#include <qtimer.h>

MainWindow::MainWindow(uint32_t width, uint32_t height, std::string name) {
	// resize widget to desired size
	resize(width, height);

	// set central widget to geVk one
	_geVkWidget = new geVkWidget(width, height, name);
	setCentralWidget(_geVkWidget);

	// menubar creation
	_menu = new DemoMenuBar();
	setMenuBar(_menu); // set it to the qmainwindow
	
	// toolbar creation
	_toolbar = new DemoToolbar("Actions");
	addToolBar(_toolbar);

	//--- Connect Signals n Slots --- //
	connect(_menu->loadSceneBtn, SIGNAL(triggered()), _geVkWidget, SLOT(loadScene()));
	connect(_menu->setUniformColorBtn, SIGNAL(triggered()), _geVkWidget, SLOT(setUniformColor()));
	connect(_menu->configureLightBtn, SIGNAL(triggered()), _geVkWidget, SLOT(configureLight()));
	connect(_menu->runBenchmarkBtn, SIGNAL(triggered()), _geVkWidget, SLOT(runBenchmark()));
	connect(_menu->othersMenu, SIGNAL(triggered(QAction*)), this, SLOT(othersMenuActions(QAction*)));
	// camera movement
	connect(_toolbar, SIGNAL(actionTriggered(QAction*)), this, SLOT(toolbarActions(QAction*)));
	connect(_toolbar->fov, SIGNAL(valueChanged(int)), _geVkWidget, SLOT(setFOV(int)));
	connect(_toolbar->bg, SIGNAL(buttonClicked(int)), _geVkWidget, SLOT(changeMovableObject(int)));

}

MainWindow::~MainWindow() {
	delete _toolbar;
}

void MainWindow::closeEvent(QCloseEvent* event) {
	_geVkWidget->closeEvent(event);
	QMainWindow::closeEvent(event);
}

void MainWindow::resizeEvent(QResizeEvent* event) {
	QMainWindow::resizeEvent(event);
	_geVkWidget->resizeEvent(event);
}

void MainWindow::toolbarActions(QAction* action) {
	// rotate 
	if (action == _toolbar->actions[0]) {
		_geVkWidget->rotateCamera('u');
	}
	else if (action == _toolbar->actions[1]) {
		_geVkWidget->rotateCamera('d');
	}
	else if (action == _toolbar->actions[2]) {
		_geVkWidget->rotateCamera('l');
	}
	else if (action == _toolbar->actions[3]) {
		_geVkWidget->rotateCamera('r');
	}
	// move
	else if (action == _toolbar->actions[4]) {
		_geVkWidget->moveCamera('u');
	}
	else if (action == _toolbar->actions[5]) {
		_geVkWidget->moveCamera('d');
	}
	else if (action == _toolbar->actions[6]) {
		_geVkWidget->moveCamera('l');
	}
	else if (action == _toolbar->actions[7]) {
		_geVkWidget->moveCamera('r');
	}
	else if (action == _toolbar->actions[8]) {
		_geVkWidget->moveCamera('f');
	}
	else if (action == _toolbar->actions[9]) {
		_geVkWidget->moveCamera('b');
	}
}

void MainWindow::othersMenuActions(QAction* action) {
	if (action == _menu->qActions[0])
		_geVkWidget->getRenderer().drawTriangle();
		
	// animations
	else if (action == _menu->qActions[1])
		_geVkWidget->playCameraAnimation();
	
	else if (action == _menu->qActions[2])
		_geVkWidget->playLightSourceAnimation();
}