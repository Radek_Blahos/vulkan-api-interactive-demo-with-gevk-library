/**
* @author Radek Blahos
* @project geVkDemo Application
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <include\ToolBar.h>
#include <qlabel.h>
#include <qcheckbox.h>

void DemoToolbar::init() {
	actions.resize(11);
	QLabel* tmp = new QLabel("Fov:");
	addWidget(tmp);
	fov = new QSpinBox();
	fov->setMaximum(160);
	fov->setMinimum(60);
	QChar degreeChar(0260);
	fov->setSuffix(degreeChar);
	actions[10] = addWidget(fov);
	addSeparator();
	// choose what to move light|camera ?
	bg = new QButtonGroup();
	bg->setExclusive(true);
	QCheckBox* camCB = new QCheckBox("Camera");
	camCB->setChecked(true);
	QCheckBox* lightCB = new QCheckBox("Light");
	bg->addButton(camCB);
	bg->addButton(lightCB);
	addWidget(camCB);
	addWidget(lightCB);
	addSeparator();
	tmp = new QLabel("Rotation:");
	addWidget(tmp);
	actions[0] = addAction("Up");
	actions[1] = addAction("Down");
	actions[2] = addAction("Left");
	actions[3] = addAction("Right");
	addSeparator();
	tmp = new QLabel("Movement:");
	addWidget(tmp);
	actions[4] = addAction("Up");
	actions[5] = addAction("Down");
	actions[6] = addAction("Left");
	actions[7] = addAction("Right");
	actions[8] = addAction("Forward");
	actions[9] = addAction("Backward");
	addSeparator();
}