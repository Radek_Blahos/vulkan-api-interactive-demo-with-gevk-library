/**
* @author Radek Blahos
* @project geVkDemo Application
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <include\MenuBar.h>

DemoMenuBar::DemoMenuBar(QWidget* parent) : QMenuBar(parent) {
	qActions.resize(3);

	// setup menu bar actions
	loadSceneBtn = addAction("Load scene");
	configureLightBtn = addAction("Configure light");
	runBenchmarkBtn = addAction("Run benchmark");
	
	// others menu handling
	othersMenu = addMenu("Others");
	setUniformColorBtn = othersMenu->addAction("Set uniform color");
	qActions[0] = othersMenu->addAction("Triangle Demo");
	qActions[1] = othersMenu->addAction("Play camera animation");
	qActions[2] = othersMenu->addAction("Play light source animation");

}