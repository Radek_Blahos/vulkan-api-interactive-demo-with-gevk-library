/**
* @author Radek Blahos
* @project geVkDemo Application
*
* @brief
*/

#ifndef DEMOTOOLBAR_H
#define DEMOTOOLBAR_H

#include <qtoolbar.h>
#include <vector>
#include <qspinbox.h>
#include <qbuttongroup.h>

class DemoToolbar : public QToolBar {
	Q_OBJECT

// attributes
public:
	std::vector<QAction*> actions;
	QSpinBox* fov;
	QButtonGroup* bg;

// methods
private:
	void init();

public:
	DemoToolbar(const QString& title, QWidget* parent = nullptr) : QToolBar(title, parent) { init(); }
	DemoToolbar(QWidget *parent = nullptr) : QToolBar(parent) { init(); }
	~DemoToolbar() { delete bg; }

	/* Core */

	/* Getters */
	

};

#endif