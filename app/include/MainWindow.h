/**
* @author Radek Blahos
* @project geVkDemo Application
*
* @brief
*/

#ifndef GEVKMAINWINDOW_H
#define GEVKMAINWINDOW_H

#include <QtWidgets\qmainwindow.h>
#include <QtgeVkWidget\geVkWidget.h>
#include <include\MenuBar.h>
#include <include\ToolBar.h>

class MainWindow : public QMainWindow {
	Q_OBJECT

// attributes
private:
	geVkWidget* _geVkWidget;
	DemoMenuBar* _menu;
	DemoToolbar* _toolbar;

// methods
private:

protected:
	void resizeEvent(QResizeEvent* event) override;

public:
	MainWindow(uint32_t width = 1366, uint32_t height = 720, std::string name = "geVk Demo");
	~MainWindow();

	void closeEvent(QCloseEvent *event);

public slots:
	void toolbarActions(QAction* action);
	void othersMenuActions(QAction* action);

};

#endif
