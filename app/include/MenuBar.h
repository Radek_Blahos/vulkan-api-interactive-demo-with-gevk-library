/**
* @author Radek Blahos
* @project geVkDemo Application
*
* @brief
*/


#ifndef DEMOMENUBAR_H
#define DEMOMENUBAR_H

#include <qmenubar.h>

class DemoMenuBar : public QMenuBar {
	Q_OBJECT

// attributes
public:
	QAction* loadSceneBtn;
	QAction* setUniformColorBtn;
	QAction* configureLightBtn;
	QAction* runBenchmarkBtn;
	QMenu* othersMenu;
	std::vector<QAction*> qActions;

// methods
public:
	DemoMenuBar(QWidget* parent = nullptr);

};

#endif